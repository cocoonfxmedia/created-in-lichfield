<?php $__env->startSection('page_title'); ?>
    <h4>News Letter Subscription Manager</h4>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('headerButtons'); ?>
<a href="/admin/newsletter-subscriptions/create" class="btn btn-link btn-float has-text"><i class="icon-user-plus text-primary"></i><span> Create Subscriber</span></a>
<?php $__env->stopPush(); ?>


<?php $__env->startSection('content'); ?>

    <table class="table datatable-basic">
        <thead>
        <tr>
            <th>Email</th>
            <th>Subscribed On</th>
            <th>Unsubscribed On</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($user->email); ?></td>
                <td><?php echo e($user->created_at->format('d/m/Y')); ?></td>
                <td>
                    <?php if( !is_null( $user->unsubscribed_at)): ?>
                        <?php echo e($user->unsubscribed_at->format('d/m/Y')); ?>

                    <?php endif; ?>
                </td>
                <dt></dt>
                <td>
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="/admin/newsletter-subscriptions/<?php echo e($user->id); ?>/edit"><i class="icon-pencil"></i> Edit</a>
                                </li>
                                <li>
                                    <?php if( is_null( $user->unsubscribed_at)): ?>
                                        <a href="/admin/newsletter-subscriptions/<?php echo e($user->id); ?>/unsubscribe"><i class="icon-user-minus"></i> Unsubscribe</a>
                                    <?php else: ?>
                                        <a href="/admin/newsletter-subscriptions/<?php echo e($user->id); ?>/resubscribe"><i class="icon-user-plus"></i> Resubscribe</a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </tbody>
    </table>

    <?php echo $__env->make('admin.common._datatable', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>