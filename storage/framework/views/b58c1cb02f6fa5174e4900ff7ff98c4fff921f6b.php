<div class="container-fluid quoteBar">
    <div class="container quoteContainer">
        <div class="row">

            <div class="col-md-12">
                <h2>Request A Quote</h2>
            </div>


            <?php echo Form::open(['id' => 'quote_frm2']); ?>


            <div id="qf2_error" class="col-md-12 hidden">
                <div class="alert alert-danger">
                    <p>There was a problem submitting your quote! Please check and try again.</p>
                </div>
            </div>


            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Shipment Details</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Address Details</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Additional Information</p>
                    </div>
                </div>
            </div>


            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Shipment Details</h3>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Form::label('qf2_consignee', 'Consignee'); ?>

                                <?php echo Form::text('qf2_consignee', old('consignee'), ['class' => 'form-control required']); ?>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label('qf2_pol', 'Port of Loading'); ?>

                                <?php echo Form::text('qf2_pol', old('qf2_pol'), ['class' => 'form-control required']); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label('qf2_pod', 'Port of Discharge'); ?>

                                <?php echo Form::text('qf2_pod', old('qf2_pod'), ['class' => 'form-control required']); ?>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label('qf2_weight', 'Weight'); ?>

                                <?php echo Form::text('qf2_weight', old('qf2_weight'), ['class' => 'form-control required']); ?>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Form::label('qf2_volume', 'Volume'); ?>

                                <?php echo Form::text('qf2_volume', old('volume'), ['class' => 'form-control required'] ); ?>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row form-group">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <lable>Transport</lable>
                                            <br>
                                            <select name="qf2_metric" id="qf2_metric" class="form-control">
                                                <option value="6000">Air</option>
                                                <option value="1000">Sea</option>
                                                <option value="333">Road</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>Qty</label> <br>
                                            <input type="number" id="qf2_qty" name="qf2_qty" value="1" class="form-control required" min="1">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Length</label> <br>
                                            <input type="number" id="qf2_length" name="qf2_length" placeholder="L" class="form-control required" min="1">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Width</label> <br>
                                            <input type="number" id="qf2_width" name="qf2_width" placeholder="W" class="form-control required" min="1">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Height</label> <br>
                                            <input type="number" id="qf2_height" name="qf2_height" placeholder="H" class="form-control required" min="1">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label>Volume</label> <br>
                                    <span id="qf2_calc_volume">0</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next
                            <i class=" icon-arrow-right5"></i></button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Address Details</h3>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Form::label('qf2_company', 'Company Name'); ?>

                                <?php echo Form::text('qf2_company', old('qf2_company'), ['class' => 'form-control required']); ?>

                            </div>

                            <div class="form-group">
                                <?php echo Form::label('qf2_name', 'Name'); ?>

                                <?php echo Form::text('qf2_name', old('qf2_name'), ['class' => 'form-control required']); ?>

                            </div>

                            <div class="form-group">
                                <?php echo Form::label('qf2_telephone', 'Telephone'); ?>

                                <?php echo Form::text('qf2_telephone', old('qf2_telephone'), ['class' => 'form-control required']); ?>

                            </div>

                            <div class="form-group">
                                <?php echo Form::label('qf2_email', 'Email'); ?>

                                <?php echo Form::email('qf2_email', old('qf2_email'), ['class' => 'form-control required']); ?>

                            </div>

                            <div class="form-group">
                                <?php echo Form::label('qf2_vat', 'VAT Number'); ?>

                                <?php echo Form::text('qf2_vat', old('qf2_vat'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next
                            <i class=" icon-arrow-right5"></i></button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="col-md-12">
                        <h3> Additional Information</h3>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Form::label('qf2_description', 'Description of goods'); ?>

                                <?php echo Form::textarea('qf2_description', old('wf_description'), ['class' => 'form-control required', 'row' => 3]); ?>

                            </div>

                            <div class="form-group">
                                <?php echo Form::label('qf2_date', 'Date of good are ready'); ?>

                                <?php echo Form::date('qf2_date', old('qf2_date'), ['class' => 'form-control', 'min' => date('Y-m-d')]); ?>

                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <?php echo Form::label('qf2_hazardous_cargo', 'hazardous Cargo'); ?>

                                    </div>
                                    <div class="col-md-1">
                                        <?php echo Form::checkbox('qf2_hazardous_cargo',1 , old('qf2_hazardous_cargo', 0)); ?>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo Form::label('qf2_un_number', 'UN Number'); ?>

                                <?php echo Form::text('qf2_un_number', old('qf2_un_number'), ['class' => 'form-control']); ?>

                            </div>
                        </div>
                        <button class="btn btn-success pull-right sendBtn">Request Quote</button>
                    </div>
                </div>
            </div>

            <?php echo Form::close(); ?>


            <div id="qf2_response" class="hidden">
                <p>Thank you for you interest.</p>
                <p>A member of our team will be in touch shourtly with your quote</p>
            </div>

        </div>
    </div>
</div>



<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
		$(document).ready(function () {

			function qf2_change() {
				var qty = $('#qf2_qty').val();
				var w = $('#qf2_width').val();
				var l = $('#qf2_length').val();
				var h = $('#qf2_height').val();
				var x = $('#qf2_metric').val();

				if (qty != '' && l != '' && w != '' && h != '') {
					var total = qty * ((w * h * l) / x);

					$('#qf2_calc_volume').html(total.toFixed(2));
				}
			}

			$('#qf2_length').change(function () {
				qf2_change();
			});
			$('#qf2_width').change(function () {
				qf2_change();
			});
			$('#qf2_height').change(function () {
				qf2_change();
			});
			$('#qf2_qty').change(function () {
				qf2_change();
			});
			$('#qf2_metric').change(function () {
				qf2_change();
			});

			qf2_change();


			var navListItems = $('div.setup-panel div a'),
				allWells = $('.setup-content'),
				allNextBtn = $('.nextBtn');

			allWells.hide();

			navListItems.click(function (e) {
				e.preventDefault();
				var $target = $($(this).attr('href')),
					$item = $(this);

				if (!$item.hasClass('disabled')) {
					navListItems.removeClass('btn-primary').addClass('btn-default');
					$item.addClass('btn-primary');
					allWells.hide();
					$target.show();
					$target.find('input:eq(0)').focus();
				}
			});

			allNextBtn.click(function () {
				var curStep = $(this).closest(".setup-content"),
					curStepBtn = curStep.attr("id"),
					nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
					curInputs = curStep.find("input[type='text'],input[type='url']"),
					isValid = true;

//				$(".form-group").removeClass("has-error");
//				for(var i=0; i<curInputs.length; i++){
//					if (!curInputs[i].validity.valid){
//						isValid = false;
//						$(curInputs[i]).closest(".form-group").addClass("has-error");
//					}
//				}

				if (isValid)
					nextStepWizard.removeAttr('disabled').trigger('click');
			});


			$('div.setup-panel div a.btn-primary').trigger('click');


			$('#quote_frm2').submit(function () {
				var errors = false;
				$('#qf2_error').addClass('hidden');

				$.each($('#quote_frm2 .required'), function (index, obj) {
					if ($(obj).val() == '') {
						errors = true;
						$(obj).addClass('error');
					}
				});


				if (errors == true) {
					$('#qf2_error').removeClass('hidden');
				}
				else {
					$.post('/quote-request', $('#quote_frm2').serialize(), function (response) {
						if (response == 'OK') {
							$('#qf2_response').removeClass('hidden');
							$('#quote_frm2').slideUp();
						}
						else {
							$('#qf2_error').removeClass('hidden');
						}
					});
				}

				return false;
			});
		});
    </script>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('css'); ?>
    <style>
        .error {
            border: 1px solid red;
        }

        .stepwizard-step p {
            margin-top: 10px;
        }

        .stepwizard-row {
            display: table-row;
        }

        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }

        .stepwizard-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }

        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;

        }

        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
    </style>
<?php $__env->stopPush(); ?>