<div class="container-fluid serviceBar">



	<div class="serviceA">
    		<div class="serviceBox">
				<a href="/supply-chain-management"><h2>Supply chain management</h2></a>
				<ul>
					<li>Managed service</li>
					<li>Supply chain designed solutions</li>
					<li>Value driven management</li>
					<li>Operational strategic support</li>
					<li>Bespoke solutions</li>
				</ul>
    		</div>
	</div>
	<div class="serviceB">
    		<div class="serviceBox">
				<a href="/seafreight"><h2>Seafreight services</h2></a>
				<ul>
					<li>Import & export</li>
					<li>FCL & LCL Services</li>
					<li>Cross trade specialist</li>
					<li>Non-containerised loads</li>
					<li>Equipment management</li>
				</ul>
    		</div>
	</div>
	<div class="serviceC">
    		<div class="serviceBox">
				<a href="/airfreight"><h2>Airfreight services</h2></a>
				<ul>
					<li>Import & export</li>
					<li>Strategic partnerships</li>
					<li>Cross trade specialist</li>
					<li>Optimised buying power</li>
					<li>Competitive pricing</li>
				</ul>
    		</div>
	</div>
	<div class="serviceD">
    		<div class="serviceBox">
				<a href="/roadfreight"><h2>Roadfreight services</h2></a>
				<ul>
					<li>Import & export</li>
					<li>FTL & LTL Services</li>
					<li>European partner network</li>
					<li>Groupage service</li>
					<li>Dedicated and express services</li>
				</ul>
    		</div>
	</div>
	<div class="serviceE">
    		<div class="serviceBox">
				<a href="/delivery-innovation-through-technology"><h2>Innovation through technology</h2></a>
				<ul>
					<li>Tailored solutions</li>
					<li>Optimisation of technology services</li>
					<li>Visibility of milestones within supply chain</li>
					<li>Dedicated support packages</li>
					<li>Innovative services</li>
				</ul>
    		</div>
	</div>
	
	

	
	
	
	
	
	


</div>