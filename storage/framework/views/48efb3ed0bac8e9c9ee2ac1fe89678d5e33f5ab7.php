<?php $__env->startSection('page_title'); ?>
	<h4>User Manager</h4>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('headerButtons'); ?>
<a href="/admin/users/create" class="btn btn-link btn-float has-text"><i class="icon-user-plus text-primary"></i><span> Create User</span></a>
<?php $__env->stopPush(); ?>


<?php $__env->startSection('content'); ?>

	<table class="table datatable-basic">
		<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Actions</th>
		</tr>
		</thead>
		<tbody>

		<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($user->name); ?></td>
				<td><?php echo e($user->email); ?></td>
				<td><?php echo e($user->roles()->first()->display_name); ?></td>
				<td>
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="/admin/users/<?php echo e($user->id); ?>/edit"><i class="icon-pencil"></i> Edit</a></li>
								<li><a href="/admin/users/<?php echo e($user->id); ?>/delete" class="do_delete"><i class="icon-bin"></i> Delete</a></li>
							</ul>
						</li>
					</ul>
				</td>
			</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		</tbody>
	</table>

	<?php echo $__env->make('admin.common._datatable', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>