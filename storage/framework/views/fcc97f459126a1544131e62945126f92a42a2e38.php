<?php $__env->startSection('meta_data'); ?>
    <title>
        <?php if( !empty($page->meta_title)): ?>
            <?php echo e($page->meta_title); ?>

        <?php else: ?>
            <?php echo e($page->title); ?>

        <?php endif; ?>
        - <?php echo e(config('app.name')); ?></title>
    <meta name="description" content="<?php echo e($page->meta_description); ?>"/>
<?php $__env->stopSection(); ?>