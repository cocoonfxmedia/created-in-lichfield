<div class="container-fluid requestBar">
    <div class="container requestContainer">
        <div class="row">


            <div class="col-md-12">

                <h2>Request Information</h2>
            </div>

            <?php echo Form::open(['id' => 'quote_frm']); ?>


            <div id="qf_error" class="col-md-12 hidden">

                <p>There was a problem submitting your quote! Please check and try again.</p>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <?php echo Form::label('qf_name', 'Name'); ?>

                    <?php echo Form::text('qf_name', old('qf_name'), ['class' => 'form-control required']); ?>

                </div>

                <div class="form-group">
                    <?php echo Form::label('qf_telephone', 'Telephone'); ?>

                    <?php echo Form::text('qf_telephone', old('qf_telephone'), ['class' => 'form-control required']); ?>

                </div>

                <div class="form-group">
                    <?php echo Form::label('qf_email', 'Email'); ?>

                    <?php echo Form::email('qf_email', old('qf_email'), ['class' => 'form-control required']); ?>

                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="form-group">
                    <?php echo Form::label('qf_company', 'Company Name'); ?>

                    <?php echo Form::text('qf_company', old('qf_company'), ['class' => 'form-control required']); ?>

                </div>

                <div class="form-group">
                    <?php echo Form::label('qf_description', 'Message'); ?>

                    <?php echo Form::textarea('qf_description', old('qf_message'), ['class' => 'form-control required', 'rows' => 5]); ?>

                </div>
            </div>


            <div class="col-md-12">
                <div class="form-group">
                    <button class="btn btn-primary">Send Request</button>
                </div>
            </div>

            <?php echo Form::close(); ?>


            <div id="qf_response" class="hidden">
                <p>Thank you for you interest.</p>
                <p>A member of our team will be in touch shourtly with your quote</p>
            </div>

        </div>
    </div>
</div>



<?php $__env->startPush('scripts'); ?>
    <script type="text/javascript">
		$(document).ready(function () {
			function qf_change() {
				var qty = $('#qf_qty').val();
				var w = $('#qf_width').val();
				var l = $('#qf_length').val();
				var h = $('#qf_height').val();
				var x = $('#qf_metric').val();

				if (qty != '' && l != '' && w != '' && h != '') {
					var total = qty * ((w * h * l) / x);

					$('#qf_calc_volume').html(total.toFixed(2));
				}
			}

			$('#qf_length').change(function () {
				qf_change();
			});
			$('#qf_width').change(function () {
				qf_change();
			});
			$('#qf_height').change(function () {
				qf_change();
			});
			$('#qf_qty').change(function () {
				qf_change();
			});
			$('#qf_metric').change(function () {
				qf_change();
			});

			qf_change();

			$('#quote_frm').submit(function () {
				var errors = false;
				$('#qf_error').addClass('hidden');
				$('#quote_form .error').removeClass('error');

				$.each($('#quote_frm .required'), function (index, obj) {
					if ($(obj).val() == '') {
						errors = true;
						$(obj).addClass('error');
					}
				});


				if (errors == true) {
					$('#qf_error').removeClass('hidden');
				}
				else {
					$.post('/quote', $('#quote_frm').serialize(), function (response) {
						if (response == 'OK') {
							$('#qf_response').removeClass('hidden');
							$('#quote_frm').addClass('hidden');
						}
						else {
							$('#qf_error').removeClass('hidden');
						}
					});
				}

				return false;
			});
		});
    </script>
<?php $__env->stopPush(); ?>


<?php $__env->startPush('css'); ?>
    <style>
        .error {
            border: 1px solid red;
        }
    </style>
<?php $__env->stopPush(); ?>