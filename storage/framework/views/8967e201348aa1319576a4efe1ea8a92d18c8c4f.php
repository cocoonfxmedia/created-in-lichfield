<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Overpass:200,200i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="/site/css/bootstrap.css">
    <link rel="stylesheet" href="/site/css/style.css">

    <style>
        body {
            font-size: 14px;
            text-align: center;
        }

        .calculatorBox {
            padding: 0;
        }

        .cur label {
            width: 22%;
        }

        .currLeft {
            width: 53%;
        }
    </style>
</head>
<body class="calculatorBox cur">

<div class="col-xs-12 col-sm-12  col-md-12  col-lg-6">

    <div class="form-group">
        <label for="weight"><?php echo e(trans('site.c02.weight')); ?> (Kg)</label>
        <input type="text" name="weight" id="weight" value="1" class="form-control">
    </div>

    <div class="form-group">
        <label for="start-place"><?php echo e(trans('site.c02.pol')); ?></label>
        <input type="text" class="form-control typeahead two-section" id="start-place" placeholder="<?php echo e(trans('site.c02.pol_placeholder')); ?>">
    </div>

    <div class="form-group">
        <label for="target-place"><?php echo e(trans('site.c02.pod')); ?></label>
        <input type="text" class="form-control typeahead two-section" id="target-place" placeholder="<?php echo e(trans('site.c02.pod_placeholder')); ?>">
    </div>


    <div class="form-group currLeft">
        <button class="btn btn-primary calculate"><?php echo e(trans('site.c02.calculate')); ?></button>
    </div>
    <div class="form-group currRight"></div>
    <div class="clearfix"></div>
    <div class="form-group">
        <div id="distance"></div>
        <div id="emissions"></div>
    </div>
</div>

<div class="clearfix"></div>

<div class="form-group">
    <p><br><?php echo e(trans('site.c02.air_caveat1')); ?>

        <br>
        <?php echo e(trans('site.c02.air_caveat2')); ?></p>
</div>


<div id="source-map"></div>


<input type="hidden" class="form-control" id="port_name_start">
<input type="hidden" class="form-control" id="country_start">
<input type="hidden" class="form-control" id="port_id_start">
<input type="hidden" class="form-control" id="lat_start">
<input type="hidden" class="form-control" id="long_start">

<input type="hidden" class="form-control" id="port_name_target">
<input type="hidden" class="form-control" id="country_target">
<input type="hidden" class="form-control" id="port_id_target">
<input type="hidden" class="form-control" id="lat_target">
<input type="hidden" class="form-control" id="long_target">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYAh5XahIuW5kiJizrIsTvjhG7CbU1UyY&libraries=places,geometry"></script>
<!-- Theme JavaScript -->
<!--<script src="js/grayscale.min.js"></script>-->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" media="all"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<script src="/site/js/air.js?v=78326"></script>
</body>
</html>