<?php $__env->startSection('page_title'); ?>
    <h4>Settings</h4>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('admin.common._message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#general-tab" data-toggle="tab">General</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="general-tab">
                <div class="col-md-9">
                    <?php echo Form::open(['url' => '/admin/config/general']); ?>


                    <?php $__currentLoopData = $settings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="form-group">
                            <?php if( Auth::user()->can('advanced-config')): ?>
                                <?php echo Form::label( $key,$key); ?>

                            <?php else: ?>
                                <?php echo Form::label( $key, str_replace('_', ' ' , $key)); ?>

                            <?php endif; ?>

                            <?php if( is_bool($value)): ?>
                                <?php echo Form::checkbox($key,1, old($key, $value)); ?>

                            <?php else: ?>
                                <?php echo Form::text($key, old($key, $value), ['class' => 'form-control']); ?>

                            <?php endif; ?>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <div class="form-group">
                        <button class="btn btn-primary">Save</button>
                    </div>

                    <?php echo Form::close(); ?>

                </div>

                <?php if( Auth::user()->can('advanced-config')): ?>
                    <div class="col-md-3">
                        <h4>Settings to templates</h4>
                        Use the following code with the
                        <em>setting_name</em> replace with the label for the relevant setting you want to add
                        <code>&lbrace;&lbrace;siteConfig('setting_name')&rbrace;&rbrace;</code>

                        <br><br>

                        <h4>Adding new settings</h4>
                        <p>All of the setting values are stored in the /config/settings.php file.</p>

                        <p>Settings values are stored in a PHP array.</p>

                        <p>The setting name must be all lower case and not contain spaces. <code>_</code> characters in the name will be replaced with spaces on this page</p>


                        <p>Copy the following line and add it to the settings.php file.</p>
                        <code>'setting_name' => 'setting value',</code>



                    </div>
                <?php endif; ?>
            </div>
        </div>

    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>