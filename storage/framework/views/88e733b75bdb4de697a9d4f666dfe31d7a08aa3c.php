<?php $__env->startSection('meta_data'); ?>
    <title><?php echo e(config('app.name')); ?></title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

<?php echo $__env->make('components.fscvideo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.servicestrip', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.introduction', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.fsccircle', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.map', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.requestcall', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- <?php echo $__env->make('components.quote', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.newsblock', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('components.casestudyblock', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> -->







<?php $__env->stopSection(); ?>
<?php echo $__env->make('site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>