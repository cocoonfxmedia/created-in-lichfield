<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Admin Panel for <?php echo e(siteConfig('site_name')); ?></title>
    <link rel="shortcut icon" href="/favicon.png">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/backoffice/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/backoffice/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo e(elixir('css/app.css')); ?>" rel="stylesheet" type="text/css">
    <?php echo $__env->yieldPushContent('css'); ?>

    <script type="text/javascript" src="/backoffice/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/ui/fab.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/notifications/sweet_alert.min.js"></script>
    <?php echo $__env->yieldPushContent('lib_scripts'); ?>
    <script type="text/javascript" src="/backoffice/js/core/app.js"></script>


</head>

<body class="<?php echo e(session('main-nav-view', '')); ?>">

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="/admin">Admin Panel <?php echo e(siteConfig('site_name')); ?></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>
    <ul class="nav navbar-nav">
        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
    </ul>
    <?php echo $__env->make('admin.common._nav_top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <?php echo $__env->make('admin.common._nav_left', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <?php if( config('cms.components.multisite') ): ?>
                <div class="alert alert-primary no-border">
                    <p>You are currently editing the
                        <b><?php echo e(siteConfig('site_name')); ?></b> site.</p>
                </div>
        <?php endif; ?>

        <!-- Page header -->
            <div class="page-header">
                <div class="page-header-content">
                    <div class="page-title">
                        <?php echo $__env->yieldContent('page_title'); ?>
                    </div>

                    <div class="heading-elements">
                        <div class="heading-btn-group">
                            <?php echo $__env->yieldPushContent('headerButtons'); ?>
                        </div>
                    </div>
                </div>

                <div class="breadcrumb-line">
                    <?php echo Breadcrumbs::render(); ?>

                    <?php echo $__env->yieldContent('breadcrumb_elements'); ?>
                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <?php echo $__env->yieldContent('content'); ?>

                <div class="footer text-muted">
                    &copy;Cocoonfxmedia Ltd
                </div>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->


    </div>
    <!-- /page content -->

</div>
<!-- /page container -->


<script>
	var navController = <?php echo json_encode( $navController ); ?>;
	$.each(navController, function (index, value) {
		$('ul.navigation-main li[data-nav-section=' + value + ']').addClass('active');
	});
</script>

<?php echo $__env->yieldPushContent('scripts'); ?>

<?php if( Session::has('msg') ): ?>
    <script type="text/javascript">

		$(document).ready(function () {
			new PNotify({
				title: '<?php echo e(Session::get('msg.title')); ?>',
				text: '<?php echo e(Session::get('msg.text')); ?>',
				addclass: 'bg-<?php echo e(Session::get('msg.state')); ?>'
			});
		});

    </script>
<?php endif; ?>

</body>
</html>