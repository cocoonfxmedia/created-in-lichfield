<h1>Quote Request</h1>

<table>
    <tr>
        <th>Company Name</th>
        <td><?php echo e($quote->company); ?></td>
    </tr>
    <tr>
        <th>Name</th>
        <td><?php echo e($quote->name); ?></td>
    </tr>
    <tr>
        <th>Email</th>
        <td><a href="mailto:<?php echo e($quote->email); ?>"><?php echo e($quote->email); ?></a></td>
    </tr>
    <tr>
        <th>Telephone No.</th>
        <td><a href="tel:<?php echo e(str_replace(' ', '', $quote->telephone)); ?>"><?php echo e($quote->telephone); ?></a></td>
    </tr>
    <?php if( !empty( $quote->vat)): ?>
        <tr>
            <th>VAT Number</th>
            <td><?php echo e($quote->vat); ?></td>
        </tr>
    <?php endif; ?>
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    
        
        
    
    <tr>
        <th>Message</th>
        <td><?php echo e($quote->description); ?></td>
    </tr>
    <?php if( !empty( $quote->date)): ?>
        <tr>
            <th>Date of good are ready</th>
            <td><?php echo e($quote->date->format('d/m/Y')); ?></td>
        </tr>
    <?php endif; ?>
</table>
