<?php $__env->startSection('meta_data'); ?>
    <title><?php echo e(trans('site.contact.title')); ?> - <?php echo e(config('app.name')); ?></title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="container-liquid">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
            <!--                <iframe src="<?php echo e(siteConfig('map_url')); ?>" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                <div class="masthead" style="background-image: url(/usr/page_masthead/28_home.jpg);">
                    <div class="container titleCont">
                        <h1 class="mainHeader">Streamlined cost effective logistics solutions</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7">
                <div class="bodyText">
                    <h1 class="defaulth1"><?php echo e(trans('site.contact.title')); ?></h1>

                    <?php echo $__env->make('components.map', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


                    <?php echo \App\Models\Page::where('url', 'contact-us')->where( 'sid', siteConfig('sid') )->first()->content; ?>



                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-5">
                <div class="captureBox">


                    <div class="contactForm">

                        <?php if( isset($_GET['sent'])): ?>
                            <br>
                            <p class="alert alert-success"><?php echo e(trans('site.contact.thanks')); ?></p>
                        <?php endif; ?>

                        <br>
                        <p class="formText"><?php echo e(trans('site.contact.form_instruction')); ?></p>
                        <?php echo Form::open( ['url' => '/contact-us/send', 'method' => 'post' ]); ?>


                        <p>
                            <?php echo Form::label('name' , trans('site.contact.name')); ?>

                            <?php echo Form::text('name'  , old('name') , [ 'class' => 'form-control' , 'placeholder' => ''] ); ?>

                        </p>

                        <p>
                            <?php echo Form::label('email' , trans('site.contact.name')); ?>

                            <?php echo Form::text('email'  , old('email') , [ 'class' => 'form-control' , 'placeholder' => ''] ); ?>

                        </p>
                        <p>
                            <?php echo Form::label('Message' , trans('site.contact.message')); ?>

                            <?php echo Form::textarea('message'  , old('message') , [ 'class' => 'form-control' , 'placeholder' => trans('site.contact.message_placeholder')] ); ?>

                        </p>

                        <p>
                            <?php echo Form::input('submit' , 'Send', trans('site.contact.send')); ?>

                        </p>

                        <?php echo $__env->make('common._errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>