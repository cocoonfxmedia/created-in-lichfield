<?php echo $__env->make('admin.common._errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<div class="form-group">
    <?php echo Form::label('email', 'Email Address'); ?>


    <?php echo Form::email('email', old('email'), ['class' => 'form-control']); ?>

</div>

<div class="form-group">

</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="/admin/newsletter-subscriptions">Cancel</a>
</div>