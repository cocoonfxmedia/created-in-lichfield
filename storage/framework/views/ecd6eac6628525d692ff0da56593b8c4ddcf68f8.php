<div class="container-fluid introductionBar">
<div class="container">
<div class="row">
	<div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
    		<div class="introductionBox">
        	<h2><?php echo $page->title; ?></h2>
		<?php  echo Blade::compileString( $page->content ); ?>
    		</div>
	</div>

</div>
</div>
</div>