<?php $__env->startSection('page_title'); ?>
    <h4>Create New Newsletter Subscription</h4>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>


    <?php echo Form::open(['url' => '/admin/newsletter-subscriptions/create']); ?>


    <?php echo $__env->make('admin.newsletter_subscriptions._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>