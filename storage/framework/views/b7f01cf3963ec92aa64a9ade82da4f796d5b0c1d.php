<?php $__env->startSection('page_title'); ?>
    <h4>Duplicate Site Content</h4>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

    <p class="alert alert-danger" id="no_same" style="display: none;">You cannot copy content to the same site! <br>Select a different destination site.</p>

    <p class="alert alert-warning">This process will duplicate the content from one site to another. Once started, this cannot be undone!</p>

    <p>To prevent conflicts, the desination site should be clean.</p>

    <?php echo Form::open(); ?>


    <div class="form-group">
        <?php echo Form::label('from', 'Source Site - content from this site will be copied'); ?>

        <?php echo Form::select('from', $siteList, old('from'), ['class' => 'form-control']); ?>

    </div>

    <div class="form-group">
        <?php echo Form::label('to', 'Destination Site - the copied content will be inserted to this site'); ?>

        <?php echo Form::select('to', $siteList, old('to'), ['class' => 'form-control']); ?>

    </div>

    <button class="btn btn-success">Start Duplicating</button>

    <?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
	$('form').submit(function () {
		var from = $('#from :selected').val();
		var to = $('#to :selected').val();

        if (from == to) {
            $('#no_same').show();
			return false;
		}

		$('#no_same').hide();
	});
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>