<?php $__env->startSection('meta_data'); ?>
    <title><?php echo e(config('app.name')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo Breadcrumbs::render(); ?>

        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container search-results">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        <h1 class="defaulth1">Search : <?php echo e($searchString); ?>

                            <span class="heading-counter"><?php echo e($resultCount); ?> results have been found.</span></h1>


                        <?php if( $resultCount == 0 ): ?>

                            <p class="alert alert-warning">
                                No results were found for your search&nbsp;"<?php echo e($searchString); ?>"
                            </p>

                            <p>Please try searching for something else</p>

                        <?php else: ?>

                            <?php if( count( $pages ) > 0): ?>
                            <!-- Subcategories -->
                                <div id="subcategories">
                                    <h2>Pages</h2>
                                    <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <article>
                                            <h3>
                                                <a class="subcategory-name" href="/<?php echo e($page->path); ?>"><?php echo e($page->title); ?></a>
                                            </h3>
                                            <p>
                                                <a href="/<?php echo e($page->path); ?>"><?php echo e(str_limit(strip_tags($page->content), 300)); ?>.... </a>
                                            </p>

                                        </article>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            <?php endif; ?>


                            <?php if( count( $articles ) > 0): ?>
                            <!-- Subcategories -->
                                <div id="subcategories">
                                    <h2>News</h2>
                                    <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <article>
                                            <h5>
                                                <a class="subcategory-name" href="/<?php echo e($page->path); ?>"><?php echo e($page->title); ?></a>
                                            </h5>
                                            <p>
                                                <a href="/<?php echo e($page->path); ?>"><?php echo e(str_limit(strip_tags($page->content), 300)); ?>.... </a>
                                            </p>
                                        </article>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            <?php endif; ?>

                            <?php if( count( $categoryList ) > 0): ?>
                            <!-- Subcategories -->
                                <div id="subcategories">
                                    <h2>Categories</h2>
                                    <?php $__currentLoopData = $categoryList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <article>
                                            <div class="subcategory-image">
                                                <a href="/category/<?php echo e($cat->slug); ?>" title="<?php echo e($cat->name); ?>"
                                                   class="img">
                                                    <img class="replace-2x"
                                                         src="<?php echo e($cat->image); ?>"
                                                         alt="<?php echo e($cat->name); ?>" width="125" height="125"/>
                                                </a>
                                            </div>
                                            <h5><a class="subcategory-name"
                                                   href="/category/<?php echo e($cat->slug); ?>"><?php echo e($cat->name); ?></a>
                                            </h5>
                                        </article>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            <?php endif; ?>


                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>


<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="/site/themes/default-bootstrap/css/category.css" type="text/css" media="all"/>
    <style>
        .search-results article a {
            color: #000;
        }
    </style>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>