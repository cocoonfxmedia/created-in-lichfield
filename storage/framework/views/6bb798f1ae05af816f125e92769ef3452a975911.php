<?php
/*
 * DO NOT ADD ANYTHING INTO THIS FILE
 * USE THE APPROPRIATE FILE IN THE /views/layouts directory
 *
 * Templates use the site
 */


$template = siteConfig( 'slug' );

if ( $template != 'default' && ! file_exists( resource_path( 'views/layouts/' . $template . '.blade.php' ) ) )
{
	$template = 'default';
}

?>



<?php echo $__env->make('layouts.'.siteConfig('slug') , array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>