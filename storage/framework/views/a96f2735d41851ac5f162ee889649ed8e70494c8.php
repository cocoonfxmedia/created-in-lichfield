<div class="form-group">
    <label for="transport"><?php echo e(trans('site.c02.type')); ?></label>
    <select id="transport" style="color: #4d5158;">
        <option value="air"><?php echo e(trans('site.c02.air')); ?></option>
        <option value="land"><?php echo e(trans('site.c02.road')); ?></option>
        <option value="sea"><?php echo e(trans('site.c02.sea')); ?></option>
    </select>
</div>



<iframe id="c02-frame" src="/c02-calc/air" frameborder="0" style="width: 100%; height: 350px;"></iframe>

<script>
    $('#transport').on('change', function(){
    	var url = '/c02-calc/' +  $('#transport').val();
    	$('#c02-frame').attr('src', url );
    });

    var emissionsTrans = '<?php echo trans( 'site.c02.distance'); ?>';
    var distanceTrans = '<?php echo trans( 'site.c02.emissions'); ?>';
</script>