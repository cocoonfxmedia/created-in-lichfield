<div class="container-fluid circleBar">
<div class="container">
<div class="row">
	<div class="col-xs-12 col-sm-12  col-md-12  col-lg-12">


<div class="universe-image-wrapper">
	<div class="universe-image">
		<div class="planet aa">
			<a style="background-image:url(/site/images/seaIcon.png)" class="inner" href="/seafreight"><span><p>Seafreight</p></span></a></div>
		<div class="planet ab">
 			<a style="background-image:url(/site/images/airIcon.png)" class="inner" href="/airfreight"><span><p>Airfreight</p></span></a></div>
		<div class="planet ac">
			<a style="background-image:url(/site/images/roadIcon.png)" class="inner" href="/roadfreight"><span><p>Roadfreight</p></span></a></div>
		<div class="planet ad">
			<a style="background-image:url(/site/images/supplyIcon.png)" class="inner" href="/supply-chain-management"><span><p>Supply Chain</p></span></a></div>
		<div class="planet ba">
			<a style="background-image:url(/site/images/analysisIcon.png)" class="inner" href="/data-analysis"><span><p>Data Analysis</p></span></a></div>
		<div class="planet bb">
			<a style="background-image:url(/site/images/measurableIcon.png)" class="inner" href="/measurable-performance"><span><p>Measurable Performance</p></span></a></div>
		<div class="planet bc">
			<a style="background-image:url(/site/images/mappingIcon.png)" class="inner" href="/process-mapping"><span><p>Process Mapping</p></span></a></div>
		<div class="planet bd">
			<a style="background-image:url(/site/images/improvIcon.png)" class="inner" href="/continuous-improvement"><span><p>Continuous Improvement</p></span></a></div>
		<div class="planet be">
			<a style="background-image:url(/site/images/optIcon.png)" class="inner" href="/optimisation"><span><p>Optimisation</p></span></a></div>
		<div class="planet bf">
			<a style="background-image:url(/site/images/impIcon.png)" class="inner" href="/implementation"><span><p>Implementation</p></span></a></div>
		<div class="planet bg">
			<a style="background-image:url(/site/images/toolIcon.png)" class="inner" href="/customer-tools"><span><p>Tools</p></span></a></div>
		<div class="planet bh">
			<a style="background-image:url(/site/images/innovateIcon.png)" class="inner" href="/delivery-innovation-through-technology"><span><p>Innovation</p></span></a></div>
		<!-- <div class="planet ca">
			<a style="background-image:url(/site/images/logo.png)" class="inner" href="/"><span><p>Globe</p></span></a></div> -->
	</div>
</div>


	</div>
</div>
</div>
</div>