<div class="navbar-collapse collapse" id="navbar-mobile">
    <ul class="nav navbar-nav navbar-right">

        <li class="dropdown" id="hdr_notifications">
            <a href="<?php echo e(siteConfig('url')); ?>" target="_blank" title="Got to website">
                <i class="icon-home2"></i>
                <span class="visible-xs-inline-block position-right">View Site</span>
            </a>
        </li>

        <?php if( config('cms.components.multisite')): ?>
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span>Site: <?php echo e(siteConfig('site_name')); ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <?php $__currentLoopData = Config::get('multisite.sites'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $siteId => $site): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if( session('currentSite', 1) != $siteId && $siteId != 0 ): ?>
                            <li><a href="/admin/set-site/<?php echo e($siteId); ?>"> <?php echo e(siteConfig('site_name', $siteId)); ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </li>
        <?php endif; ?>

        <li class="dropdown dropdown-user">
            <a class="dropdown-toggle" data-toggle="dropdown">

                <span><?php echo e(Auth::user()->name); ?></span>
                <i class="caret"></i>
            </a>

            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="/admin/logout" id="doAuthLogout"><i class="icon-switch2"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
