<div class="container-fluid caseBar">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12  col-md-12  col-lg-12">
                <div class="caseTitle"><h2>Featured case studies</h2></div>
            </div>
            <div class="col-xs-12 col-sm-12  col-md-6  col-lg-3">
                <div class="caseBox">
                    <h2>Forwarding services</h2>
                    <ul>
                        <?php $__currentLoopData = $caseStudyWidget->where('category_id', 1 )->splice(0,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $study): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/case-studies/<?php echo e($study->url); ?>"><?php echo e($study->title); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12  col-md-6  col-lg-3">
                <div class="caseBox">
                    <h2>Sea freight services</h2>
                    <ul>
                        <?php $__currentLoopData = $caseStudyWidget->where('category_id', 2 )->splice(0,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $study): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/case-studies/<?php echo e($study->url); ?>"><?php echo e($study->title); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12  col-md-6  col-lg-3">
                <div class="caseBox">
                    <h2>Road freight services</h2>
                    <ul>
                        <?php $__currentLoopData = $caseStudyWidget->where('category_id', 3 )->splice(0,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $study): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/case-studies/<?php echo e($study->url); ?>"><?php echo e($study->title); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12  col-md-6  col-lg-3">
                <div class="caseBox">
                    <h2>Supply chain solutions</h2>
                    <ul>
                        <?php $__currentLoopData = $caseStudyWidget->where('category_id', 4 )->splice(0,5); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $study): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li><a href="/case-studies/<?php echo e($study->url); ?>"><?php echo e($study->title); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>