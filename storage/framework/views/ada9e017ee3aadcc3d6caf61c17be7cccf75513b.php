<?php if(count( $errors ) > 0): ?>
    <div class="alert alert-danger no-border">

        <p>
            <strong><?php echo e(trans('site.error.oops')); ?></strong> <?php echo e(trans('site.error.title')); ?>

        </p>

        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>