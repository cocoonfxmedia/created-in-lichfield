<?php $__env->startPush('scripts'); ?>
	<script type="text/javascript" src="/backoffice/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="/backoffice/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript">

		$(function () {


			// Table setup
			// ------------------------------

			// Setting datatable defaults
			$.extend($.fn.dataTable.defaults, {
				autoWidth: false,
				columnDefs: [{
					orderable: false,
					width: '100px'
				}],
				dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
				drawCallback: function () {
					$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
				},
				preDrawCallback: function () {
					$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
				}
			});


			// Basic datatable
			$('.datatable-basic').DataTable();


			// External table additions
			// ------------------------------

			// Add placeholder to the datatable filter option
			$('.dataTables_filter input[type=search]').attr('placeholder', '...');


			// Enable Select2 select for the length option
			$('.dataTables_length select').select2({
				minimumResultsForSearch: Infinity,
				width: 'auto'
			});
		});


	</script>
<?php $__env->stopPush(); ?>