<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="https://fonts.googleapis.com/css?family=Overpass:200,200i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="/site/css/bootstrap.css">
    <link rel="stylesheet" href="/site/css/style.css">

    <style>
        body {
            font-size: 14px;
            text-align: center;
        }

        .calculatorBox {
            padding: 0;
        }

        .cur label {
            width: 22%;
        }

        .currLeft {
            width: 53%;
        }
    </style>

</head>
<body class="calculatorBox cur">

<div class="col-xs-12 col-sm-12  col-md-12  col-lg-6">


    <div class="form-group">
        <label for="weight"><?php echo e(trans('site.c02.weight')); ?> (Kg)</label>
        <input type="text" name="weight" id="weight" value="1" class="form-control">
    </div>

    <div class="form-group">
        <label for="start-place"><?php echo e(trans('site.c02.start')); ?></label>
        <input type="text" class="form-control typeahead" id="start-place" placeholder="<?php echo e(trans('site.c02.start_placeholder')); ?>">
    </div>

    <div class="form-group">
        <label for="target-place"><?php echo e(trans('site.c02.finish')); ?></label>
        <input type="text" class="form-control typeahead" id="target-place" placeholder="<?php echo e(trans('site.c02.finish_placeholder')); ?>">
    </div>

    <div class="form-group currLeft">
        <button class="btn btn-primary calculate"><?php echo e(trans('site.c02.calculate')); ?></button>
    </div>

    <div class="clearfix"></div>
    <div class="form-group">
        <div id="distance"></div>
        <div id="emissions"></div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group">
        <p><br><?php echo e(trans('site.c02.air_caveat1')); ?>

            <br>
            <?php echo e(trans('site.c02.air_caveat2')); ?></p>
    </div>

</div>

<div id="source-map"></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js" type="text/javascript"></script>

<script>

	function initMap() {
		var mapOptions = {
			center: new google.maps.LatLng(0, 0)
		};
		map = new google.maps.Map(document.getElementById('source-map'), mapOptions);

		new AutocompleteDirectionsHandler(map);
	}

	/**
	 * @constructor
	 */
	function AutocompleteDirectionsHandler(map) {
		this.map = map;
		this.originPlaceId = null;
		this.destinationPlaceId = null;
		this.travelMode = 'DRIVING';
		var originInput = document.getElementById('start-place');
		var destinationInput = document.getElementById('target-place');
		var modeSelector = document.getElementById('mode-selector');
		this.directionsService = new google.maps.DirectionsService;
		this.directionsDisplay = new google.maps.DirectionsRenderer;
		this.directionsDisplay.setMap(map);

		var originAutocomplete = new google.maps.places.Autocomplete(
			originInput, {placeIdOnly: true});
		var destinationAutocomplete = new google.maps.places.Autocomplete(
			destinationInput, {placeIdOnly: true});

		this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
		this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');
	}


	AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function (autocomplete, mode) {
		var me = this;
		autocomplete.bindTo('bounds', this.map);
		autocomplete.addListener('place_changed', function () {
			var place = autocomplete.getPlace();
			if (!place.place_id) {
				window.alert("<?php echo trans('site.c02.select_dropdown'); ?>");
				return;
			}
			if (mode === 'ORIG') {
				me.originPlaceId = place.place_id;
			} else {
				me.destinationPlaceId = place.place_id;
			}
			me.route();
		});

	};

    var distance = 0;
	var c02Standard = 62;


	function emissionsCalc()
    {
		var weight = parseInt($('#weight').val()) / 1000;

		var emissions = (weight * distance * c02Standard) / 1000;

		$('#distance').html('<?php echo trans('site.c02.distance'); ?>' + parseInt(distance) + ' Km');
		$('#emissions').html('<?php echo trans( 'site.c02.emissions'); ?>: ' + parseFloat(emissions).toFixed(4) + ' Kg C02');

	}

	AutocompleteDirectionsHandler.prototype.route = function () {
		if (!this.originPlaceId || !this.destinationPlaceId) {
			return;
		}
		var me = this;

		this.directionsService.route({
			origin: {'placeId': this.originPlaceId},
			destination: {'placeId': this.destinationPlaceId},
			travelMode: this.travelMode
		}, function (response, status) {
			if (status === 'OK') {
				$('#slider').slider("destroy");
				me.directionsDisplay.setDirections(response);

				distance = response.routes[0].legs[0].distance.value / 1000;

				emissionsCalc();


			} else {
				window.alert('Directions request failed due to ' + status);
			}
		});
	};

	$('.calculate').click( function(){
		emissionsCalc();
		return false;
    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYAh5XahIuW5kiJizrIsTvjhG7CbU1UyY&libraries=places&callback=initMap" async defer></script>


</body>
</html>