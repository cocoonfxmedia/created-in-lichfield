<?php echo $__env->make('common._cms_page_meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->startSection('content'); ?>

    <?php if( $page->slider != 0 ): ?>
        <?php echo $__env->make('components.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if( !is_null( $page->masthead )): ?>
        <div class="masthead" style="background-image: url(<?php echo e($page->masthead); ?>);">
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo Breadcrumbs::render(); ?>

        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        <h1 class="defaulth1"><?php echo $page->title; ?></h1>
						<?php  echo Blade::compileString( $page->content ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo $__env->make('components.calculators', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php echo $__env->make('components.trust', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>