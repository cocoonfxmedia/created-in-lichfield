<?php

return [

	/*
	 * Paypal environment 
	 * 'live' or 'sandbox'
	 */
	'env' => 'live',
	
	
	/*
	 * Paypay user email address
	 */
	'merchant_email' => 'info@cocoonfxmedia.co.uk'
];
