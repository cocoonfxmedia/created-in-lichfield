<?php

return [


	/*
   |--------------------------------------------------------------------------
   | CMS Functionality
   |--------------------------------------------------------------------------
   |
   | Switch on/off CMS components in the admin area and front end
   |
   */


	'components'   => [
		'multisite'          => false,
		'pages'              => true,
		'blog'               => true,
		'menuEditor'         => true,
		'slider'             => true,
		'teamMembers'        => true,
		'newsletters'        => true,
		'gallery'            => true,
		'case-studies'       => true,

		/*
	   |--------------------------------------------------------------------------
	   | eCommerce Functionality
	   |--------------------------------------------------------------------------
	   |
	   | Switch on eCommerce functionality
	   |
	   */

		//switch on/off all ecommerce functionality
		'ecommerce'          => false,

		/*
		 * eCommerce components
		 */
		'ecommerce_vouchers' => false,
		'ecommerce_reviews'  => false,
		'ecommerce_queries'  => false,
		'mass_update'        => false,
		'mass_import'        => false,
		'priceAdjust'        => true,
		'stockControl'       => false,
		'click_and_collect'  => false
	],


	/*
   |--------------------------------------------------------------------------
   | Component - Menus
   |--------------------------------------------------------------------------
   |
   | List of available menu positions
   |
   */
	'menus'        => [
		'default' => 'Default',
		'footer'  => 'Footer',
		'top'     => 'top'
	],

	/*
   |--------------------------------------------------------------------------
   | Stock Management
   |--------------------------------------------------------------------------
   |
   | Configuration of stock management and quantities
   |
   */

	//enable stock control
	//if false products will always so as 'in stock' on the front end
	'stockControl' => false,

	//The default quantity for adding to cart
	'defaultQty'   => 1,


	/*
   |--------------------------------------------------------------------------
   | Ecommerce Shipping
   |--------------------------------------------------------------------------
   |
   | Configuration for ecommerce shipping
   |
   */

	'shipping'       => [
		'flatRate' => 10.00
	],


	/*
   |--------------------------------------------------------------------------
   | Enabled streamline checkout
   |--------------------------------------------------------------------------
   |
   | Set to FALSE to allow user to select the payment service provider to use
   |
   | Set to the payment gateway key from the paymentMethods array below to force
   | user to user a single payment gateway
   */
	'forcePayMethod' => false,

	/*
   |--------------------------------------------------------------------------
   | Available Payment Gateways
   |--------------------------------------------------------------------------
   |
   | Enable payment gateways for checkout
   |
   */
	'paymentMethods' => [
		'paypal'   => true,
		'worldpay' => true
	],


	/*
   |--------------------------------------------------------------------------
   | Component - Meet The Team
   |--------------------------------------------------------------------------
   |
   | Configuration options for the 'meet the team' component
   |
   */
	'team'           => [
		//set the base URL to the meet the team page
		//
		'bio_page_url' => '/meet-the-team/'
	],


];
