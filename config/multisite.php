<?php

return [


	/*
	 * Site Information
	 * Access site information by siteId
	 *  - slug is used to prefix file names for SEO
	 */
	'sites' => [
		0 => null,

		/****************************************************************************************************
		 *   *** IMPORTANT *** side/siteId of 0 is NOT valid and causes the the site to default to #1
		 ***************************************************************************************************/
		1 => [
			'slug'       => 'default',
			'local'      => 'www.fcsoceans.cfx.local',
			'stage'      => 'fscoceans.caterpillar-cms.co.uk',
			'production' => 'www.fscoceans.com'
		],
		2 => [
			'slug'       => 'ifscgroup',
			'local'      => 'ifscgroup.fcsoceans.cfx.local',
			'stage'      => 'ifscgroup.caterpillar-cms.co.uk',
			'production' => 'www.ifscgroup.com'
		],
		3 => [
			'slug'       => 'ifscuk',
			'local'      => 'ifscuk.fcsoceans.cfx.local',
			'stage'      => 'ifscuk.caterpillar-cms.co.uk',
			'production' => 'www.ifscuk.com'
		],
		4 => [
			'slug'       => 'ifsciraq',
			'local'      => 'ifsciraq.fcsoceans.cfx.local',
			'stage'      => 'ifsciraq.caterpillar-cms.co.uk',
			'production' => 'www.ifsciraq.com'
		],
		5 => [
			'slug'       => 'ifscchina',
			'local'      => 'ifscchina.fcsoceans.cfx.local',
			'stage'      => 'ifscchina.caterpillar-cms.co.uk',
			'production' => 'www.ifscchina.com'
		],
		6 => [
			'slug'       => 'ifscdubai',
			'local'      => 'ifscdubai.fcsoceans.cfx.local',
			'stage'      => 'ifscdubai.caterpillar-cms.co.uk',
			'production' => 'www.ifscdubai.com'
		],
		7 => [
			'slug'       => 'ifschongkong',
			'local'      => 'ifschongkong.fcsoceans.cfx.local',
			'stage'      => 'ifschongkong.caterpillar-cms.co.uk',
			'production' => 'www.ifschongkong.com'
		],
		8 => [
			'slug'       => 'ifscjordan',
			'local'      => 'ifscjordan.fcsoceans.cfx.local',
			'stage'      => 'ifscjordan.caterpillar-cms.co.uk',
			'production' => 'www.ifscjordan.com'
		],
		9 => [
			'slug'       => 'ifscuae',
			'local'      => 'ifscuae.fcsoceans.cfx.local',
			'stage'      => 'ifscuae.caterpillar-cms.co.uk',
			'production' => 'www.ifscuae.com'
		],

	],

	'ipWhitelist' => [
		'127.0.0.1'
	]


];
