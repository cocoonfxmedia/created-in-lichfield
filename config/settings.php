<?php
/**
 * This settings file does not work for multisite
 */
return [

	'site_name' => 'fscoceans.com',
	'locale'    => 'en',

	/*
	 * Settings values should added below this line
	 */

	'quote_form_email_to' => 'info@fscoceans.com',


	//Customer email settings
	'email_from'          => 'info@fscoceans.com',
	'email_from_name'     => 'FSC Oceans',

	'email_to' => 'info@fscoceans.com',


	'ga_code' => 'UA-114982335-1',

	'company_name' => 'FSC Oceans Ltd',

	'address_line1' => 'address 1',
	'address_line2' => 'address 2',
	'address_line3' => '',
	'phone_no'      => '0203 542 1380',
	'display_email' => 'info@fscoceans.com',

	'map_url' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.4329477687957!2d-0.21250518446185962!3d51.46856687962873!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760f74b976dce7%3A0x50cb50e251592d1d!2sFulham%2C+London+SW6+3JA!5e0!3m2!1sen!2suk!4v1519911870941',

	'offices' => 'Dubai Airport,25.2608983,55.3710589|Dubai Tower,25.1850332,55.2746709|Hong Kong,22.365483,114.1171288|China,23.1199959,113.3208532|Iraq Bagdad,33.2933579,44.4230245|Iraq Basra,29.5735886,46.4144138|Jordon,31.9759686,35.8587698|London,51.4685302,-0.2115553',

	/*
	 * ^^^^ New setting values need to be added above this line ^^^^
	 */

];
