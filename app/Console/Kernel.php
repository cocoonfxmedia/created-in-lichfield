<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Currencey;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		//
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	protected function schedule( Schedule $schedule )
	{
		//collect currency exchange rates
		$schedule->call( function () {

			$currencies = [
				'GBP_USD',
				'GBP_EUR',
				'GBP_CNY'
			];

			foreach ( $currencies as $cur )
			{
				$url      = 'https://free.currencyconverterapi.com/api/v5/convert?q=' . $cur . '&compact=y';
				$response = json_decode( file_get_contents( $url ) );


				$rec           = new Currencey();
				$rec->currency = $cur;
				$rec->value    = number_format( $response->$cur->val, 3 );
				$rec->date     = date( 'Y-m-d' );
				$rec->save();
			}

		} )->daily();
	}

	/**
	 * Register the Closure based commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		require base_path( 'routes/console.php' );
	}
}
