<?php

namespace App\Traits;


trait ForceHttp
{
    /**
     * Appends a http:// to any input missing a transfer protocol.
     *
     * @param String $attribute
     * @return void
     */
    protected function forceHttp(array $attributes)
    {
        $input = $this->all();

        foreach ($attributes as $attribute ) {
            if (empty($input[$attribute])) {
                continue;
            }

            if (!preg_match("~^(?:f|ht)tps?://~i", $input[$attribute])) {
                $input[$attribute] = "http://" . $input[$attribute];
            }
        }

        $this->merge($input);
    }
}