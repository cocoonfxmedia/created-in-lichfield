<?php

namespace App\Http\Middleware;

use App\Models\Basket;
use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;

class BasketWidget {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next )
	{
		$session = Input::cookie( 'trp_basket' );


		//no basket session
		if ( ! $session )
		{
			View::share( "bw_basket", [] );

			return $next( $request );
		}

		$basket = Basket::where( 'session_id', $session )->with( 'product' )->get();


		if ( count( $basket ) > 0 )
		{
			View::share( 'bw_basket', $basket );

			//summarise the basket if there is anything in it
			$summary = [
				'count'    => 0,
				'subtotal' => 0,
				'shipping' => Config::get('cms.shipping.flatRate'),
				'total'    => 0
			];

			foreach ( $basket as $item )
			{
				$summary['count'] += $item->quantity;
				$summary['subtotal'] += $item->quantity * $item->price;
			}

			$summary['total'] = $summary['shipping'] + $summary['subtotal'];

			View::share( 'bw_summary', $summary );
		}
		else
		{
			View::share( "bw_basket", [] );
		}

		return $next( $request );
	}
}
