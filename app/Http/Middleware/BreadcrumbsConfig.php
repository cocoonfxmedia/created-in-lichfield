<?php

namespace App\Http\Middleware;

use App\Http\Requests\Request;
use Closure;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;

class BreadcrumbsConfig
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$segment = $request->segment(1);

    	if( $segment == 'admin' )
	    {
		    Breadcrumbs::setDivider( '<span class="navigation-pipe">&gt;</span>' );
		    Breadcrumbs::addCrumb('<i class="icon-home"></i>', '/');
	    }
	    else
	    {
		    Breadcrumbs::setDivider( '' );
	    }


	    Breadcrumbs::setCssClasses( 'breadcrumb' );


        return $next($request);
    }
}
