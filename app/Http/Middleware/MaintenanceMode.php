<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;

class MaintenanceMode
{
	protected $except = [
		'admin/*',
		'admin',
		'hook/*'
	];

	/**
	 * The application implementation.
	 *
	 * @var \Illuminate\Contracts\Foundation\Application
	 */
	protected $app;

	/**
	 * Create a new middleware instance.
	 *
	 * @param  \Illuminate\Contracts\Foundation\Application  $app
	 * @return void
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 *
	 * @throws \Symfony\Component\HttpKernel\Exception\HttpException
	 */
	public function handle($request, Closure $next)
	{

		if( $this->shouldPassThrough($request) )
		{
			return $next($request);
		}

		if ($this->app->isDownForMaintenance()) {
			$data = json_decode(file_get_contents($this->app->storagePath().'/framework/down'), true);

			throw new MaintenanceModeException($data['time'], $data['retry'], $data['message']);
		}

		return $next($request);
	}


	/**
	 * Determine if the request has a URI that should pass through CSRF verification.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return bool
	 */
	protected function shouldPassThrough($request)
	{
		foreach ($this->except as $except) {
			if ($except !== '/') {
				$except = trim($except, '/');
			}

			if ($request->is($except)) {
				return true;
			}
		}

		return false;
	}
}
