<?php

namespace App\Http\Middleware;

use App\Models\BlogArticle;
use Closure;
use Illuminate\Support\Facades\View;

class LastestNews {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next )
	{

		$sid = 1;
		if(!is_null(session("site")))
		{
			$sid =  session( 'site' )['sid'];
		}

		if ( $sid== '1' || $sid == 3 )
		{
			$latestNews = BlogArticle::where( 'published', 1 )->orderBy( 'created_at', 'DESC' )->limit( 3 )->get();
		}
		else
		{
			$max = 2;

			if( $sid == 5 )
			{
				$max = 3;
			}

			$latestNews = BlogArticle::where( 'published', 1 )->where( 'sid', session( 'site' )['sid'] )->orderBy( 'created_at', 'DESC' )->limit( $max )->get();
		}


		View::share( 'latesNews', $latestNews );

		/**
		 * Latest events;
		 */
		$events = [];

//		$rawEvents = json_decode(file_get_contents( storage_path('app/calendars/site_' . session('site')['sid'])));
;

		if ( file_exists( storage_path( 'app/calendars/site_' .  $sid ) ) )
		{

			$rawEvents = json_decode( file_get_contents( storage_path( 'app/calendars/site_' . session( 'site' )['sid'] ) ) );

			$rawEvents = collect( $rawEvents )->sortBy('Start');

			//find event closest to the current date
			$ct = time();
			$ptr = 0;
			foreach( $rawEvents as $i => $event )
			{
				if( strtotime($event->Start) > time() )
				{
					$ptr = $i;
					break;
				}
			}


			$count = 0;
			$maxNewsCount = 2;



			if( session('site')['sid'] == 1 )
			{
				$maxNewsCount  = 3;
			}

			if( count($rawEvents) > 0 )
			{
				for ( $x = $ptr; $count < $maxNewsCount && $x >= 0; $x -- )
				{
					$te = [
						'Subject'      => $rawEvents[ $x ]->Subject,
						'IsAllDay'     => $rawEvents[ $x ]->IsAllDay,
						'DisplayStart' => $rawEvents[ $x ]->DisplayStart
					];

					if ( ! $rawEvents[ $x ]->IsAllDay )
					{
						$te['DisplayEnd'] = $rawEvents[ $x ]->DisplayEnd;
					}

					$events[] = $te;

					$count ++;
				}
			}


//			foreach ( $rawEvents as $re )
//			{
//				$count++;
//				if( $count > $maxNewsCount )
//				{
//					break;
//				}
//
//
//				$te = [
//					'Subject'      => $re->Subject,
//					'IsAllDay'     => $re->IsAllDay,
//					'DisplayStart' => $re->DisplayStart
//				];
//
//				if ( ! $te['IsAllDay'] )
//				{
//					$te['DisplayEnd'] = $re->DisplayEnd;
//				}
//
//				$events[] = $te;
//			}
		}

//		dd( $events );

		View::share( 'latestEvents', $events );


		return $next( $request );
	}
}
