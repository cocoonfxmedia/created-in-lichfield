<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class Multisite
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next )
	{
		//Check component enabled
		if ( ! Config::get( 'cms.components.multisite' ) )
		{
			return $next( $request );
		}


		$sites = collect( config( 'multisite.sites' ) );

		$sid = $sites->count();
		do
		{
			$sid --;
		} while ( $sites[ $sid ][ env( 'APP_ENV' ) ] != $_SERVER['SERVER_NAME'] && $sid > 1 );


		/*
		 * Make site data available for use
		 */
		session( [ 'sid' => $sid ] );

		/*
		 * Override other settings
		 */

		//App settings
		Config::set( 'app.name', siteConfig( 'site_name' ) );

		App::setLocale( siteConfig( 'locale' ) );

		$protocol = 'http';
		if ( env( 'FORCE_SSL' ) )
		{
			$protocol = 'https';
		}
		Config::set( 'app.url', $protocol . '://' . $_SERVER['SERVER_NAME'] );

		//Mail settings
		Config::set( 'mail.from.address', siteConfig( 'email_from' ) );
		Config::set( 'mail.from.name', siteConfig( 'site_name' ) );
		Config::set( 'mail.to.contact', siteConfig( 'email_to' ) );


		return $next( $request );
	}
}
