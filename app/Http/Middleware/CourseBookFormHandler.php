<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CourseBookFormHandler {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next )
	{

		if ( Input::get( 'form' ) != 'bookCourse' )
		{
			return $next( $request );
		}

		//sent from
		$emailFrom = 'no-reply@' . $_SERVER['SERVER_NAME'];

		// build email
		$sendResult = Mail::send( 'emails.admin.courseEnquiry', Input::all(), function ( $msg )
		{
			$emailCompnens = explode('@', session('site')['email']);

			$msg->from( 'no-reply@' . $emailCompnens[1] )
			    ->subject( 'Course Booking Enquiry from ' . Input::get( 'schoolName' ) . ' for ' . Input::get( 'courseTitle' ) )
				->to( session( 'site' )['email'] );

		} );

		Session::flash( 'bookCourseResponse', '1' );

		return $next( $request );
	}
}
