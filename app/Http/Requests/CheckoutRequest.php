<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CheckoutRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if ( ! Auth::guest() )
		{
			return true;
		}

		return redirect( '/login' );
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'cgv' => 'required'
		];
	}

	public function messages()
	{
		return [
			'cgv.required' => 'You must agree to the terms of service in order to place your order'
		];
	}
}
