<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsletterSubscription extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'subscriber_email' => 'required|email'
		];
	}

	/**
	 * Get the messages that are returned by the request rules
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'subscriber_email.required' => 'Newsletter: We need your email address so we can send you our newsletter',
			'subscriber_email.email'    => 'Newsletter: The email address you gave us doesn\'t appear to be valid'
		];
	}
}
