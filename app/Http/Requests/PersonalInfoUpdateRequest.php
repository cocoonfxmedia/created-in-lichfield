<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalInfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
	        'last_name' => 'required',
	        'email' => 'required|email'
        ];
    }

    public function messages()
    {
    	return [
    		'email.required' => 'Please enter your email address',
		    'email.email' => 'The email address you\'ve enteres doesn\'t appear to be valid',
		    'first_name.required' => 'Please enter your full name',
		    'last_name.required' => 'Please enter your full name'
	    ];
    }
}
