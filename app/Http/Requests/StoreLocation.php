<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class StoreLocation extends FormRequest
{
	public function authorize(): bool
	{
		return true;
	}

	public function rules(): array
	{
		return [
			'categories' => 'required|array',
			'categories.*' => 'exists:art_categories,id',
			'gallery_id' => 'exists:galleries,id|nullable',
			'latitude' => 'numeric|nullable',
			'longitude' => 'numeric|nullable',
			'title' => 'required|string|max:255',
			'description' => 'string|max:5000|nullable',
			'external_link' => 'url|nullable',
			'header_image' => 'image|nullable',
			'remove_header_image' => 'boolean',
			'artists' => 'exists:artists,id|nullable',
			'address' => 'string|max:255|nullable',
		];
	}
}
