<?php

namespace App\Http\Requests\Admin;
use App\Http\Requests\Request;

use Illuminate\Support\Facades\Input;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'first_name' => 'required',
			'last_name' => 'required',
			'email'      => 'required|email|unique:users'
		];

		if ( ! empty( Input::get( 'password' ) ) )
		{
			$rules['pass_confirm'] = 'required|same:password';
		}

		return $rules;
	}

}
