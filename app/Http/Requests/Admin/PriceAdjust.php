<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PriceAdjust extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'amount' => 'required|numeric'
		];
	}

	public function messages()
	{
		return [
			'amount.required' => 'You must enter a % value to adjust product prices by',
			'amount.numeric'   => 'You must enter a valid % value to adjust product prices by'
		];
	}
}
