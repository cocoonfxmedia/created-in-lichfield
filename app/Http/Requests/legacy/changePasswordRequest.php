<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class changePasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'password'		 => 'required',
			'passConfirm'	 => 'required|same:password'
		];
	}

	/**
	 * Specify the error messages
	 *
	 * @return array
	 */
	public function messages()
	{

		return [
			'passConfirm.same'		 => 'Your passwords don\'t match',
			'password.required'		 => 'You need to enter a new password',
			'passConfirm.required'	 => 'You need to confirm your new password'
		];
	}

}
