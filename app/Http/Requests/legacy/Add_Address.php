<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Input;

/**
 * Class Add_Address
 * @package Elem\Http\Requests
 */
class Add_Address extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$billingRules = [ 'billing-address1'  => 'required|min:2',
						  'billing-town_city' => 'required|min:2',
						  'billing-county'    => 'required|min:2',
						  'billing-postcode'  => 'required|min:5' ];

		$shippingRules = [ 'shipping-address1'  => 'required|min:2',
						   'shipping-town_city' => 'required|min:2',
						   'shipping-county'    => 'required|min:2',
						   'shipping-postcode'  => 'required|min:5' ];

		if( env('SHOP_TYPE') == 'kiosk' )
		{
			if( Input::get('kiosk_pay_option') == 'paypal' && !Input::get( 'click_and_collect' ) )
			{
				return array_merge( $billingRules, $shippingRules );
			}


			if( Input::get('kiosk_pay_option') == 'cash' &&  !Input::get( 'click_and_collect' ) )
			{
				return $shippingRules;
			}


			if( Input::get('kiosk_pay_option') == 'paypal' && Input::get( 'click_and_collect' ) )
			{
				return $billingRules;
			}

			return [];

		}


		if( !\Input::get( 'click_and_collect' ) )
		{
			return array_merge( $billingRules, $shippingRules );
		}




		return $billingRules;
	}

	/**
	 * Personalise the error messages
	 *
	 * @return array
	 */
	public function messages()
	{

		return [ 'shipping-name.required'      => 'You must enter a shipping name',
				 'shipping-address1.required'  => 'You must enter a shipping address',
				 'shipping-town_city.required' => 'You must enter a shipping town or city',
				 'shipping-county.required'    => 'You must enter a shipping county',
				 'shipping-postcode.required'  => 'You must enter a shipping postcode',
				 'billing-name.required'       => 'You must enter a billing name',
				 'billing-address1.required'   => 'You must enter a billing address',
				 'billing-town_city.required'  => 'You must enter a billing town or city',
				 'billing-county.required'     => 'You must enter a billing county',
				 'billing-postcode.required'   => 'You must enter a billing postcode', ];
	}

}
