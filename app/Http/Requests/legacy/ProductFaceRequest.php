<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductFaceRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title'	 => 'required',
			'image'	 => 'required|mimes:png'
		];
	}

	public function messages()
	{
		return [
			'title.required' => 'You must enter a title for the print face',
			'image.required' => 'You must upload an image for the printable face',
			'image.mimes'	 => 'Product face images must be supplied as a PNG file'
		];
	}

}
