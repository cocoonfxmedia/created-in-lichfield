<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class Add_Address
 * @package Elem\Http\Requests
 */
class AdminCustomerUpdateRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'passConfirm' => 'same:password',
		];
	}

}
