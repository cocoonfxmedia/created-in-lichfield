<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubcategoryRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' 			=> 'required|unique:subcategories',
			'category_id'	=> 'required|integer|min:1'
		];
	}

	/**
	 * Specify the error messages
	 *
	 * @return array
	 */
	public function messages()
	{

		return [
			'name.required' 		=> 	'You must enter a subcategory name',
			'name.unique'			=>	'That subcategory name already exists',
			'category_id.required'	=> 	'You must choose a parent category',
			'category_id.min'		=>	'You must choose a parent category'
		];

	}

}
