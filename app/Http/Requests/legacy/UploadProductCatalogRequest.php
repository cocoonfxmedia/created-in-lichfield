<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class Add_Address
 * @package Elem\Http\Requests
 */
class UploadProductCatalogRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'catalog' => 'required'
		];
	}

	/**
	 * Specify the error messages
	 *
	 * @return array
	 */
	public function messages()
	{

		return [
			'catalog.required' => 'You must supply a CSV product catalog',
		];
	}

}
