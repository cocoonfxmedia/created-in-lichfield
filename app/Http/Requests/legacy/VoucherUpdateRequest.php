<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VoucherUpdateRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'label'		 => 'required|',
			'code'		 => 'required',
			'discount'	 => 'required|numeric',
			'expires_on' => 'date'
		];
	}

	public function messages()
	{
		return [
			'label.required'	 => 'You must enter a label for your voucher code',
			'code.unique'		 => 'This voucher code already exists',
			'code.required'		 => 'You must enter a voucher code',
			'discount.required'	 => 'You must enter an amount to discount the order by',
			'expires_on.date'	 => 'The expiry date must be a valid date',
		];
	}

}
