<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddressSaveRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'address_1'	 => 'required',
			'town_city'	 => 'required',
			'county'	 => 'required',
			'postcode'	 => 'required',
			'fullname'	 => 'required',
		];
	}

	/**
	 * Specify the error messages
	 *
	 * @return array
	 */
	public function messages()
	{

		return [
			'name.required'		 => 'You must enter a label for this address',
			'address_1.required' => 'You must enter the first line of your address',
			'town_city.required' => 'Your must enter the Town or City for this address'
		];
	}

}
