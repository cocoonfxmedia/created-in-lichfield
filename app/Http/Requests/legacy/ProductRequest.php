<?php namespace App\Http\Requests;

class ProductRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [ 'name'         => 'required|unique:products',
				   'product_code' => 'required|unique:products',
				   'description'  => 'required' ];

		if( Request::has( 'cf_enabled' ) )
		{
			$rules[ 'cf_label' ] = 'required';
			$rules[ 'cf_char' ] = 'required|integer';
			$rules[ 'cf_lines' ] = 'required|integer';
		}

		return $rules;
	}

	public function messages()
	{
		return [ 'name.required'         => 'You must enter a name',
				 'name.unique'           => 'The name must be unique',
				 'product_code.required' => 'You must enter a product code',
				 'product_code.unique'   => 'The product code must be unique',
				 'description.required'  => 'You must enter a description',
				 'cf_label.required'     => 'You must set a label for the custom field',
				 'cf_char.required'      => 'You must set the max number of characters that can be include',
				 'cf_lines.required'     => 'Your must set the number of lines for the custom field',
				 'cf_lines.integer'      => 'The number of lines must be a number',
				 'cf_char.integer'       => 'The number of characters must be a number' ];
	}

}
