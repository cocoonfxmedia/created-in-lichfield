<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class PaymentRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'card_number' => 'required',
			'expiry_month' => 'required',
			'expiry_year' => 'required|min:2|max:2',
			'cvv' => 'required|min:3|max:3',
		];
	}

	public function messages()
	{
		return [
			'card_number.required' => 'You must enter a card number',
			'expiry_year.min' => 'The expiry year must be two digits i.e. YY',
			'expiry_year.max' => 'The expiry year must be two digits i.e. YY',
		];

	}

}