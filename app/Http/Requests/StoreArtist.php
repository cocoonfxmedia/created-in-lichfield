<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\ForceHttp;

final class StoreArtist extends FormRequest
{
	use ForceHttp;

	public function authorize(): bool
	{
		return true;
	}

	public function rules(): array
	{
		$this->forceHttp([
			'website', 
			'facebook_link', 
			'twitter_link', 
			'instagram_link',
		]);

		return [
			'name' => 'required|string|max:255',
			'status' => 'string|in:pending,active,inactive',
			'website' => 'max:255|string|nullable',
			'bio' => 'string|max:5000',
			'facebook_link' => 'max:255|string|nullable',
			'twitter_link' => 'max:255|string|nullable',
			'instagram_link' => 'max:255|string|nullable',
			'categories' => 'required|array',
			'categories.*' => 'exists:art_categories,id',
			'profile_image' => 'image',
			'remove_profile_image' => 'boolean',
			'header_image' => 'image',
			'remove_header_image' => 'boolean',
			'can_manage_events' => 'bool',
			'can_manage_locations' => 'bool',
		];
	}
}
