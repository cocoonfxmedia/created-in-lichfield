<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class StoreEvent extends FormRequest
{
	public function authorize(): bool
	{
		return true;
	}

	public function rules(): array
	{
		return [
			'category_id' => 'exists:art_categories,id|nullable',
			'location_id' => 'exists:locations,id|nullable',
			'gallery_id' => 'exists:galleries,id|nullable',
			'latitude' => 'numeric|nullable',
			'longitude' => 'numeric|nullable',
			'title' => 'required|string|max:255',
			'at' => 'sometimes|required|date_format:Y-m-d H:i:s',
			'date_submit' => 'sometimes|required|date_format:Y-m-d',
			'time' => 'sometimes|required',
			'price' => 'required|numeric',
			'description' => 'string|max:5000|nullable',
			'external_link' => 'url|nullable',
			'header_image' => 'image|nullable',
			'event_code' => 'string|max:255|nullable',
		];
	}
}
