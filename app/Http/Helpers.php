<?php
/**
 * Helper functions for rendering the nav
 */


/**
 * Recursivley generate nav elements
 *
 * @param $nav
 *
 * @return string
 */
function generateNav( $nav )
{


	$output = '';

	foreach ( $nav as $li )
	{
		$output .= '<li>';

		switch ( $li->type )
		{
			case 'link':
				$output .= '<a href="' . $li->destination . '">' . $li->label . '</a>';
				break;

			case 'product_category':
				$label  = ( ( empty( $li->label ) ) ? $li->productCategory->name : $li->label );
				$output .= '<a href="/category/' . $li->productCategory->slug . '">' . $label . '</a>';
				break;

			case 'page':
				try
				{
					$label  = ( ( empty( $li->label ) ) ? $li->page->title : $li->label );
					$output .= '<a href="/' . $li->page->path . '">' . $label . '</a>';
				} catch ( Exception $e )
				{
					//no output as page object is empty
				}
				break;
		}


		if ( count( $li->children ) > 0 )
		{
			$output .= '<ul>';

			$output .= generateNav( $li->children );

			$output .= '</ul>';
		}

		$output .= '</li>';
	}

	return $output;
}


/**
 * Test if current page is the homepage
 *
 * @return bool
 */
function isHome()
{
	if ( is_null( \Illuminate\Support\Facades\Request::segment( 1 ) ) )
	{
		return true;
	}

	return false;
}

/**
 * Get settings for this site
 *
 * @param $key
 *
 * @return \Illuminate\Config\Repository|mixed
 */
function siteConfig( $key, $sid = null )
{
	$sidOveride = false;

	if ( ! config( 'cms.components.multisite' ) )
	{
		$sid = 1;
	} else
	{
		if ( is_null( $sid ) )
		{
			if ( \Illuminate\Support\Facades\Request::segment( 1 ) == 'admin' )
			{
				$sid = session( 'currentSite', 1 );
			} else
			{
				$sid = session( 'sid', 1 );
			}
		} else
		{
			$sidOveride = true;
		}
	}


	switch ( $key )
	{
		case 'sid':
		case 'id':
			return $sid;
			break;

		case 'slug';
			return config( 'multisite.sites.' . $sid . '.slug' );
			break;

		case 'url';
			if ( ! config( 'cms.components.multisite' ) )
			{
				return env( 'APP_URL' );
			}

			$protocol = 'http';
			if ( env( 'FORCE_SSL' ) )
			{
				$protocol = 'https';
			}

			return $protocol . '://' . config( 'multisite.sites.' . $sid . '.' . env( 'APP_ENV' ) );
			break;

		default:
			$settingsFile = 'settings';

			if ( config( 'multisite.sites.' . $sid . '.slug' ) !== 'default' )
			{
				$settingsFile .= '_' . config( 'multisite.sites.' . $sid . '.slug' );
			}
			if ( ! file_exists( config_path( $settingsFile . '.php' ) ) )
			{
				$settingsFile = 'settings';
			}

			return config( $settingsFile . '.' . $key );
	}
}