<?php

namespace App\Http\ViewComposers;

use App\Models\CaseStudy;
use App\Models\CaseStudyCategory;
use Illuminate\View\View;
use App\Repositories\UserRepository;

class CaseStudyCategoriesComposer
{


	/**
	 * Bind data to the view.
	 *
	 * @param  View $view
	 *
	 * @return void
	 */
	public function compose( View $view )
	{
		$view->with( 'caseStudyWidget', CaseStudy::published()->orderBy( 'title' )->get() );
	}
}