<?php

namespace App\Http\Controllers;

use App\Models\CaseStudy;
use App\Models\CaseStudyCategory;
use Illuminate\Http\Request;

class CaseStudyController extends Controller
{

	/**
	 * Main blog page
	 *
	 * @return mixed
	 */
	public function index()
	{

		return view( 'case_studies.index' )->with( [
			'articles' => CaseStudy::published()
			                       ->where( 'sid', siteConfig( 'sid' ) )
			                       ->orderBy( 'created_at', 'DESC' )
			                       ->get()
		] );

	}

	/**
	 * Single article
	 *
	 * @param $slug
	 *
	 * @return mixed
	 */
	public function article( $slug )
	{

		$article = CaseStudy::where( 'url', $slug )
		                    ->published()
		                    ->first();

		if ( ! $article )
		{
			abort( 404 );
		}

		return view( 'case_studies.article' )
			->with( 'article', $article );
	}

	/**
	 * category page
	 *
	 * @param $slug
	 *
	 * @return $this
	 */
	public function category( $slug )
	{
		$payload = [
			'category' => CaseStudyCategory::where( 'slug', $slug )->active()->first()
		];

		if ( ! $payload['category'] )
		{
			abort( 404 );
		}


		$payload['articles'] = CaseStudy::published()
		                                ->where( 'category_id', $payload['category']->id )
		                                ->where( 'sid', siteConfig( 'sid' ) )
		                                ->orderBy( 'created_at', 'DESC' )
		                                ->get();

		return view( 'case_studies.index' )->with( $payload );
	}

}
