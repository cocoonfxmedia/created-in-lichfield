<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 09:59
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\EditBlogArticleRequest;
use App\Http\Requests\Admin\NewBlogArticleRequest;
use App\Http\Requests\Request;
use App\Models\BlogArticle;
use App\Models\BlogCategory;
use App\Models\Page;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class BlogController extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Blog', '/admin/blog' );

		$this->addNavElement( 'blog' );
		$this->addNavElement( 'blog-articles' );
	}

	/**
	 * List view
	 * @return Page collection
	 */
	public function adminIndex()
	{
		return view( 'admin.blog.list', [
			'articles' => BlogArticle::where( 'sid', siteConfig( 'sid' ) )->get()
		] );
	}


	/**
	 * New blog article form
	 *
	 * @return mixed
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New article', '/admin/blog/create' );

		return view( 'admin.blog.create', [ 'categories' => $this->categoryList() ] );
	}


	/**
	 * Create new page
	 * @return mixed
	 */
	public function store( NewBlogArticleRequest $request )
	{
		$page = new BlogArticle();

		$data = Input::all();

		$page->title            = Input::get( 'title' );
		$page->meta_title       = Input::get( 'meta_title' );
		$page->meta_description = Input::get( 'meta_description' );
		$page->content          = Input::get( 'content' );
		$page->category_id      = Input::get( 'category_id' );
		$page->sid              = siteConfig( 'sid' );

		$page->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );

		if ( empty( $data['url'] ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_slug( $data['url'], '-' );
		}

		$tba = BlogArticle::where( 'url', $page->url );
		if ( $tba )
		{
			$page->url .= '-' . str_random( 3 );
		}

		$page->save();


		/*
		 * Process thumbnail files
		 */
		if ( Input::file( 'thumbnail' ) )
		{
			$filename = $page->id . '_' . Input::file( 'thumbnail' )->getClientOriginalName();

			Input::file( 'thumbnail' )->move( base_path() . '/public/usr/blog_thumbs/', $filename );

			$page->featured_image = $filename;
			$page->save();
		}

		return redirect( 'admin/blog' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New article saved'
		] );
	}

	/**
	 * Delete page
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function destroy( $page_id )
	{
		$page = BlogArticle::find( $page_id );

		$page->delete();

		return redirect( '/admin/blog' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Article removed'
		] );
	}

	/**
	 * Blog Article edit view
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function edit( $page_id )
	{
		Breadcrumbs::addCrumb( 'Edit Article', '/admin/blog/edit/' . $page_id );

		return view( 'admin.blog.edit', [
			'article'    => BlogArticle::find( $page_id ),
			'categories' => $this->categoryList()
		] );
	}

	/**
	 * Update page object
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function update( EditBlogArticleRequest $request, $page_id )
	{
		$page = BlogArticle::find( $page_id );

		$page->update( Input::all() );

		$page->published = ( ( Input::get( 'published' ) == '1' ) ? 1 : 0 );

		//ensure URL is not empty
		if ( empty( $page->url ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_slug( $page->url, '-' );
		}

		$page->save();


		/*
		 * Process thumbnail files
		 */
		if ( Input::file( 'thumbnail' ) )
		{
			$filename = $page->id . '_' . Input::file( 'thumbnail' )->getClientOriginalName();

			Input::file( 'thumbnail' )->move( base_path() . '/public/usr/blog_thumbs/', $filename );

			$page->featured_image = $filename;
			$page->save();
		}


		return redirect( '/admin/blog/edit/' . $page_id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Article updated'
		] );
	}


	/**
	 * View single blog post
	 *
	 * @param $url
	 *
	 * @return mixed
	 */
	public function view( $url )
	{
		$page = BlogArticle::find_by_url( $url );


		//check page is available
		if ( ! $page )
		{
			abort( 404 );
		}

		return view( 'cms_templates . ' . $page->template )->with( 'page', $page );
	}


	/**
	 * Form view categoryList
	 *
	 * @return array
	 */
	public function categoryList()
	{
		$cats = [];

		$categories = BlogCategory::active()->get();

		foreach ( $categories as $c )
		{
			$cats[ $c->id ] = $c->title;
		}

		return $cats;
	}


	/**
	 * Trash bin list
	 *
	 * @return mixed
	 */
	public function trash()
	{
		Breadcrumbs::addCrumb( 'Recycle Bin', '/admin/blog/trash' );

		return view( 'admin.blog.trash', [ 'articles' => BlogArticle::onlyTrashed()->get() ] );
	}

	/**
	 * Restore a page
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function restore( $id )
	{
		BlogArticle::withTrashed()->where( 'id', $id )->restore();

		return redirect( '/admin/blog' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Article removed from the recycle bin'
		] );
	}

	/**
	 * Empty the recycle bin
	 *
	 * @return mixed
	 */
	public function emptyTrash()
	{
		BlogArticle::onlyTrashed()->forceDelete();

		return redirect( '/admin/blog' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Recycle bin has been emptied'
		] );
	}

}