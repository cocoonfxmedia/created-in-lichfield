<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProductCategory;
use App\Http\Requests\ProductFaceRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UploadProductCatalogRequest;
use App\Models\Product;
use App\Models\ProductColours;
use App\Models\ProductFace;
use App\Models\ProductSizes;
use App\Models\RelatedProduct;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Facades\Input;

class ProductController extends AdminController {


	/**
	 * ProductController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Products', '/admin/products' );

		$this->addNavElement('products');
		$this->addNavElement('products-catalog');
	}

	/**
	 * Display all the products that are active
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::where( 'active', true )->get();

		return view( 'admin.product.index' )->with( 'products', $products );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New product', '#' );

		$categories = productCategory::fetchForSelect();

		return view( 'admin.product.create', [ 'categories' => $categories ] );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store( ProductRequest $request )
	{
		$product_id = Product::createProduct( $request->all() );

		return redirect( '/admin/products/' . $product_id . '/edit' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Product saved'
		] );
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id )
	{
		Breadcrumbs::addCrumb( 'Edit', '#' );

		$product       = Product::where( 'id', $id )->first();
		$categories    = productCategory::fetchForSelect();
		$colours       = ProductColours::fetchForProduct( $id );
		$sizes         = ProductSizes::fetchForProduct( $id );
		$productTitles = Product::select( 'name' )->active()->get();

		return view( 'admin.product.edit' )
			->with( 'product', $product )
			->with( 'categories', $categories )
			->with( 'colours', $colours )
			->with( 'sizes', $sizes )
			->with( 'productTitles', $productTitles );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function update()
	{
		$data = Input::all();

		$data['price']      = str_replace( ",", '', $data['price'] );
		$data['sale_price'] = str_replace( ',', '', $data['sale_price'] );

		if ( empty( $data['dimension_l'] ) )
		{
			$data['dimension_l'] = 0;
		}
		if ( empty( $data['dimension_w'] ) )
		{
			$data['dimension_w'] = 0;
		}
		if ( empty( $data['dimension_h'] ) )
		{
			$data['dimension_h'] = 0;
		}

		Product::updateProduct( $data );

		if ( Input::only( 'save_and_new' )['save_and_new'] == 'yes' )
		{
			return redirect()->to( '/admin/products/create' );
		}

		return redirect()->back()->with( 'msg', [ 'state' => 'success', 'text' => 'Changes saved' ] );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id )
	{

		$product = Product::find( $id );

		$product->delete();

		return redirect()->back()->with( 'msg', [ 'state' => 'success', 'text' => 'Product removed' ] );
	}

	/**
	 * Create new product colour
	 *
	 * @return mixed
	 */
	public function createColour()
	{
		ProductColours::createColour( Input::all() );

		return redirect()->back()->with( 'msg', [ 'state' => 'success', 'text' => 'New colour saved' ] );
	}


	/**
	 * Delete product colour
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function deleteColour( $id )
	{
		ProductColours::find( $id )->delete();

		return redirect()->back()->with( 'msg', [ 'state' => 'success', 'text' => 'Colour removed' ] );
	}

	/**
	 * Create new product size
	 *
	 * @return mixed
	 */
	public function createSize()
	{
		ProductSizes::createSize( Input::all() );

		return redirect()->back()->with( 'msg', [ 'state' => 'success', 'text' => 'Size option created' ] );
	}

	/**
	 * Delete product size
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function deleteSize( $id )
	{
		ProductSizes::find( $id )->delete();

		return redirect()->back()->with( 'msg', [ 'state' => 'success', 'text' => 'Size removed' ] );
	}


	/**
	 *
	 * Import product catalog form
	 */
	public function import()
	{
		return view( 'admin.product.import' )->with( 'messages', null );
	}

	/**
	 * Process CSV product catalog
	 */
	public function importProcessor( UploadProductCatalogRequest $request )
	{
		$messages = [];

		$filename = date( 'Ymdghs' ) . '_' . Input::file( 'catalog' )->getClientOriginalName();

		Input::file( 'catalog' )->move( base_path() . '/storage/app/csv_catalogs/', $filename );

		//Read the file
		ini_set( "auto_detect_line_endings", "1" );
		$uploadedFile = fopen( base_path() . '/storage/app/csv_catalogs/' . $filename, "r" ) or die( "Unable to open file!" );
		$rawCatalog = fread( $uploadedFile, filesize( base_path() . '/storage/app/csv_catalogs/' . $filename ) );
		fclose( $uploadedFile );

		//fix for MS Excel EOL delimiter
		$rawCatalog = str_replace( "\r", "\n", $rawCatalog );

//		dd( $rawCatalog );
		//convert content to data object 
		$catalog = Formatter::make( $rawCatalog, Formatter::CSV )->toArray();

		Log::info( '*** Product catalog import started *** ' . count( $catalog ) . ' products to import' );
		$messages[] = count( $catalog ) . ' products found in the catalog';


//		dd( $catalog );

		foreach ( $catalog as $item )
		{
			//check that the product does not already exist
			$product = Product::where( 'product_code', '=', strtoupper( $item['sku'] ) )->first();
			if ( ! is_null( $product ) )
			{
				//skip this product
				Log::info( 'Product with SKU ' . $item['sku'] . ' already exists in the site product catalog so was skipped' );
				$messages[] = 'Product with SKU ' . $item['sku'] . ' already exists in the site product catalog so was skipped';

				continue;
			}

			//Create new product


			$product                   = new Product();
			$product->product_code     = strtoupper( $item['sku'] );
			$product->name             = $item['name'];
			$product->slug             = str_slug( $item['name'] );
			$product->description      = $item['description'];
			$product->price            = $item['price'];
			$product->sale_price       = $item['sale_price'];
			$product->meta_title       = $item['meta_title'];
			$product->meta_description = $item['meta_description'];
			$product->active           = 1;

			$product->featured = ( ( strtoupper( $item['featured'] ) == 'Y' ) ? 1 : 0 );
			$product->image    = ( ( ! empty( $item['image'] ) ) ? $item['image'] : null );

			$product->qty          = $item['qty'];
			$product->weight       = $item['weight'];
			$product->dimension_w  = $item['dimension_w'];
			$product->dimension_h  = $item['dimension_h'];
			$product->dimension_l  = $item['dimension_l'];
			$product->package_type = $item['package_type'];

			//link to category
			$category = productCategory::where( 'name', '=', $item['category'] )->first();


			//Create new category if category was not found
			if ( ! $category )
			{
				$category = New Category();

				$category->name   = $item['category'];
				$category->slug   = str_slug( $item['category'] );
				$category->active = 1;

				$category->save();

				$messages[] = 'New catgeory created: ' . $category->name;
			}

			$product->category_id = $category->id;


			//Handle images
			$product->image   = $item['Image_1'];
			$product->image_1 = $item['Image_2'];
			$product->image_2 = $item['Image_3'];
			$product->image_3 = $item['Image_4'];


			//generate product ID
			$product->save();


			//Create sizes for the product
			$sizes = explode( ";", $item['sizes'] );

			foreach ( $sizes as $src_size )
			{
				$size = new ProductSizes();

				$size->product_id = $product->id;
				$size->size       = trim( $src_size );

				$size->save();
			}


			//Create the colours for the product
			$colourPallet = [
				'BLACK' => '000000',
				'WHITE' => 'ffffff'
			];

			$colours = explode( ";", $item['colours'] );

			foreach ( $colours as $src_colour )
			{
				$productColour = new ProductColours();

				$productColour->product_id = $product->id;

				if ( str_contains( $src_colour, '|' ) )
				{
					//#hex colour is defined
					try
					{
						$colourComponents = explode( "|", $src_colour );

						$productColour->name = trim( $colourComponents[0] );
						$productColour->hex  = str_replace( "#", '', trim( $colourComponents[1] ) );

						$productColour->save();
					} catch ( Exception $e )
					{
						Log::info( 'Product SKU: ' . $product->product_code . ' - Error creating colour: ' . $src_colour );

						$messages[] = 'Product SKU: ' . $product->product_code . ' - Error creating colour: ' . $src_colour;
					}
				}
				else
				{
					//hex colour not defined
					$src_colour = trim( $src_colour );

					$colourIndex = strtoupper( $src_colour );

					if ( isset( $colourPallet[ $colourIndex ] ) )
					{
						$productColour->name = $src_colour;
						$productColour->hex  = $colourPallet[ $colourIndex ];

						$productColour->save();
					}
					else
					{
						$messages[] = 'Product with SKU ' . $product->product_code . ' - Unable to create color "' . $src_colour . '", this colour was skipped';
					}
				}
			}
		}

		Log::info( '*** Product Import Complete ***' );
		$messages[] = 'Product catalogue import completed';

		return view( 'admin.product.import' )->with( 'messages', $messages );
	}


	/**
	 * Attach a new related product
	 *
	 * @param type $id
	 *
	 * @return Response
	 */
	public function relatedProduct( $id )
	{
		$product = Product::find( $id );

		$rProduct = Product::where( 'name', '=', Input::get( 'rproduct_name' ) )->get()->first();

		if ( ! $rProduct )
		{
			Session::flash( 'relation_msg', 'The related product doesn\'t exist' );
		}
		else
		{
			$relation = new RelatedProduct();

			$relation->parent_product = $product->id;
			$relation->child_product  = $rProduct->id;

			$relation->save();
		}

		return redirect( 'admin/products/' . $id . '/edit' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Related product added'
		] );
	}

	/**
	 * Delete a product relation
	 *
	 * @param type $id
	 *
	 * @return Redirect
	 */
	public function deleteRelaiton( $parent_product, $child_product )
	{
		$relation = RelatedProduct::where( 'parent_product', '=', $parent_product )
		                          ->where( 'child_product', '=', $child_product )
		                          ->first();

		$relation->delete();

		return redirect( 'admin/products/' . $parent_product . '/edit' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Product relationship removed'
		] );
	}

	/**
	 * Product order list
	 *
	 * @return Response
	 */
	public function order()
	{
		return view( 'admin.product.order' )->with( 'products', Product::active()->inOrder()->get() );
	}

	/**
	 * Save changed product order
	 *
	 * @return mixed
	 */
	public function orderSave()
	{
		$productOrder = explode( ",", Input::get( 'product_order' ) );


		foreach ( $productOrder as $order => $pid )
		{
			$product = Product::find( $pid );

			$product->page_order = $order;

			$product->save();
		}

		Session::flash( 'status', 'Product Order Saved' );

		return redirect( 'admin/products/order' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Products reordered'
		] );
	}

	/**
	 * Mass Update admin UI
	 *
	 * @return mixed
	 *
	 * @since version
	 */
	public function massUpdate()
	{
		return view( 'admin.product.mass_update' )->with( 'messages', null );
	}

	/**
	 * Download current product catalog
	 *
	 * @return mixed
	 *
	 * @since version
	 */
	public function export()
	{

		$payload = [];

//		$payload [] = [
//			'id',
//			'sku',
//			'name',
//			//			'description',
//			'category',
//			'price',
//			'sale_price',
//			'meta_title',
////			'meta_description',
//			'featured',
//			'qty',
//			'weight',
//			//			'package_type',
//			'dimension_w',
//			'dimension_h',
//			'dimension_l'
//		];


		$products = Product::all();


		foreach ( $products as $p )
		{
			$featured = ( ( $p->featured == 1 ) ? 'y' : 'n' );


			$payload[] = [
				'id'          => $p->id,
				'sku'         => $p->product_code,
				'name'        => str_replace( ',', ' - ', str_replace( '""', '""', $p->name ) ),
				//				$p->description,
				'category'    => ( ( ! $p->category ) ? 'Unknown' : $p->category->name ),
				'price'       => str_replace( ',', '', $p->price ),
				'sale_price'  => str_replace( ',', '', $p->sale_price ),
				'meta_title'  => str_replace( ',', ' - ', str_replace( '""', '""', $p->meta_title ) ),
				//				$p->meta_description,
				'featured'    => $featured,
				'qty'         => str_replace( ',', '', $p->qty ),
				'weight'      => $p->weight,
				//				$p->package_type,
				'dimension_w' => $p->dimension_w,
				'dimension_h' => $p->dimension_h,
				'dimension_l' => $p->dimension_l
			];
		}

		$catalog = Formatter::make( $payload, Formatter::ARR )->toCSV();


		return response( $catalog )
			->header( 'Content-Type', 'text/csv; charset=utf-8' )
			->header( 'Content-Disposition', 'attachment; filename=catalog.csv' );

	}

	/**
	 * Mass update product functionality
	 *
	 * @return $this
	 */
	public function massUpdateProcessor()
	{
		$messages = [];

		$filename = date( 'Ymdghs' ) . '_' . Input::file( 'catalog' )->getClientOriginalName();

		Input::file( 'catalog' )->move( base_path() . '/storage/app/mass_update_catalogs/', $filename );

		//Read the file
		ini_set( "auto_detect_line_endings", "1" );
		$uploadedFile = fopen( base_path() . '/storage/app/mass_update_catalogs/' . $filename, "r" ) or die( "Unable to open file!" );
		$rawCatalog = fread( $uploadedFile, filesize( base_path() . '/storage/app/mass_update_catalogs/' . $filename ) );
		fclose( $uploadedFile );

		//fix for MS Excel EOL delimiter
		$rawCatalog = str_replace( "\r", "\n", $rawCatalog );

//		dd( $rawCatalog );
		//convert content to data object
		$catalog = Formatter::make( $rawCatalog, Formatter::CSV )->toArray();

		unset( $catalog[0] );

		Log::info( '*** Product catalog mass update started *** ' . count( $catalog ) . ' products to process' );
		$messages[] = count( $catalog ) . ' products found in the catalog';


//		dd( $catalog );


		foreach ( $catalog as $item )
		{

			//check that the product does not already exist
			$product = Product::find( $item['id'] );
			if ( ! $product )
			{
				if ( $item['id'] == '' && $item['name'] == '' )
				{
					//skip blank line
					continue;
				}
				//skip this product
				Log::info( 'Product with SKU ' . $item['sku'] . ' was not found in the catalogue' );
				$messages[] = 'Product with SKU ' . $item['sku'] . ' was not found in the catalogue';

				continue;
			}

			//Create new product

			$product->product_code = strtoupper( $item['sku'] );
			$product->name         = $item['name'];
			$product->slug         = str_slug( $item['name'] );
//			$product->description      = $item[ 'description' ];
			$product->price      = $item['price'];
			$product->sale_price = $item['sale_price'];
			$product->meta_title = $item['meta_title'];

			$product->featured = ( ( strtoupper( $item['featured'] ) == 'Y' ) ? 1 : 0 );

			$product->qty         = $item['qty'];
			$product->weight      = $item['weight'];
			$product->dimension_w = $item['dimension_w'];
			$product->dimension_h = $item['dimension_h'];
			$product->dimension_l = $item['dimension_l'];
			//$product->package_type = $item[ 'package_type' ];

			//link to category
			$category = productCategory::where( 'name', '=', $item['category'] )->first();


			//Create new category if category was not found
			if ( ! $category )
			{
				$category = New Category();

				$category->name   = $item['category'];
				$category->slug   = str_slug( $item['category'] );
				$category->active = 1;

				$category->save();

				$messages[] = 'New catgeory created: ' . $category->name;
			}

			$product->category_id = $category->id;

			//generate product ID
			$product->save();

		}

		Log::info( '*** Product Update Complete ***' );
		$messages[] = 'Product catalogue update completed';

		return view( 'admin.product.mass_update' )->with( 'messages', $messages );
	}

}
