<?php namespace App\Http\Controllers\Admin;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use App\Models\ProductCategory;

//use Elem\Http\Requests\CategoryRequest;
//use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProductCategoryController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		Breadcrumbs::addCrumb( 'Products', '/admin/products' );
		Breadcrumbs::addCrumb( 'Product Categories', '/admin/product-categories' );

		$this->addNavElement('products');
		$this->addNavElement('products-categories');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view( 'admin.product.category.index', [ 'categories' => ProductCategory::all() ] );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New Category', '#' );

		$category_catelog = ProductCategory::active()->get();

		$categories = [];

		$categories[0] = 'No Parent';

		foreach ( $category_catelog as $cat )
		{
			$categories[ $cat->id ] = $cat->name;
		}

		return view( 'admin.product.category.create', [ 'categories' => $categories ] );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		ProductCategory::createCategory( Input::all() );

		return redirect()->route( 'categories' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Product category created'
		] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id )
	{
		Breadcrumbs::addCrumb( 'Edit', '/admin/product-categories/edit/' . $id );

		$category_catelog = ProductCategory::active()->get();

		$categories = [];

		$categories[0] = 'No Parent';

		foreach ( $category_catelog as $cat )
		{
			if ( $cat->id != $id )
			{
				$categories[ $cat->id ] = $cat->name;
			}
		}


		return view( 'admin.product.category.edit', [
			'category' => ProductCategory::find( $id ),
			'categories' => $categories
		] );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function update( $id )
	{
		ProductCategory::updateCategory( $id );

		return redirect( '/admin/product-categories' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Product category updated'
		] );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id )
	{
		$category = ProductCategory::find( $id );

		$category->delete();

		return redirect( '/admin/product-categories' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Product category removed'
		] );
	}

}
