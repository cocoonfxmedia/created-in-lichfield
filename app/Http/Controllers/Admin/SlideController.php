<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\EditSlideRequest;
use App\Http\Requests\Admin\NewSlideRequest;
use App\Models\Slide;
use App\Models\Slider;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SlideController extends AdminController
{

	protected $slider;

	public function __construct( Request $request )
	{
		parent::__construct();

		$this->slider = Slider::find( $request->segment( 3 ) );
	}

	/**
	 * Display a listing of slides
	 *
	 * @return Response
	 */
	public function index( $slider_id )
	{	
		$this->breadcrumbInit();

		$slides = Slide::where( 'sid', $slider_id )->get();

		return View( 'admin.slides.list', [ 'slider' => $this->slider, 'slides' => $slides ] );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create( $slider_id )
	{
		$this->breadcrumbInit();
		Breadcrumbs::addCrumb( 'New Slide', 'admin/sliders/' . $this->slider->id . '/slides/create' );

		return view( 'admin.slides.create', [ 'slider' => $this->slider, 'slide' => [] ] );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store( $slider_id, NewSlideRequest $request )
	{
		$slide      = new Slide( Input::all() );
		$slide->sid = $this->slider->id;

		$slide->save();

		if ( Input::file( 'image' ) )
		{
			$storage_path = base_path() . '/public/usr/dip/';
			$filename     = $slide->id . '.' . Input::file( 'image' )->getClientOriginalExtension();

			Input::file( 'image' )->move( $storage_path, $filename );

			$slide->image = $filename;

			$slide->save();
		}

		return redirect( 'admin/sliders/' . $this->slider->id . '/slides/edit/' . $slide->id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New slide created'
		] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $slide_id, $id )
	{
		$this->breadcrumbInit();
		Breadcrumbs::addCrumb( 'Edit Slide', '/admin/sliders/' . $this->slider->id . '/slides/edit/' . $id );

		$slide = Slide::find( $id );

		return view( 'admin.slides.edit' )->with( 'slide', $slide );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function update( $slider_id, $id, EditSlideRequest $request )
	{
		$slide = Slide::find( $id );

		$slide->update( $request->input() );


		if ( Input::file( 'image' ) )
		{
			$storage_path = base_path() . '/public/usr/dip/';
			$filename     = $slide->id . '.' . Input::file( 'image' )->getClientOriginalExtension();

			Input::file( 'image' )->move( $storage_path, $filename );

			$slide->image = $filename;
		}

		$slide->save();

		return redirect( 'admin/sliders/' . $this->slider->id . '/slides/edit/' . $slide->id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Slide updated'
		] );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $slider_id, $id )
	{
		Slide::find( $id )->delete();

		return redirect( 'admin/sliders/' . $this->slider->id . '/slides' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Slide removed'
		] );
	}

	/**
	 * Init the Admin Breadcrumb
	 *
	 * @return void
	 */
	private function breadcrumbInit()
	{
		Breadcrumbs::addCrumb('Slider Manager', '/admin/sliders');
		Breadcrumbs::addCrumb($this->slider->name, '/admin/sliders/' . $this->slider->id . '/slides');

		$this->addNavElement('sliders');
	}
}
