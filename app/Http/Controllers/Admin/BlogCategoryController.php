<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use App\Models\BlogCategory;
use Illuminate\Support\Facades\Input;

class BlogCategoryController extends AdminController
{

	public function __construct()
	{
		Parent::__construct();

		Breadcrumbs::addCrumb('Blog', url('admin/blog') );
		Breadcrumbs::addCrumb( 'Categories', '/admin/blog-categories' );

		$this->addNavElement('blog');
		$this->addNavElement('blog-categories');
	}

	/**
	 * Admin UI for blog category list
	 *
	 * @return $this
	 */
	public function adminCategoryIndex()
	{
		return view( 'admin.blog.category.list' )->with( 'categories', BlogCategory::all() );
	}

	/**
	 * Create new blog category
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function categoryCreate()
	{
		Breadcrumbs::addCrumb( 'New Category', '/admin/blog-categories/create' );

		return view( 'admin.blog.category.create' );
	}

	/**
	 * Edit existing blog category
	 *
	 * @param $id
	 *
	 * @return $this
	 */
	public function categoryEdit( $id )
	{
		$category = BlogCategory::find( $id );

		Breadcrumbs::addCrumb( 'Edit: ' . $category->title, '/admin/blog-categories/create' );

		return view( 'admin.blog.category.edit' )->with( 'cat', $category );
	}

	/**
	 * Delete blog category
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function categoryDestroy( $id )
	{

		$category = BlogCategory::find( $id );

		$category->delete();

		return redirect( 'admin/blog-categories' )->with('msg', ['state' => 'success', 'text' => 'Category deleted']);
	}

	/**
	 * Save changes to blog category
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function categoryUpdate( $id )
	{
		$category = BlogCategory::find( $id );

		$category->update( Input::all() );

		$category->active = ( ( Input::get( 'active' ) == '1' ) ? 1 : 0 );

		$category->save();

		return redirect( 'admin/blog-categories' )->with( 'msg', 'Category updates successfully' );
	}

	/**
	 * Save new blog category
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function categoryStore()
	{
		$category = new BlogCategory();

		$category->title = Input::get( 'title' );

		if ( Input::get( 'slug' ) )
		{
			$category->slug = str_slug( Input::get( 'slug' ) );
		}
		else
		{
			$category->slug = str_slug( $category->title );
		}

		$category->active = ( ( Input::get( 'active' ) == 'on' ) ? 1 : 0 );


		$category->save();


		return redirect('admin/blog-categories')->with('msg', 'New blog category added');
	}
}
