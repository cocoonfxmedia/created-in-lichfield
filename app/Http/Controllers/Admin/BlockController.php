<?php

namespace App\Http\Controllers\Admin;

use App\Models\Block;
use App\Models\BlockPanel;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BlockController extends AdminController {

	/**
	 * BlockController constructor.
	 */
	public function __construct() {
		parent::__construct();

		Breadcrumbs::addCrumb( 'Pages', '/admin/pages' );
		Breadcrumbs::addCrumb( 'Blocks', '/admin/blocks' );

		$this->addNavElement('pages');
		$this->addNavElement('pages-blocks');
	}

	/**
	 * List available blocks
	 * @return $this
	 */
	public function index() {
		return view( 'admin.blocks.list' )->with( 'blocks', Block::all() );
	}

	/**
	 * Delete content block
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete( $id ) {
		$block = Block::find( $id );

		$panels = BlockPanel::where( 'block', $block->id )->delete();

		$block->delete();


		return redirect( 'admin/blocks' )->with( 'msg', [ 'state' => 'success', 'text' => 'Content block deleted' ] );
	}

	/**
	 * Unppublish content block
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function unpublish( $id ) {
		$block            = Block::find( $id );
		$block->published = 0;

		$block->save();

		return redirect( 'admin/blocks' )->with( 'msg', [
			'state' => 'success',
			'text'  => $block->name . ' has been unpublished'
		] );
	}


	/**
	 * Publish content block
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function publish( $id ) {
		$block            = Block::find( $id );
		$block->published = 1;

		$block->save();

		return redirect( 'admin/blocks' )->with( 'msg', [
			'state' => 'success',
			'text'  => $block->name . ' has been published'
		] );
	}

	/**
	 * Edit content block details
	 *
	 * @param $id
	 *
	 * @return $this
	 */
	public function edit( $id ) {
		Breadcrumbs::addCrumb( 'Edit Details', '#' );

		return view( 'admin.blocks.edit' )->with( 'block', Block::find( $id ) );
	}

	/**
	 * Create new content block
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		Breadcrumbs::addCrumb( 'New Block', '#' );

		return view( 'admin.blocks.create' );
	}

	/**
	 * Create new content block
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store() {
		$block              = new Block();
		$block->name        = Input::get( 'name' );
		$block->published   = true;
		$block->description = Input::get( 'description' );

		if ( ! Input::get( 'slug' ) ) {
			$block->slug = str_slug( $block->name );
		} else {
			$block->slug = str_slug( Input::get( 'slug' ) );
		}

		$block->save();

		return redirect( 'admin/blocks' )->with( 'msg', [
			'state' => 'success',
			'text'  => $block->name . ' content block created'
		] );
	}

	/**
	 * Save changes to content block
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update( $id ) {
		$block = Block::find( $id );

		$block->name        = Input::get( 'name' );
		$block->description = Input::get( 'description' );

		if ( Input::get( 'slug' ) ) {
			$block->slug = str_slug( Input::get( 'slug' ) );
		} else {
			$block->slug = str_slug( $block->name );
		}

		$block->save();


		return redirect( 'admin/blocks/' . $block->id . '/edit' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Details updated'
		] );
	}

	/**
	 * View content panels for a block
	 *
	 * @param $block
	 *
	 * @return $this
	 */
	public function view( $block ) {
		$block = Block::find( $block );

		$panels = BlockPanel::where( 'block', $block->id )->orderBy( 'order' )->get();

		Breadcrumbs::addCrumb( $block->name, '#' );

		return view( 'admin/blocks.panels.list' )
			->with( 'block', $block )
			->with( 'panels', $panels );
	}

	/**
	 * Create new content panel
	 *
	 * @param $block
	 *
	 * @return $this
	 */
	public function createPanel( $block ) {
		Breadcrumbs::addCrumb( 'Create New Panel', '#' );

		return view( 'admin.blocks.panels.create' )->with( 'block', Block::find( $block ) );
	}

	/**
	 * Save  new content panel
	 *
	 * @param $block
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function storePanel( $block ) {
		$panel = new BlockPanel();

		$panel->title   = Input::get( 'title' );
		$panel->block   = $block;
		$panel->content = Input::get( 'content' );

		$panel->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );


		$panel->save();


		return redirect( 'admin/blocks/' . $block );
	}


	/**
	 * Edit Content panel
	 *
	 * @param $block
	 * @param $id
	 *
	 * @return $this
	 */
	public function editPanel( $block, $id ) {
		$block = Block::find( $block );
		$panel = BlockPanel::find( $id );

		return view( 'admin.blocks.panels.edit' )->with( 'panel', $panel )->with( 'block', $block );
	}


	/**
	 * Save changes to the content block panel
	 *
	 * @param $block
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function updatePanel( $block, $id ) {
		$panel = BlockPanel::find( $id );

		$panel->update( Input::all() );
		$panel->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );

		$panel->save();

		return redirect( 'admin/blocks/' . $block );
	}

	/**
	 * Delete a content panel from the block
	 *
	 * @param $block
	 * @param $panel
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function deletePanel( $block, $panel ) {
		$panel = BlockPanel::find( $panel )->delete();

		return redirect( 'admin/blocks/' . $block );
	}

	/**
	 * save the order of the panels in a content block
	 *
	 * @param $block
	 */
	public function updateOrder( $block ) {
		$nav = json_decode( Input::get( 'nav' ) );

		foreach ( $nav as $position => $li ) {
			$img = BlockPanel::find( $li->id );

			$img->order = $position;

			$img->save();
		}

		echo 'OK';

	}


	public function uploadFile( Request $request ) {
		//check current year folder exists
		if ( ! is_dir( public_path( 'usr/docs/' . date( 'Y' ) ) ) ) {
			mkdir( public_path( 'usr/docs/' . date( 'Y' ) ) );
//			chmod( public_path( 'usr/docs/' . date( 'Y' ) ), 755 );
		}

		//check that the site specific directory exists
		if ( ! is_dir( public_path( 'usr/docs/' . date( 'Y' ) . '/' . siteConfig('id') ) ) ) {

			mkdir( public_path( 'usr/docs/' . date( 'Y' ) . '/' . siteConfig('id') ) );
//			chmod( public_path( 'usr/docs/' . date( 'Y' ) . '/' . siteConfig('id') ), 755 );
		}


		// Process file
		$path = $request->file( 'file' )->path();

		rename( $path, public_path( 'usr/docs/' . date( 'Y' ) . '/' . siteConfig('id')  . '/' . $_FILES['file']['name'] ) );

		//fix file permissions on server
		chmod( public_path( 'usr/docs/' . date( 'Y' ) . '/' . siteConfig('id') . '/' . $_FILES['file']['name'] ), 644 );

		$fileInfo = pathinfo( public_path( 'usr/docs/' . date( 'Y' ) . '/' . siteConfig('id') . '/' . $_FILES['file']['name'] ) );


		switch ( strtolower( $fileInfo['extension'] ) ) {
			case 'pdf':
				$typeIcon = 'icon-file-pdf';
				break;

			case 'doc':
			case 'docx':
				$typeIcon = 'icon-file-word';
				break;

			case 'xls':
			case 'xlsx':
				$typeIcon = 'icon-file-excel';
				break;

			case 'ppt':
			case 'pptx':
				$typeIcon = 'icon-file-presentation';
				break;

			default:
				$typeIcon = 'icon-file-text2';
		}


		return [
			'url'  => '/usr/docs/' . date( 'Y' ).  '/' . siteConfig('id') . '/' . $_FILES['file']['name'],
			'name' => $fileInfo['filename'],
			'type' => $typeIcon
		];
	}


	public function dupe() {
		$block = Block::find( Input::get( 'blockId' ) );

		//Create new block
		$newBlock              = new Block();
		$newBlock->name        = Input::get( 'newName' );
		$newBlock->slug        = str_slug( $newBlock->name );
		$newBlock->description = $block->description;
		$newBlock->published   = $block->published;

		$newBlock->save();

		$blockPanels = BlockPanel::where( 'block', $block->id )->get();

		foreach ( $blockPanels as $bp ) {
			$nbp          = new BlockPanel();
			$nbp->block   = $newBlock->id;
			$nbp->title   = $bp->title;
			$nbp->content = $bp->content;
			$nbp->order   = $bp->order;

			$nbp->save();
		}

		return redirect( 'admin/blocks' )->with( 'msg', [] );
	}

}
