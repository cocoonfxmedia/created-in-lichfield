<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Currencey;

class CurrencyController extends AdminController
{
	public function index()
	{
		$this->addNavElement( 'currency' );

		Breadcrumbs::addCrumb( 'Currency Exchange Rates', '#' );

		$dateLbound = Carbon::now( 'Europe/London' )->subDays( 91 )->format( 'Y-m-d' );

		$currencyLog = Currencey::where( 'date', '>', $dateLbound )->orderBy( 'date' )->get();

		$dataset     = [];
		$currentDate = '';
		$line        = '';


		$dataset = "['Date', 'USD', 'EUR', 'CNY'";

		foreach ( $currencyLog as $cur )
		{
			if ( $currentDate != $cur->date )
			{
				$line        = rtrim( $line, ', ' );
				$line        .= '],' . PHP_EOL;
				$dataset     .= $line;
				$currentDate = $cur->date;
				$line        = "['" . $cur->date->format( 'd/m/Y' ) . "', ";
			}

			$line .= $cur->value . ", ";
		}

		$dataset = rtrim( $dataset, ',' . PHP_EOL );


		return view( 'admin.currency.index' )
			->with( 'dataset', $dataset );
	}
}
