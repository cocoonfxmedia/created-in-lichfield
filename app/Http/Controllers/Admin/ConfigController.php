<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\loginUser;
use App\Models\Course;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Infinety\Config\Rewrite;

class ConfigController extends AdminController
{
	private $settingsFile = 'settings';

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Settings', '#' );

		$this->addNavElement( 'config' );


	}

	/**
	 * List of courses
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$this->getSettingsFile();


		$settings = config( $this->settingsFile );

		return view( 'admin.tools.config.index' )->with( 'settings', $settings );
	}

	/**
	 * Commit new course to database
	 *
	 * @return mixed
	 */
	public function store()
	{
		$this->getSettingsFile();

		$settings = config( $this->settingsFile );

		foreach ( $settings as $key => $value )
		{
			if ( is_bool( $value ) )
			{
				$v = false;
				if ( Input::has( $key ) )
				{
					$v = true;
				}

			} else
			{
				$v = rtrim( Input::get( $key ) );
			}

			$settings[ $key ] = $v;
		}


		$writeConfig = new Rewrite();
		$writeConfig->toFile( config_path( $this->settingsFile . '.php' ), $settings );

		$exitCode = Artisan::call( 'cache:clear' );


		return view( 'admin.tools.config.index' )->with( 'settings', $settings )->with( 'msg', [
			'state' => 'success',
			'title' => 'General settings updated'
		] );
	}

	private function getSettingsFile()
	{
		//set up the settings file

		if ( siteConfig( 'slug' ) != 'default' )
		{
			$this->settingsFile = 'settings_' . siteConfig( 'slug' );
		}
	}

}
