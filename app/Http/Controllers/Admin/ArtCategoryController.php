<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreArtCategory;
use App\Models\ArtCategory;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;

final class ArtCategoryController extends AdminController
{
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb('Categories', '/admin/categories');

		$this->addNavElement('categories');
	}

	public function index()
	{
		return view('admin.categories.index', [
			'categories' => ArtCategory::withTrashed()->get(),
		]);
	}

	public function destroy(ArtCategory $category)
	{
		$category->delete();

		return redirect('/admin/categories')->with('msg', [
			'state' => 'success',
			'text' => 'Category deleted',
		]);
	}

	public function edit(ArtCategory $category)
	{
		return view('admin.categories.edit', [
			'category' => $category,
		]);
	}

	public function update(ArtCategory $category, StoreArtCategory $request)
	{
		$category->update($request->all());

		return redirect("/admin/categories/edit/{$category->id}")->with('msg', [
			'state' => 'success',
			'text' => 'Category updated',
		]);
	}

	public function create()
	{
		return view('admin.categories.create');
	}

	public function store(StoreArtCategory $request)
	{
		ArtCategory::create($request->all());

		return redirect('admin/categories')->with('msg', [
			'state' => 'success',
			'text' => 'New category saved',
		]);
	}
}
