<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreLocation;
use App\Models\ArtCategory;
use App\Models\Artist;
use App\Models\Gallery;
use App\Models\Location;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;

final class LocationController extends AdminController
{
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb('Locations', '/admin/locations');

		$this->addNavElement('locations');
	}

	public function index()
	{
		return view('admin.locations.index', [
			'locations' => Location::withTrashed()->get(),
		]);
	}

	public function destroy(Location $location)
	{
		$location->delete();

		return redirect('/admin/locations')->with('msg', [
			'state' => 'success',
			'text' => 'Location deleted',
		]);
	}

	public function edit(Location $location)
	{
		return view('admin.locations.edit', [
			'location' => $location,
			'location_artists' => $location->artists()->select('id', 'name')->without('categories')->get()->toJson(),
			'categories' => ArtCategory::all(),
			'artists' => Artist::select('id', 'name')->without('categories')->get(),
			'galleries' => Gallery::pluck('name', 'id'),
		]);
	}

	public function update(Location $location, StoreLocation $request)
	{
		$location->fill($request->all());

		if ($request->hasFile('header_image')) {
			$location->header_image = $request->file('header_image')->storePublicly('public/locations');
			$location->header_image = str_replace( 'public/', '/storage/', $location->header_image );
		} elseif (true == $request->remove_header_image) {
			$location->header_image = null;
		}

		$location->save();
		$location->categories()->sync($request->categories);
		$location->artists()->sync($request->artists);

		return redirect("/admin/locations/edit/{$location->id}")->with('msg', [
			'state' => 'success',
			'text' => 'Location updated',
		]);
	}

	public function create()
	{
		return view('admin.locations.create', [
			'categories' => ArtCategory::all(),
			'location_artists' => '[]',
			'artists' => Artist::all()->map(function ($i) {return ['id' => $i->id, 'title' => $i->name];}),
			'galleries' => Gallery::pluck('name', 'id'),
		]);
	}

	public function store(StoreLocation $request)
	{
		$location = new Location($request->all());

		if ($request->hasFile('header_image')) {
			$location->header_image = $request->file('header_image')->storePublicly('public/locations');
			$location->header_image = str_replace( 'public/', '/storage/', $location->header_image );
		} 
		elseif (true == $request->remove_header_image) {
			$location->header_image = null;
		}

		$location->save();
		$location->categories()->sync($request->categories);
		$location->artists()->sync($request->artists);

		return redirect('admin/locations')->with('msg', [
			'state' => 'success',
			'text' => 'New location saved',
		]);
	}
}
