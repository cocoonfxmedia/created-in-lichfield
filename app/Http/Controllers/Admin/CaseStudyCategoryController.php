<?php

namespace App\Http\Controllers\Admin;

use App\Models\CaseStudy;
use App\Models\CaseStudyCategory;
use Illuminate\Http\Request;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use App\Models\BlogCategory;
use Illuminate\Support\Facades\Input;

class CaseStudyCategoryController extends AdminController
{

	public function __construct()
	{
		Parent::__construct();

		Breadcrumbs::addCrumb( 'Case Studies', url( 'admin/case-studies' ) );
		Breadcrumbs::addCrumb( 'Categories', '/admin/case-study-categories' );

		$this->addNavElement( 'case-studies' );
		$this->addNavElement( 'case-study-categories' );
	}

	/**
	 * Admin UI for  category list
	 *
	 * @return $this
	 */
	public function adminCategoryIndex()
	{
		return view( 'admin.case_studies.category.list' )->with( 'categories', CaseStudyCategory::all() );
	}

	/**
	 * Create new  category
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function categoryCreate()
	{
		Breadcrumbs::addCrumb( 'New Category', '/admin/case-study-categories/create' );

		return view( 'admin.case_studies.category.create' );
	}

	/**
	 * Edit existing  category
	 *
	 * @param $id
	 *
	 * @return $this
	 */
	public function categoryEdit( $id )
	{
		$category = CaseStudyCategory::find( $id );

		Breadcrumbs::addCrumb( 'Edit: ' . $category->title, '/admin/case-study-categories/create' );

		return view( 'admin.case_studies.category.edit' )->with( 'cat', $category );
	}

	/**
	 * Delete  category
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function categoryDestroy( $id )
	{
		$category = CaseStudyCategory::find( $id );
		$category->delete();

		return redirect( 'admin/case-study-categories' )->with('msg', ['state' => 'success', 'text' => 'Case study category deleted']);
	}

	/**
	 * Save changes to category
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function categoryUpdate( $id )
	{
		$category = CaseStudyCategory::find( $id );

		$category->update( Input::all() );

		$category->active = ( ( Input::get( 'active' ) == '1' ) ? 1 : 0 );

		$category->save();

		return redirect( 'admin/case-study-categories' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Category updated successfully'
		] );
	}

	/**
	 * Save new blog category
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function categoryStore()
	{
		$category = new CaseStudyCategory();

		$category->title = Input::get( 'title' );

		if ( Input::get( 'slug' ) )
		{
			$category->slug = str_slug( Input::get( 'slug' ) );
		} else
		{
			$category->slug = str_slug( $category->title );
		}

		$category->active = ( ( Input::get( 'active' ) == 'on' ) ? 1 : 0 );


		$category->save();


		return redirect( 'admin/case-study-categories' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New blog category added'
		] );
	}
}
