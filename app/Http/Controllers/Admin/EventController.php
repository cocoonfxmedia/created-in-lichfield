<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreEvent;
use App\Models\ArtCategory;
use App\Models\Artist;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\Location;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Carbon;

final class EventController extends AdminController
{
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb('Events', '/admin/events');

		$this->addNavElement('events');
	}

	public function index()
	{
		return view('admin.events.index', [
			'events' => Event::withTrashed()->get(),
		]);
	}

	public function destroy(Event $event)
	{
		$event->delete();

		return redirect('/admin/events')->with('msg', [
			'state' => 'success',
			'text' => 'Event deleted',
		]);
	}

	public function edit(Event $event)
	{
		return view('admin.events.edit', [
			'event' => $event,
			'event_artists' => $event->artists()->select('id', 'name')->without('categories')->get()->toJson(),
			'categories' => ArtCategory::all(),
			'locations' => Location::pluck('title', 'id'),
			'artists' => Artist::select('id', 'name')->without('categories')->get(),
			'galleries' => Gallery::pluck('name', 'id'),
		]);
	}

	public function update(Event $event, StoreEvent $request)
	{
		$event->fill($request->all());

		$event->at = new Carbon($request->date_submit.' '.$request->time);
		$event->setAttribute('status', $request->status);

		if ($request->hasFile('header_image')) {
			$event->header_image = $request->file('header_image')->storePublicly('public/events');
			$event->header_image = str_replace( 'public/', '/storage/', $event->header_image );
		}

		$event->save();
		$event->categories()->sync($request->categories);
		$event->artists()->sync($request->artists);

		return redirect("/admin/events/edit/{$event->id}")->with('msg', [
			'state' => 'success',
			'text' => 'Event updated',
		]);
	}

	public function create()
	{
		return view('admin.events.create', [
			'event_artists' => '[]',
			'categories' => ArtCategory::all(),
			'locations' => Location::pluck('title', 'id'),
			'artists' => Artist::select('id', 'name')->without('categories')->get(),
			'galleries' => Gallery::pluck('name', 'id'),
		]);
	}

	public function store(StoreEvent $request)
	{
		$event = new Event($request->all());
		$event->at = new Carbon($request->date_submit . ' ' . $request->time);
		
		if ($request->has('status')) {
			$event->setAttribute('status', $request->status);		
		}

		if ($request->hasFile('header_image')) {
			$event->header_image = $request->file('header_image')->storePublicly('public/events');
			$event->header_image = str_replace( 'public/', '/storage/', $event->header_image );
		}

		$event->save();
		$event->categories()->sync($request->categories);
		$event->artists()->sync($request->artists);

		return redirect('admin/events')->with('msg', [
			'state' => 'success',
			'text' => 'New event saved',
		]);
	}
}
