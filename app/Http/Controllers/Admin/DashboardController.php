<?php

namespace App\Http\Controllers\Admin;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;

class DashboardController extends AdminController
{
    public function index()
    {
    	$this->addNavElement('dashboard');

    	return view('admin.dashboard.page');
    }
}
