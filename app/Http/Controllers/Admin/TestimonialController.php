<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 09:59
 */

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\Testimonial;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;

class TestimonialController extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Testimonials', '/admin/testimonials' );

		$this->addNavElement( 'testimonials' );
	}

	/**
	 * List view
	 * @return Page collection
	 */
	public function adminIndex()
	{
		return view( 'admin.testimonials.list', [
			'articles' => Testimonial::where( 'sid', siteConfig( 'sid' ) )->get()
		] );
	}


	/**
	 * New blog article form
	 *
	 * @return mixed
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New testimonial', '/admin/testimonials/create' );

		return view( 'admin.testimonials.create' );
	}


	/**
	 * Create new page
	 * @return mixed
	 */
	public function store()
	{
		$page      = new CaseStudy( Input::all() );
		$page->sid = siteConfig( 'sid' );

		$page->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );

		if ( empty( $data['url'] ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_replace( " ", '-', strtolower( $data['url'] ) );
		}

		$page->save();

		return redirect( 'admin/testimonials' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New testimonial saved'
		] );
	}

	/**
	 * Delete page
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function destroy( $page_id )
	{
		$page = Testimonial::find( $page_id );

		$page->delete();

		return redirect( '/admin/testimonials' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Testimonial removed'
		] );
	}

	/**
	 * Blog Article edit view
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function edit( $page_id )
	{
		Breadcrumbs::addCrumb( 'Edit Testimonial', '/admin/testimonials/edit/' . $page_id );

		return view( 'admin.testimonials.edit', [
			'article' => Testimonial::find( $page_id )
		] );
	}

	/**
	 * Update page object
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function update( $page_id )
	{
		$page = Testimonial::find( $page_id );

		$page->update( Input::except( [ '_token' ] ) );

		$page->published = ( ( Input::get( 'published' ) == '1' ) ? 1 : 0 );

		//ensure URL is not empty
		if ( empty( $page->slug ) )
		{
			$page->slug = str_slug( $page->title, '-' );
		} else
		{
			$page->slug = str_slug( $page->url );
		}

		$page->save();


		return redirect( '/admin/testimonials/edit/' . $page_id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Testimonial updated'
		] );
	}


	/**
	 * Trash bin list
	 *
	 * @return mixed
	 */
	public function trash()
	{
		Breadcrumbs::addCrumb( 'Recycle Bin', '/admin/testimonials/trash' );

		return view( 'admin.testimonials.trash', [ 'articles' => Testimonial::onlyTrashed()->get() ] );
	}

	/**
	 * Restore a page
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function restore( $id )
	{
		Testimonial::withTrashed()->where( 'id', $id )->restore();

		return redirect( '/admin/testimonials' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Testimonial removed from the recycle bin'
		] );
	}

	/**
	 * Empty the recycle bin
	 *
	 * @return mixed
	 */
	public function emptyTrash()
	{
		Testimonial::onlyTrashed()->forceDelete();

		return redirect( '/admin/testimonials' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Recycle bin has been emptied'
		] );
	}


}