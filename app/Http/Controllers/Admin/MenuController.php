<?php namespace App\Http\Controllers\Admin;

use App\Models\ProductCategory;
use App\Models\MenuItem;
use App\Models\Page;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class MenuController extends AdminController
{

	/**
	 * MenuController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Menus', '/admin/menus' );

		$this->addNavElement( 'menus' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//build page list for drop down in add form
		$menu = 'default';


		//menu list
		$menuList = Config::get( 'cms.menus' );

		//set the selected menu
		$selectedMenu = Input::cookie( 'laravel_menu_selected' );
		if ( ! $selectedMenu )
		{
			$selectedMenu = 'default';
		}

		//get nav items
		$menu = MenuItem::with( 'children' )
		                ->where( 'menu', '=', $selectedMenu )
		                ->where( 'sid', siteConfig('sid') )
		                ->root()
		                ->orderBy( 'order', 'ASC' )
		                ->get();


		$systemPages = [
			'home'    => 'Home',
			'contact' => 'Contact Us',
			'sitemap' => 'Sitemap'
		];

		if ( config( 'cms.components.blog' ) )
		{
			$systemPages['blog'] = 'Blog/News';
		}

		if ( config( 'cms.components.ecommerce' ) )
		{

			$systemPages = array_merge( $systemPages, [
				'account'   => 'My Account',
				'addresses' => 'My Addresses',
				'orders'    => 'My Orders',
				'identity'  => 'My Personal Info',
				'checkout'  => 'Checkout'
			] );
		}


		//TODO:: remove eccomerce pages from menu editor when disabled


		return view( 'admin.menu.edit', [
			'syspages'           => $systemPages,
			'pages'              => $this->generatePageList(),
			'menu'               => $menu,
			'menuList'           => $menuList,
			'selectedMenu'       => $selectedMenu,
			'product_categories' => $this->generateProductCategoryList()
		] )->withCookie( 'laravel_menu_selected', $selectedMenu );
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		if ( $data['type'] == 'sys_page' )
		{
			$data['type'] = 'link';

			switch ( $data['destination'] )
			{
				case 'home':
					$data['destination'] = '/';
					$data['label']       = 'Home';
					break;

				case 'contact':
					$data['destination'] = '/contact-us';
					$data['label']       = 'Contact Us';
					break;

				case 'checkout':
					$data['destination'] = '/checkout';
					$data['label']       = 'Checkout';
					break;

				case 'sitemap':
					$data['destination'] = '/sitemap';
					$data['label']       = 'Sitemap';
					break;

				case 'account':
					$data['destination'] = '/my-account';
					$data['label']       = 'My Account';
					break;

				case 'addresses':
					$data['destination'] = '/my-account/addresses';
					$data['label']       = 'My Addresses';
					break;

				case 'orders':
					$data['destination'] = '/my-account/orders';
					$data['label']       = 'My Orders';
					break;

				case 'identity':
					$data['destination'] = '/my-account/identity';
					$data['label']       = 'My Personal Info';
					break;

				case 'blog':
					$data['destination'] = '/' . config( 'cms.blog.url' );
					$data['label']       = 'Blog';
					break;
			}

			if ( Input::get( 'label' ) )
			{
				$data['label'] = Input::get( 'label' );
			}
		}


		//set the selected menu
		$selectedMenu = Input::cookie( 'laravel_menu_selected' );
		if ( ! $selectedMenu )
		{
			$selectedMenu = 'default';
		}


		$navItem       = new MenuItem( $data );
		$navItem->menu = $selectedMenu;
		$navItem->sid  = siteConfig('sid');
		$navItem->save();

		$this->scrubViews();


		return redirect( '/admin/menus' )->with( 'msg', [ 'state' => 'success', 'text' => 'Menu item saved' ] );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update()
	{
		$item = MenuItem::find( Input::get( 'id' ) );

		$item->destination = Input::get( 'destination' );

		$item->label = '';
		if ( Input::get( 'label' ) )
		{
			$item->label = Input::get( 'label' );
		}

		$item->save();

		$this->scrubViews();

		echo 'OK';
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id )
	{
		$li = MenuItem::find( $id );

		$li->delete();

		$this->scrubViews();
	}

	/**
	 * Generate CMS page list of admin area select element
	 *
	 * @return array
	 */
	private function generatePageList()
	{
		$pages = Page::where( 'sid', siteConfig('sid') )->get();


		$pageList = [];

		foreach ( $pages as $p )
		{
			$pageList[ $p->id ] = $p->title;
		}

		return $pageList;
	}

	/**
	 * Generate list of products categories for select element in admin UI
	 *
	 * @return array
	 */
	private function generateProductCategoryList()
	{
		$categories = [];

		$cats = ProductCategory::all();

		foreach ( $cats as $cat )
		{
			$categories[ $cat->id ] = $cat->name;
		}

		return $categories;
	}

	/**
	 * Process order AJAx submission
	 */
	public function reorder()
	{
		$nav = json_decode( Input::get( 'nav' ) );


		foreach ( $nav as $position => $li )
		{
			$menuItem = MenuItem::find( $li->id );

			$menuItem->parent = $li->parent;
			$menuItem->order  = $position;

			$menuItem->save();
		}

		$this->scrubViews();

		echo 'OK';
	}

	/**
	 * Change the selected menu in the menu editor
	 *
	 * @return mixed
	 */
	public function menuSwitcher()
	{
		return redirect( '/admin/menus' )->withCookie( cookie()->forever( 'laravel_menu_selected', Input::get( 'menu' ) ) );
	}

	/**
	 * Clean up the compiled views directory
	 */
	private function scrubViews()
	{
		$success = File::cleanDirectory( storage_path() . '/framework/views/' );
	}


	/**
	 * Bulk create product categories
	 */
	public function massImportCategories()
	{
		$categories = ProductCategory::active()->root()->orderBy( 'name', 'ASC' )->with( 'children' )->get();

		//the menu to assign the categories to
		$menu = 'default';

		//enable processing of 2nd level
		//this will only import 2 levels of categories
		$level2 = true;

		foreach ( $categories as $i => $cat )
		{
			$menuItem              = new MenuItem();
			$menuItem->active      = 1;
			$menuItem->type        = 'product_category';
			$menuItem->destination = $cat->id;
			$menuItem->menu        = $menu;
			$menuItem->order       = $i;
			$menuItem->parent      = 0;

			$menuItem->save();

			if ( $level2 )
			{
				foreach ( $cat->children as $ci => $childCat )
				{
					$menuChildItem              = new MenuItem();
					$menuChildItem->active      = 1;
					$menuChildItem->type        = 'product_category';
					$menuChildItem->destination = $childCat->id;
					$menuChildItem->menu        = $menu;
					$menuChildItem->order       = strval( $i ) . strval( $ci );
					$menuChildItem->parent      = $menuItem->id;

					$menuChildItem->save();
				}
			}
		}

	}


}
