<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 09:59
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Request;
use App\Models\BlogArticle;
use App\Models\BlogCategory;
use App\Models\CaseStudy;
use App\Models\CaseStudyCategory;
use App\Models\Page;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class CaseStudyController extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Case Studies', '/admin/case-studies' );

		$this->addNavElement( 'case-studies' );
	}

	/**
	 * List view
	 * @return Page collection
	 */
	public function adminIndex()
	{
		return view( 'admin.case_studies.list', [
			'articles' => CaseStudy::where( 'sid', siteConfig( 'sid' ) )->get()
		] );
	}


	/**
	 * New blog article form
	 *
	 * @return mixed
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New case study', '/admin/case-studies/create' );

		return view( 'admin.case_studies.create', [ 'categories' =>  CaseStudyCategory::pluck( 'title', 'id' ) ] );
	}


	/**
	 * Create new page
	 * @return mixed
	 */
	public function store()
	{
		$page = new CaseStudy();

		$data = Input::all();

		$page->title            = Input::get( 'title' );
		$page->meta_title       = Input::get( 'meta_title' );
		$page->meta_description = Input::get( 'meta_description' );
		$page->content          = Input::get( 'content' );
		$page->sid              = siteConfig( 'sid' );
		$page->category_id      = Input::get( 'category_id' );

		$page->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );

		if ( empty( $data['url'] ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_slug( $data['url'], '-' );
		}

		$page->save();


		/*
		 * Process thumbnail files
		 */
		if ( Input::file( 'thumbnail' ) )
		{
			$filename = $page->id . '_' . Input::file( 'thumbnail' )->getClientOriginalName();

			if ( ! is_dir( public_path( '/usr/case_study_thumbs' ) ) )
			{
				mkdir( public_path( '/usr/case_study_thumbs' ) );
			}

			Input::file( 'thumbnail' )->move( base_path() . '/public/usr/case_study_thumbs/', $filename );

			$page->featured_image = $filename;
			$page->save();
		}

		return redirect( 'admin/case-studies' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New case study saved'
		] );
	}

	/**
	 * Delete page
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function destroy( $page_id )
	{
		$page = CaseStudy::find( $page_id );

		$page->delete();

		return redirect( '/admin/case-studies' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Case study removed'
		] );
	}

	/**
	 * Blog Article edit view
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function edit( $page_id )
	{
		Breadcrumbs::addCrumb( 'Edit Article', '/admin/case-studies/edit/' . $page_id );

		return view( 'admin.case_studies.edit', [
			'article'    => CaseStudy::find( $page_id ),
			'categories' => CaseStudyCategory::where('sid', siteConfig('sid'))->pluck( 'title', 'id' )
		] );
	}

	/**
	 * Update page object
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function update( $page_id )
	{
		$page = CaseStudy::find( $page_id );

		$page->update( Input::all() );

		$page->published = ( ( Input::get( 'published' ) == '1' ) ? 1 : 0 );

		//ensure URL is not empty
		if ( empty( $page->url ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_slug( $page->url, '-' );
		}

		$page->save();


		/*
		 * Process thumbnail files
		 */
		if ( Input::file( 'thumbnail' ) )
		{
			$filename = $page->id . '_' . Input::file( 'thumbnail' )->getClientOriginalName();

			if ( ! is_dir( public_path( '/usr/case_study_thumbs' ) ) )
			{
				mkdir( public_path( '/usr/case_study_thumbs' ) );
			}

			Input::file( 'thumbnail' )->move( base_path() . '/public/usr/case_study_thumbs/', $filename );

			$page->featured_image = $filename;
			$page->save();
		}


		return redirect( '/admin/case-studies/edit/' . $page_id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Case study updated'
		] );
	}


	/**
	 * Trash bin list
	 *
	 * @return mixed
	 */
	public function trash()
	{
		Breadcrumbs::addCrumb( 'Recycle Bin', '/admin/case-studies/trash' );

		return view( 'admin.case_studies.trash', [ 'articles' => CaseStudy::onlyTrashed()->get() ] );
	}

	/**
	 * Restore a page
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function restore( $id )
	{
		CaseStudy::withTrashed()->where( 'id', $id )->restore();

		return redirect( '/admin/case-studies' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Case study removed from the recycle bin'
		] );
	}

	/**
	 * Empty the recycle bin
	 *
	 * @return mixed
	 */
	public function emptyTrash()
	{
		CaseStudy::onlyTrashed()->forceDelete();

		return redirect( '/admin/case-studies' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Recycle bin has been emptied'
		] );
	}


}