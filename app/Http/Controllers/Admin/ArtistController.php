<?php

declare( strict_types=1 );

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreArtist;
use App\Models\ArtCategory;
use App\Models\Artist;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

final class ArtistController extends AdminController
{
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Artists', '/admin/artists' );

		$this->addNavElement( 'artists' );
	}

	public function index()
	{
		return view( 'admin.artists.index', [
			'artists' => Artist::all(),
		] );
	}

	public function delete( Artist $artist )
	{
		$artist->delete();

		return redirect( '/admin/artists' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Artist deleted',
		] );
	}

	public function edit( Artist $artist )
	{
		return view( 'admin.artists.edit', [
			'artist' => $artist,
			'artist_categories' => $artist->categories,
			'categories' => ArtCategory::pluck( 'title', 'id' ),
		] );
	}

	public function update( Artist $artist, StoreArtist $request )
	{
		$artist->fill($request->except('password'));
		$artist->setAttribute('status', $request->status);

		if ( $request->filled( 'password' ) )
		{
			$artist->password = bcrypt( $request->get( 'password' ) );
		}

		if ( $request->hasFile( 'profile_image' ) )
		{
			$artist->profile_image = $request->file( 'profile_image' )->storePublicly( 'public/artists' );
			$artist->profile_image = str_replace( 'public/', '/storage/', $artist->profile_image );
		} 
		elseif (true == $request->remove_profile_image) 
		{
			$artist->profile_image = null;
		} 

		if ( $request->hasFile( 'header_image' ) )
		{
			$artist->header_image = $request->file( 'header_image' )->storePublicly( 'public/artists' );
			$artist->header_image = str_replace( 'public/', '/storage/', $artist->header_image );
		} 
		elseif (true == $request->remove_header_image) 
		{
			$artist->header_image = null;
		} 

		$artist->save();
		$artist->categories()->sync($request->categories);

		return redirect( "/admin/artists/edit/{$artist->id}" )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Artist updated',
		] );
	}

	public function create()
	{
		return view( 'admin.artists.create', [
			'categories' => ArtCategory::pluck( 'title', 'id' ),
			'artist_categories' => collect([]),
		] );
	}

	public function store( StoreArtist $request )
	{
		$artist           = new Artist( $request->except( 'password' ) );
		$artist->password = bcrypt( $request->get( 'password' ) );

		if ($request->filled('status')) {
			$artist->setAttribute('status', $request->status);
		}

		if ( $request->hasFile( 'profile_image' ) )
		{
			$artist->profile_image = $request->file( 'profile_image' )->storePublicly( 'public/artists' );
			$artist->profile_image = str_replace( 'public/', '/storage/', $artist->profile_image );
		}

		if ( $request->hasFile( 'header_image' ) )
		{
			$artist->header_image = $request->file( 'header_image' )->storePublicly( 'public/artists' );
			$artist->header_image = str_replace( 'public/', '/storage/', $artist->header_image );
		}

		$artist->save();

		return redirect( 'admin/artists' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New artist saved',
		] );
	}

	public function trash()
	{
		Breadcrumbs::addCrumb('Recycle Bin', '/admin/artists/trash');

		return view('admin.artists.trash', [
			'artists' => Artist::onlyTrashed()->get(),
		]);
	}

	public function restore($artist_id)
	{
		Artist::withTrashed()->findOrFail($artist_id)->restore();
		
		return redirect("/admin/artists/edit/{$artist_id}")->with('msg', [
			'state' => 'success',
			'text' => 'Artist restored',
		]);
	}

	public function destroy($artist_id)
	{
		Artist::withTrashed()->findOrFail($artist_id)->forceDelete();

		return redirect("/admin/artists/trash")->with('msg', [
			'state' => 'success',
			'text' => 'Artist permanently deleted',
		]);
	}

	public function emptyTrash()
	{
		Artist::onlyTrashed()->forceDelete();

		return redirect("/admin/artists/trash")->with('msg', [
			'state' => 'success',
			'text' => 'Artists permanently deleted',
		]);
	}

	public function toggleEventPermissions( Request $request )
	{
		Artist::whereIn( 'id', $request->all() )
		      ->get()
		      ->each( function ( Artist $artist ) {
			      $artist->update( [ 'can_manage_events' => ! $artist->can_manage_events ] );
		      } );
	}

	public function toggleLocationPermissions( Request $request )
	{
		Artist::whereIn( 'id', $request->all() )
		      ->get()
		      ->each( function ( Artist $artist ) {
			      $artist->update( [ 'can_manage_locations' => ! $artist->can_manage_locations ] );
		      } );
	}
}
