<?php

namespace App\Http\Controllers\Admin;

use App\Models\TeamMember;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TeamMemberController extends AdminController
{

	/**
	 * TeamMemberController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Team Members', '/admin/team-members' );

		$this->addNavElement( 'team-members' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		$members = TeamMember::orderBy( 'order' )->get();


		return view( 'admin.team.list', [ 'members' => $members ] );

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view( 'admin.team.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		$member = new TeamMember( Input::all() );

		$member->published = ( ( Input::get( 'published' ) == '1' ) ? 1 : 0 );


		//handle file upload
		if ( $request->hasFile( 'image' ) )
		{
			if ( ! is_dir( public_path( 'usr/team_members' ) ) )
			{
				mkdir( public_path( 'usr/team_members' ) );
			}


			$request->file( 'image' )
			        ->move( public_path() . '/usr/team_members', $member->id . '.' . $request->file( 'image' )
			                                                                                 ->getClientOriginalExtension() );

			$member->image = $member->id . '.' . $request->file( 'image' )->getClientOriginalExtension();
		}


		//handle file upload
		if ( $request->hasFile( 'image_over' ) )
		{
			if ( ! is_dir( public_path( 'usr/team_members' ) ) )
			{
				mkdir( public_path( 'usr/team_members' ) );
			}


			$request->file( 'image_over' )
			        ->move( public_path() . '/usr/team_members', $member->id . '-over.' . $request->file( 'image_over' )->getClientOriginalExtension() );

			$member->image_over = $member->id . '-over.' . $request->file( 'image_over' )->getClientOriginalExtension();
		}

		$member->slug = str_slug( $member->name );

		$member->save();


		return redirect( '/admin/team-members' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New team member added'
		] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id )
	{

		Breadcrumbs::addCrumb( 'Edit Member\'s bio', '/admin/team-members/edit/' . $id );

		return view( 'admin.team.edit' )->with( 'member', TeamMember::find( $id ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id )
	{
		$member = TeamMember::find( $id );
		$member->update( Input::all() );

		$member->published = ( ( Input::get( 'published' ) == '1' ) ? 1 : 0 );

		//handle file upload
		if ( $request->hasFile( 'image' ) )
		{
			if ( ! is_dir( public_path( 'usr/team_members' ) ) )
			{
				mkdir( public_path( 'usr/team_members' ) );
			}


			$request->file( 'image' )
			        ->move( public_path() . '/usr/team_members', $member->id . '.' . $request->file( 'image' )
			                                                                                 ->getClientOriginalExtension() );

			$member->image = $member->id . '.' . $request->file( 'image' )->getClientOriginalExtension();
		}


		//handle file upload
		if ( $request->hasFile( 'image_over' ) )
		{
			if ( ! is_dir( public_path( 'usr/team_members' ) ) )
			{
				mkdir( public_path( 'usr/team_members' ) );
			}


			$request->file( 'image_over' )
			        ->move( public_path() . '/usr/team_members', $member->id . '-over.' . $request->file( 'image_over' )->getClientOriginalExtension() );

			$member->image_over = $member->id . '-over.' . $request->file( 'image_over' )->getClientOriginalExtension();
		}

		$member->slug = str_slug( $member->name );

		$member->save();

		return redirect( '/admin/team-members/' . $member->id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Team member profile updated'
		] );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id )
	{
		$member = TeamMember::find( $id );
		$member->delete();

		return redirect( '/admin/team-members' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Team member deleted'
		] );
	}

	/**
	 * Update the order of team members
	 */
	public function updateOrder()
	{
		$list = json_decode( Input::get( 'nav' ) );

		foreach ( $list as $pos => $id )
		{
			$member        = TeamMember::find( $id );
			$member->order = $pos;
			$member->save();
		}

		echo 'OK';
	}
}
