<?php

namespace App\Http\Controllers\Admin\Tools;

use App\Models\BlogArticle;
use App\Models\CaseStudy;
use App\Models\Course;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\Slide;
use App\Models\Slider;
use App\Models\Testimonial;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Admin\AdminController;

class DupeSiteController extends AdminController
{

	/**
	 * Duplicate site tool
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		Breadcrumbs::addCrumb( 'Tools', '#' );
		Breadcrumbs::AddCrumb( 'Duplicate Site', '#' );

		$sites    = config( 'multisite.sites' );
		$siteList = [];
		foreach ( $sites as $sid => $site )
		{
			$siteList[ $sid ] = siteConfig( 'site_name', $sid );
		}

		return view( 'admin.tools.dupe_site', [ 'siteList' => $siteList ] );
	}

	/**
	 * Duplicate site
	 */
	public function processor()
	{
		$from = Input::get( 'from' );
		$to   = Input::get( 'to' );

		//back the site up before doing anything
		//Docs: https://docs.spatie.be/laravel-backup/v3/introduction
//		Artisan::call('backup:run');

		/*
		 * homepage banners
		 */
		echo '<h2>Processing Sliders</h2>';

		Slider::where( 'sid', $to )->delete();


		$sliders = Slider::where( 'sid', $from )->get();

		$sliderMap = [];

		foreach ( $sliders as $s )
		{
			$newS      = $s->replicate();
			$newS->sid = $to;

			$newS->save();

			$sliderMap[ $s->id ] = $newS->id;
		}

		//remap slides
		$slides = Slide::whereIn( 'sid', array_keys( $sliderMap ) )->get();

		foreach ( $slides as $slide )
		{
			$newSlide      = $slide->replicate();
			$newSlide->sid = $sliderMap[ $slide->sid ];


			//cop the slide image file
			if ( file_exists( public_path( 'usr/dip/' . $slide->image ) ) )
			{
				$newSlide->save(); //generate new ID

				$fileParts = explode( '.', $slide->image );

				//duplicate the file
				copy( public_path( 'usr/dip/' . $slide->image ), public_path( 'usr/dip/' . $newSlide->id . '.' . end( $fileParts ) ) );

				$newSlide->image = $newSlide->id . '.' . end( $fileParts );

				$newSlide->save();
			}

		}


		/*
		 * pages
		 */
		echo '<h2>Processing Pages</h2>';

		//purge the current pages
		Page::where( 'sid', $to )->delete();

		$pageMap = [];

		$pages = Page::where( 'sid', $from )->get();

		foreach ( $pages as $page )
		{
			$newPage      = $page->replicate();
			$newPage->sid = $to;

			if ( $page->slide != 0 )
			{
				//map old slider to new slider
				$newPage->slider = $sliderMap[ $page->slider ];
			}

			$newPage->save();

			$pageMap[ $page->id ] = $newPage->id;
		}

		//reprocess the pages to map the parents
		//doing this separately ensures that the new ID for the parent is known
		$pages = Page::where( 'sid', $to )->whereNotNull( 'parent_id' )->get();

		foreach ( $pages as $page )
		{
			$page->parent_id = $pageMap[ $page->parent_id ];
			$page->save();
		}


		/*
		 * menus
		 */
		echo '<h2>Processing Menus</h2>';

		$menuMap = [];

		//purge current menu structure
		MenuItem::where( 'sid', $to )->delete();

		$menuItems = MenuItem::where( 'sid', $from )->get();

		foreach ( $menuItems as $mi )
		{
			//double check that the page exists
			if ( $mi->type == 'page' && ! isset( $pageMap[ $mi->destination ] ) )
			{
				continue;
			}

			$newMi      = $mi->replicate();
			$newMi->sid = $to;

			$newMi->save();

			$menuMap[ $mi->id ] = $newMi->id;
		}

		//map parent IDs
		// this is done separately to ensure that the parent has been mapped
		$menuItems = MenuItem::where( 'sid', $to )->where( 'parent', '!=', 0 )->get();

		foreach ( $menuItems as $mi )
		{
			$mi->parent = $menuMap[ $mi->parent ];
			$mi->save();
		}

		/*
		 * courses
		 */
		echo '<h2>Processing Courses</h2>';

		Course::where( 'sid', $to )->delete();

		$courses = Course::where( 'sid', $from )->get();

		foreach ( $courses as $c )
		{
			$newC      = $c->replicate();
			$newC->sid = $to;

			$newC->save();
		}


		/*
		 * Testimonials
		 */
		echo '<h2>Processing Testimonials</h2>';
		$testimonials = Testimonial::where( 'sid', $from )->get();

		foreach ( $testimonials as $t )
		{
			$newT      = $t->replicate();
			$newT->sid = $to;

			$newT->save();
		}


		/*
		 * Case studies
		 */
		echo '<h2>Processing Case Studies</h2>';
		$casestudies = CaseStudy::where( 'sid', $from )->get();
		foreach ( $casestudies as $c )
		{
			$newC      = $c->replicate();
			$newC->sid = $to;
			$newC->save();
		}

		/*
		 * Blog
		 */
		echo '<h2>Processing Blog Articles</h2>';
		$articles = BlogArticle::where( 'sid', $from )->get();
		foreach ( $articles as $a )
		{
			$newA      = $a->replicate();
			$newA->sid = $to;
			$newA->save();
		}


		/**
		 * Galleries
		 */
		echo '<h2>Processing Galleries</h2>';
		$galleries = Gallery::where( 'sid', $from )->get();

		foreach ( $galleries as $g )
		{
			$newG      = $g->replicate();
			$newG->sid = $to;
			$newG->save();

			//files
			$files = Storage::files( '/usr/gallery/' . $g->id );

			mkdir( public_path( 'usr/gallery/' . $newG->id ) );
			foreach ( $files as $f )
			{
				copy( public_path( $f ), public_path( str_replace( '/' . $g->id . '/', '/' . $newG->id . '/', $f ) ) );
			}

			//gallery images
			$images = GalleryImage::where( 'gallery', $g->id )->get();

			foreach ( $images as $i )
			{
				$newI          = $i->replicate();
				$newI->gallery = $newG->id;
				$newI->save();
			}

		}


		echo '<p>..dupe process complete.</p>';
	}
}
