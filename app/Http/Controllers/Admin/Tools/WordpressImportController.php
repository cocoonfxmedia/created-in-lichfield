<?php

namespace App\Http\Controllers\Admin\Tools;

use App\Models\BlogArticle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;
use SoapBox\Formatter\Formatter;

/**
 * This class is used to import WordPress export XML files into the CMS
 *
 * Class WordpressImportController
 * @package App\Http\Controllers\Admin\Tools
 */
class WordpressImportController extends AdminController
{

	public function index()
	{


		$filename = base_path( '/storage/app/import/wordpress.xml' );

		//Read the file
		ini_set( "auto_detect_line_endings", "1" );
		$uploadedFile = fopen( $filename, "r" ) or die( "Unable to open file!" );
		$rawInput = fread( $uploadedFile, filesize( $filename ) );
		fclose( $uploadedFile );

		//fix for MS Excel EOL delimiter
//		$rawInput = str_replace( "\r", "\n", $rawInput );
		$rawInput = str_replace( ':encoded', '', $rawInput );
		$rawInput = str_replace( 'wp:', '', $rawInput );

		//convert content to data object
		$input = json_decode( Formatter::make( $rawInput, Formatter::XML )->toJson() );


//		dd($input->channel->item  );

		foreach ( $input->channel->item as $a )
		{

			$article           = new BlogArticle();
			$article->content  = $a->content;
			//theme images
			$article->content = str_replace('http://carpetsurgeon.co.uk/wp-content/themes/car-cfx/images/', '/site/images/', $article->content );
			//href links
			$article->content = str_replace('http://www.carpetsurgeon.co.uk/', '/', $article->content);
			$article->content = str_replace('http://www.carpetsurgeon.co.uk', '/', $article->content);

			$article->category_id = 1;
			$article->title    = $a->title;

			$article->created_at = $a->post_date;

			$article->url = str_replace('https://www.carpetsurgeon.co.uk/', '', $a->link);

			if( $a->status != 'publish')
			{
				$article->published = 0;
			}

			$article->save();
		}

	}
}
