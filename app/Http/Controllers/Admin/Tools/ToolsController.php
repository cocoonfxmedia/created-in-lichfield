<?php

namespace App\Http\Controllers\Admin\Tools;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AdminController;

class ToolsController extends AdminController
{

	public function index()
	{
		Breadcrumbs::addCrumb( 'Power Tools', '/admin/tools' );


		return view( 'admin.tools.index' );
	}
}
