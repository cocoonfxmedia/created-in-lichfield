<?php
/**
 * Created by PhpStorm.
 * User: garethwalrond
 * Date: 08/10/2016
 * Time: 12:12
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{

	/**
	 * Nav hilight controller
	 * @var array
	 */
	protected $navController = [];

	public function __construct()
	{
		//Nav hiligher controller
		View::share( 'navController', $this->navController );

		/*
		 * Multisite ID
		 */
		if ( ! siteConfig('sid') )
		{
			session( [ 'currentSite' => 1 ] );
		}


		/*
		 * Set up Breadcrumbs
		 */
		Breadcrumbs::setDivider( '' );
		Breadcrumbs::setCssClasses( 'breadcrumb' );

		// Add the root for the breadcrumb trail
		Breadcrumbs::addCrumb( '<i class="icon-grid3 position-left"></i> Dashboard', '/admin/' );
	}

	/**
	 * Add new hilight element to the main nav controller
	 *
	 * @param $elementIdentifier
	 */
	protected function addNavElement( $elementIdentifier )
	{
		$this->navController[] = $elementIdentifier;

		View::share( 'navController', $this->navController );
	}

}