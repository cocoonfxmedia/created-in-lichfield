<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;


class WysiwygController extends AdminController {
	/**
	 * Upload MP3 file via redactor WYSIWYG editor
	 * @return array
	 */
	public function mp3( Request $request ) {

		//ensure that the directory for the current site exixts
		$path = public_path( 'usr/audio/' . siteConfig('id') );

		if ( ! is_dir( $path ) ) {
			mkdir( $path );
		}

		rename( $request->file( 'file' )->path(), $path . '/' . $_FILES['file']['name'] );
		chmod( $path . '/' . $_FILES['file']['name'], 644 );

		return [
			'status' => 'OK',
			'url'    => '/usr/audio/' . siteConfig('id') . '/' . $_FILES['file']['name']
		];
	}
}
