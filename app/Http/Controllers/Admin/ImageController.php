<?php

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{

	/**
	 * Get avaialble images to use in redactor
	 *
	 * @return mixed
	 */
	public function availableImages()
	{
		$images = Image::where('sid', 1)->get();

		foreach( $images as $img )
		{
			$img->url = url('usr/img/' . $img->filename );
			$img->thumb = $img->url;
		}

		return $images;
	}

	/**
	 * UPload image
	 *
	 * @param Request $request
	 *
	 * @return Image
	 */
	public function uploadImage( Request $request )
	{
		$path = $request->file('file')->path();

		rename($path, public_path('usr/img/' . $_FILES['file']['name']));

		//fix file permissions on server
		chmod( public_path('usr/img/' . $_FILES['file']['name']), 644 );


		$image = new Image();
		$image->filename = $_FILES['file']['name'];
		$image->sid = siteConfig('id');

		$image->save();


		$image->thumb = '/usr/img/' . $image->filename;
		$image->url = $image->thumb;


		return $image;
	}


}
