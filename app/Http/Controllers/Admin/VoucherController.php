<?php

namespace App\Http\Controllers\Admin;

use App\BasketVoucher;
use App\Http\Requests;
use App\Http\Requests\VoucherAdminRequest;
use App\Http\Requests\VoucherUpdateRequest;
use App\Models\Voucher;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;

class VoucherController extends AdminController {

	/**
	 * VoucherController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Discount Vouchers', '\admin\vouchers' );

		$this->addNavElement('vouchers');
	}

	/**
	 * Display a list of all vouchers
	 *
	 * @return Response
	 */
	public function index()
	{
		return view( 'admin.voucher.list' )->with( 'vouchers', Voucher::all() );
	}

	/**
	 * Create form for voucher code
	 *
	 * @return View
	 */
	public function add()
	{
		Breadcrumbs::addCrumb( 'New voucher', '/admin/vouchers/create' );

		return view( 'admin.voucher.create' )->with( 'voucher', New Voucher() );
	}

	/**
	 * Create new voucher code
	 *
	 * @param VoucherAdminRequest $request
	 *
	 * @return Redirect
	 */
	public function create( VoucherAdminRequest $request )
	{
		$voucher = New Voucher();

		$voucher->label    = Input::get( 'label' );
		$voucher->code     = Input::get( 'code' );
		$voucher->type     = Input::get( 'type' );
		$voucher->discount = Input::get( 'discount' );

		$date = explode( "/", Input::get( 'expires_on' ) );

		if ( count( $date ) == 3 )
		{
			$voucher->expires_on = $date[2] . '-' . $date[1] . '-' . $date[0] . ' 00:00:00';
		}
		else
		{
			$voucher->expires_on = '';
		}

		$voucher->save();

		return redirect( 'admin/vouchers' )->with('msg', ['state' => 'success', 'text' => 'New voucher added'] );
	}

	/**
	 * Edit form for voucher code
	 *
	 * @param type $voucher_id
	 *
	 * @return View
	 */
	public function edit( $voucher_id )
	{
		Breadcrumbs::addCrumb( 'Edit', '#' );

		$voucher = Voucher::find( $voucher_id );

		return view( 'admin.voucher.edit' )->with( 'voucher', $voucher );
	}

	/**
	 * Save changes to voucher code
	 *
	 * @param type $voucher_id
	 * @param VoucherUpdateRequest $request
	 *
	 * @return Redirect
	 */
	public function update( $voucher_id, VoucherUpdateRequest $request )
	{
		$voucher = Voucher::find( $voucher_id );


		$voucher->label    = Input::get( 'label' );
		$voucher->code     = Input::get( 'code' );
		$voucher->type     = Input::get( 'type' );
		$voucher->discount = Input::get( 'discount' );

		$date = explode( "/", Input::get( 'expires_on' ) );

		if ( count( $date ) == 3 )
		{
			$voucher->expires_on = $date[2] . '-' . $date[1] . '-' . $date[0] . ' 00:00:00';
		}
		else
		{
			$voucher->expires_on = '';
		}


		$voucher->save();

//		$voucher->update( \Input::all() );


		return redirect( 'admin/vouchers/edit/' . $voucher->id )->with('msg', ['state' => 'success', 'text' => 'Changes saved'] );
	}

	/**
	 * Delete a voucher code
	 *
	 * @param type $voucher_id
	 *
	 * @return Redirect
	 */
	public function delete( $voucher_id )
	{
		$voucher = Voucher::find( $voucher_id );


		$voucher->delete();

		return redirect( 'admin/vouchers' )->with('msg', ['state' => 'success', 'text' => 'Voucher deleted'] );
	}

}
