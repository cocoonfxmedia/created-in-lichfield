<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class UserPrefferenceController extends Controller {

	/**
	 * Set the sidebar mode
	 */
	public function navMenuExpander() {
		session( ['main-nav-view' =>  Input::get( 'class' )] );
	}
}
