<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Http\Requests\AdminCustomerUpdateRequest;
use Illuminate\Support\FacadesInput;

class CustomerController extends AdminController {

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Customers', '/admin/customers' );

		$this->addNavElement('orders');
	}

	/**
	 * Display list of customers
	 * @return View
	 */
	public function index()
	{
		$users = User::all();

		return view( 'admin.customer.list' )->with( 'customers', $users );
	}

	/**
	 * View orders associated with a customer
	 *
	 * @param type $customer_id
	 *
	 * @return View
	 */
	public function orders( $customer_id )
	{
		$customer = User::find( $customer_id );

		$orders = Order::where( 'user_id', '=', $customer_id )->get();

		Breadcrumbs::addCrumb( 'Orders', '#' );

		return view( 'admin.order.index' )
			->with( 'customer', $customer )
			->with( 'orders', $orders );
	}

	/**
	 * Edit form for customer details
	 *
	 * @param type $customer_id
	 *
	 * @return View
	 */
	public function edit( $customer_id )
	{
		Breadcrumbs::addCrumb( 'Update Customer Profile', '#' );

		$customer = User::find( $customer_id );

		return view( 'admin.customer.edit' )->with( 'customer', $customer );
	}

	/**
	 * Save changes to a customer's account
	 *
	 * @param type $customer_id
	 * @param AdminCustomerUpdateRequest $request
	 *
	 * @return Redirect
	 */
	public function update( $customer_id, AdminCustomerUpdateRequest $request )
	{
		$customer = User::find( $customer_id );


		if ( ! Input::get( 'passConfirm' ) )
		{
			//contact details update
			$customer->landline = Input::get( 'landline' );
			$customer->mobile   = Input::get( 'mobile' );
			$customer->name     = Input::get( 'name' );
		}
		else
		{
			//password update
			$customer->setPassword( Input::get( 'password' ) );
		}


		$customer->save();

		return redirect( 'admin/customers' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Customer information updated'
		] );
	}

}
