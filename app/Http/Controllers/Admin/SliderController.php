<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slide;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Http\Controllers\Admin\AdminController;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class SliderController extends AdminController
{

	/**
	 * SliderController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Slider Manager', '/admin/sliders' );

		$this->addNavElement( 'sliders' );
	}


	/**
	 * List all sliders for the current site
	 *
	 * @return $this
	 */
	public function index()
	{
		$sliders = Slider::where( 'sid', siteConfig( 'sid' ) )->get();

		return View( 'admin.sliders.list' )->with( 'sliders', $sliders );
	}

	public function create()
	{
		Breadcrumbs::addCrumb( 'New Slider', 'admin/sliders/create' );

		return view( 'admin.sliders.create' )->with( 'slide', [] );
	}

	public function store()
	{
		$slider = new Slider( Input::all() );

		$slider->sid      = siteConfig( 'sid' );
		$slider->homepage = ( ( Input::has( 'homepage' ) ) ? Input::get( 'homepage' ) : 0 );
		$slider->save();

		return redirect( 'admin/sliders' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New slider created'
		] );
	}

	public function edit( $id )
	{
		Breadcrumbs::addCrumb( 'Edit Slider', '/admin/sliders/edit/' . $id );

		$slider = Slider::find( $id );

		return view( 'admin.sliders.edit' )->with( 'slider', $slider );
	}


	public function update( $id )
	{
		$slider = Slider::find( $id );

		$slider->update( Input::all() );
		$slider->homepage = ( ( Input::has( 'homepage' ) ) ? 1 : 0 );

		$slider->save();

		return redirect( 'admin/sliders/edit/' . $slider->id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Slider updated'
		] );
	}


	public function destroy( $id )
	{
		$slider = Slider::find( $id );

		Slide::where( 'sid', $slider->id )->delete();
		$slider->delete();

		return redirect( 'admin/sliders' )->with( 'msg', [ 'state' => 'success', 'text' => 'Slider deleted' ] );
	}

}
