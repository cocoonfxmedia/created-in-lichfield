<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Session;

class SwitchSiteController extends AdminController
{
	/**
	 * Change the session admin site ID
	 *
	 * @param $newSiteId
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function index( $newSiteId )
	{
		session( [ 'currentSite' => $newSiteId ] );

		return redirect( '/admin' );
	}
}