<?php

namespace App\Http\Controllers\Admin;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Models\Product;

class PriceAdjustController extends AdminController {
	/**
	 * Mass adjust price by percentage
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		Breadcrumbs::addCrumb( 'Products', '/admin/products' );
		Breadcrumbs::addCrumb( 'Mass Price Adjustment', '#' );

		$this->addNavElement('products');
		$this->addNavElement('products-mas-price-adjust');

		return view( 'admin.product.price_adjust' );
	}

	/**
	 * Process the price adjustment
	 *
	 * @param Requests\Admin\PriceAdjust $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function process( Requests\Admin\PriceAdjust $request )
	{
		$products = Product::all();

		$adjust = 1.00;

		if ( Input::get( 'direction' ) == 'up' )
		{
			$adjust += ( floatval( Input::get( 'amount' ) ) / 100 );
		}
		else
		{
			$adjust -= ( floatval( Input::get( 'amount' ) ) / 100 );
		}

		foreach ( $products as $p )
		{
			$p->price      = $p->price * $adjust;
			$p->sale_price = str_replace( ',', '', $p->sale_price ) * $adjust;
			$p->save();
		}

		return redirect( '/admin/price-adjust' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'All prices have been adjusted ' . strtoupper( Input::get( 'direction' ) ) . ' by ' . Input::get( 'amount' ) . '%'
		] );
	}
}
