<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 09:59
 */

namespace App\Http\Controllers\Admin;

use App\Models\Block;
use App\Models\Gallery;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\Slider;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class PageController extends AdminController
{

	private $blockPlaces = [
		''                  => 'No Link',
		'message_from_head' => 'Message from our head',
		'great_things'      => '10 great things',
		'vision'            => 'Our vision',
		'philosophy'        => 'Our philosophy'
	];

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Pages', '/admin/pages' );

		$this->addNavElement( 'pages' );
		$this->addNavElement( 'pages-pages' );
	}

	/**
	 * List view
	 * @return Page collection
	 */
	public function index()
	{
		return view( 'admin.pages.list', [
			'pages' => Page::where( 'sid', siteConfig( 'sid' ) )->get()
		] );
	}


	/**
	 * New page form
	 * @return mixed
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New page', '/admin/pages/created' );

		return view( 'admin.pages.create', [
			'templates'    => $this->templateList(),
			'page_list'    => $this->pageList(),
			'block_list'   => Block::where( 'published', 1 )->get(),
			'gallery_list' => Gallery::where( 'published', 1 )->where( 'sid', siteConfig( 'id' ) )->get(),
			'block_places' => $this->blockPlaces,
			'sliderList'   => $this->sliderList()
		] );
	}


	/**
	 * Create new page
	 * @return mixed
	 */
	public function store()
	{
		$page = new Page();

		$data = Input::all();

		$page->title            = Input::get( 'title' );
		$page->meta_title       = Input::get( 'meta_title' );
		$page->meta_description = Input::get( 'meta_description' );
		$page->content          = Input::get( 'content' );
		$page->template         = Input::get( 'template' );
		$page->parent_id        = ( ( Input::get( 'parent_id' ) ) ? Input::get( 'parent_id' ) : null );

		$page->block_link    = Input::get( 'block_link' );
		$page->block_content = Input::get( 'block_content' );

		$page->sid = siteConfig( 'sid' );

		$page->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );

		if ( empty( $data['url'] ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_replace( " ", '-', strtolower( $data['url'] ) );
		}

		$page->path = $page->url;

		if ( ! is_null( $page->parent_id ) )
		{
			$page->save();

			$page->path = $page->parent->path . '/' . $page->path;
		}

		$page->save();


		if ( Input::file( 'block_image' ) )
		{
			$filename = $page->id . '_' . Input::file( 'block_image' )->getClientOriginalName();


			Input::file( 'block_image' )->move( base_path() . '/public/usr/homepage_blocks/', $filename );

			$page->block_image = $filename;

		}

		if ( Input::file( 'masthead' ) )
		{
			if ( ! is_dir( public_path( 'usr/page_masthead' ) ) )
			{
				mkdir( public_path( 'usr/page_masthead' ) );
			}


			$filename = $page->id . '_' . Input::file( 'masthead' )->getClientOriginalName();

			Input::file( 'masthead' )->move( base_path() . '/public/usr/page_masthead/', $filename );

			$page->masthead = $filename;
		}


		$page->save();


		// Insert page info the default meenu
		$mi = new MenuItem();

		$mi->type        = 'page';
		$mi->destination = $page->id;
		$mi->active      = 1;
		$mi->order       = 999;
		$mi->parent      = 0;
		$mi->menu        = config( 'cms.default_menu' );
		$mi->save();

		return redirect( 'admin/pages' )->with( 'msg', [ 'state' => 'success', 'text' => 'New page saved' ] );
	}

	/**
	 * Delete page
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function destroy( $page_id )
	{
		$page = Page::find( $page_id );

		$page->delete();

		return redirect( '/admin/pages' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Page removed'
		] );
	}

	/**
	 * Page edit view
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function edit( $page_id )
	{
		Breadcrumbs::addCrumb( 'Edit', '/admin/pages/edit/' . $page_id );

		return view( 'admin.pages.edit', [
			'page'         => Page::find( $page_id ),
			'templates'    => $this->templateList(),
			'page_list'    => $this->pageList( $page_id ),
			'block_list'   => Block::where( 'published', 1 )->get(),
			'gallery_list' => Gallery::where( 'published', 1 )->where( 'sid', siteConfig( 'id' ) )->get(),
			'block_places' => $this->blockPlaces,
			'sliderList'   => $this->sliderList()
		] );
	}

	/**
	 * Update page object
	 *
	 * @param $page_id
	 *
	 * @return mixed
	 */
	public function update( $page_id )
	{
		$page = Page::find( $page_id );


		$stub_changed = false;
		if ( Input::get( 'url' ) != $page->url )
		{
			$stub_changed = true;
		}

		$page->update( Input::all() );

		$page->published = ( ( Input::get( 'published' ) ) ? 1 : 0 );

		$parent_change = false;
		if ( Input::get( 'parent_id' ) != $page->parent_id )
		{
			$parent_change = true;
		}


		$page->parent_id = ( ( Input::get( 'parent_id' ) ) ? Input::get( 'parent_id' ) : null );

		//ensure URL is not empty
		if ( empty( $page->url ) )
		{
			$page->url = str_slug( $page->title, '-' );
		} else
		{
			$page->url = str_replace( " ", '-', strtolower( $page->url ) );
		}


		$page->path = '';

		if ( ! is_null( $page->parent_id ) )
		{
			$page->path = $page->parent->path . '/';
		}

		$page->path .= $page->url;


		if ( Input::file( 'block_image' ) )
		{
			$filename = $page->id . '_' . Input::file( 'block_image' )->getClientOriginalName();

			Input::file( 'block_image' )->move( base_path() . '/public/usr/homepage_blocks/', $filename );

			$page->block_image = $filename;
		}

		//Page masthead
		if ( Input::get( 'remove_masthead' ) == '1' )
		{
			unlink( public_path( $page->masthead ) );
			$page->masthead = null;
		}

		if ( Input::file( 'masthead' ) )
		{
			if ( ! is_dir( public_path( 'usr/page_masthead' ) ) )
			{
				mkdir( public_path( 'usr/page_masthead' ) );
			}


			$filename = $page->id . '_' . Input::file( 'masthead' )->getClientOriginalName();

			Input::file( 'masthead' )->move( base_path() . '/public/usr/page_masthead/', $filename );

			$page->masthead = $filename;
		}


		$page->save();


		//cascade the URL change down through the child pages
		//if ( $stub_changed && $page->children->count() > 0 )
		//{
		//	$this->rework_url( $page->children, $page->path );
		//}

		return redirect( '/admin/pages/edit/' . $page_id )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Page updated'
		] );
	}

	/**
	 * Interativley rework the page paths
	 *
	 * @param Page $pages
	 * @param $root
	 */
	private function rework_url( $pages, $root )
	{
		foreach ( $pages as $page )
		{
			if ( $page->path !== $root )
			{
				//process this url
				$page->path = $root . '/' . $page->url;
				$page->save();
			}

			if ( $page->children->count() > 0 )
			{
				$this->rework_url( $page->chilldren, $page->path );
			}
		}
	}

	/**
	 * Generate list of available templates
	 *
	 * @return array
	 */
	private function templateList()
	{
		$files     = scandir( base_path() . '/resources/views/page_templates/' );
		$templates = [];

		unset( $files[0], $files[1] );


		foreach ( $files as $f )
		{
			$templates[ str_replace( ".blade.php", '', $f ) ] = ucwords( str_replace( "_", ' ', str_replace( ".blade.php", '', $f ) ) );
		}

		return $templates;

	}

	/**
	 * Generate a list of available pages that can be parent
	 *
	 * @param int $exclude_page
	 *
	 * @return array
	 */

	private function pageList( $exclude_page = 0 )
	{
		$catalog = [];

		$catalog[''] = 'No parent page';

		if ( config( 'cms.components.multisite' ) )
		{
			$pages = Page::where( 'sid', siteConfig( 'id' ) )->get();
		} else
		{
			$pages = Page::all();
		}


		foreach ( $pages as $page )
		{
			if ( $page->id != $exclude_page ) //prevent a page parenting itself
			{
				$catalog[ $page->id ] = $page->title;
			}
		}

		return $catalog;
	}

	/**
	 * Trash bin list
	 *
	 * @return mixed
	 */
	public function trash()
	{
		Breadcrumbs::addCrumb( 'Recycle Bin', '/admin/pages/trash' );

		return view( 'admin.pages.trash', [ 'pages' => Page::onlyTrashed()->get() ] );
	}

	/**
	 * Restore a page
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function restore( $id )
	{
		Page::withTrashed()->where( 'id', '=', $id )->restore();

		return redirect( '/admin/pages' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Page removed from the recycle bin'
		] );
	}

	/**
	 * Empty the recycle bin
	 *
	 * @return mixed
	 */
	public function emptyTrash()
	{
		Page::onlyTrashed()->forceDelete();

		return redirect( '/admin/pages' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Recycle bin has been emptied'
		] );
	}

	/**
	 * Reconcile the path attribute for all pages
	 */
	public function repath()
	{
		echo '<h1>Repath function</h1><pre>';
		$pages = Page::all();

		foreach ( $pages as $page )
		{

			if ( ! is_null( $page->parent_id ) )
			{
				//var_dump( $page->parent );
				$page->path = $page->parent->path . '/' . $page->url;
			} else
			{
				$page->path = $page->url;
			}

			$page->save();
		}

		echo '<p>Repath function complete</p>';
	}


	/**
	 * Build list of sliders for dropdown menu
	 *
	 * @return array
	 */
	protected function sliderList()
	{
		$list    = [];
		$list[0] = 'No slider enabled on this page';

		$sliders = Slider::where( 'sid', siteConfig( 'id' ) )->orderBy( 'name' )->get();

		foreach ( $sliders as $slider )
		{
			$list[ $slider->id ] = $slider->name;
		}

		return $list;
	}

}