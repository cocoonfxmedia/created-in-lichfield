<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Models\Product;
use App\Models\ProductQuery;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ProductQueryController extends AdminController {

	protected $product = null;

	/**
	 * ProductQueryController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Product Queries', '/admin/product-queries' );

		$this->addNavElement('products');
		$this->addNavElement('products-queries');
	}

	/**
	 * Admin view for all product queries
	 *
	 * @return view
	 */
	public function index()
	{

		$queries = ProductQuery::all();

		return view( 'admin.product.query.index', [ 'queries' => $queries ] );
	}

	/**
	 * Admin view for a product query
	 *
	 * @param $query_id
	 *
	 * @return mixed
	 */
	public function view( $query_id )
	{
		Breadcrumbs::addCrumb( 'Details', '/admin/product-queries/' . $query_id );

		$query = ProductQuery::find( $query_id );

		if ( ! $query )
		{
			abort( 404 );
		}

		return view( 'admin.product.query.view', [ 'query' => $query ] );
	}

	/**
	 * AJAX call to mark the query as replied to
	 *
	 * @param $query_id
	 */
	public function markReplied( $query_id )
	{
		$query = ProductQuery::find( $query_id );

		$query->replied = 1;
		$query->save();
	}


}
