<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Models\Review;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ReviewController extends AdminController {

	protected $review = null;

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Product Reviews', '#' );

		$this->addNavElement('products');
		$this->addNavElement('products-reviews');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$reviews = Review::all();

		return view( 'admin.reviews.list' )->with( 'reviews', $reviews );
	}

	/**
	 * Mark review as approved
	 *
	 * @param $review_id
	 *
	 * @return string
	 */
	public function approve( $review_id )
	{
		$review              = Review::find( $review_id );
		$review->approved_at = date( "Y-m-d G:i:s" );
		$review->save();

		return 'OK';
	}

	/**
	 * Mark review as rejected
	 *
	 * @param $review_id
	 *
	 * @return string
	 */
	public function reject( $review_id )
	{
		$review             = Review::find( $review_id );
		$review->deleted_at = date( 'Y-m-d G:i:s' );
		$review->save();

		return 'OK';
	}

	/**
	 * @param $review_id
	 *
	 * @return \Illuminate\Support\Collection|null|static
	 */
	public function show( $review_id )
	{
		$review = Review::find( $review_id );

		if ( $review->deleted_at )
		{
			$review->status = 'Rejected';
		}
		elseif ( $review->approved_at )
		{
			$review->status = 'Approved';
		}
		else
		{
			$review->status = 'Pending';
		}

		$review->product_name = $review->product->name;


		$review->date = $review->created_at->format( 'd/m/y' );


		return $review;
	}

}
