<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\Admin\UserRequest;
use App\Models\Role;
use App\Models\User;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class UserManagerController extends AdminController {

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'User Manager', '/admin/users' );

		$this->addNavElement('users');
	}

	/**
	 * Display a list of user accounts
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::admin()->get();

		return view( 'admin.users.list' )->with( 'users', $users );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New User', '/admin/users/create' );

		return view( 'admin.users.create' )->with( 'roles', $this->getRoles() )->with('roleId', 1);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store( UserRequest $request )
	{
		$user = new User();

		$user->first_name = Input::get( 'first_name' );
		$user->last_name  = Input::get( 'last_name' );
		$user->email      = Input::get( 'email' );
		$user->admin      = 1;

		if ( ! empty( Input::get( 'password' ) ) )
		{
			$user->password = Hash::make( Input::get( 'password' ) );
		}

		$user->save();

		$user->attachRole( Input::get( 'role' ) );

		return redirect( '/admin/users' )->with( 'msg', 'User account for ' . Input::get( 'name' ) . ' has been created' );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id )
	{
		Breadcrumbs::addCrumb( 'Edit', '/admin/users/edit/' . $id );
		$user = User::find( $id );

		return view( 'admin.users.edit' )->with( 'user', $user )->with( 'roles', $this->getRoles() )->with('roleId', $user->roles()->first()->id );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function update( $id )
	{
		$user = User::find( $id );

		$user->first_name = Input::get( 'first_name' );
		$user->last_name  = Input::get( 'last_name' );
		$user->email      = Input::get( 'email' );

		if ( ! empty( Input::get( 'password' ) ) )
		{
			$user->password = Hash::make( Input::get( 'password' ) );
		}

		$user->save();


		/*
		 * Update user role
		 */
		DB::table( 'role_user' )->where( 'user_id', $user->id )->delete();

		$user->attachRole( Input::get( 'role' ) );


		return redirect( '/admin/users' )->with( 'msg', [ 'state' => 'success', 'text' => 'User account updated' ] );
	}

	/**
	 * Delete user account
	 *
	 * @param $user_id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete( $user_id )
	{

		$user = User::find( $user_id );

		$user->delete();

		return redirect( '/admin/users' )->with( 'msg', [ 'state' => 'success', 'text' => 'User account deleted' ] );
	}

	/**
	 * Generate list of available roles
	 *
	 * @return array
	 */
	protected function getRoles()
	{
		$roles = [];

		$roleCatalog = Role::all();

		foreach ( $roleCatalog as $role )
		{
			$roles[ $role->id ] = $role->display_name;
		}

		return $roles;
	}

}
