<?php

namespace App\Http\Controllers\Admin;

use App\Models\NewsletterSubscription;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class NewsletterSubscriptionsController extends AdminController
{


	/**
	 * NewsletterSubscriptionsController constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Newsletter Subscriptions', '/admin/newsletter-subscriptions' );

		$this->addNavElement( 'newsletter-subscriptions' );
	}

	/**
	 * List view of subscriptions
	 *
	 * @return $this
	 */
	public function index()
	{
		$subscriptions = NewsletterSubscription::where( 'sid', siteConfig( 'sid' ) )->get();

		return view( 'admin.newsletter_subscriptions.list' )->with( 'subscriptions', $subscriptions );
	}


	/**
	 * Create new subscrption object
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		$subscription        = new NewsletterSubscription();
		$subscription->email = Input::get( 'email' );
		$subscription->sid   = siteConfig( 'sid' );
		$subscription->save();

		return redirect( 'admin/newsletter-subscriptions' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'New subscription added'
		] );
	}

	/**
	 * Update subscription object
	 *
	 * @param $subscriptionId
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update( $subscriptionId )
	{
		$subscription = NewsletterSubscription::find( $subscriptionId );

		$subscription->email = Input::get( 'email' );

		$subscription->save();

		return redirect( 'admin/newsletter-subscriptions/' . $subscription->id . '/edit' )->with( 'msg', [
			'state' => 'success',
			'text'  => 'Subscription updated'
		] );
	}

	/**
	 * Unsubscribe a user from the list
	 *
	 * @param $subscription_id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function unsubscribe( $subscription_id )
	{
		$subscription = NewsletterSubscription::find( $subscription_id );

		$subscription->unsubscribed_at = date( 'Y-m-d G:i:s' );
		$subscription->save();

		return redirect( 'admin/newsletter-subscriptions' )->with( 'msg', [
			'state' => 'success',
			'text'  => $subscription->email . ' has been unsubscribed from the mailing list'
		] );
	}

	/**
	 * Reactivate a subscription
	 *
	 * @param $subscriptionId
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function resubscribe( $subscriptionId )
	{
		$subscription = NewsletterSubscription::find( $subscriptionId );

		$subscription->unsubscribed_at = null;
		$subscription->created_att     = date( 'Y-m-d G:i:s' );
		$subscription->save();

		return redirect( '/admin/newsletter-subscriptions' )->with( 'msg', [
			'state' => 'success',
			'text'  => $subscription->email . ' resubscribed to the mailing list'
		] );
	}

	/**
	 * New newsletter subscription form
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New Subscription', '#' );

		return view( 'admin.newsletter_subscriptions.create' );
	}

	/**
	 * @param $subscriptionId
	 *
	 * @return $this
	 */
	public function edit( $subscriptionId )
	{
		Breadcrumbs::addCrumb( 'Edit Subscription', '#' );

		$subscription = NewsletterSubscription::find( $subscriptionId );

		return view( 'admin.newsletter_subscriptions.edit' )->with( 'subscription', $subscription );
	}

}
