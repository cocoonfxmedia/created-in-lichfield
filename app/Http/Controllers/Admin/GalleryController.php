<?php

namespace App\Http\Controllers\Admin;

use App\Models\ArtCategory;
use App\Models\Gallery;
use App\Models\GalleryImage;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class GalleryController extends AdminController {

	public function __construct()
	{
		parent::__construct();
		Breadcrumbs::addCrumb( 'Galleries', '/admin/gallery' );

		$this->addNavElement('galleries');
	}

	/**
	 * List of available galleries
	 * @return $this
	 */
	public function index()
	{
		return view( 'admin.galleries.list' )->with( 'galleries', Gallery::where( 'sid', siteConfig('id') )->get() );
	}

	/**
	 * Publish gallery
	 *
	 * @param $id
	 */
	public function publish( $id )
	{
		$gallery            = Gallery::find( $id );
		$gallery->published = 1;
		$gallery->save();

		return redirect( 'admin/gallery' );
	}

	/**
	 * Unpublish gallery
	 *
	 * @param $id
	 */
	public function unpublish( $id )
	{

		$gallery            = Gallery::find( $id );
		$gallery->published = 0;
		$gallery->save();

		return redirect( 'admin/gallery' );
	}

	/**
	 * Delete gallery
	 *
	 * @param $id
	 */
	public function delete( $id )
	{
		$gallery = Gallery::find( $id );

		GalleryImage::where( 'gallery', $gallery->id )->delete();

		$gallery->delete();

		return redirect( 'admin/gallery' );
	}

	/**
	 * Edit existing gallery page
	 *
	 * @param $id
	 *
	 * @return $this
	 */
	public function edit( $id )
	{
		return view( 'admin.galleries.edit' )->with( 'gallery', Gallery::find( $id ) )
			->with('categories', ArtCategory::all());
	}

	/**
	 * Create new gallery page
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		return view( 'admin.galleries.create' )
			->with('categories', ArtCategory::select('id', 'title')->get());
	}

	/**
	 * Create new gallery
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store()
	{
		$gallery = new Gallery();

		$gallery->name = Input::get( 'name' );
		$gallery->description = Input::get('description');

		if ( ! Input::get( 'slug' ) )
		{
			$gallery->slug = str_slug( $gallery->name );
		}
		else
		{
			$gallery->slug = str_slug( Input::get( 'slug' ) );
		}

		$gallery->sid = siteConfig('id');

		$gallery->save();

		//set up gallery filespace
		$directory = public_path("usr/gallery/{$gallery->id}");
		if (!is_dir($directory)) {
			mkdir($directory);
			chmod($directory, 1755);
		}

		return redirect( 'admin/gallery' );
	}

	/**
	 * Save edited gallery details
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update( $id )
	{
		$gallery = Gallery::find( $id );

		$gallery->name = Input::get( 'name' );
		$gallery->description = Input::get('description');

		if ( ! Input::get( 'slug' ) )
		{
			$gallery->slug = str_slug( $gallery->name );
		}
		else
		{
			$gallery->slug = str_slug( Input::get( 'slug' ) );
		}

		$gallery->save();

		return redirect( 'admin/gallery' )->with( 'msg', 'Gallery details updated' );
	}

	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function view( $id )
	{
		$gallery = Gallery::find( $id );

		Breadcrumbs::addCrumb( $gallery->name, '#' );

		return view( 'admin.galleries.gallery' )
			->with('categories', ArtCategory::all())
			->with( 'images', GalleryImage::where( 'gallery', $gallery->id )->orderBy( 'order' )->get() )
			->with( 'gallery', $gallery );
	}

	/**
	 * Upload image through dropzone
	 *
	 * @param Request $request
	 * @param $id
	 */
	public function imageUpload( Request $request, $id )
	{
		$image = new GalleryImage();

		$image->gallery = $id;


		//file handler
		$path = $request->file( 'file' )->path();

		rename( $path, public_path( 'usr/gallery/' . $id . '/' . $_FILES['file']['name'] ) );

		//fix file permissions on server
		chmod( public_path( 'usr/gallery/' . $id . '/' . $_FILES['file']['name'] ), 0644 );


		$image->filename = $_FILES['file']['name'];

		$image->save();

		echo 'OK';
	}

	/**
	 * Save image attributes
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function updateImage()
	{

		$image = GalleryImage::find( Input::get( 'image' ) );

		$image->title = Input::get( 'title' );
		$image->latitude = Input::get( 'latitude' );
		$image->longitude = Input::get( 'longitude' );
		$image->category_id = Input::get( 'category_id' );

		$image->save();


		return redirect('admin/gallery/' . $image->gallery )->with('msg', ['state' => 'success', 'text' => 'Image attributes updated']);
	}

	/**
	 * Save order via ajax
	 *
	 * @param $id
	 */
	public function updateOrder( $id)
	{
		$nav = json_decode( Input::get( 'nav' ) );

		foreach ( $nav as $position => $li )
		{
			$img = GalleryImage::find( $li->id );

			$img->order  = $position;

			$img->save();
		}

		echo 'OK';

	}

	/**
	 * Delete image from the gallery
	 * @param $id
	 *
	 * @return string
	 */
	public function removeImage( $id )
	{
		$img = GalleryImage::find( $id )->delete();

		return 'OK';
	}

}
