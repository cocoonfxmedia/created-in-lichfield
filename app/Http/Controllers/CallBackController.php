<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallBackRequest;
use App\Mail\CallBackFormMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class CallBackController extends Controller
{
	public $payload;

	public function send( CallBackRequest $request )
	{

		$this->payload = Input::all();


		Mail::to( siteConfig( 'email_to' ) )
		    ->send( new CallBackFormMessage( $this->payload ) );


		return 'OK';
	}
}
