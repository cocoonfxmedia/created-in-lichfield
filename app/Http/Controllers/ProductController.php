<?php

namespace App\Http\Controllers;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Product;
use App\Models\ProductColours;
use App\Models\ProductSizes;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller {


	public function quickview( $product_id, $slug )
	{
		$product = Product::active()->find($product_id);

		if ( ! $product )
		{
			abort( 404 );
		}

		return view( 'product.quick_view' )->with( 'product', $product );
	}

	/**
	 * Show a single product
	 *
	 * @param $slug
	 *
	 * @return $this|\Illuminate\View\View
	 */
	public function show( $product_id, $slug )
	{
		$product = Product::active()->find($product_id);

		if ( ! $product )
		{
			abort( 404 );
		}

		$product->views ++;
		$product->save();

		//set breadcrumbs
		Breadcrumbs::addCrumb( $product->name, '/product/' . $product->slug );

		//product options
		$colours = ProductColours::fetch( $product->id );
		$sizes   = ProductSizes::fetch( $product->id );

		//colour filter init
		$colourFilter = false;

		if ( Input::get( 'colour' ) )
		{
			$colourFilter = Input::get( 'colour' );
		}
		elseif ( count( $colours ) > 0 )
		{
			$colourFilter = $colours->first()->id;
		}

		$defaultColour = '000000';
		if ( count( $colours ) )
		{
			$defaultColour = $colours[0]->hex;
		}


		if ( $product )
		{
			return view( 'product.detail' )
				->with( 'product', $product )
				->with( 'colours', $colours )
				->with( 'sizes', $sizes )
				->with( 'defaultColour', $defaultColour )
				->with( 'colourFilter', $colourFilter )
				->with( 'reviews', $product->reviews()->approved()->get() );
		}
		else
		{
			abort( 404 );
		}
	}
}
