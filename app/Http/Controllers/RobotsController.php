<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RobotsController extends Controller
{
	/**
	 * Dynamix robots.txt file based on environment
	 *
	 * @return string
	 */
	public function file()
	{

		if ( env( 'APP_ENV' ) == 'production' )
		{
			$content = 'User-agent: *' . PHP_EOL . 'Allow: /';
		} else
		{
			$content = 'User-agent: *' . PHP_EOL . 'Disallow: /';
		}

		$content .= PHP_EOL . 'Sitemap: ' . url( 'sitemap.xml' );


		$response = response( $content )->header( 'Content-Type', 'text/plain' );
		$response->header( 'Content-Length', strlen( $response->getOriginalContent() ) );

		return $response;
	}
}
