<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogCategory;
use App\Models\BlogArticle;

class BlogController extends Controller
{

	/**
	 * Main blog page
	 *
	 * @return mixed
	 */
	public function index()
	{

		$payload = [
			'articles'    => BlogArticle::published()->where( 'sid', siteConfig( 'sid' ) )->orderBy( 'created_at', 'DESC' )->get(),
			'newsArchive' => $this->archiveDates(),
			'categories'  => BlogCategory::orderBy( 'title' )->get(),
			'newsStories' => BlogArticle::published()->where( 'sid', siteConfig( 'sid' ) )->orderBy( 'created_at', 'DESC' )->limit( 5 )->get()
		];

		return view( 'blog.index' )->with( $payload );

	}

	/**
	 * Single article
	 *
	 * @param $slug
	 *
	 * @return mixed
	 */
	public function article( $slug )
	{

		$payload = [
			'article'     => BlogArticle::where( 'url', $slug )->published()->first(),
			'newsArchive' => $this->archiveDates(),
			'categories'  => BlogCategory::orderBy( 'title' )->get(),
			'newsStories' => BlogArticle::published()->where( 'sid', siteConfig( 'sid' ) )->orderBy( 'created_at', 'DESC' )->limit( 5 )->get()
		];

		if ( ! $payload['article'] )
		{
			abort( 404 );
		}

		return view( 'blog.article' )->with( $payload );
	}

	/**
	 * Blog category page
	 *
	 * @param $slug
	 *
	 * @return $this
	 */
	public function category( $slug )
	{
		$payload = [
			'category'    => BlogCategory::where( 'slug', $slug )->active()->first(),
			'categories'  => BlogCategory::orderBy( 'title' )->get(),
			'newsStories' => BlogArticle::published()->where( 'sid', siteConfig( 'sid' ) )->orderBy( 'created_at', 'DESC' )->limit( 5 )->get()
		];

		if ( ! $payload['category'] )
		{
			abort( 404 );
		}


		$payload['articles'] = BlogArticle::published()
		                                  ->where( 'category_id', $payload['category']->id )
		                                  ->where( 'sid', siteConfig( 'sid' ) )
		                                  ->orderBy( 'created_at', 'DESC' )
		                                  ->get();

		return view( 'blog.index' )->with( $payload );
	}

	/**
	 * Get the list of months that contain news articles
	 *
	 * @return array
	 */
	private function archiveDates()
	{
		//TODO:: build list of blog a archive dates
		return [];
	}

}
