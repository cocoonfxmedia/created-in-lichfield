<?php

namespace App\Http\Controllers;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

use App\Http\Requests;

class AccountController extends Controller {

	/**
	 * My Account landing page
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		Breadcrumbs::addCrumb( 'My Account', '/my-account' );

		return view( 'account.dashboard' );
	}
}
