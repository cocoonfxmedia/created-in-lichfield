<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalInfoUpdateRequest;
use App\Models\NewsletterSubscription;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class PersonalInfoController extends Controller {

	/**
	 * Personal info edit form
	 *
	 * @return $this
	 */
	public function index()
	{
		Breadcrumbs::addCrumb( 'My Account', '/my-account' );
		Breadcrumbs::addCrumb( 'My Personal Information', '#' );


		$newsletterSubscription = NewsletterSubscription::where( 'email', Auth::user()->email )->whereNull( 'unsubscribed_at' )->count();

		return view( 'account.personal_info' )
			->with( 'user', Auth::user() )
			->with( 'newsSubscription', $newsletterSubscription );
	}


	/**
	 * @param PersonalInfoUpdateRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update( PersonalInfoUpdateRequest $request )
	{
		$user = User::find( Auth::user()->id );
		$user->update( Input::all() );

		if ( Input::get( 'passwd' ) )
		{
			$user->password = Hash::make( Input::get( 'passwd' ) );
		}

		$user->save();


		/**
		 * Update newsletter subscripiton
		 */
		$newsletterSubscription = NewsletterSubscription::where( 'email', Auth::user()->email )->first();

		if ( Input::get( 'newsletter' ) )
		{
			if( ! $newsletterSubscription )
			{
				//new subscription
				$newSubscription        = new NewsletterSubscription();
				$newSubscription->email = Auth::user()->email;
				$newSubscription->save();

			}
			elseif ( ! is_null( $newsletterSubscription->unsubscribed_at ) && Input::get( 'newsletter' ) )
			{
				$newsletterSubscription->unsubscribed_at == null;
				$newsletterSubscription->save();
			}



		}
		elseif ( $newsletterSubscription && ! Input::get( 'newsletter' ) )
		{
			//unsubscribe
			$newsletterSubscription->unsubscribed_at = date( 'Y-m-d G:i:s' );
			$newsletterSubscription->save();
		}


		return redirect( '/my-account/identity' );
	}
}
