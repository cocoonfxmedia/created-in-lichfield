<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller {

	/**
	 * AddressController constructor.
	 */
	public function __construct()
	{
		Breadcrumbs::addCrumb( 'My Account', '/my-account' );
		Breadcrumbs::addCrumb( 'Addresses', '/my-account/addresses' );
	}


	public function index()
	{
		$addresses = Address::where( 'user_id', Auth::user()->id )->get();

		return view( 'account.addresses' )
			->with( 'addresses', $addresses );
	}

	/**
	 * New address form
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		Breadcrumbs::addCrumb( 'New Address', '#' );

		return view( 'account.new_address' );
	}

	/**
	 * Create new address
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store()
	{
		$address = new Address( Input::all() );

		$address->user_id = Auth::user()->id;

		$address->save();

		return redirect( '/my-account/addresses' );
	}

	/**
	 * Remove Address
	 *
	 * @param $address_id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function remove( $address_id )
	{
		$address = Address::find( $address_id );

		$address->delete();

		return redirect( '/my-account/addresses' );
	}

	public function edit( $address_id )
	{
		//TODO::edit allow update of addresses\

	}

	public function update()
	{

	}
}
