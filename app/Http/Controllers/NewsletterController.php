<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Models\NewsletterSubscription;
use Illuminate\Support\Facades\Config;

class NewsletterController extends Controller {

	/**
	 * Handle newsletter subscription submission
	 *
	 * @param Requests\NewsletterSubscription $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function index( Requests\NewsletterSubscription $request )
	{
		$subscription        = new NewsletterSubscription();
		$subscription->email = Input::get( 'subscriber_email' );

		if ( Config::get( 'cms.components.multisite' ) )
		{
			$subscription->sid = siteConfig('sid');
		}

		$subscription->save();

		return back()->with( 'subscriber_msg', [
			'state' => 'success',
			'text'  => 'Newsletter : You have successfully subscribed to this newsletter.'
		] );
	}

}
