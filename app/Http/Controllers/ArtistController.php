<?php

declare( strict_types=1 );

namespace App\Http\Controllers;

use App\Http\Requests\StoreArtist;
use App\Models\ArtCategory;
use App\Models\Artist;

final class ArtistController extends Controller
{
	public function index()
	{
		return view( 'artists.index', [
			'artists' => Artist::active()->get(),
		] );
	}

	public function show( Artist $artist )
	{
		return view( 'artists.show', [
			'artist' => $artist,
		] );
	}

	public function edit( Artist $artist )
	{
		return view( 'artists.edit', [
			'artist'     => $artist,
			'categories' => ArtCategory::all(),
		] );
	}

	public function update( Artist $artist, StoreArtist $request )
	{
		if ( $request->hasFile( 'profile_image' ) )
		{
			$artist->profile_image = $request->file( 'profile_image' )->storePublicly( 'public/artists' );
			$artist->profile_image = str_replace( 'public/', '/storage/', $artist->profile_image );
		}
		elseif (true == $request->remove_profile_image) {
			$artist->profile_image = null;
		}

		if ( $request->hasFile( 'header_image' ) )
		{
			$artist->header_image = $request->file( 'header_image' )->storePublicly( 'public/artists' );
			$artist->header_image = str_replace( 'public/', '/storage/', $artist->header_image );
		}
		elseif (true == $request->remove_header_image) {
			$artist->header_image = null;
		}

		$artist->update( $request->all() );
		$artist->categories()->sync($request->categories);

		return redirect()->route( 'artists.show', $artist );
	}
}
