<?php

namespace App\Http\Controllers;

use App\Http\Requests\EnquiryRequest;
use App\Mail\ContactFormMessage;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

	protected $payload;

	protected $postfix = '';

	public function __construct()
	{
		parent::__construct();

		Breadcrumbs::addCrumb( 'Contact', '/contact-us' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return View sResponse
	 */
	public function index()
	{

		return view( 'contact.form' );
	}

	/**
	 * Process email submission
	 *
	 * @return Redirect to thank you page
	 */
	public function send( EnquiryRequest $request )
	{

		//File is file-754 field

		$this->payload = [
			'body'      => Input::get( 'message' ),
			'name'      => Input::get( 'name' ),
			'email'     => Input::get( 'email' ),
			'telephone' => Input::get( 'ContactNumber' ),
			'address'   => Input::get( 'Address' ),
			'postcode'  => Input::get( 'Postcode' ),
			'help'      => Input::get( 'Howcanwehelp' ),
		];


		if ( siteConfig( 'sid' ) == '2' )
		{
			$this->payload['postfix'] = ' on Carpet Fitting Surgeon';
		}


		if ( $request->hasFile( 'file-754' ) )
		{
			$this->payload['attachment']    = $request->file( 'file-754' )->path();
			$this->payload['attachment_as'] = $_FILES['file-754']['name'];
		}

		Mail::to( siteConfig( 'email_to' ) )
		    ->send( new ContactFormMessage( $this->payload ) );


		return redirect( '/contact-us?sent=1' );
	}

	/**
	 * Thank you messages after contact form is completed
	 *
	 * @return View response
	 */
	public function sent()
	{
		Breadcrumbs::addCrumb( 'Message Sent', '#' );

		return view( 'contact.thanks' );
	}


	/**
	 * AJAX handler for callback request form
	 *
	 * @return string
	 */
	public function callbackRequest()
	{

		$this->payload = [
			'query'    => Input::get( 'cb_query' ),
			'name'     => Input::get( 'cb_name' ),
			'phone'    => Input::get( 'cb_phone' ),
			'referrer' => Input::get( 'referrer' )
		];


		if ( siteConfig( 'sid' ) == '2' )
		{
			$this->postfix = ' on Carpet Fitting Surgeon';
		}

		Mail::send( 'emails.admin.callbackRequest', $this->payload, function ( $msg ) {
			$msg->from( Config::get( 'mail.from.address' ), $this->payload['name'] )
			    ->subject( 'Callback request from ' . $this->payload['name'] . $this->postfix )
			    ->to( Config::get( 'mail.to.callback' ) );
		} );

		return 'OK';
	}

}
