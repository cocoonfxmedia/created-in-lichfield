<?php

namespace App\Http\Controllers;

use App\Models\TeamMember;
use Illuminate\Http\Request;

class TeamController extends Controller
{

	public function bio( $slug )
	{
		return view( 'components.team_bio' )->with( 'member', TeamMember::where( 'slug', $slug )->first() );
	}

}
