<?php

namespace App\Http\Controllers;

use App\Port;
use Illuminate\Http\Request;

class C02CalcController extends Controller
{

	public function air()
	{
		return view( 'components.c02calc.air' );
	}

	public function land()
	{
		return view( 'components.c02calc.land' );
	}

	//

	public function sea()
	{
		return view( 'components.c02calc.sea' );
	}

	public function single()
	{
		return view( 'single' );
	}
}
