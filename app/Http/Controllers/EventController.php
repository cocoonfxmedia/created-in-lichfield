<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreEvent;
use App\Models\ArtCategory;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\Location;
use App\Models\Artist;

final class EventController extends Controller
{
	public function index()
	{
		return view('events.index', [
			'events' => Event::active()->upcoming()->get(),
			'locations' => Location::all(),
			'artists' => Artist::all(),
		]);
	}

	public function show(Event $event)
	{
		return view('events.show', [
			'event' => $event,
			'locations' => Location::all(),
			'artists' => Artist::all(),
		]);
	}

	public function create()
	{
		return view('events.create', [
			'categories' => ArtCategory::all(),
			'eventCategories' => '[]',
			'locations' => Location::all(),
			'galleries' => Gallery::all(),
		]);
	}

	public function store(StoreEvent $request)
	{
		$event = new Event($request->all());

		if ($request->hasFile('header_image')) {
			$event->header_image = $request->file('header_image')->storePublicly('public/events');
			$event->header_image = str_replace( 'public/', '/storage/', $event->header_image );
		}

		$event->save();
		$event->artists()->save(auth('artist')->user());
		$event->categories()->sync($request->categories);

		return redirect()->route('events.show', $event);
	}

	public function edit(Event $event)
	{
		return view('events.edit', [
			'event' => $event,
			'eventCategories' => $event->categories->toJson(),
			'categories' => ArtCategory::all(),
			'locations' => Location::all(),
			'galleries' => Gallery::all(),
		]);
	}

	public function update(Event $event, StoreEvent $request)
	{
		if ($request->hasFile('header_image')) {
			$event->header_image = $request->file('header_image')->storePublicly('public/events');
			$event->header_image = str_replace( 'public/', '/storage/', $event->header_image );
		}

		$event->update($request->all());
		$event->categories()->sync($request->categories);

		return redirect()->route('events.show', $event);
	}

	public function destroy(Event $event)
	{
		$event->delete();

		return redirect()->route('events.index');
	}
}
