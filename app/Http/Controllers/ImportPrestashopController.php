<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

/**
 * Class ImportPrestashopController
 * @package App\Http\Controllers
 *

		The following database tables need to be added to the CFX hub database:
		 * ips_images
		 * ips_catgeory
		 *  ips_category_lang
		 *  ips_category_product
		 *  ips_product
		 *  ips_product_land
 *
 *      Product Category images needs to be added directly to the /public/usr/product_category/ directory
		
		Product images in the /image/p/ directory need to be added to the /storage/app/import/product_images/ directory
 *
 * 
 */
class ImportPrestashopController extends Controller {

	public function index()
	{
		$this->categories();
		$this->categoryImages();
		$this->products();
		$this->productImages();
		$this->deactiveNullImages();
	}

	/**
	 * Category import function
	 */
	public function categories()
	{
		$oldCategories = DB::table( 'ips_category_lang' )->where( 'id_lang', 1 )->get();

		foreach ( $oldCategories as $oc )
		{
			$nc = new ProductCategory();

			$nc->legacy_id   = $oc->id_category;
			$nc->name        = $oc->name;
			$nc->description = $oc->description;
			$nc->active      = 1;

			$nc->meta_title       = $oc->meta_title;
			$nc->meta_description = $oc->meta_description;

			$nc->slug = $oc->link_rewrite;

			$nc->save();
		}

		//category relationships
		$oldCategories = DB::table( 'ips_category' )->get();

		foreach ( $oldCategories as $oc )
		{
			$nc = ProductCategory::where( 'legacy_id', $oc->id_category )->first();

			//check that new category exists
			if ( ! is_null( $nc ) )
			{
				if ( $oc->id_parent == 0 )
				{
					$nc->parent_id = 0;
					$nc->save();
				}
				else
				{
					//find new parent
					$np = ProductCategory::where( 'legacy_id', $oc->id_parent )->first();

					//check the new parent was found
					if ( ! is_null( $np ) )
					{
						$nc->parent_id = $np->id;

						$nc->save();
					}
				}
			}
		}
	}

	/**
	 * Product Import Function
	 */
	public function products()
	{
		$oldProducts = DB::table( 'ips_product_lang' )->where( 'id_lang', 1 )->get();

		foreach ( $oldProducts as $op )
		{
			//stage 1 product attributes
			$np = new Product();

			$np->name             = $op->name;
			$np->description      = $op->description;
			$np->meta_title       = $op->meta_title;
			$np->meta_description = $op->meta_description;
			$np->product_code     = null;
			$np->legacy_id        = $op->id_product;
			$np->slug             = str_slug( $np->name );

			$np->category_id = 0;
			$np->price       = 0;

			$np->save();
		}


		//stage 2 product attributes
		$oldProducts = DB::table( 'ips_product' )->get();

		foreach ( $oldProducts as $op )
		{
			$np = Product::where( 'legacy_id', $op->id_product )->first();

			//check that the product exists
			if ( ! is_null( $np ) )
			{
				$np->price        = $op->price;
				$np->qty          = $op->quantity;
				$np->product_code = $op->reference;

				$np->weight      = $op->weight;
				$np->dimension_w = $op->width;
				$np->dimension_h = $op->height;
				$np->dimension_l = $op->depth;
				$np->condition   = $op->condition;

				$np->save();
			}
		}

		//product category relationships
		$relationships = DB::table( 'ips_category_product' )->get();

		foreach ( $relationships as $rel )
		{
			$product = Product::where( 'legacy_id', $rel->id_product )->first();

			if ( ! is_null( $product ) )
			{
				$category = ProductCategory::where( 'legacy_id', $rel->id_category )->first();

				if ( ! is_null( $category ) )
				{
					$product->category_id = $category->id;
					$product->save();
				}
			}
		}
	}


	public function productImages()
	{
		$images = DB::table( 'ips_image' )->get();


		foreach ( $images as $img )
		{
			$length = strlen( $img->id_image );
			$id     = [];

			for ( $x = 0; $x < $length; $x ++ )
			{
				$id[] = substr( $img->id_image, $x, 1 );
			}

			$path = base_path( 'storage/app/import/product_images' );
			foreach ( $id as $i )
			{
				$path .= '/' . $i;
			}

			$path .= '/' . $img->id_image . '.jpg';

			//check image was included as part of import dump
			if ( file_exists( $path ) )
			{
				$product = Product::where( 'legacy_id', $img->id_product )->first();

				//check product exists
				if ( ! is_null( $product ) )
				{
					rename( $path, base_path( 'public/usr/products/' . $product->id . '.jpg' ) );

					$product->image = $product->id . '.jpg';
					$product->save();
				}
				else
				{
					echo '<p style="color: red;">Cannot find product with legacy ID: ' . $img->id_product . '</p>';
				}
			}
			else
			{
				echo '<p style="color: red;">Unable to locate file ' . $path . '</p>';
			}

		}
	}


	/**
	 * Attach category images
	 */
	public function categoryImages()
	{
		$categories = ProductCategory::all();

		foreach ( $categories as $cat )
		{
			if ( file_exists( base_path( 'public/usr/product_categories/' . $cat->legacy_id . '.jpg' ) ) )
			{
				$cat->image = $cat->legacy_id . '.jpg';
				$cat->save();
			}
		}
	}

	/**
	 * Deactivate all products with null images
	 */
	public function deactiveNullImages()
	{
		$products = Product::where( 'image', null )->get();

		foreach ( $products as $p )
		{
			$p->active = 0;
			$p->save();
		}
	}

}
