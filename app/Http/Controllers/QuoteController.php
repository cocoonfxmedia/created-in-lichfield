<?php

namespace App\Http\Controllers;

use App\Mail\QuoteReceipt;
use App\Models\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class QuoteController extends Controller
{

	/**
	 * Handler for request information form
	 *
	 * @return string
	 */
	public function submit()
	{

		$data = [];
		foreach ( Input::except( [ 'qf_metric' ] ) as $key => $value )
		{
			$data[ str_replace( 'qf_', '', $key ) ] = $value;
		}

		$quote = new Quote( $data );


//		$quote->calc_volume = number_format( $quote->qty * ( ( $quote->height * $quote->width * $quote->length ) / intval( Input::get( 'qf_metric' ) ) ), 2, '.', '' );
//
//		switch ( Input::get( 'qf_metric' ) )
//		{
//			case '333':
//				$quote->transport_type = 'Road Freeght';
//				break;
//
//			case '1000':
//				$quote->transport_type = 'Sea Freight';
//				break;
//
//			case '6000':
//				$quote->transport_type = 'Road Frieght';
//				break;
//		}

		$quote->save();


		//send backoffice email
		Mail::to( 'info@fscoceans.com' )
		    ->cc( 'alan.hewitt@fscoceans.com' )
		    ->send( new \App\Mail\Quote( $quote ) );


		//send customer email
//		Mail::to( $quote->email )->send( new QuoteReceipt( $quote ) );

		return 'OK';

	}


	/**
	 * Stepped form hander
	 *
	 * @return string
	 */
	public function submit2()
	{
		$data = [];
		foreach ( Input::except( [ 'qf2_metric' ] ) as $key => $value )
		{
			$data[ str_replace( 'qf2_', '', $key ) ] = $value;
		}

		$quote = new Quote( $data );


		$quote->calc_volume = number_format( $quote->qty * ( ( $quote->height * $quote->width * $quote->length ) / intval( Input::get( 'qf2_metric' ) ) ), 2, '.', '' );

		switch ( Input::get( 'qf2_metric' ) )
		{
			case '333':
				$quote->transport_type = 'Road Freeght';
				break;

			case '1000':
				$quote->transport_type = 'Sea Freight';
				break;

			case '6000':
				$quote->transport_type = 'Road Frieght';
				break;
		}

		$quote->save();


		//send backoffice email
		Mail::to( 'info@fscoceans.com' )
		    ->cc( 'alan.hewitt@fscoceans.com' )
		    ->send( new \App\Mail\Quote2( $quote ) );


		//send customer email
		Mail::to( $quote->email )->send( new QuoteReceipt( $quote ) );

		return 'OK';
	}
}
