<?php

namespace App\Http\Controllers;

use App\Models\Basket;
use App\Models\Http\Requests;
use App\Models\Product;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class BasketController extends Controller {

	/**
	 * Add an item to the basket
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function add()
	{
		$data = Input::except( '_token' );

		return $this->injectProduct( $data );
	}

	/**
	 * Quick add product to cart
	 *
	 * @param $product_id
	 */
	public function quickAdd( $product_id )
	{
		$data = [
			'id'       => $product_id,
			'quantity' => Config::get('cms.defaultQty')
		];

		return $this->injectProduct( $data );
	}

	/**
	 * Actually add product to the cart
	 *
	 * @param $product_id
	 * @param $quantity
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	protected function injectProduct( $data )
	{
		$product = Product::find( $data['id'] );

		//set the product price
		$data['price'] = $product->rawPrice;

		if ( $product->sale_price > 0 )
		{
			$data['price'] = $product->sale_price;
		}

		//inject teh basket item
		Basket::add( $data );

		$sessionId = Input::cookie( 'trp_basket' );
		//build view data
		$product_summary = Basket::where( 'product_id', $product->id )->where( 'session_id', $sessionId )->first();

		$basket = Basket::where( 'session_id', $sessionId )->get();


		$basketSummary          = [];
		$basketSummary['count'] = 0;
		$basketSummary['total'] = 0;

		foreach ( $basket as $li )
		{
			$basketSummary['total'] += $li->price * $li->quantity;
			$basketSummary['count'] += $li->quantity;
		}

		$basketSummary['shipping'] = Config::get( 'cms.shipping.flatRate' );

		$basketSummary['grossTotal'] = $basketSummary['total'] + $basketSummary['shipping'];


		// render view
		return view( 'cart._add_item_lightbox', [
			'product'         => $product,
			'product_summary' => $product_summary,
			'basketSummary'   => $basketSummary
		] );
	}

	/**
	 * Remove an item from the basket
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function remove( $id )
	{
		Basket::removeItem( $id );

		return back();
	}

	/**
	 * Update the Qty of an order line item
	 *
	 * @return Redirect to basket
	 */
	public function changeQty()
	{

		$item           = Basket::find( Input::get( 'li_id' ) );
		$item->quantity = Input::get( 'quantity' );
		$item->save();

		return redirect()->back();
	}

	/**
	 * AJAX - get updated cart widget view
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function widget()
	{
		return view( 'cart._widget' );
	}

}