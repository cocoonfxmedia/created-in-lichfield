<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 09:59
 */

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Slide;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;

class PageController extends Controller
{


	/**
	 * Render page from database
	 *
	 * @param $slug0
	 * @param null $slug1
	 * @param null $slug2
	 * @param null $slug3
	 *
	 * @return $this
	 */
	public function view( $slug0, $slug1 = null, $slug2 = null, $slug3 = null )
	{
		// support for 4 layers of parenting
		$url = $slug0;

		if ( ! is_null( $slug1 ) )
		{
			$url .= '/' . $slug1;

			if ( ! is_null( $slug2 ) )
			{
				$url .= '/' . $slug2;

				if ( ! is_null( $slug3 ) )
				{
					$url .= '/' . $slug3;
				}
			}
		}


		$page = Page::find_by_url( $url );


		//check page is available
		if ( ! $page )
		{
			abort( 404 );
		}

		//set up breadcrumbs
		Breadcrumbs::addCrumb( $page->title, $url );

		$slides = null;
		if ( $page->slider != '0' )
		{
			$slides = Slide::where( 'sid', $page->slider )->orderBy( 'order' )->get();
		}


		//set up section nav
		$segment = \Illuminate\Support\Facades\Request::segment( 1 );

		$sectionNav = Page::where( 'sid', siteConfig('sid') )->where( 'path', $segment )->with( 'children' )->first();

//		return view( 'page_templates.' . $page->template, [ 'page' => $page, 'sectionNav' => $sectionNav->children, 'slides' => $slides ] );

		return view( 'page_templates.' . $page->template )
			->with( 'page', $page )
			->with( 'sectionNav', $sectionNav->children )
			->with( 'slides', $slides );
	}

	/**
	 * Manual route to 404 page
	 *
	 * @return view
	 */
	public function pageNotFound()
	{
		return view( 'errors.404' );
	}

}