<?php

namespace App\Http\Controllers;

use App\Mail\FormOutput;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class EmailFormController extends Controller
{

	/**
	 * Generic email form handler
	 *
	 * @return string
	 */
	public function send()
	{
		$payload = Input::except( [ '_token', 'subject', 'to' ] );
		$subject = Input::get( 'subject' );


		Mail::to( Input::get( 'to' ) )
		    ->send( new FormOutput( $subject, $payload ) );

		return 'OK';
	}
}
