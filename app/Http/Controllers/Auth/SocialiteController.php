<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use Laravel\Socialite\Facades\Socialite;

final class SocialiteController extends Controller
{
	public function __construct()
	{
		config([
			'services.facebook.redirect' => route('socialite.callback', 'facebook'),
			'services.twitter.redirect' => route('socialite.callback', 'twitter'),
			'services.instagram.redirect' => route('socialite.callback', 'instagram'),
		]);

		parent::__construct();
	}

	public function redirectToProvider(string $provider)
	{
		return Socialite::driver($provider)->redirect();
	}

	public function handleProviderCallback(string $provider)
	{
		$user = Socialite::driver($provider)->user();

		$artist = Artist::firstOrCreate(
			['provider' => $provider, 'provider_id' => $user->getId()],
			['name' => $user->getName(), 'email' => $user->getEmail()]
		);

		auth('artist')->login($artist, true);

		if ($artist->wasRecentlyCreated) {
			return redirect()->route('artists.edit', $artist);
		}

		return redirect()->to('/');
	}
}
