<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\View\View;

final class ArtistRegisterController extends Controller
{
	use RegistersUsers;

	public function showRegistrationForm(): View
	{
		return view('artists_auth.register');
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:artists',
			'password' => 'required|string|min:6|confirmed',
			'accept_terms' => 'required'
		]);
	}

	protected function create(array $data): Artist
	{
		return Artist::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);
	}

	protected function guard(): Guard
	{
		return Auth::guard('artist');
	}

	protected function redirectTo(): string
	{
		return route('artists.edit', auth('artist')->user());
	}
}
