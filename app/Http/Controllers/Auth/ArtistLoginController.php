<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

final class ArtistLoginController extends Controller
{
	use AuthenticatesUsers;

	protected $redirectTo = '/';

	public function showLoginForm(): View
	{
		return view('artists_auth.login');
	}

	protected function guard(): Guard
	{
		return Auth::guard('artist');
	}

	/**
	 * Send the response after the user was authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	protected function sendLoginResponse(Request $request)
	{
		$request->session()->regenerate();
		
		// inject the artist id into the session
		Session::put('artist_id', $this->guard()->user()->id);

		$this->clearLoginAttempts($request);

		return $this->authenticated($request, $this->guard()->user())
			? : redirect()->intended($this->redirectPath());
	}

	/**
	 * Where to redirect a logged in artist
	 *
	 * @return String
	 */
	public function redirectTo(): String
	{
		return '/artist/' . $this->guard()->user()->id . '/edit';
	}
}
