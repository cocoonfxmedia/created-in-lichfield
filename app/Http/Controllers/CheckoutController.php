<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\Request;
use App\Models\Address;
use App\Models\Basket;
use App\Models\Gateway;
use App\Models\Http\Requests;
use App\Models\Http\Requests\Add_Address;
use App\Models\Http\Requests\PaymentRequest;
use App\Models\Order;
use App\Models\Payment;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\FacadesInput;
use Illuminate\Support\FacadesSession;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\NewsletterSubscription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller {

	/**
	 * Show the basket
	 *
	 * @return $this
	 */
	public function index()
	{
		Breadcrumbs::addCrumb( 'Your shopping cart', '#' );


		$basket = Basket::fetch();


		$address = false;
		if ( ! Auth::guest() )
		{
			$address = Address::where( 'user_id', Auth::user()->id )->first();

			//force user to have an address
			if ( ! $address )
			{
				return redirect( '/my-account/addresses/create' );
			}
		}

		return view( 'checkout.page' )
			->with( 'address', $address )
			->with( 'basket', $basket['items'] )
			->with( 'total', $basket['total'] )
			->with( 'vouchers', $basket['vouchers'] );
	}


	/**
	 * Create a new account
	 */
	public function registerAccount()
	{
		//create new user
		$user = new User( Input::all() );

		$user->password = Hash::make( Input::get( 'passwd' ) );

		$user->save();

		//set up newsletter subscription
		if ( Input::get( 'newsletter' ) == '1' )
		{
			$newsletterSubscription = new NewsletterSubscription();

			$newsletterSubscription->email = $user->email;
			$newsletterSubscription->save();
		}

		//log the user in
		Auth::login( $user );


		//process address
		$address = new Address( Input::all() );

		$address->user_id = $user->id;
		$address->type    = 'shipping';
		$address->label   = 'Default';

		$address->save();


		//return to the checkout
		return redirect( '/checkout' );
	}

	/**
	 * Process Payment
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function processPayment( CheckoutRequest $request )
	{
		//set the address for this order
		$address     = Address::where( 'user_id', Auth::user()->id )->first();
		$address_ids = [ $address->id, $address->id ];

		$order = Order::createOrder( Basket::fetch(), $address_ids );

		$order->message = Input::get( 'message' );
		$order->save();

		//clean up the current basket
//		Basket::cleanup();


		//Generate Payment User Journey
		switch ( Input::get( 'pay_method' ) )
		{
			case 'paypal':
				return $this->paypal( $order );
				break;

			case 'worldpay':
				return $this->worldpay( $order );
				break;
		}

	}

	/**
	 * Generate paypal user journey
	 *
	 * @param $order
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	protected function paypal( $order )
	{
		/*
		 * Build PayPal URL
		 */
		$pp_url = 'https://www.paypal.com/cgi-bin/webscr';

		if ( strtoupper( Config::get( 'paypal.env' ) ) == 'SANDBOX' )
		{
			$pp_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		}

		$pp_url .= '?cmd=_xclick&business=' . Config::get( 'paypal.merchant_email' );

		$pp_url .= '&item_name=' . Config::get( 'app.name' ) . ' Order No ' . $order->id;
		$pp_url .= '&item_number=' . $order->id;
		$pp_url .= '&amount=' . $order->order_value;
		$pp_url .= '&shipping=' . $order->shipping;
		$pp_url .= '&currency_code=GBP&quantity=1&no_note=1';

		$pp_url .= '&return=' . url( 'checkout/complete/' . $order->id );
		$pp_url .= '&notify_url=' . url( 'hook/paypal-ipn' );


		//redirect the user to paypal standard
		return redirect( $pp_url );
	}


	/**
	 * Generate World Pay user journey
	 *
	 * @param $order
	 *
	 * @return $this
	 */
	protected function worldpay( $order )
	{
		$url = 'https://secure.worldpay.com/wcc/purchase?';

		//set the test service destination
		if( strtoupper(Config::get('worldpay.env')) != 'LIVE')
		{
			$url = 'https://secure-test.worldpay.com/wcc/purchase?';
		}


		$url .= 'instId=' . Config::get( 'worldpay.installation_id' );

		$orderTotal = $order->order_value + $order->shipping;

		$url .= '&amount=' . $orderTotal;

		$url .= '&cartId=' . $order->id;



		$url .= '&currency=GBP';
		$url .= '&desc=' . urlencode(Config::get( 'app.name' ) . ' Order No ' . $order->id );

		$url .= '&testMode=' . ( ( strtoupper( Config::get( 'worldpay.env' ) ) != 'LIVE' )?  '100' : '0' );
		return redirect( $url );
	}


	/**
	 * Display order complete message to customer
	 *
	 * @param type $order_id
	 *
	 * @return View
	 */
	public function paymentComplete( $order_id )
	{
		Breadcrumbs::addCrumb( 'Your shopping cart', '/checkout' );
		Breadcrumbs::AddCrumb( 'Order Processing', '#' );

		$order = Order::with( 'customer' )->find( $order_id );

		Basket::cleanup();

		/**
		 * Handle PayPal post data
		 */
		if ( $order->pay_method == 'paypal' )
		{
			$paymentMessage = new Payment();

			$paymentMessage->order_id = $order->id;
			$paymentMessage->provider = 'paypal';
			$paymentMessage->msg_type = 'checkout';
			$paymentMessage->payload  = json_encode( $_POST );
			$paymentMessage->status   = 'pending';

			$paymentMessage->save();
		}

		return view( 'checkout.complete', [ 'order' => $order ] );
	}


}
