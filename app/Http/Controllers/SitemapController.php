<?php

namespace App\Http\Controllers;

use App\Models\BlogArticle;
use App\Models\CaseStudy;
use App\Models\Manufacturer;
use App\Models\ProductCategory;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;

class SitemapController extends Controller
{

	/**
	 * Display user friendly sitemap
	 *
	 * @return Response
	 */
	public function index()
	{
		$manufacturers = Manufacturer::active()->orderBy( 'name', 'ASC' )->get();

		return view( 'sitemap.html' )
			->with( 'categories', ProductCategory::active()->rootCategories()->get() )
			->with( 'pages', Page::where( 'sid', siteConfig( 'sid' ) )->where( 'published', 1 )->get() )
			->with( 'blogArticles', BlogArticle::published()->where( 'sid', siteConfig( 'sid' ) )->get() )
			->with( 'caseStudies', CaseStudy::published()->where( 'sid', siteConfig( 'sid' ) )->get() );
	}

	/**
	 * Display search engine friendly sitemap
	 *
	 * @return Reposnse
	 */
	public function xml()
	{
		$contents = View::make( 'sitemap.xml' )
		                ->with( 'categories', ProductCategory::active()->with( 'products' )->get() )
		                ->with( 'pages', Page::where( 'published', 1 )->where( 'sid', siteConfig( 'sid' ) )->get() )
		                ->with( 'blogArticles', BlogArticle::published()->where( 'sid', siteConfig( 'sid' ) )->get() )
		                ->with( 'caseStudies', CaseStudy::published()->where( 'sid', siteConfig( 'sid' ) )->get() );

		$response = Response::make( $contents, 200 );
		$response->header( 'Content-Type', 'text/xml' );

		return $response;
	}

}
