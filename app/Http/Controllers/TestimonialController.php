<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
	/**
	 * Testimonials page
	 *
	 * @return mixed
	 */
	public function index()
	{
		$testimonials = Testimonial::published()
		                           ->where( 'sid', siteConfig( 'sid' ) )
		                           ->orderBy( 'created_at', 'DESC' )
		                           ->get();

		return view( 'testimonials.index' )
			->with( 'testimonials', $testimonials );
	}


	/**
	 * Individual testimonial
	 *
	 * @param $url
	 *
	 * @return $this
	 */
	public function view( $url )
	{
		$testimonial = Testimonial::where( 'slug', $url )->published()->first();

		if ( ! $testimonial )
		{
			abort( 404 );
		}

		return view( 'testimonials.article' )
			->with( 'testimonial', $testimonial );
	}


}
