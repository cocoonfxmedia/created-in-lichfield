<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CurrencyConverterController extends Controller
{
	public function index()
	{

		$from   = urlencode( Input::get( 'cur_from' ) );
		$to     = urlencode( Input::get( 'cur_to' ) );
		$amount = urlencode( Input::get( 'cur_amount' ) );

		$locale = $from . '_' . $to;

		$url      = 'https://free.currencyconverterapi.com/api/v5/convert?q=' . $locale . '&compact=y';
		$response = json_decode( file_get_contents( $url ) );


		return number_format( $response->$locale->val * floatval( $amount ), 2 );;


	}
}
