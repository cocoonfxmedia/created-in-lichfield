<?php

namespace App\Http\Controllers\webhooks;

use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class WorldpayPaymentMessageController extends Controller {

	/**
	 * @var \App\Models\Order
	 */
	protected $order;


	public function handle()
	{
		/*
		 * Validate message is for this site
		 */
		if ( Config::get( 'worldpay.installation_id' ) != Input::get( 'instId' ) )
		{
			//Invalid message handler
			Log::info( 'WordPay payment notification received for installation ID ' . Input::get( 'instId' ) . ' which does not belong to this site' );

			return;
		}

		/*
		 * ID the order this message relates to
		 */

		$this->order = Order::find( Input::get( 'cartId' ) );

		if ( ! $this->order )
		{
			Log::info( 'Unable to find order #' . Input::get( 'cartId' ) . ' for Worldpay transaction ID ' . Input::get( 'transId' ) );

			//notify admin
			//TODO::notify admin on worldpay failure to locate the order

			//log the notification message
			$this->createPayment( ( ( strtoupper( Input::get( 'transStatus' ) ) == 'Y' ) ? 'success' : 'failed' ) );

			return;
		}


		/*
		 * Handle payment transaction status
		 */
		$paymentStatus = 'pending';

		if ( strtoupper( Input::get( 'transStatus' ) ) == 'Y' )
		{
			$paymentStatus = 'success';

			Log::info( 'Payment for order #' . Input::get( 'cartId' ) . ' has been processed successfully' );

			/*
			 * Stage order on for fulfillment
			 */
			$this->order->status = 'paid';
			$this->order->save();

			$this->order->triggerFulfillment();

		}
		else
		{
			$paymentStatus = 'failed';

			Log::info( 'Payment for order #' . Input::get( 'cartId' ) . ' has NOT been processed successfully' );
		}


		/*
		 * Log the payment message
		 */
		$this->createPayment( $paymentStatus );

	}

	/**
	 * Generate payment object
	 *
	 * @param $paymentStatus
	 */
	protected function createPayment( $paymentStatus )
	{
		$paymentMessage           = new Payment();
		$paymentMessage->payload  = json_encode( $_POST );
		$paymentMessage->provider = 'worldpay';
		$paymentMessage->msg_type = 'payment_notification';
		$paymentMessage->order_id = $_POST['cartId'];
		$paymentMessage->status   = $paymentStatus;

		$paymentMessage->save();
	}
}
