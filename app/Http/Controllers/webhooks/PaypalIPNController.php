<?php

namespace App\Http\Controllers\webhooks;

use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use App\Models\Order;
use Fahim\PaypalIPN\PaypalIPNListener;

class PaypalIPNController extends Controller {


	protected $order;

	public function handle()
	{
		//usage: https://github.com/sh0umik/Laravel5-PaypalIPN

		$test_override = false;

		if ( ! $test_override )
		{
			$ipn              = new PaypalIPNListener();
			$ipn->use_sandbox = false;


			if ( $_POST['receiver_email'] !== Config::get( 'paypal.merchant_email' ) )
			{
				//this message isn't meant for us
				return;
			}

//			$verified = $ipn->processIpn();
			$verified = true;

			$report = $ipn->getTextReport();


			Log::info( "-----new payment-----" );
			Log::info( $report );

			$order_id     = $_POST['item_number1'];
			$order_status = $_POST['payment_status'];


			/*
			 * Log payment message
			 */

			$paymentMessage           = new Payment();
			$paymentMessage->payload  = json_encode( $_POST );
			$paymentMessage->provider = 'paypal';
			$paymentMessage->msg_type = 'ipn';
			$paymentMessage->order_id = $_POST['item_number1'];

			$paymentMessage->save();

		}
		else
		{
			//testing overrides
			$order_status = 'Completed';
			$order_id     = 7;
			$verified     = true;
		}


		$this->order = Order::findOrFail( $order_id );

		//check if order already processed
		if ( $this->order->status != 'pending' )
		{
			Log::info( 'Order ' . $this->order->id . ' has been processed passed payment phase' );

			return;
		}

		if ( $verified )
		{
			switch ( $order_status )
			{
				//Payment sucessful
				case 'Completed':
				case 'Processed':

					$this->order->status = 'paid';

					$this->order->triggerFulfillment();


					Log::info( 'Successful payment processed' );

					$paymentMessage->status = 'success';

					break;


				//payment failed
				case 'Denied':
				case 'Expired':
				case 'Failed':
				case 'Voided':

					$this->order->status = 'payment failed';

					//send email to customer
					Mail::send(
						'emails.paymentFailed', [ 'order' => $this->order ], function ( $msg )
					{
						$msg->to( $this->order->customer->email, $this->order->customer->name )
						    ->subject( 'Order No ' . $this->order->id . ' - Payment Failed' )
						    ->from( Config::get( 'mail.from.address.', Config::get( 'mail.from.name' ) ) );
					} );

					Log::info( 'Failed payment processed' );

					$paymentMessage->status = 'failed';

					break;
			}

			$paymentMessage->save();
		}
		else
		{
			$this->order->status = 'payment error';

			//Send email to customer
			Mail::send(
				'emails.paymentError', [ 'order' => &$this->order ], function ( $msg )
			{
				$msg->to( $this->order->customer->email, $this->order->customer->name )
				    ->subject( 'Order No ' . $this->order->id . ' - Payment Error' )
				    ->from( Config::get( 'mail.from.address', Config::get( 'mail.from.name  ' ) ) );
			} );

			Log::info( 'There was an issue processing this payment' );
		}

		$this->order->save();
	}
}
