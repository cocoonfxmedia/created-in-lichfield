<?php

namespace App\Http\Controllers;

use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;

class OrderHistoryController extends Controller
{

	public function index()
	{
		Breadcrumbs::addCrumb('My Account', '/my-account');
		Breadcrumbs::addCrumb('Order History', '#');

		return view('account.orders.no_orders');
	}
}
