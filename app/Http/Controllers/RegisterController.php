<?php

namespace App\Http\Controllers;

use App\Models\NewsletterSubscription;
use App\Models\User;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests;

class RegisterController extends Controller {

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		Breadcrumbs::addCrumb( 'Authentication', '/login' );
		Breadcrumbs::addCrumb( 'Register', '/register' );


		return view( 'account.register' );
	}

	/**
	 * Create new user
	 */
	public function register()
	{
		$user = new User( Input::all() );

		$user->password = Hash::make( Input::get( 'passwd' ) );

		$user->save();

		//set up newsletter subscription
		if ( Input::get( 'newsletter' ) == '1' )
		{
			$newsletterSubscription = new NewsletterSubscription();

			$newsletterSubscription->email = $user->email;
			$newsletterSubscription->save();
		}

		//log the user in
		Auth::login( $user );

		return redirect( '/my-account/addresses/create' );
	}
}
