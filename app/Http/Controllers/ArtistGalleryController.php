<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artist;
use App\Models\Gallery;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use App\Models\GalleryImage;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreGallery;

class ArtistGalleryController extends Controller
{
    public function edit(Artist $artist)
    {
        return view('galleries.edit',
        [ 
            'artist' => $artist,
            'gallery' => $artist->gallery()->with('images')->first() ?: new Gallery(),
        ]);
    }

    public function update(Artist $artist, StoreGallery $request )
    {
        $gallery = $artist->gallery ? : new Gallery();

        $gallery->name = $artist->name . ' - Gallery';
        $gallery->description = $request->get('description');

        // first time out.
        if ($gallery->exists) {
            if ($request->has('slug')) {
                $gallery->slug = str_slug($gallery->name);
            } else {
                $gallery->slug = str_slug($request->get('slug'));
            }

            $gallery->sid = siteConfig('id');
        }

        //set up gallery filespace
        $directory = public_path("usr/gallery/{$gallery->id}");
        if (!is_dir($directory)) {
            mkdir($directory);
            chmod($directory, 1755);
        }

        $gallery->save();
        $artist->gallery()->associate($gallery)->save();

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $this->storeImage($image, $gallery);
            }
        }

        return redirect()->route('artists.show', $artist);
    }

    public function destroyImage(Artist $artist, int $id)
    {
        GalleryImage::findOrFail($id)->delete();
        
        return redirect()->route('artists.gallery.edit', $artist);
    }

    private function storeImage(UploadedFile $upload, Gallery $gallery)
    {
        $image = new GalleryImage();
        $upload->storePublicly('usr/gallery/tmp');

        $finalDestination = 'usr/gallery/' . $gallery->id . '/' . $upload->hashName();
        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

        // Backend doesn't use standard storage folder so, have to move the file for consistency.
        rename($storagePath.'usr/gallery/tmp/'. $upload->hashName(), public_path($finalDestination));
        chmod(public_path($finalDestination), 0644);
        
        $image->gallery = $gallery->id;
        $image->title = $upload->getClientOriginalName();
        $image->filename = $upload->hashName();
        $image->save();
    }
}
