<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;

class ProductCategoryController extends Controller {

	protected $breadcrumbs = [];

	/**
	 * Product Category Views
	 *
	 * @param $slug
	 *
	 * @return $this
	 */
	public function show( $category_id, $slug )
	{
		$category = ProductCategory::with( 'children' )->with( 'parent' )->find( $category_id );

		if ( ! $category )
		{
			//url does not link to category
			abort( 404 );
		}

		/*
		 * Products
		 */
		$productCount = $category->totalProducts( $category );

		if ( Input::get( 'v' ) == 'all' )
		{
			$products = Product::active()->where( 'category_id', $category->id )->orderBy( 'page_order', 'ASC' )->get();
		}
		else
		{
			$products = Product::active()->where( 'category_id', $category->id )->orderBy( 'page_order', 'ASC' )->get();
		}

		/*
		 * Breacrumbs
		 */
		$this->breadcrumbTrail( $category );
		$this->breadcrumbs = array_reverse( $this->breadcrumbs ); //flip the crumbs into correct order

		foreach ( $this->breadcrumbs as $crumb )
		{
			Breadcrumbs::addCrumb( $crumb->name, '/category/'. $crumb->slug );
		}

		/**
		 * View
		 */
		return view( 'product.category' )
			->with( 'category', $category )
			->with( 'productCount', $productCount )
			->with( 'products', $products );
	}

	/**
	 * Interate over the parent categories
	 *
	 * @param $category
	 */
	protected function breadcrumbTrail( $category )
	{
		if ( ! is_null( $category ) )
		{
			$this->breadcrumbs[] = $category;

			$this->breadcrumbTrail( $category->parent );
		}
	}
}
