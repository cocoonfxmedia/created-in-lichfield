<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class C02ProxyController extends Controller
{

	private $endpoint = '';

	public function airList( Request $request )
	{
		// http://distances.com/airlist/?searchText=heathro&maxResults=1000

		$this->endpoint = 'http://distances.com/airlist/';

		return $this->send( $request );
	}

	public function airRoute( Request $request )
	{
		// http://distances.com/airroute/?long1=-0.461941&lat1=51.4706&long2=8.5491695404053&lat2=47.464698791504

		$this->endpoint = 'http://distances.com/airroute/';

		return $this->send( $request );
	}


	public function seaList( Request $request )
	{
		// http://distances.com/list/?searchText=port&maxResults=1000

		$this->endpoint = 'http://distances.com/list/';

		return $this->send( $request );
	}


	public function seaRoute( Request $request )
	{
		// http://distances.com/route/?long1=138.5&lat1=-34.85&long2=-3&lat2=53.416667

		$this->endpoint = 'http://distances.com/route/';

		return $this->send( $request );
	}


	protected function send( $request )
	{

		$url = $this->endpoint . str_replace( $request->url(), '', $request->fullUrl() );

		$opts = array(
			'http' => array(
				'method' => "GET",
				'header' => "X-Requested-With: XMLHttpRequest\r\n"
			)
		);

		$context = stream_context_create( $opts );

		return response()->json( json_decode( file_get_contents( $url, false, $context ) ) );
	}
}
