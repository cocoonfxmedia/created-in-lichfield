<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\StoreLocation;
use App\Models\ArtCategory;
use App\Models\Gallery;
use App\Models\Location;

final class LocationController extends Controller
{
	public function index()
	{
		return view('locations.index', [
			'locations' => Location::all(),
		]);
	}

	public function show(Location $location)
	{
		return view('locations.show', [
			'location' => $location,
		]);
	}

	public function create()
	{
		return view('locations.create', [
			'location_categories' => '[]',
			'categories' => ArtCategory::all(),
			'galleries' => Gallery::all(),
		]);
	}

	public function store(StoreLocation $request)
	{
		$location = new Location($request->all());

		if ($request->hasFile('header_image')) {
			$location->header_image = $request->file('header_image')->storePublicly('public/locations');
			$location->header_image = str_replace( 'public/', '/storage/', $location->header_image );
		}

		$location->save();
		$location->artists()->save(auth('artist')->user());
		$location->categories()->sync($request->categories);

		return redirect()->route('locations.show', $location);
	}

	public function edit(Location $location)
	{
		return view('locations.edit', [
			'location' => $location,
			'location_categories' => $location->categories->toJson(),
			'categories' => ArtCategory::all(),
			'galleries' => Gallery::all(),
		]);
	}

	public function update(Location $location, StoreLocation $request)
	{
		$location->fill($request->all());

		if ($request->hasFile('header_image')) {
			$location->header_image = $request->file('header_image')->storePublicly('public/locations');
			$location->header_image = str_replace( 'public/', '/storage/', $location->header_image );
		}

		$location->save();
		$location->categories()->sync($request->categories);

		return redirect()->route('locations.show', $location);
	}

	public function destroy(Location $location)
	{
		$location->delete();

		return redirect()->to('/');
	}
}
