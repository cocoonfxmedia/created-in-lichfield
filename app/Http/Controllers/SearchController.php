<?php

namespace App\Http\Controllers;

use App\Models\BlogArticle;
use App\Models\Page;
use App\Models\ProductCategory;
use App\Models\Product;
use Creitive\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Manufacturer;

class SearchController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$searchString = Input::get( 'q' );

		if ( ! Input::get( 'q' ) )
		{
			$categoryList     = [];
			$productList      = [];
			$manufacturerList = [];
			$pages            = [];
			$articles         = [];
			$multisiteMap     = [];
		} else
		{

			/*
			 * ecommerce sarch results
			 */
//			$categoryList     = ProductCategory::where( 'name', 'like', '%' . $searchSring . '%' )->get();
//			$productList      = Product::where( 'name', 'like', '%' . $searchSring . '%' )
//			                           ->orWhere( 'product_code', 'like', '%' . $searchSring . '%' )
//			                           ->get();
//			$manufacturerList = Manufacturer::where( 'name', 'like', '%' . $searchSring . '%' )->get();

			$categoryList     = [];
			$productList      = [];
			$manufacturerList = [];


			//pages
			$pages = Page::where( 'published', 1 )
			             ->where( 'sid', siteConfig( 'sid' ) )
			             ->where( 'title', 'like', '%' . $searchString . '%' )
			             ->get();


			//blog articles
			$articles = BlogArticle::where( 'title', 'like', '%' . $searchString . '%' )
			                       ->where( 'published', 1 )
			                       ->where( 'sid', siteConfig( 'sid' ) )->get();


		}


		$resultCount = count( $categoryList ) + count( $productList ) + count( $manufacturerList ) + count( $pages ) + count( $articles );

		//single category found
		if ( count( $categoryList ) == 1 && $resultCount == 1 )
		{
			return redirect( '/category/' . $categoryList[0]->slug );
		}

		//single product found
		if ( count( $productList ) == 1 && $resultCount == 1 )
		{
			redirect( '/product/' . $productList[0]->slug );
		}

		//single manufacturer found
		if ( count( $manufacturerList ) == 1 && $resultCount == 1 )
		{
			redirect( '/manufacturer/' . $manufacturerList[0]->slug );
		}

		//breadcrumbs
		Breadcrumbs::addCrumb( 'Search', '#' );


		//manufacturers widget
		if ( config( 'cms.components.ecommerce' ) )
		{
			$manufacturers = Manufacturer::active()->orderBy( 'name', 'ASC' )->get();
		} else
		{
			$manufacturers = [];
		}


		return view( 'search' )
			->with( 'products', $productList )
			->with( 'categoryList', $categoryList )
			->with( 'maufacturerList', $manufacturerList )
			->with( 'resultCount', $resultCount )
			->with( 'manufacturers', $manufacturers )
			->with( 'pages', $pages )
			->with( 'articles', $articles )
			->with( 'searchString', $searchString );
	}

}