<?php

namespace App\Http\Controllers;

use App\Models\BlogArticle;
use App\Models\Page;
use App\Models\Event;
use App\Models\Artist;
use App\Models\Location;
use App\Models\Product;
use App\Models\Slide;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

//			if ( ! ( session( 'site' ) ) )
//			{
//				$viewFile = 'home.default';
//			} else
//			{
//				$slug = session( 'site' )['slug'];
//
//				if ( $slug == 'manor-trust' )
//				{
//					$slug = 'default';
//				}
//
//				$viewFile = 'home.' . $slug;
//			}

		$page = Page::where( 'is_homepage', 1 )->where('sid', siteConfig('sid'))->first();

		$event = Event::active()->upcoming()->take(4)->get();

		$locations = Location::all();

		$artists = Artist::active()->take(4)->get();


//		$hpBlocks = Page::where( 'block_link', '>', '' )->where( 'sid', siteConfig('sid') )->get();

//		$homepageBlocks = [];
//
//		foreach ( $hpBlocks as $block )
//		{
//			$homepageBlocks[ $block->block_link ] = $block;
//		}


		return view( 'page_templates.' . $page->template )
			->with('page', $page )
			->with('locations', $locations )
			->with('artists', $artists )
			->with('events', $event);
//			->with( 'hpBlocks', $homepageBlocks );

	}
}
