<?php

namespace App\Listeners;

use App\Events\MSCalendarPull;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MS365CalendarPuller
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MSCalendarPull  $event
     * @return void
     */
    public function handle(MSCalendarPull $event)
    {

	    foreach ( config( 'multisite.sites' ) as $site )
	    {

		    $username = $site['365_username'];
		    $password = $site['365_password'];
		    $URL      = 'https://outlook.office365.com/api/v1.0/users/' . $site['365_username'] . '/events';

		    if ( ! empty( $username ) && ! empty( $password ) )
		    {

			    $ch = curl_init();
			    curl_setopt( $ch, CURLOPT_URL, $URL );
			    curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
			    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			    curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY );
			    curl_setopt( $ch, CURLOPT_USERPWD, "$username:$password" );

			    $status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
			    $result      = json_decode( curl_exec( $ch ) );
			    curl_close( $ch );

			    if ( is_null( 'result' ) )
			    {
				    //unable to poll the graph

			    }
			    else
			    {


				    if ( ! isset( $result->value ) )
				    {
					    //failed to fetch the feed
					    //TODO::handle fetch error
					    $code    = $result->error->code;
					    $message = $result->error->message;

					    dd( $result );
				    }
				    else
				    {
					    $events = [];
					    foreach ( $result->value as $event )
					    {
						    //format the date
						    if ( $event->IsAllDay )
						    {
							    $event->DisplayStart = date( 'F j', strtotime( $event->Start ) );
						    }
						    else
						    {
							    $event->DisplayStart = date( 'F j g:ia', strtotime( $event->Start ) );
							    $event->DisplayEnd   = date( 'F j g:ia', strtotime( $event->End ) );
						    }

						    $events[] = $event;

					    }


					    //cache data feed to a local file
					    if ( ! file_exists( storage_path( 'calendars/site_' . $site['sid'] ) ) )
					    {
						    touch( storage_path( 'app/calendars/site_' . $site['sid'] ) );
					    }
					    file_put_contents( storage_path( 'app/calendars/site_' . $site['sid'] ), json_encode( $events ) );

				    }
			    }
		    }
	    }

    }
}
