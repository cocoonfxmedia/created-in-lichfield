<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CallBackFormMessage extends Mailable
{
	use Queueable, SerializesModels;


	private $payload;


	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct( $payload )
	{
		$this->payload = $payload;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from( siteConfig( 'email_from' ) )
		            ->replyTo( $this->payload['email'], $this->payload['name'] )
		            ->subject( 'Enquiry from ' . $this->payload['name'] )
		            ->view( 'emails.admin.callbackRequest' )->with( $this->payload );
	}
}
