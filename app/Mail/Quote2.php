<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Quote2 extends Mailable
{
	use Queueable, SerializesModels;

	public $quote;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct( \App\Models\Quote $quote )
	{
		$this->quote = $quote;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from( $this->quote->email, $this->quote->name . ' @ ' . $this->quote->company )
		            ->subject( 'New quote request from website' )
		            ->view( 'emails.admin.quote_request2' );
	}
}
