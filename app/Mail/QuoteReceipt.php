<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuoteReceipt extends Mailable
{
	use Queueable, SerializesModels;

	public $quote;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct( \App\Models\Quote $quote )
	{
		$this->quote = $quote;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from( config( 'settings.email_from', 'settings.email_from_name' ) )
		            ->subject( 'Your Quote' )
		            ->view( 'emails.quote_reciept' );
	}
}
