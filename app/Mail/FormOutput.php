<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormOutput extends Mailable
{
	use Queueable, SerializesModels;

	public $payload;
	public $subject;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(  string $subject,  array $payload )
	{
		$this->payload = $payload;
		$this->subject = $subject;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->subject( $this->subject )
		            ->view( 'emails.generic_form', [ 'payload' => $this->payload ] );
	}
}
