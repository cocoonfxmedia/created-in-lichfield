<?php

namespace App\Providers;

use App\Models\Artist;
use App\Models\Event;
use App\Models\Location;
use App\Policies\ArtistPolicy;
use App\Policies\EventPolicy;
use App\Policies\LocationPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Artist::class => ArtistPolicy::class,
        Event::class => EventPolicy::class,
        Location::class => LocationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
