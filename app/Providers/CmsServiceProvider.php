<?php namespace App\Providers;

use App\Models\Block;
use App\Models\BlockPanel;
use App\Models\Course;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\ProductCategory;
use App\Models\Slide;
use App\Models\Slider;
use Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		/*
		 * Category Catalog inject
		 *
		 */

		$catalog = ProductCategory::active()->root()->orderBy( 'name', 'ASC' )->with( 'children' )->get();
		View::share( 'catalog_categories', $catalog );


		/**
		 * Menu Directive
		 */
		Blade::directive( 'menu', function ( $menu ) {

			$menu = str_replace( "('", '', $menu );
			$menu = str_replace( "')", '', $menu );

			$output = '<?php' . PHP_EOL;

			$output .= '$nav = \App\Models\MenuItem::root()->where( \'menu\', \'=\', \'' . $menu . '\' )->where( \'sid\', siteConfig(\'sid\') )->with( \'children\' )->orderBy( \'order\', \'ASC\' )->get();' . PHP_EOL;

			$output .= 'echo generateNav( $nav );' . PHP_EOL;

			$output .= '?>' . PHP_EOL;

			return $output;
		} );

		/*
		 * Gallery Directive
		 */
		Blade::directive( 'gallery', function ( $gallerySlug ) {

			$gallerySlug = str_replace( "('", '', $gallerySlug );
			$gallerySlug = str_replace( "')", '', $gallerySlug );

			$gallerySlug = str_replace( "'", '', $gallerySlug );

			$gallery = Gallery::where( 'slug', $gallerySlug )->where( 'published', 1 )->where( 'sid', siteConfig('id') )->first();



			$output = '';

			if ( $gallery )
			{
				$images = GalleryImage::where( 'gallery', $gallery->id )->orderBy( 'order' )->get();

				$output = view( 'components.gallery' )->with( 'gallery', $images )->render();

			}

			return $output;
		} );

		/*
		 * Block Directive
		 */
		Blade::directive( 'block', function ( $blockSlug ) {
			$blockSlug = str_replace( "'", '', $blockSlug );

			$output = '';

			$block = Block::where( 'published', 1 )->where( 'slug', $blockSlug )->first();


			if ( $block )
			{
				$panels = BlockPanel::where( 'block', $block->id )->orderBy( 'order' )->get();


				//split planels into 2 columns
				$panels1 = [];
				$panels2 = [];
				foreach ( $panels as $i => $p )
				{
					if ( $i % 2 == 0 )
					{
						$panels1[] = $p;
					} else
					{
						$panels2[] = $p;
					}
				}


				$panels = [ $panels1, $panels2 ];


				$output = view( 'components.blocks' )->with( 'panels', $panels )->render();
			}

			return $output;

		} );

		/*
		 * Course Booking Form Directive
		 */
		Blade::directive( 'BookCourseForm', function ( $attr ) {
			$courses = Course::where( 'sid', session( 'site' )['sid'] )->orderBy( 'order' )->get();

			$courseList = [];
			foreach ( $courses as $c )
			{
				$courseList[ $c->name ] = $c->name;
			}

			return view( 'components.bookCourseFrm', [ 'courses' => $courseList ] )->render();

		} );


		Blade::directive( 'slider', function ( $slider ) {

			$slides = [];

			if ( is_int( $slider ) )
			{
				$slides = Slide::where( 'sid', $slider )->orderBy( 'order' )->get();
			} else
			{
				$slider = Slider::where( 'name', $slider )->first();

				if ( ! is_null( $slider ) )
				{
					$slides = Slide::where( 'sid', $slider->id )->orderBy( 'order' )->get();
				}
			}

			if ( count( $slides ) == 0 )
			{
				return '';
			}

			$output = '';

			if ( count( $slides ) > 0 )
			{

				$output .= '<div class="slider" style="width: 100%;"><ul>';

				foreach ( $slides as $slide )
				{
					$output .= '<li>';

					if ( ! empty( $slide->link ) )
					{
						$output .= '<a href="' . $slide->link . '">';
					}

					if ( ! empty( $slide->image ) )
					{
						$output .= ' <img src="/usr/dip/' . $slide->image . '" alt=""/>';
					}

					if ( ! empty( $slide->text ) )
					{
						$output .= ' <h1>' . $slide->text . '</h1>';
					}

					if ( ! empty( $slide->link ) )
					{
						$output .= '</a>';
					}


					$output .= '</li>';
				}

				$output .= '</ul></div>';




			}


			return $output;
		} );

	}


	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind( 'Illuminate\Contracts\Auth\Registrar', 'Elem\Services\Registrar' );
	}

}
