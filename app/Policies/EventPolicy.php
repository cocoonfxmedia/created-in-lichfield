<?php

declare(strict_types=1);

namespace App\Policies;

use App\Models\Artist;
use App\Models\Event;

final class EventPolicy
{
	public function create(Artist $artist): bool
	{
		return $artist->can_manage_events;
	}

	public function manage(Artist $artist, Event $event): bool
	{
		return $artist->can_manage_events && $event->artists->contains($artist);
	}
}
