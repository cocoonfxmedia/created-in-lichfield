<?php

declare(strict_types=1);

namespace App\Policies;

use App\Models\Artist;
use App\Models\Location;

final class LocationPolicy
{
	public function create(Artist $artist): bool
	{
		return $artist->can_manage_locations;
	}

	public function manage(Artist $artist, Location $location): bool
	{
		return $artist->can_manage_locations && $location->artists->contains($artist);
	}
}
