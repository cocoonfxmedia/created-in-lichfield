<?php

declare(strict_types=1);

namespace App\Policies;

use App\Models\Artist;
use Illuminate\Support\Facades\Request;

final class ArtistPolicy
{
	public function manage(Artist $artist, Artist $target): bool
	{
		return $artist->is($target);
	}

	public function deleteGalleryImage(Artist $artist, $target): bool
	{
		$id = (int) Request::route('id');
		return isset($target->gallery) && $target->gallery->images->contains('id', $id);
	}
}
