<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $fillable = [
    	'name'
    ];

    public $timestamps = false;


}
