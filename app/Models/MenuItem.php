<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItem extends Model
{

	use SoftDeletes;

	/**
	 * Mass assigned attributes
	 *
	 * @var array
	 */
	public $fillable = [
		'destination',
		'type',
		'label',
		'menu'
	];


	/**
	 * Only show active menu items
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeActive( $query )
	{
		return $query->where( 'active', '=', 1 );
	}

	/**
	 * Page linked to menu item
	 *
	 * @return mixed
	 */
	public function page()
	{
		return $this->hasOne( 'App\Models\Page', 'id', 'destination' );
	}

	/**
	 * Product Category linked to menu item
	 *
	 * @return mixed
	 */
	public function productCategory()
	{
		return $this->hasOne( 'App\Models\ProductCategory', 'id', 'destination' );
	}

	/**
	 * Show only root items
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeRoot( $query )
	{
		return $query->where( 'parent', '=', '0' );
	}

	/**
	 * Get child menu items
	 *
	 * @return mixed
	 */
	public function children()
	{
		return $this->hasMany( 'App\Models\MenuItem', 'parent', 'id' )->orderBy('order', 'ASC');
	}

}
