<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 10:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseStudy extends Model
{

	use SoftDeletes;

	public $table = 'case_studies';

	/**
	 * Unguarded attributes
	 * @var array
	 */
	public $fillable = [
		'title',
		'content',
		'meta_title',
		'meta_description',
		'published',
		'url',
		'category_id'
	];

	/**
	 * Get blog article by URL
	 *
	 * @param $url
	 *
	 * @return Page
	 */
	static public function find_by_url( $url )
	{
		$page = CaseStudy::where( 'url', '=', $url )->where( 'published', '=', 1 )->first();


		if ( ! $page )
		{
			return false;
		}

		if ( empty( $page->meta_title ) )
		{
			$page->meta_title = $page->title;
		}

		return $page;

	}


	/**
	 * Published articles scope
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePublished( $query )
	{
		return $query->where( 'published', '=', 1 );
	}

	/**
	 * Get article summary
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getSummaryAttribute( $value )
	{
		if ( ! empty( $this->attributes['meta_description'] ) )
		{
			$summary = $this->attributes['meta_description'];
		} else
		{
			$summary = strip_tags( $this->attributes['content'] );
		}

		return str_limit( $summary, 200 );
	}

	/**
	 * Get related case studies
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo( 'App\Models\CaseStudyCategory' );
	}

	/**
	 * Get the URL link for a case study
	 *
	 * @param $value
	 *
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	public function getLinkAttribute( $value )
	{
		return url( config( 'cms.case-studies.url' ) . '/' . $this->attributes['url'] );
	}


}