<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColours extends Model {

	/**
	 * @param $data
	 */
	public static function createColour( $data )
	{

		if ( ! $data['name'] && ! $data['hex'] ){
			return false;
		}

	    $c = new ProductColours;

		$c->product_id = $data['product_id'];
		$c->name = $data['name'];
		$c->hex	= $data['hex'];

		$c->save();

	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public static function fetchForProduct($id)
	{
		return ProductColours::where('product_id' , $id )->get();
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public static function fetch($id)
	{

		return ProductColours::where('product_id' , $id)->get();
	}

}