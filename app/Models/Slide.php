<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Slide extends Model {

	use SoftDeletes;

	public $fillable = [ 'name', 'image', 'link', 'text' ];

}
