<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
	public $timestamps = false;

	public $dates = [
		'created_at',
		'date'
	];

	public $fillable = [
		'company',
		'name',
		'telephone',
		'email',
		'vat',
		'consignee',
		'pol',
		'pod',
		'weight',
		'volume',
		'qty',
		'width',
		'length',
		'height',
		'description',
		'date',
		'un_number',
		'hazardous_cargo'
	];
}
