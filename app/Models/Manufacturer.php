<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model {
	/**
	 * Disable eloquent timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * Only show active manufacturers
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeActive( $query )
	{
		return $query->where( 'active', 1 );
	}
}
