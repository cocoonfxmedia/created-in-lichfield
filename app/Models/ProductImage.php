<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 07:26
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class ProductImage extends Model {

	/**
	 * Disable default eloquent timestamps
	 * @var bool
	 */
	public $timestamps = false;


	/**
	 * Unguarded attributes
	 * @var array
	 */
	public $fillabel = [ 'image', 'product_id'];


	/**
	 * Parent Relationship
	 * @return mixed
	 */
	public function product()
	{
		return $this->belongTo('App\Models\Product');
	}

}