<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property int|null $category_id
 * @property int|null $location_id
 * @property int $artist_id
 * @property int|null $gallery_id
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string $title
 * @property Carbon $at
 * @property float $price
 * @property string|null $description
 * @property string|null $external_link
 * @property string|null $header_image
 * @property string|null $event_code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Location|null $location
 * @property ArtCategory|null $category
 * @property Artist $artist
 * @property Gallery|null $gallery
 * @property Carbon|null $deleted_at
 */
final class Event extends Model
{
	use SoftDeletes;

	protected $with = [
		'categories',
	];

	protected $fillable = [
		'location_id',
		'gallery_id',
		'latitude',
		'longitude',
		'title',
		'at',
		'price',
		'description',
		'external_link',
		'event_code',
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'at',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = [
		'event_date',
		'date',
		'time',
	];

	/**
	 * Limit a query to only return upcoming events.
	 */
	public function scopeUpcoming(Builder $query)
	{
		return $query->where('at', '>', Carbon::now());
	}

	/**
	 * Only return active events.
	 */
	public function scopeActive(Builder $query)
	{
		return $query->whereStatus('active');
	}

	public function artists(): BelongsToMany
	{
		return $this->BelongsToMany(Artist::class, 'events_artists');
	}

	public function location(): BelongsTo
	{
		return $this->belongsTo(Location::class);
	}

	public function categories(): BelongsToMany
	{
		return $this->BelongsToMany(ArtCategory::class, 'art_categories_events');
	}

	public function gallery(): BelongsTo
	{
		return $this->belongsTo(Gallery::class);
	}

	public function getEventDateAttribute()
	{
		return $this->at->toDayDateTimeString();
	}

	public function getDateAttribute()
	{
		return $this->at->format('d F, Y');
	}

	public function getTimeAttribute()
	{
		return $this->at->format('h:i A');

	}
}
