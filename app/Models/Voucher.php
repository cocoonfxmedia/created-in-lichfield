<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model {

	use SoftDeletes;

	/**
	 * Ungaurded fields
	 * @var type
	 */
	public $fillable = [
		'label',
		'code',
		'discount',
		'expires_on',
		'type'
	];

	public $dates = [
		'created_at',
		'updated_at',
		'expires_on',
		'deleted_at'
	];


}
