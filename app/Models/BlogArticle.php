<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 10:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogArticle extends Model
{

	use SoftDeletes;

	/**
	 * Unguarded attributes
	 * @var array
	 */
	public $fillable = [
		'title',
		'content',
		'meta_title',
		'meta_description',
		'published',
		'url',
		'category_id'
	];

	/**
	 * Get blog article by URL
	 *
	 * @param $url
	 *
	 * @return Page
	 */
	static public function find_by_url( $url )
	{
		$page = BlogArticle::where( 'url', '=', $url )->where( 'published', '=', 1 )->first();


		if ( ! $page )
		{
			return false;
		}

		if ( empty( $page->meta_title ) )
		{
			$page->meta_title = $page->title;
		}

		return $page;

	}

	/**
	 * Blog category relationship
	 *
	 * @return mixed
	 */
	public function category()
	{
		return $this->belongsTo( 'App\Models\BlogCategory' );
	}

	/**
	 * Published articles scope
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePublished( $query )
	{
		return $query->where( 'published', '=', 1 );
	}

	/**
	 * Get article summary
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getSummaryAttribute( $value )
	{
		if ( ! empty( $this->attributes['meta_description'] ) )
		{
			$summary = $this->attributes['meta_description'];
		} else
		{
			$summary = strip_tags( $this->attributes['content'] );
		}

		return str_limit( $summary, 200 );
	}


	/**
	 * Get the link to the article
	 *
	 * @param $value
	 *
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	public function getLinkAttribute( $value )
	{
		return siteConfig( 'url' ) . '/blog/article/' . $this->attributes['url'];
	}

}