<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currencey extends Model
{
	public $table = 'currency_tracker';

	public $timestamps = false;

	public $casts = [ 'date' => 'date' ];
}
