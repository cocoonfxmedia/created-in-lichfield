<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

/**
 * @property int $id
 * @property int|null $category_id
 * @property string $name
 * @property string|null $website
 * @property string|null $bio
 * @property string|null $facebook_link
 * @property string|null $twitter_link
 * @property string|null $instagram_link
 * @property string|null $header_image
 * @property string|null $profile_image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $email
 * @property string|null $password
 * @property string|null $provider
 * @property string|null $provider_id
 * @property string|null $remember_token
 * @property bool $can_manage_events
 * @property bool $can_manage_locations
 * @property ArtCategory|null $category
 * @property Carbon|null $deleted_at
 */
final class Artist extends Authenticatable
{
	use SoftDeletes;

	protected $fillable = [
		'name',
		'website',
		'bio',
		'facebook_link',
		'twitter_link',
		'instagram_link',
		'email',
		'password',
		'provider',
		'provider_id',
		'can_manage_events',
		'can_manage_locations',
	];

	protected $hidden = [
		'password',
		'remember_token',
	];

	protected $with = [
		'categories',
	];

	protected $casts = [
		'can_manage_events' => 'bool',
		'can_manage_locations' => 'bool',
	];

	public function scopeCanEdit(Builder $query)
	{
		return $query
			->where('can_manage_events', true)
			->orWhere('can_manage_locations', true)
		;
	}

	/**
	 * Only return activated artists
	 *
	 * @param Builder $query
	 * @return Builder
	 */
	public function scopeActive(Builder $query): Builder
	{
		return $query->whereStatus('active');
	}

	public function categories(): BelongsToMany
	{
		return $this->belongsToMany(ArtCategory::class, 'art_categories_artists', 'artist_id', 'art_categories_id');
	}

	public function events(): BelongsToMany
	{
		return $this->BelongsToMany(Event::class, 'events_artists');
	}

	public function locations(): BelongsToMany
	{
		return $this->belongsToMany(Location::class, 'locations_artists');
	}

	public function gallery(): BelongsTo
	{
		return $this->belongsTo(Gallery::class);
	}

	/**
	 * Helper function, Determins if the current logged in user can edit this artist
	 *
	 * @return boolean
	 */
	public function canEdit():bool
	{
		return Auth::guard('artist')->user() && Auth::guard('artist')->user()->can('manage', $this);
	}
}
