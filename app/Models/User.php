<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable {
	use Notifiable;
//	use SoftDeletes;
	use EntrustUserTrait; // add this trait to your user model

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'first_name',
		'last_name',
		'email',
		'password',
		'landline',
		'mobile',
		'sn'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * @param $data
	 */
	public static function createUser( $data )
	{

		$u = new User;

		$u->name     = $data['name'];
		$u->email    = $data['email'];
		$u->password = Hash::make( $data['password'] );

		$u->admin = 0;

		$u->save();
	}

	/**
	 * Fetches the users ID, fetches the session ID if a user is not logged in
	 *
	 * @return mixed
	 */
	public static function getUsersID()
	{
		if ( \Auth::user() )
		{
			$user_id = \Auth::user()->id;
		}
		else
		{
			$user_id = \Session::getID();
		}

		return $user_id;
	}

	/**
	 * Set the users password
	 *
	 * @param type $newPassword
	 */
	public function setPassword( $newPassword )
	{
		$this->password = \Hash::make( $newPassword );
	}

	/**
	 * Get orders made by the user
	 * @return Eloquent relationship
	 */
	public function orders()
	{
		return $this->hasMany( 'App\Models\Order' );
	}

	/**
	 * Admin user accounts
	 *
	 * @param $query
	 *
	 * @return Query Builder
	 */
	public function scopeAdmin( $query )
	{
		return $query->where( 'admin', '=', '1' );
	}

	public function scopeCustomers( $query )
	{
		return $query->where( 'admin', 0 );
	}

	/**
	 * Get full name
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getNameAttribute( $value )
	{
		return trim( $this->attributes['title'] . ' ' . $this->attributes['first_name'] . ' ' . $this->attributes['last_name'] );
	}


}
