<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use App\Models\BasketVoucher;
use Illuminate\Support\Facades\Input;

class Basket extends Model {

	/**
	 * @var string
	 */
	public $table = 'basket';

	/**
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * @param $id
	 * @param $qty
	 */
	public static function add( $data )
	{
		$basket_id = Input::cookie( 'trp_basket' );


		if ( ! $basket_id )
		{
			Cookie::queue( Cookie::make( 'trp_basket', \Session::getID(), 300 ) );
			$basket_id = \Session::getID();
		}


		$b = Basket::where( 'product_id', $data['id'] )->where( 'session_id', $basket_id )->first();

		if ( ! $b )
		{
			$b           = new Basket();
			$b->quantity = 0;
		}


		$b->session_id = $basket_id;

		$b->product_id = $data['id'];
		$b->quantity += $data['quantity'];

		if ( isset( $data['custom_design'] ) )
		{
			$b->custom_design = $data['custom_design'];
		}

		if ( isset( $data['custom_text'] ) )
		{
			$b->custom_text = $data['custom_text'];
		}

		if ( isset( $data['colour'] ) )
		{
			$b->colour = $data['colour'];
		}
		if ( isset( $data['size'] ) )
		{
			$b->size = $data['size'];
		}
		$b->price = $data['price'];

		$b->save();

		//Clean out any old cruft
		Basket::housekeeping();

	}

	/**
	 * Collect the basket data
	 *
	 * @return mixed
	 */
	public static function fetch()
	{
		$basket_id = Input::cookie( 'trp_basket' );

		if ( ! $basket_id )
		{
			Cookie::queue( Cookie::make( 'trp_basket', \Session::getID(), 300 ) );
			$basket_id = \Session::getID();
		}

		$basket = Basket::where( 'session_id', $basket_id )->get();

		$total = 0;
		foreach ( $basket as $b )
		{

			//Get the product name from the DB
			$b->product_name = Product::select( 'name' )->where( 'id', $b->product_id )->first()->name;

			//Work out the total
			$total = $total + ( $b->price * $b->quantity );
		}

		$vouchers = BasketVoucher::where( 'session_id', '=', $basket_id )->get();

		$voucherTotal = 0;
		foreach ( $vouchers as $code )
		{

			if ( $code->detail->type == 'direct' )
			{
				$voucherTotal += $code->detail->discount;
			}
			else
			{
				$voucherTotal = $voucherTotal + ( $total * ( $code->detail->discount / 100 ) );
			}
		}

		$shipping = Config::get('cms.shipping.flateRate');

		$total = $total - $voucherTotal;
		if ( $total < 0 )
		{
			$total = 0;
		}

		return $basket = [
			'items' => $basket,
			'shipping' => $shipping,
			'total' => $total,
			'vouchers' => $vouchers ];
	}

	/**
	 * Method fires and cleans out old items within the basket if they are over two days old
	 * Method is fired at basket::add()
	 */
	public static function housekeeping()
	{
		$date = new \DateTime;
		$date->modify( '-2 days' );
		$formatted_date = $date->format( 'Y-m-d H:i:s' );

		Basket::where( 'updated_at', '<', $formatted_date )->delete();
	}

	/**
	 * This isnt finished but we might not neeed it at all.
	 */
	public static function checkForDuplicates( $basket, $data )
	{
//		foreach( $basket as $item){
//			$duplicate = false;
//			if (
//				$data['product_id'] == $item->product_id &&
//				$data['size'] == $item->size &&
//				$d
//			){
//				$duplicate = true;
//			}
//		}
//
//		if ( $duplicate ){
//			dd('Found it');
//		}
	}

	/**
	 * Fetches the price for the item
	 *
	 * @param $product_id
	 *
	 * @return mixed
	 */
	public static function getPrice( $product_id )
	{
		$product = Product::where( 'id', $product_id )->select( 'price', 'sale_price' )->first();

		$price = $product->price;

		if ( $product->sale_price > 0 )
		{
			$price = $product->sale_price;
		}

		return $price;
	}

	/**
	 * Remove an item from the basket
	 *
	 * @param $id
	 */
	public static function removeItem( $id )
	{
		$basket_id = Input::cookie( 'trp_basket' );

		Basket::where( 'session_id', $basket_id )
		      ->where( 'id', $id )
		      ->delete();
	}

	/**
	 * Get the count of the basket.
	 * Return a nice sentence with the $ess variable
	 */
	public static function count()
	{
		$count = count( static::fetch()['items'] );

		$ess = 's';

		if ( $count == 1 )
		{
			$ess = '';
		}

		echo '<a href="/shop/basket">' . $count . ' Item' . $ess . '</a>';
	}

	public static function summary()
	{

		$basketObj = new Basket();

		$basket = $basketObj->fetch();

		return [
			'count' => count( $basket['items'] ),
			'total' => $basket['total']
		];
	}

	/**
	 * Destroy the current basket session
	 */
	public static function cleanup()
	{
		$basket_id = Input::cookie( 'trp_basket' );

		Cookie::queue( Cookie::forget( 'trp_basket' ) );

		Basket::where( 'session_id', $basket_id )->delete();
		BasketVoucher::where( 'session_id', $basket_id )->delete();
	}

	/**
	 * Get basket line item total
	 *
	 * @param $value
	 *
	 * @return mixed
	 */
	public function getTotalAttribute( $value )
	{
		return $this->attributes['price'] * $this->attributes['quantity'];
	}

	/**
	 * Product relationships
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function product()
	{
		return $this->hasOne( '\App\Models\Product', 'id', 'product_id' );
	}

}
