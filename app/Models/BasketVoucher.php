<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Voucher;

class BasketVoucher extends Model {


	/**
	 * Get voucher details
	 * @return Voucher
	 */
	public function detail()
	{
		return $this->hasOne( 'App\Models\Voucher', 'id', 'voucher_id' );
	}

}
