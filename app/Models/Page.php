<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 10:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class Page extends Model
{

	use SoftDeletes;

	/**
	 * Unguarded attributes
	 * @var array
	 */
	public $fillable = [
		'title',
		'content',
		'meta_title',
		'meta_description',
		'published',
		'url',
		'template',
		'block_link',
		'block_content',
		'slider',
		'is_homepage'
	];

	/**
	 * Get page by URL
	 *
	 * @param $url
	 *
	 * @return Page
	 */
	static public function find_by_url( $url )
	{
		$page = Page::where( 'path', '=', $url )->where( 'published', 1 )->where( 'sid', siteConfig( 'sid' ) )->first();


		if ( ! $page )
		{
			return false;
		}

		if ( empty( $page->meta_title ) )
		{
			$page->meta_title = $page->title;
		}

		return $page;

	}

	/** Page template accessor
	 *
	 *
	 * @param $value
	 *
	 * @return array
	 */
	public function getTemplateAttribute( $value )
	{

		if ( is_null( $value ) )
		{
			return 'default';
		}

		return $value;
	}


	/**
	 * Parent Pages
	 *
	 * @return mixed
	 */
	public function parent()
	{
		return $this->belongsTo( 'App\Models\Page', 'parent_id' );
	}


	/**
	 * Child Pages
	 *
	 * @return mixed
	 */
	public function children()
	{
		return $this->hasMany( 'App\Models\Page', 'parent_id' );
	}

	/**
	 * Get the page masthead
	 *
	 * @param $value
	 *
	 * @return null|string
	 */
	public function getMastheadAttribute( $value )
	{
		if ( empty( $value ) )
		{
			return null;
		}

		return '/usr/page_masthead/' . $value;
	}


}