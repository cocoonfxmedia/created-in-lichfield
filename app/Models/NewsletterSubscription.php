<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterSubscription extends Model {
	/**
	 * cast attributes to Carbon instanace
	 * @var array
	 */
	public $dates = [ 'created_at', 'updated_at', 'unsubscribed_at' ];

	/**
	 * List subscribers
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeSubscribed( $query )
	{
		return $query->whereNull( 'unsubscribed_at' );
	}

	/**
	 * Check if user is subscribed
	 *
	 * @return bool
	 */
	public function getIsSubscribedAttribute()
	{
		if( !is_null( $this->attributes['unsubscribed_at']))
		{
			return true;
		}

		return false;
	}
}
