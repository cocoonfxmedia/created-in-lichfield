<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Gallery extends Model
{
	public function images(): HasMany
	{
		return $this->hasMany(GalleryImage::class, 'gallery');
	}
}
