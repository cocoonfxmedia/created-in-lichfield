<?php
/**
 * Created by PhpStorm.
 * User: gareth_w
 * Date: 07/06/2016
 * Time: 10:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model
{

	use SoftDeletes;

	/**
	 * Unguarded attributes
	 * @var array
	 */
	public $fillable = [
		'title',
		'slug'
	];

	/**
	 * Get blog article by URL
	 *
	 * @param $url
	 *
	 * @return Page
	 */
	static public function find_by_url( $url )
	{
		$cat = BlogCategory::where( 'slug', '=', $url )->active()->first();

		return $cat;

	}

	/**
	 * Build full URL to
	 *
	 * @param $url
	 *
	 * @return mixed
	 */
	public function getUrlAttribute( $url )
	{

		//TODO:: insert full URL here
		return $url;
	}

	/**
	 * Active blog categories
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeActive( $query )
	{
		return $query->where( 'active', '=', 1 );
	}

}