<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\FacadesConfig;


class ProductCategory extends Model {

	public $table = 'product_categories';


	public $fillable = [
		'name',
		'meta_description',
		'meta_keywords',
		'meta_title',
		'legacy_id',
		'description',
		'parent_id'
	];


	/**
	 * @return array|bool
	 */
	public static function fetchForSelect()
	{

		$categories = ProductCategory::where( 'active', true )->pluck( 'name', 'id' )->toArray();

		//Shove the defaults into the start of the array
		$categories[0] = "Please choose one";

		ksort( $categories );

		if ( $categories )
		{
			return $categories;
		}

		return false;
	}

	/**
	 * @param $data
	 */
	public static function createCategory( $data )
	{
		$c = new ProductCategory();

		$c->name        = $data['name'];
		$c->description = $data['description'];
		$c->meta_title  = $data['meta_title'];
//		$c->meta_keywords = $data[ 'meta_keywords' ];
		$c->meta_description = $data['meta_description'];
		$c->slug             = str_slug( str_replace( "&", 'and', $data['name'] ) );
		$c->active           = 1;

		if ( ! isset( $data['parent_id'] ) )
		{
			$c->parent_id = 0;
		}
		else
		{
			$c->parent_id = $data['parent_id'];
		}


		if ( isset( $data['legacy_id'] ) )
		{
			$c->legacy_id = $data['legacy_id'];
		}

		$c->save();

		//Get the ID of the just saved instance
		$id = $c->id;

		$storage_path = base_path() . '/public/usr/product_categories/';

		// If there is an image, update it
		if ( isset( $data['image'] ) )
		{
			$filename = $id . '.' . Input::file( 'image' )->getClientOriginalExtension();

			Input::file( 'image' )->move( $storage_path, $filename );

			$c->image = $filename;
		}

		$c->save();
	}

	/**
	 * Update a category
	 */
	public static function updateCategory( $id )
	{
		$data = Input::all();

		$storage_path = base_path() . '/public/usr/product_categories/';

		$category = ProductCategory::find( $id );

		$category->name        = $data['name'];
		$category->description = $data['description'];
//		$category->meta_keywords = $data[ 'meta_keywords' ]; //Removed from the category page
		$category->meta_description = $data['meta_description'];
		$category->meta_title       = $data['meta_title'];
		$category->slug             = str_slug( str_replace( "&", 'and', $data['name'] ) );

		if ( ! isset( $data['parent_id'] ) )
		{
			$category->parent_id = 0;
		}
		else
		{
			$category->parent_id = $data['parent_id'];
		}

		if ( isset( $data['active'] ) && $data['active'] == 'on' )
		{
			$category->active = true;
		}
		else
		{
			$category->active = false;
		}

		// If there is an image, update it
		if ( isset( $data['image'] ) )
		{

			$filename = $id . '.' . Input::file( 'image' )->getClientOriginalExtension();

			Input::file( 'image' )->move( $storage_path, $filename );

			$category->image = $filename;
		}

		$category->save();
	}

	/**
	 * Fetch all the categories
	 *
	 * @return mixed
	 */
	public static function showAll()
	{
		return ProductCategory::where( 'active', true )->get();
	}

	/**
	 * Parent Product
	 *
	 * @return Category
	 */
	public function parent()
	{
		return $this->belongsTo( 'App\Models\ProductCategory', 'parent_id' );
	}

	/**
	 * Get child categories
	 *
	 * @return Category collection
	 */
	public function children()
	{
		return $this->hasMany( 'App\Models\ProductCategory', 'parent_id', 'id' );
	}

	/**
	 * Get products related to this category
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function products()
	{
		return $this->hasMany( 'App\Models\Product', 'category_id', 'id' );
	}

	/**
	 * Only show active categories
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeActive( $query )
	{
		return $query->where( 'active', '=', 1 );
	}

	/**
	 * Get the root product categories
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeRootCategories( $query )
	{
		return $query->where( 'parent_id', '=', 0 );
	}

	/**
	 * Prepend url to category image
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getImageAttribute( $value )
	{
		if ( ! empty( $value ) )
		{
			$value = '/usr/product_categories/' . $value;
		}

		return $value;
	}

	/**
	 * Get the total number of active products in a category
	 *
	 * @param $value
	 *
	 * @return mixed
	 */
	public function totalProducts()
	{
		return Product::active()->where( 'category_id', '=', $this->attributes['id'] )->count();
	}

	/**
	 * Get root categories with no parent
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeRoot( $query )
	{
		return $query->where( 'parent_id', '=', 0 );
	}


	/**
	 * Prepend category ID to the slug to ensure URL is unique
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getSlugAttribute( $value )
	{
		return $this->attributes['id'] . '/' . $value;
	}


}
