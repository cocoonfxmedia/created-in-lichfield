<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ProductFace extends Model {
	/*
	 * Eloquent atuo timestamps
	 */

	public $timestamps = false;


	/*
	 * Fillabel guarding
	 */
	public $fillable = [ 'title', 'image' ];

	public static function createFace( $data )
	{

		$face				 = new ProductFace();
		$face->title		 = $data[ 'title' ];
		$face->product_id	 = $data[ 'product_id' ];
		$face->colour_id	 = $data[ 'colour_id' ];

		//create new database obj
		$face->save();


		$storage_path = \Config::get( 'elem.product_face_folder' );



		if ( isset( $data[ 'image' ] ) )
		{

			$filename = $face->id . '.' . \Input::file( 'image' )->getClientOriginalExtension();

			\Input::file( 'image' )->move(
			$storage_path, $filename
			);

			$face->image = $filename;

			//update db
			$face->save();
		}
	}

	public static function deleteFace( $id )
	{
		$face = productFace::find( $id );


		$storage_path = \Config::get( 'elem.product_face_folder' );


		if ( file_exists( $storage_path . $face->image ) )
		{
			unlink( $storage_path . $face->image );
		}


		$face->destroy( $face->id );
	}

	/**
	 * Reverse relationship with Product 
	 * 
	 * @return Product
	 */
	public function product()
	{
		return $this->belongsTo( 'Elem\Product' );
	}

	/**
	 * Get related product face colour
	 * 
	 * @return productColours
	 */
	public function colour()
	{
		return $this->hasone( 'App\Models\ProductColours', 'id', 'colour_id' );
	}

	/**
	 * Limit result to faces linted to colour
	 * 
	 * @param Builder $query
	 * @param int $colourId
	 * @return Query Builder
	 */
	public function scopeFilterColour( Builder $query, $colourId )
	{
		if ( !$colourId ) // if colour not set to explicit colour id
		{
			return $query;
		}

		return $query->whereIn( 'colour_id', [0, $colourId ] );
	}

}
