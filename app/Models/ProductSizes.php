<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSizes extends Model {

	/**
	 * @param $data
	 */
	public static function createSize( $data )
	{

		if ( ! $data['size'] ){
			return false;
		}

		$c = new ProductSizes;

		$c->product_id = $data['product_id'];
		$c->size = $data['size'];

		$c->save();

	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public static function fetchForProduct($id)
	{

		return ProductSizes::where('product_id' , $id )->get();

	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public static function fetch($id)
	{

		return ProductSizes::where('product_id' , $id)->get();
	}

}
