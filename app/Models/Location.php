<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property int|null $category_id
 * @property int $artist_id
 * @property int|null $gallery_id
 * @property float $latitude
 * @property float $longitude
 * @property string $title
 * @property string $address
 * @property string|null $description
 * @property string|null $external_link
 * @property string|null $header_image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property ArtCategory|null $category
 * @property Artist $artist
 * @property Gallery|null $gallery
 * @property Carbon|null $deleted_at
 */
final class Location extends Model
{
	use SoftDeletes;

	protected $with = [
		'categories',
	];

	protected $fillable = [
		'category_id',
		'gallery_id',
		'latitude',
		'longitude',
		'title',
		'address',
		'description',
		'external_link',
	];

	public function artists(): BelongsToMany
	{
		return $this->BelongsToMany(Artist::class, 'locations_artists');
	}

	public function categories(): BelongsToMany
	{
		return $this->BelongsToMany(ArtCategory::class, 'art_categories_locations' );
	}

	public function gallery(): BelongsTo
	{
		return $this->belongsTo(Gallery::class);
	}
}
