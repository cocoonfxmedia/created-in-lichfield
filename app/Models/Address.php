<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Address extends Model {

	/**
	 * Mass assignment acceptable fields
	 * @var type
	 */
	public $fillable = [
		'label',
		'fao_first_name',
		'fao_last_name',
		'company',
		'address_1',
		'address_2',
		'town_city',
		'county',
		'country',
		'postcode',
		'user_id'
	];

	/**
	 * Method to save add addresses passed.
	 *
	 * @param $data
	 */
	public static function saveAddresses( $data )
	{
		$ids = [ 0, 0 ];

		//See if billing and or shipping are set
		if ( strlen( $data['shipping-address1'] ) > 1 )
		{

			$data['address_type'] = 'shipping';
			$ids[0]               = Address::saveAddress( $data );
		}

		if ( strlen( $data['billing-address1'] ) > 1 )
		{

			$data['address_type'] = 'billing';
			$ids[1]               = Address::saveAddress( $data );
		}

		return $ids;
	}

	/**
	 * Method to save single address
	 *
	 * @param $data
	 */
	public static function saveAddress( $data )
	{

		$user_id = User::getUsersID();

		//Lets see if there is an address of this type already
		$a = Address::where( 'user_id', $user_id )->where( 'type', $data['address_type'] )->first();

		if ( ! count( $a ) > 0 )
		{
			//There is not an address so create one
			$a = new Address;
		}

		$a->type    = $data['address_type'];
		$a->user_id = $user_id;
//		$a->name	 = $data[ $data[ 'address_type' ] . '-' . 'name' ];

		$a->fullname  = $data[ $data['address_type'] . '-' . 'fullname' ];
		$a->address_1 = $data[ $data['address_type'] . '-' . 'address1' ];
		$a->address_2 = $data[ $data['address_type'] . '-' . 'address2' ];
		$a->town_city = $data[ $data['address_type'] . '-' . 'town_city' ];
		$a->county    = $data[ $data['address_type'] . '-' . 'county' ];
		$a->postcode  = $data[ $data['address_type'] . '-' . 'postcode' ];

		$a->save();

		return $a->id;
	}

	/**
	 * Find all the addresses for the user
	 *
	 * @return mixed
	 */
	public static function findForUser()
	{
		$user_id = User::getUsersID();

		return Address::where( 'user_id', $user_id )->get();
	}

	/**
	 * Return just the billing address
	 *
	 * @return mixed
	 */
	public static function findBilling()
	{
		$user_id = User::getUsersID();

		return Address::where( 'user_id', $user_id )->where( 'type', 'billing' )->first();
	}

	/**
	 * @param $shipping
	 * @param $billing
	 *
	 * @return mixed
	 */
	public static function fetchOrderAddresses( $shipping, $billing )
	{
		$data = Address::whereIn( 'id', [ $shipping, $billing ] )->get();

		$addresses['shipping'] = $data[0];
		$addresses['billing']  = $data[1];

		return $addresses;
	}

}
