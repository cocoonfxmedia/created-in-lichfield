<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockPanel extends Model
{
    public $fillable = [
    	'title',
	    'content'
    ];
}
