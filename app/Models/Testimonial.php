<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{

	public $fillable = [ 'title', 'body', 'published', 'slug', 'sid', 'meta_title', 'meta_description' ];


	/**
	 * Return only published testimonials
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePublished( $query )
	{
		return $query->where( 'published', 1 );
	}

	/**
	 * Get summary of testimonial body
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getSummaryAttribute( $value )
	{
		return str_limit( strip_tags( $this->attributes['body'] ), 225 );
	}

	/**
	 * Get the URl for testimonial
	 *
	 * @param $value
	 *
	 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
	 */
	public function getLinkAttribute( $value )
	{
		return url( config( 'cms.testimonials.url' ) . '/' . $this->attributes['slug'] );
	}
}
