<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model {

	/**
	 * Get line item colour options
	 * @return Eloquent relationship
	 */
	public function colourOption()
	{
		return $this->hasOne( 'App\Models\ProductColours', 'id', 'colour' );
	}


	/**
	 * Get line item size options
	 * @return Eloquent relationship
	 */
	public function sizeOption()
	{
		return $this->hasOne( 'App\Models\ProductSizes', 'id', 'size' );
	}

	/**
	 * Print output filename
	 * @return String
	 */
	public function getOutputFile()
	{
		$filename = str_replace( '.png', '.pdf', $this->attributes[ 'custom_design' ] );

		return $filename;
	}

	/**
	 * Product included in the line item
	 * @return Eloquent Relationship
	 */
	public function product()
	{
		return $this->hasOne( 'App\Models\Product', 'id', 'product_id' );
	}

	/**
	 * Format the line item price
	 * @param $value
	 * @return string
	 */
	public function getPriceAttribute( $value )
	{
		return number_format( $value, 2 );
	}

}
