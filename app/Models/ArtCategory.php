<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $title
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon|null $deleted_at
 */
final class ArtCategory extends Model
{
	use SoftDeletes;

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	protected $fillable = [
		'title',
	];
}
