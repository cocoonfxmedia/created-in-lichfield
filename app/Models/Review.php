<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {


	/**
	 * Eloquent default timestamp fields used
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * Mass assignment guarding
	 * @var array
	 */
	public $fillable = [ 'comment',
		'rating',
		'name',
		'email',
		'product_id' ];

	/**
	 * Only approved reviews
	 * @param $query
	 * @return Query Builder
	 */
	public function scopeApproved( $query )
	{
		return $query->where( 'approved_at', '>', '1790-01-01' );
	}

	/**
	 * Parent product
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product()
	{
		return $this->belongsTo( 'App\Models\Product', 'product_id', 'id' );
	}

}
