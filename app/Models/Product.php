<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class Product extends Model {

	public $fillable = [
		'name',
		'meta_title',
		'meta_description',
		'meta_keywords',
		'description',
		'product_code',
		'price',
		'sale_price',
		'featured',
		'cf_enabled',
		'cf_label',
		'cf_char',
		'cf_lines',
		'weight',
		'dimension_w',
		'dimension_h',
		'dimension_l',
		'package_type'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo( 'App\Models\ProductCategory', 'category_id' );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public static function fetchAll()
	{
		return Product::all();
	}

	/**
	 * Create a new product
	 *
	 * @param $data
	 */
	public static function createProduct( $data )
	{

		$p = new Product;

		$p->name         = $data['name'];
		$p->product_code = $data['product_code'];
		$p->slug         = str_slug( $data['name'] );
		$p->category_id  = $data['category_id'];
		$p->description  = $data['description'];
		$p->featured     = ( ( isset( $data['featured'] ) ) ? 1 : 0 );

		if ( isset( $data['weight'] ) )
		{
			$p->weight = $data['weight'];
		}

		if ( isset( $data['package_type'] ) )
		{
			$p->package_type = $data['package_type'];
		}

		if ( isset( $data['dimension_w'] ) )
		{
			$p->dimension_w = $data['dimension_w'];
			$p->dimension_l = $data['dimension_l'];
			$p->dimension_h = $data['dimension_h'];
		}

		//custom field
		$p->cf_enabled = ( ( isset( $data['cf_enabled'] ) ) ? 1 : 0 );
		$p->cf_label   = $data['cf_label'];
		$p->cf_char    = $data['cf_char'];
		$p->cf_lines   = $data['cf_lines'];

		if ( isset( $data['meta_title'] ) )
		{
			$p->meta_title = $data['meta_title'];
		}

		if ( isset( $data['meta_description'] ) )
		{
			$p->meta_description = $data['meta_description'];
		}

		if ( isset( $data['meta_keywords'] ) )
		{
			$p->meta_keywords = $data['meta_keywords'];
		}


		$p->save();

		return $p->id;
	}

	/**
	 * @param $data
	 */
	public static function updateProduct( $data )
	{
		$input = Input::all();

		$storage_path = base_path() . '/public/usr/products/';

		$product_id = $data['id'];

		$product = Product::where( 'id', $data['id'] )->first();

		if ( isset( $input['image'] ) )
		{

			$filename = $product_id . '.' . Input::file( 'image' )->getClientOriginalExtension();

			Input::file( 'image' )->move( $storage_path, $filename );

			$product->image = $filename;
		}


		if ( isset( $input['image_1'] ) )
		{

			$filename = $product_id . '_1.' . Input::file( 'image_1' )->getClientOriginalExtension();

			Input::file( 'image_1' )->move( $storage_path, $filename );

			$product->image_1 = $filename;
		}

		if ( isset( $input['image_2'] ) )
		{

			$filename = $product_id . '_2.' . Input::file( 'image_2' )->getClientOriginalExtension();

			Input::file( 'image_2' )->move( $storage_path, $filename );

			$product->image_2 = $filename;
		}

		if ( isset( $input['image_3'] ) )
		{

			$filename = $product_id . '_3.' . Input::file( 'image_3' )->getClientOriginalExtension();

			Input::file( 'image_3' )->move( $storage_path, $filename );

			$product->image_3 = $filename;
		}


		$data['cf_enabled'] = ( ( isset( $data['cf_enabled'] ) ) ? 1 : 0 );

		if ( empty( $data['weight'] ) )
		{
			$data['weight'] = 0;
		}


		$product->update( $data );


		if ( isset( $data['featured'] ) )
		{
			$product->featured = 1;
		}
		else
		{
			$product->featured = 0;
		}

		$product->category_id = $data['category_id'];

		$product->product_code = $data['product_code'];

		$product->price = $data['price'];


		$product->slug = str_slug( $data['name'] );


//		if ( isset( $data['active'] ) && $data['active'] == 'on' )
//		{
//			$product->active = true;
//		}
//		else
//		{
//			$product['active'] = false;
//		}

		$product->save();
	}

	/**
	 * Fetch all the active products in the store.
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public static function showAll()
	{

		return Product::with( 'category' )->where( 'active', true )->get();
	}

	/**
	 * @param $slug
	 *
	 * @return mixed
	 */
	public static function getProductsWithinCategory( $slug )
	{
		$category_id = Category::findIDFromSlug( $slug );

		return Product::where( 'category_id', $category_id )
		              ->active()
		              ->inOrder()
		              ->get();
	}

	/**
	 * @param $slug
	 *
	 * @return mixed
	 */
	public static function getProduct( $slug )
	{
		$product = Product::where( 'slug', $slug )->where( 'active', true )->first();

		return $product;
	}

	/**
	 * Get printing faces for this product
	 * @return ProductFace
	 */
	public function faces()
	{
		return $this->hasMany( 'Apps\Models\ProductFace' );
	}

	/**
	 * limite scope to featured products
	 *
	 * @param type $query
	 *
	 * @return Query
	 */
	public function scopeFeatured( $query )
	{
		return $query->where( 'featured', '=', 1 )->where( 'active', '=', 1 )->orderBy( 'page_order', 'ASC' );
	}

	/**
	 * Only return products currently active in the catalog
	 *
	 * @param type $query
	 *
	 * @return type
	 */
	public function scopeActive( $query )
	{
		return $query->where( 'active', '=', 1 );
	}

	/**
	 * Get related products
	 *
	 * @return Relationship
	 */
	public function relatedProducts()
	{
		return $this->belongsToMany( 'App\Models\Product', 'related_products', 'parent_product', 'child_product' );
	}

	/**
	 * Get formatted product description
	 * @return string
	 */
	public function LongDescription()
	{
		$tmp = explode( "\r\n", $this->attributes['description'] );


		$output = '';
		foreach ( $tmp as $line )
		{
			if ( $line !== '' )
			{
				$output .= '</p>' . $line . '</p>';
			}
		}

		return $output;
	}

	/**
	 * return results by set order
	 *
	 * @param Query Builder $query
	 *
	 * @return Query Builder
	 */
	public function scopeInOrder( $query )
	{
		return $query->orderBy( 'page_order' );
	}

	/**
	 * Product Reviews
	 * @return Eloquent relationship
	 */
	public function reviews()
	{
		return $this->hasMany( 'App\Models\Review', 'product_id' );
	}

	/**
	 * Price Formatter
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getPriceAttribute( $value )
	{
		return number_format( $value, 2 );
	}

	public function getRawPriceAttribute( $value )
	{
		return $this->attributes['price'];
	}


	/**
	 * Child image relationship
	 * @return mixed ProductImage collection
	 */
	public function images()
	{
		return $this->hasMany( 'App\Models\ProductImage' );
	}

	public function hasMultipleImages()
	{
		if ( ! strlen( $this->image_1 ) && ! strlen( $this->image_2 ) && ! strlen( $this->image_3 ) )
		{
			return false;
		}

		return true;
	}

	/**
	 * Modify slug to include product ID to ensure unique URL
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getSlugAttribute( $value )
	{

		$value = $this->attributes['id'] . '/' . $value;

		return $value;
	}

	public function getQtyAttribute( $value )
	{
		if ( Config::get( 'cms.stockControl' ) )
		{
			return $value;
		}

		return 999;
	}

}
