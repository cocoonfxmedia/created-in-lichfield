<?php

namespace App\Models;

use App\Models\OrderLines;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\FacadesSession;

class Order extends Model {

	/**
	 * Create a new order from a basket model
	 *
	 * @param $basket
	 * @param $addresses
	 */
	public static function createOrder( $basket, $address_ids )
	{

		//Create an order
		DB::beginTransaction();
		$order = null;

		try
		{
			$order = Order::createOrderRecord( $basket, $address_ids );

			Order::addOrderLines( $order->id, $basket );
			Order::addVouchers( $order->id, $basket );

		} catch ( Exception $e )
		{
			DB::rollback();
		}

		//Everything worked perfectly. Commit the transaction
		DB::commit();

		return $order;
	}

	/**
	 * Create the order record and return the inserted ID
	 *
	 * @param $basket
	 * @param $addresses
	 *
	 * @return mixed
	 */
	public static function createOrderRecord( $basket, $address_ids )
	{
		$user_id = User::getUsersID();

		$o = new Order;

		$o->user_id     = $user_id;
		$o->order_value = $basket['total'];
		$o->shipping    = 10.00;

		$o->shipping_address = $address_ids[0];
		$o->billing_address  = $address_ids[1];

		$o->pay_method = Input::get('pay_method');


		$o->save();

		return $o;
	}

	/**
	 * Add the order lines to the database
	 *
	 * @param $order_id
	 * @param $basket
	 */
	public static function addOrderLines( $order_id, $basket )
	{
		foreach ( $basket['items'] as $line )
		{
			DB::table( 'order_lines' )->insert(
				[
					'order_id'      => $order_id,
					'product_id'    => $line->product_id,
					'product_name'  => $line->product_name,
					'quantity'      => $line->quantity,
					'price'         => $line->price,
					'colour'        => $line->colour,
					'size'          => $line->size,
					'custom_design' => $line->custom_design,
					'custom_text'   => $line->custom_text
				] );
		}
	}

	/**
	 * Attach vouchers to the order
	 *
	 * @param type $order_id
	 * @param type $basket
	 */
	public static function addVouchers( $order_id, $basket )
	{
		foreach ( $basket['vouchers'] as $v )
		{
			$voucherCode = new OrderVoucher();
			$voucher     = Voucher::find( $v->voucher_id );

			$voucherCode->order_id   = $order_id;
			$voucherCode->voucher_id = $voucher->id;
			$voucherCode->label      = $voucher->label;
			$voucherCode->discount   = $voucher->discount;
			$voucherCode->code       = $voucher->code;
			$voucherCode->type       = $voucher->type;

			$voucherCode->save();
		}
	}

	/**
	 * Get order model by ID
	 *
	 * @param type $id
	 *
	 * @return Elem\Order
	 */
	public static function fetchByID( $id )
	{

		return Order::where( 'id', $id )->first();
	}

	/**
	 * Get line items related to order number
	 *
	 * @param type $id
	 *
	 * @return list of OrderLine models
	 */
	public static function fetchOrderLinesByID( $id )
	{
		return DB::table( 'order_lines' )->where( 'order_id', $id )->get();
	}

	/**
	 * Get list of vouchers related to order number
	 *
	 * @param $id
	 *
	 * @return list of OrderVoucher models
	 */
	public static function fetchOrderVouchersByID( $id )
	{
		return DB::table( 'order_vouchers' )->where( 'order_id', $id )->get();
	}

	/**
	 * Get the total number of orders for a customer
	 * @return int total orders
	 */
	public static function count()
	{
		$user_id = \Auth::user()->id;

		return Order::where( 'user_id', $user_id )->count();
	}

	/**
	 * Get the related customer for this order
	 * @return Eloquent relationship
	 */
	public function customer()
	{
		return $this->belongsTo( 'App\Models\User', 'user_id' );
	}

	/**
	 * Get the order shipping address
	 * @return Eloquent relationship
	 */
	public function shippingAddress()
	{
		return $this->hasOne( 'App\Models\Address', 'id', 'shipping_address' );
	}

	/**
	 * Get the order billing adress
	 * @return Eloquent relationship
	 */
	public function billingAddress()
	{
		return $this->hasOne( 'App\Models\Address', 'id', 'billing_address' );
	}

	/**
	 * Get the line items attached to the order
	 * @return Eloquent relationship
	 */
	public function lineItems()
	{
		return $this->hasMany( 'App\Models\OrderLine' );
	}

	/**
	 * Get vouchers attached to the order
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function vouchers()
	{
		return $this->hasMany( 'App\Models\OrderVoucher' );
	}

	/**
	 * Get open orders
	 *
	 * @param type $query
	 *
	 * @return Query builder
	 */
	public function scopeOpen( $query )
	{
		return $query->where( 'status', '!=', 'cancelled' )->where( 'status', '!=', 'complete' );
	}

	/**
	 *
	 * @param Query $query
	 * @param array $filters
	 *
	 * @return Query
	 */
	public function scopeFilter( $query, $filters = [] )
	{
		if ( ! empty( $filters['lBound'] ) )
		{
			$tmp               = explode( '/', $filters['lBound'] );
			$filters['lBound'] = $tmp['2'] . '-' . $tmp[1] . '-' . $tmp[0];

			$query = $query->where( 'created_at', '>=', $filters['lBound'] . ' 00:00:00' );
		}

		if ( ! empty( $filters['uBound'] ) )
		{
			$tmp               = explode( '/', $filters['uBound'] );
			$filters['uBound'] = $tmp['2'] . '-' . $tmp[1] . '-' . $tmp[0];

			$query = $query->where( 'created_at', '=<', $filters['uBound'] . ' 23:59:59' );
		}

		return $query;
	}

	/**
	 * Send the order for fulfillment
	 */
	public function triggerFulfillment()
	{
		//send email to customer
		Mail::send(
			'emails.orderConfirmation', [ 'order' => $this ], function ( $msg )
		{
			$msg->to( $this->customer->email, $this->customer->name )
			    ->subject( 'Order No ' . $this->id . ' - Confirmation' )
			    ->from( Config::get( 'mail.from.address', Config::get( 'mail.from.name' ) ) );
		} );

		//send email to admin
		Mail::send(
			'emails.admin.newOrder', [ 'order' => $this ], function ( $msg )
		{
			$msg->subject( 'New Order Ref: ' . $this->id )
			    ->to( Config::get( 'mail.to.orderFulfillment' ) )
			    ->from( Config::get( 'mail.from.address', Config::get( 'mail.from.name' ) ) );
		} );
	}

	/**
	 * Get cahiser getails
	 * @return Eloquent Relationship
	 */
	public function cashierInfo()
	{
		return $this->hasOne( 'App\Models\User', 'id', 'cashier' );
	}

	/**
	 * Order value formatter
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getOrderValueAttribute( $value )
	{
		return number_format( $value, 2 );
	}

	/**
	 * Get the total billable for the order
	 *
	 * @param $value
	 *
	 * @return string
	 */
	public function getTotalAttribute( $value )
	{
		return number_format( $this->attributes['shipping'] + $this->attributes['order_value'], 2, '.', '' );
	}

}
