<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

final class GalleryImage extends Model
{
    protected $append = [
        'uri',
    ];

    public function getUriAttribute(){
        return "/usr/gallery/{$this->gallery}/{$this->filename}";
    }
}
