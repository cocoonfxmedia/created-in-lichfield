<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

class TeamMember extends Model {

	use SoftDeletes;

	/**
	 * Unguarded attributes exposed to mass assignment
	 *
	 * @var array
	 */
	public $fillable = [
		'name',
		'email',
		'position',
		'qualifications',
		'specialities',
		'bio',
		'published'
	];


	/**
	 * @param $value
	 *
	 * @return string
	 */
	public function getImageAttribute( $value )
	{
		if ( is_null( $value ) )
		{
			return '/images/anonymous.png';
		}


		return '/usr/team_members/' . $value;
	}


	/**
	 * @param $value
	 *
	 * @return string
	 */
	public function getImageOverAttribute( $value )
	{
		if ( is_null( $value ) )
		{
			return '/images/anonymous-over.png';
		}


		return '/usr/team_members/' . $value;
	}

	/**
	 * Get URL to profile page
	 *
	 * @param $value
	 */
	public function getUrlAttribute( $value )
	{
		return Config::get( 'cms.team.bio_page_url' ) . $this->attributes['slug'];
	}

	/**
	 * Only return published team members
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePublished( $query )
	{
		return $query->where( 'published', 1 );
	}
}
