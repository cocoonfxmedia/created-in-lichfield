<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderVoucher extends Model {


	/**
	 * Ungaurded fields
	 * @var type
	 */
	public $fillable = [ 'label',
		'code',
		'discount',
		'voucher_id',
		'order_id' ];

	public $timestamps = [ 'created_at',
		'updated_at' ];

	/**
	 * Form the discount amount value
	 * @param $value
	 * @return string
	 */
	public function getDiscountAttribute( $value )
	{
		return number_format( $value, 2 );
	}


}
