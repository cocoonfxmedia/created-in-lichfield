<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

	//disable default eloquent timestamps
	public $timestamps = false;


	//caste attributes to Carbon instance
	public $dates = [
		'received_at'
	];


}
