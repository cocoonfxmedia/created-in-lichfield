<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductQuery extends Model {


	/**
	 * Eloquent default timestamp fields used
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * Mass assignment guarding
	 * @var array
	 */
	public $fillable = [
		'name',
		'email',
		'message',
		'product_id'
	];


	public function product()
	{
		return $this->belongsTo('App\Models\Product');
	}

}
