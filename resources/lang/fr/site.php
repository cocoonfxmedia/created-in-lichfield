<?php
return [

	'search'      => 'SEARCH',
	'home'        => 'Home',

	//Error messaging
	'error'       => [
		'oops'  => 'Whoops!',
		'title' => 'Something went wrong.'
	],

	//Footer
	'copywrite'   => 'ALL RIGHTS RESERVED',
	'design_by'   => 'Web Design by',


	//Contact Us Page
	'contact'     => [
		'title'               => 'Contact us',
		'form_instruction'    => 'Please get in contact by filling in the form below',
		'name'                => 'Name',
		'email'               => 'Email',
		'telephone'           => 'Telephone',
		'message'             => 'Message',
		'message_placeholder' => 'Type your message here',
		'send'                => 'Send Message',
		'thanks'              => 'Your message has been successfully sent to our team.'
	],

	//News
	'news'        => 'News',
	'news_articles' => 'News & Articles',
	'categories'  => 'Categories',
	'no_articles' => 'There are no articles found here.',
	'read_more' => 'Read More',

	'tools'       => 'Useful Tools',
	'calculators' => 'Calculators',

	'vwc' => [
		'title' => 'Volumetric weight calculator',
		'transport', 'Transport',
		'air' => 'Air',
		'land' => 'land',
		'sea' => 'Sea',
		'qty' => 'Qty',
		'length' => 'Length',
		'width' => 'Width',
		'height' => 'Height',
		'volume' => 'Volume',
		'caveat1' => 'Quick tool to work out the volumetric weight.',
		'caveat2' => 'If the volumetric weight is heigher then the actual weight this is what you would be charged at'
	],

	'currency' => [
		'title' => 'Currency Convertor',
		'from' => 'From',
		'to' => 'To',
		'amount' => 'Amount',
		'convert' => 'Convert',
		'value' => 'Exchanged value'
	],

	'c02' => [
		'title' => 'Calculator for Freight Forwarding',
		'calculate' => 'Calculate',
		'type' => 'Freight Type',
		'air' => 'Airfreight',
		'road' => 'Roadfreight',
		'sea' => 'Seafreight',
		'weight' => 'Weight',
		'pol' => 'Departure',
		'pol_placeholder' => 'Port of loading',
		'pod' => 'Arrival',
		'pod_placeholder' => 'Port of distcharge',
		'air_caveat1' => 'Formulas and assumptions used for the calculation of CO2 emissions are based on accredited sources such as the Greenhouse Gas Protocol, the Clean Cargo Working Group or DEFRA; and served as a framework for the Global Transport Carbon Calculator (GTCC). ',
		'air_caveat2' => ' Distances are taken from Google Direction API and can actually shipping line and routes may vary dependant on local condition.',
		'start' => 'Start',
		'start_placeholder' => 'Driving from',
		'finish' => 'Finish',
		'finish_placeholder' => 'Driving to',
		'select_dropdown' => 'Please select an option from the dropdown list.',
		'distance' => 'Distance',
		'emissions' => 'Emissions'
	],

];