/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

//Vue.component('example', require('./components/Example.vue'));
Vue.component('notificationlist', require('./components/NotificationsList.vue'));
Vue.component('messageslist', require('./components/MessagesList.vue'));
Vue.component('tags-input', require('./components/TagsInput.vue'));


const app = new Vue({
    el: '#app',

    data: {
        notificationCount: 0,
        notifications: [],
        messageCount: 0,
        messages: [],
        reminderCount: 0,
        reminders: []
    },
    created: function () {
        /**
         * Get UI Notifications
         */
        // $.get('/hb/notifications', function (data) {
        //     this.notifications = data;
        //     this.notificationCount = this.notifications.length;
        // }.bind(this));
        //
        // $.get('/hb/reminders', function (data) {
        //     this.reminders = data;
        //     this.reminderCount = this.reminders.length;
        // }.bind(this));
        //
        // $.get('/hb/messages', function (data) {
        //     this.messages = data;
        //     this.messageCount = this.messages.length;
        // }.bind(this));
    },

    methods: {
        /**
         * Google Analytics reporting for UI messaging usage
         */
        recordNotificationsView: function (event) {
            ga('send', 'event', 'Messages', 'Notifications View');
        },
        recordRemindersView: function (event) {
            ga('send', 'event', 'Messages', 'Reminders View');
        },
        recordMessagesView: function (event) {
            ga('send', 'event', 'Messages', 'Messages View');
        }
    },
});



