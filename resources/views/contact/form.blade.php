@extends('site')

@section('meta_data')
    <title>{{trans('site.contact.title')}} - {{config('app.name')}}</title>
@endsection


@section('content')


    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7">
                <div class="bodyText">
                    <h1 class="defaulth1">{{trans('site.contact.title')}}</h1>

                    


                    {!! \App\Models\Page::where('url', 'contact-us')->where( 'sid', siteConfig('sid') )->first()->content !!}


                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-5">
                <div class="captureBox">


                    <div class="contactForm">

                        @if( isset($_GET['sent']))
                            <br>
                            <p class="alert alert-success">{{trans('site.contact.thanks')}}</p>
                        @endif

                        <br>
                        <p class="formText">{{trans('site.contact.form_instruction')}}</p>
                        {!! Form::open( ['url' => '/contact-us/send', 'method' => 'post' ]) !!}

                        <p>
                            {!! Form::label('name' , trans('site.contact.name')) !!}
                            {!! Form::text('name'  , old('name') , [ 'class' => 'form-control' , 'placeholder' => ''] ) !!}
                        </p>

                        <p>
                            {!! Form::label('email' , trans('site.contact.name')) !!}
                            {!! Form::text('email'  , old('email') , [ 'class' => 'form-control' , 'placeholder' => ''] ) !!}
                        </p>
                        <p>
                            {!! Form::label('Message' , trans('site.contact.message')) !!}
                            {!! Form::textarea('message'  , old('message') , [ 'class' => 'form-control' , 'placeholder' => trans('site.contact.message_placeholder')] ) !!}
                        </p>

                        <p>
                            {!! Form::input('submit' , 'Send', trans('site.contact.send')) !!}
                        </p>

                        @include('common._errors')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
