@extends('site')

@section('meta_data')
    <title>{{trans('site.contact.title')}} - {{config('app.name')}}</title>
@endsection

@section('content')
    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">
            <h1 class="page-heading bottom-indent">{{trans('site.contact.title')}}</h1>

            {!! Breadcrumbs::render() !!}


            <p class="alert alert-success">{{trans('site.contact.thanks')}}</p>
            <ul class="footer_links clearfix">
                <li>
                    <a class="btn btn-default button button-small" href="/">
				<span>
					<i class="icon-chevron-left"></i>{{trans('site.home')}}
				</span>
                    </a>
                </li>
            </ul>


        </div><!-- #center_column -->
    </div>
@endsection