@extends('emails.layout')

@section('email_content')

    <table>
        @foreach( $payload as $attr => $value )
            <tr>
                <th>{{title_case($attr)}}</th>
                <td>{{$value}}</td>
            </tr>
        @endforeach
    </table>

@endsection