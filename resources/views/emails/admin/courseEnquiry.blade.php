<table>
	<tr>
		<th>Course Title:</th>
		<td>{{$courseTitle}}</td>
	</tr>
	<tr>
		<td>Name:</td>
		<td>{{$name}}</td>
	</tr>
	<tr>
		<td>School Name:</td>
		<td>{{$schoolName}}</td>
	</tr>
	<tr>
		<td>Email:</td>
		<td>{{$email}}</td>
	</tr>
	<tr>
		<td>Telephone Number:</td>
		<td>{{$telephone}}</td>
	</tr>
	<tr>
		<td>How many attendees</td>
		<td>{{$attendees}}</td>
	</tr>
	<tr>
		<td>Name of Attendees</td>
		<td>{{$attendeeNames}}</td>
	</tr>
	<tr>
		<td>Name of person to Invoice</td>
		<td>{{$invoiceName}}</td>
	</tr>
	<tr>
		<td>Email address to send invoice to</td>
		<td>{{$invoiceEmail}}</td>
	</tr>
</table>