<h1>Question about {{$product->name}}</h1>

<table>
	<tr>
		<th class="pull-right">Name</th>
		<td></td>
		<td id="review-details-name">{{$name}}</td>
	</tr>
	<tr>
		<th class="pull-right">Email</th>
		<td></td>
		<td id="review-details-email">{{$email}}</td>
	</tr>
	<tr>
		<th class="pull-right">Product</th>
		<td></td>
		<td id="review-details-product"><a href="{{config('app.url')}}/shop/product/{{$product->slug}}">{{$product->name}}</a></td>
	</tr>
	<tr>
		<th class="pull-right">Question</th>
		<td></td>
		<td id="review-details-comment">{{$question}}</td>
	</tr>
</table>