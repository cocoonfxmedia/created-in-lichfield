<h1>Pending Product Review</h1>

<table>
	<tr>
		<th class="pull-right">Date</th>
		<td style="width: 20px;"> </td>
		<td id="review-details-date">{{$review->created_at->format('d/m/Y')}}</td>
	</tr>
	<tr>
		<th class="pull-right">Name</th>
		<td></td>
		<td id="review-details-name">{{$review->name}}</td>
	</tr>
	<tr>
		<th class="pull-right">Email</th>
		<td></td>
		<td id="review-details-email">{{$review->email}}</td>
	</tr>
	<tr>
		<th class="pull-right">Product</th>
		<td></td>
		<td id="review-details-product">{{$review->product->name}}</td>
	</tr>
	<tr>
		<th class="pull-right">Rating</th>
		<td></td>
		<td id="review-details-rating">{{$review->rating}}/5</td>
	</tr>
	<tr>
		<th class="pull-right">Comment</th>
		<td></td>
		<td id="review-details-comment">{{$review->comment}}</td>
	</tr>
</table>

<p><a href="{{url('/admin/reviews')}}">Click here to login</a> and review the pending product reviews</p>