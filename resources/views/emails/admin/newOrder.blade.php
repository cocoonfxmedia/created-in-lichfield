<h1>New Order</h1>

<table>
	<tr>
		<th style="text-align: left">Name</th>
		<td>{{$order->customer->name}}</td>
	</tr>
	<tr>
		<th style="text-align: left;">Email</th>
		<td>{{$order->customer->email}}</td>
	</tr>
	<tr>
		<th style="text-align: left">Order Ref</th>
		<td>{{$order->id}}
	</tr>
	<tr>
		<th style="text-align: left;">Payment processed on</th>
		<td>{{date('d/m/Y')}}</td>
	</tr>
</table>


<table style="border: 0;">
	<tr>
		<td><h2>Shipping Address</h2></td>
		<td width="20"></td>
		<td><h2>Billing Address</h2></td>
	</tr>
	<tr>


		<td><p>
				@if( $order->click_and_collect )

					This is a click and collect order for collection from the {{$order->store}}

				@else

					{{$order->shippingAddress->fullname}}<br/>
					{{$order->shippingAddress->address_1}}<br/>

					@if( $order->shippingAddress->address_2 != '' )
						{{$order->shippingAddress->address_2}}<br/>
					@endif

					@if( $order->shippingAddress->town_city != '' )
						{{$order->shippingAddress->town_city}}<br/>
					@endif

					{{$order->shippingAddress->county}}<br/>
					{{$order->shippingAddress->postcode}}

				@endif
			</p></td>
		<td></td>
		<td><p>
				{{$order->billingAddress->fullname}}<br/>
				{{$order->billingAddress->address_1}}<br/>

				@if( $order->billingAddress->address_2 != '' )
					{{$order->billingAddress->address_2}}<br/>
				@endif

				@if( $order->billingAddress->town_city != '' )
					{{$order->billingAddress->town_city}}<br/>
				@endif

				{{$order->billingAddress->county}}<br/>
				{{$order->billingAddress->postcode}}
			</p>
		</td>
	</tr>
</table>

<h2>Order Details</h2>
<table>
	<thead>
	<tr>
		<th style="text-align: left;">Product</th>
		<th style="text-align: left;">Qty</th>
		<th style="text-align: left;">Price</th>
	</tr>
	</thead>
	<tbody>
	@foreach( $order->lineItems as $item )
		<tr>
			<td>{{$item->product_name}}</td>
			<td>{{$item->quantity}}</td>
			<td>{{$item->price}}</td>
		</tr>
	@endforeach
	</tbody>
</table>
