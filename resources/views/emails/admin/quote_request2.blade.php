<h1>Quote Request</h1>

<table>
    <tr>
        <th>Company Name</th>
        <td>{{$quote->company}}</td>
    </tr>
    <tr>
        <th>Name</th>
        <td>{{$quote->name}}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td><a href="mailto:{{$quote->email}}">{{$quote->email}}</a></td>
    </tr>
    <tr>
        <th>Telephone No.</th>
        <td><a href="tel:{{str_replace(' ', '', $quote->telephone)}}">{{$quote->telephone}}</a></td>
    </tr>
    @if( !empty( $quote->vat))
        <tr>
            <th>VAT Number</th>
            <td>{{$quote->vat}}</td>
        </tr>
    @endif
    <tr>
        <th>Consignee</th>
        <td>{{$quote->consignee}}</td>
    </tr>
    <tr>
        <th>Port of Loading</th>
        <td>{{$quote->pol}}</td>
    </tr>
    <tr>
        <th>Port of Discharge</th>
        <td>{{$quote->pod}}</td>
    </tr>
    <tr>
        <th>Weight</th>
        <td>{{$quote->weight}}</td>
    </tr>
    <tr>
        <th>Volume</th>
        <td>{{$quote->volume}}</td>
    </tr>
    <tr>
        <th>Freight Type</th>
        <td>{{$quote->transport_type}}</td>
    </tr>
    <tr>
        <th>Qty</th>
        <td>{{$quote->qty}}</td>
    </tr>
    <tr>
        <th>Length</th>
        <td>{{$quote->length}}</td>
    </tr>
    <tr>
        <th>Width</th>
        <td>{{$quote->width}}</td>
    </tr>
    <tr>
        <th>Height</th>
        <td>{{$quote->height}}</td>
    </tr>
    <tr>
        <th>Calculated Volume</th>
        <td>{{$quote->calc_volume}}</td>
    </tr>
    <tr>
        <th>Description of goods</th>
        <td>{{$quote->description}}</td>
    </tr>
    <tr>
        <th>Description of goods</th>
        <td>{{$quote->description}}</td>
    </tr>
    @if( !empty( $quote->date))
        <tr>
            <th>Date of good are ready</th>
            <td>{{$quote->date->format('d/m/Y')}}</td>
        </tr>
    @endif
    <tr>
        <th>Hazardous Cargo</th>
        <td>
            @if( $quote->hazardous_cargo == 1 )
                Yes
            @else
                No
            @endif
        </td>
    </tr>
    @if( !empty( $quote->un_number))
        <tr>
            <th>UN Number</th>
            <td>{{$quote->un_number}}</td>
        </tr>
    @endif
</table>
