@if( count( $manufacturers) > 0 )

    <?php $topManufacturers = $manufacturers->chunk( 5 ); ?>

    <!-- Block manufacturers module -->
    <div id="manufacturers_block_left" class="block blockmanufacturer">
        <p class="title_block">
            <a href="/manufacturers" title="Manufacturers">
                Manufacturers
            </a>
        </p>
        <div class="block_content list-block">
            <ul>
                @foreach( $topManufacturers as $man )
                    <li class="first_item">
                        <a href="{!! url('/manufacturers/' . $man->slug) !!}" title="More about {{$man->name}}">
                            {{$man->name}}
                        </a>
                    </li>
                @endforeach
            </ul>
            <form action="/index.php" method="get">
                <div class="form-group selector1">
                    <select class="form-control" name="manufacturer_list">
                        <option value="0">All manufacturers</option>
                        @foreach( $manufacturers as $man )
                            <option value="{!! url('/manufacturers/' . $man->slug) !!}">{{$man->name}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        </div>
    </div>
    <!-- /Block manufacturers module -->
@endif