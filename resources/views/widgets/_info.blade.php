<!-- Block CMS module -->
<section id="informations_block_left_1" class="block informations_block_left">
    <p class="title_block">
        Information
    </p>
    <div class="block_content list-block">
        <ul>
            <li>
                <a href="/delivery" title="Delivery">
                    Shipments and returns
                </a>
            </li>
            <li>
                <a href="/contact-us" title="Our stores">
                    Contact Us
                </a>
            </li>
        </ul>
    </div>
</section>
<!-- /Block CMS module -->