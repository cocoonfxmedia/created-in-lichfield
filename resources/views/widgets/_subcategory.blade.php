@if( count( $category->children) > 0 )
    <!-- Block categories module -->
    <div id="categories_block_left" class="block">
        <h2 class="title_block">
            {{$category->name}}
        </h2>
        <div class="block_content">
            <ul class="tree dhtml">

                @foreach( $category->children as $subcategory )
                    <li>
                        <a href="/category/{{$subcategory->slug}}" title="{{$subcategory->name}}">
                            {{$subcategory->name}}
                        </a>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
    <!-- /Block categories module -->
@endif