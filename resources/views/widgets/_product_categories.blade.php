<!-- Block categories module -->
<div id="categories_block_left" class="block">
    <h2 class="title_block">
        Categories
    </h2>
    <div class="block_content">
        <ul class="tree dhtml">


            @foreach( $catalog_categories as $category )
                <li>
                    <a href="/category/{{$category->slug}}" title="{{$category->meta_description}}">
                        {{$category->name}}
                    </a>
                    @if( count( $category->children) > 0 )
                        <ul>
                            @foreach( $category->children as $subcategory )
                                <li>
                                    <a href="/category/{{$subcategory->slug}}" title="{{$subcategory->meta_description}}">
                                        {{$subcategory->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>

            @endforeach

        </ul>
    </div>
</div>
<!-- /Block categories module -->