@extends('layouts.basic')




@section('content')

<div class="container-fluid artistLogin">
	<div class="container">
	<div class="row">
		<div class=" col-xs-12 col-sm-4 col-sm-push-4">
		<div class="loginBox">

		<h1>Welcome Back!</h1>
		<form action="{{ route('artists-auth.login') }}" method="post">
		{{ csrf_field() }}

		<label for="email">Email</label>
		<input type="email" name="email" id="email" value="{{ old('email') }}">

		<br>

		<label for="password">Password</label>
		<input type="password" name="password" id="password">

		<br>

		
		<input type="checkbox" name="remember"> 
		<label>Remember me</label>

		<br>

		<button>Log in</button>
	</form>

	<br>

	<a href="{{ route('socialite', 'facebook') }}">Log in with Facebook</a>

	<br>

	<a href="{{ route('socialite', 'twitter') }}">Log in with Twitter</a>

	<br>

	<a href="{{ route('socialite', 'instagram') }}">Log in with Instagram</a>

		</div>
		</div>
	</div>
	</div>
</div>



@endsection


