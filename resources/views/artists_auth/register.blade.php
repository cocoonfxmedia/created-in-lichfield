@extends('layouts.basic')

@section('content')

<div class="container-fluid artistLogin">
	<div class="container">
	<div class="row">
		<div class=" col-xs-12 col-sm-4 col-sm-push-4">
		<div class="loginBox">

	<form action="{{ route('artists-auth.register') }}" method="post">
		{{ csrf_field() }}

		<label for="email">Email</label>
		<input type="email" name="email" id="email" value="{{ old('email') }}">

		<br>

		<label for="name">Name</label>
		<input type="text" name="name" id="name" value="{{ old('name') }}">

		<br>

		<label for="password">Password</label>
		<input type="password" name="password" id="password">

		<br>

		<label for="password_confirmation">Confirm Password</label>
		<input type="password" name="password_confirmation" id="password_confirmation">
		
		<br>

		<label for="accept_terms">Accept Terms and Contions</label> <br>
		<input type="checkbox" name="accept_terms" id="accept_terms">
		<label class="form-check-label" for="optin">I accept the <a href="/terms">terms & conditions</a></label>
		<br>

		<button>Register</button>
	</form>

	<br>

	<a href="{{ route('socialite', 'facebook') }}">Register with Facebook</a>

	<br>

	<a href="{{ route('socialite', 'twitter') }}">Register with Twitter</a>

	<br>

	<a href="{{ route('socialite', 'instagram') }}">Register with Instagram</a>
            
            
        </div>
		</div>
	</div>
	</div>
</div>


@endsection
