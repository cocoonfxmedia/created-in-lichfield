@extends('layouts.basic')

@section('content')

@if( !empty( $location->header_image))
<div class="headerImgsingle" style="background-image:url('{{$location->header_image}}');"> <a class="back" href="/locations">&#8592; Back to locations</a></div>
@endif

<!--- start container div -->
<div class="container">
  <!--- start row div -->
  <div class="row">
    <!--- start div -->
    <div class="col-sm-12 col-md-8 col-md-offset-2">

      <div class="locationImages">

        <!--
			@if( !empty( $location->profile_image))
				<img class="profileImg" src="{{$artist->profile_image}}" width="100px"/>
			@endif
            -->

      </div>

      <div class="locationInfo">
        <h1>{{$location->title}}</h1>
        <div class="locationCategory">
          <!-- artist category link to go here -->
        </div>
        <div class="locationContact">
          <!-- Email: {{$location->email}} | -->
          Website: <a href="{{$location->external_link}}" target="_blank">{{$location->external_link}}</a>
        </div>


        {{--
			 @if ($artist>canEdit())
				<a href="{{ route('artists.edit', $artist) }}">Edit Profile</a>
        <a href="{{ route('artists.gallery.edit', $artist) }}">Edit Gallery</a>
        @endif


        <div class="artistSocial">
          <a href="{{$artist->facebook_link}}" target="_blank"><img src="#facebook" width="48px" /></a>
          <a href="{{$artist->twitter_link}}" target="_blank"><img src="#twitter" width="48px" /></a>
          <a href="{{$artist->instagram_link}}" target="_blank"><img src="#instagram" width="48px" /></a>
        </div>
      </div>
      --}}


      <div class="locationBio">
        {{$location->description}}
      </div>
      <div class="locationGallery">
        {{$location->gallery_id}}
      </div>
    </div>

  </div>
 </div>

</div>
@endsection
