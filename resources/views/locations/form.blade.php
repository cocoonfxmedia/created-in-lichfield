@php($location = optional($location ?? null))

<label for="title">Title</label>
<input type="text" name="title" id="title" value="{{ old('title', $location->title) }}">

<br>

<label for="description">Description (optional)</label>
<textarea name="description" id="description">{{ old('description', $location->description) }}</textarea>

<br>

<label for="address">Address (optional)</label>
<textarea name="address" id="address">{{ old('address', $location->address) }}</textarea>

@if (config('services.google-maps.key'))
	<button id="geocode">Geocode</button>
@endif

<br>

<label for="latitude">Latitude (optional)</label>
<input type="number" name="latitude" id="latitude" step="0.00000001" value="{{ old('latitude', $location->latitude) }}">

<br>

<label for="longitude">Longitude (optional)</label>
<input type="number" name="longitude" id="longitude" step="0.00000001" value="{{ old('longitude', $location->longitude) }}">

<br>

<label for="external_link">External Link (optional)</label>
<input type="url" name="external_link" id="external_link" value="{{ old('external_link', $location->external_link) }}">

<br>

<label for="header_image">Header Image (optional)</label>
<input type="file" name="header_image" id="header_image" accept="image/*">

<br>

<label for="category_id">Category (optional)</label>
<tags-input name="categories" placeholder="Categories..." :whitelist='{!! $categories->toJson() !!}' :default='{!! $location_categories !!}'></tags-input>
<br>

<label for="gallery_id">Gallery (optional)</label>
<select name="gallery_id" id="gallery_id">
	<option {{ old('gallery_id', $location->gallery_id) ? '' : 'selected' }} value="">None</option>
	@foreach ($galleries as $gallery)
		@php($selected = $gallery->id == old('gallery_id', $location->gallery_id))
		<option value="{{ $gallery->id }}" {{ $selected ? 'selected' : '' }}>{{ $gallery->name }}</option>
	@endforeach
</select>

<br>

@if (config('services.google-maps.key'))
	<script>
		function initMap() {
			var geocoder = new google.maps.Geocoder();
			var $address = document.getElementById('address');
			var $latitude = document.getElementById('latitude');
			var $longitude = document.getElementById('longitude');

			document.getElementById('geocode').addEventListener('click', function (event) {
				event.preventDefault();

				geocoder.geocode({
					address: $address.value,
					region: 'gb',
				}, function (results, status) {
					if (status !== 'OK') {
						console.warn(status);
						return;
					}

					$latitude.value = results[0].geometry.location.lat().toFixed(8);
					$longitude.value = results[0].geometry.location.lng().toFixed(8);
				});
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google-maps.key') }}&callback=initMap" async defer></script>
@endif
