@extends('layouts.basic')

@section('content')
	<form action="{{ route('locations.update', $location) }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('put') }}

		@include('locations.form')

		<button>Update</button>
	</form>

	<form action="{{ route('locations.destroy', $location) }}" method="post">
		{{ csrf_field() }}
		{{ method_field('delete') }}

		<button>Delete</button>
	</form>
@endsection
