@extends('layouts.basic')

@section('content')
	<form action="{{ route('locations.store') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}

		@include('locations.form')

		<button>Create</button>
	</form>
@endsection
