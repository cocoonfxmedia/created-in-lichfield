@extends('layouts.basic')

@section('content')

<div class="col-sm-12">
  <h1>Locations</h1>
</div>

<!-- start bootstrap grid container -->
<div class="container-fluid">

  <!-- start bootstrap grid row-->
  <div class="row">

    @foreach ($locations as $location)

    <div class="col-sm-4 col-md-3">
      <div class="lsbox lsboxBdr">

        @if( !empty( $location->header_image))
        <a href="{{ route('locations.show', $location) }}">
          <div class="lsboxheaderImg" style="background-image:url('{{$location->header_image}}');"> </div>
        </a>
        @endif


        <div class="lsboxInfo">

          <h2>{{$location->title}}</h2>


          <a class="lsboxMoreinfo" href="{{ route('locations.show', $location) }}">More info</a>

        </div>


      </div>
    </div>


    @endforeach


    <!-- end bootstrap grid row-->
  </div>
  <!-- end bootstrap grid container-->
</div>
@endsection
