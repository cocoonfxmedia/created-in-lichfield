<ul>
    @foreach( $subnav->sortBy('title') as $li )
        @if( $li->published )
            <li>
                <a href="/{{$li->path}}">{{$li->title}}</a>
                @if( count( $li->children) > 0 )
                    @include('common._section_nav', ['subnav' => $li->children->sortBy('title')] )
                @endif
            </li>
        @endif
    @endforeach
</ul>