@if (count( $errors ) > 0)
    <div class="alert alert-danger no-border">

        <p>
            <strong>{{trans('site.error.oops')}}</strong> {{trans('site.error.title')}}
        </p>

        <ul>
            @foreach ( $errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif