@if(session('subscriber_msg'))

    <div class="alert alert-success" role="alert">
        Thanks for wanting to keep in touch. When we have something important to tell you we&rsquo;ll let you know.
    </div>

@else

    <p class="bold">Keep in touch. Sign up to receive our latest news and information.</p>

    {!! Form::open(['url' => 'newsletter', 'class'=> 'news-subscription form-inline']) !!}
    <div class="form-group">
        {!!  Form::text('subscriber_email', old('subscriber_email') , ['class' => 'form-control', 'placeholder' => 'Email']) !!}
        <button class="btn btn-default">Send</button>
    </div>

    {!! Form::close() !!}

@endif