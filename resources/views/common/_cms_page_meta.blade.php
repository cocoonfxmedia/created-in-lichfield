@section('meta_data')
    <title>
        @if( !empty($page->meta_title))
            {{$page->meta_title}}
        @else
            {{$page->title}}
        @endif
        - {{config('app.name')}}</title>
    <meta name="description" content="{{$page->meta_description}}"/>
@endsection