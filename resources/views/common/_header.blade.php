<div class="header-container">
    <header id="header">
        <div class="nav">
            <div class="container">
                <div class="row">
                    <nav><!-- Block user information module NAV  -->


                        @if( Auth::guest() )
                            <div class="header_user_info">
                                <a class="login" href="/login" rel="nofollow"
                                   title="Log in to your customer account">
                                    Sign in
                                </a>
                            </div>
                        @else
                            <div class="header_user_info">
                                <a class="logout" href="/logout" rel="nofollow"
                                   title="Log me out">
                                    Sign out
                                </a>
                            </div>
                            <div class="header_user_info">
                                <a class="login" href="/my-account" rel="nofollow"
                                   title="View your customer account">
                                    My Account
                                </a>
                            </div>
                    @endif
                    <!-- /Block usmodule NAV -->
                        <div id="contact-link">
                            <a href="/contact-us" title="Contact us">Contact us</a>
                        </div>
                        <span class="shop-phone"><i
                                    class="icon-phone"></i>Call us now: <strong>01827 767910</strong></span>
                    </nav>
                </div>
            </div>
        </div>
        <div>
            <div class="container">
                <div class="row">
                    <div id="header_logo">
                        <a href="/" title="Industrial Printer Scan">
                            <img class="logo img-responsive"
                                 src="/site/img/industrial-printer-scan-logo-1466174355.jpg"
                                 alt="Industrial Printer Scan" width="3417" height="1725"/>
                        </a>
                    </div>
                    <!-- Block search module TOP -->
                    <div id="search_block_top" class="col-sm-4 clearfix">
                        <form id="searchbox" method="get" action="/search">
                            <input class="search_query form-control" type="text" id="search_query_top" name="q"
                                   placeholder="Search" value=""/>
                            <button type="submit" class="btn btn-default button-search">
                                <span>Search</span>
                            </button>
                        </form>
                    </div>
                    <!-- /Block search module TOP --><!-- MODULE Block cart -->
                    <div class="col-sm-4 clearfix">
                        <div class="shopping_cart">
                            @include('cart._widget')
                        </div>
                    </div>

                    <div id="layer_cart" style="top: 120px;"></div> <!-- #layer_cart -->
                    <div class="layer_cart_overlay"></div>

                    <!-- /MODULE Block cart -->
                    <!-- Menu -->
                    <div id="block_top_menu" class="sf-contener clearfix col-lg-12">
                        <div class="cat-title">Menu</div>
                        @include('common._main_nav')
                    </div>
                    <!--/ Menu -->
                </div>
            </div>
        </div>
    </header>
</div>