@if( count( $slides) > 0  )

<div class="container-fluid sliderBarb">
<div class="row">
    <div class="slider" style="height: 800px; width: 100%;">
                    <ul>
                        @foreach( $slides as $slide )
                            <li>
                                @if( !empty( $slide->link))
                                    <a href="{{$slide->link}}">
                                @endif
                                        <img src="/usr/dip/{{$slide->image}}" alt=""/>
                                        {!! $slide->text !!}
                                @if( !empty( $slide->link))
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
    </div>
</div>
</div>

@endif


@push('scripts')
<script type="text/javascript" src="/site/js/unslider-min.js"></script>
<script type="text/javascript">

	jQuery(document).ready(function ($) {
		$('.slider').unslider({
			autoplay: true,
			infinite: true,
			nav: true,
			arrows: true,
			speed: 750, //How fast (in milliseconds) Unslider should animate between slides.
			delay: 5000 //If autoplay is set to true, how many milliseconds should pass between moving the slides?
		});
	});


</script>
@endpush