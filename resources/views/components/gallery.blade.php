<div class="gallery">

    @foreach( $gallery as $image)
        <div class="col-md-4">
            <a href="/usr/gallery/{{$image->gallery}}/{{$image->filename}}" rel="gallery" class="fancybox" title="{{$image->title}}">
                <img src="/usr/gallery/{{$image->gallery}}/{{$image->filename}}" alt="{{$image->title}}">
                <span class="title">{{$image->title}}</span>
            </a>
        </div>
    @endforeach

</div>
<div class="clearfix"></div>

@push('css')
<link rel="stylesheet" href="/js/plugins/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
@endpush

@push('scripts')
<script type="text/javascript" src="/js/plugins/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
@endpush