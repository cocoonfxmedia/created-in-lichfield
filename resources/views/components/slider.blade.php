@if( count( $slides) > 0  )

<div class="container-fluid sliderBar">
<div class="row">
    <div class="slider" style="height: 400px; width: 100%;">
        <ul>
            @foreach( $slides as $slide )
                <li>
                    @if( !empty( $slide->link))
                        <a href="{{$slide->link}}">
                            @endif

                            @if( !empty( $slide->image))
                                <img src="/usr/dip/{{$slide->image}}" alt=""/>
                            @endif
                            @if( !empty( $slide->text) )
                                <h1>{!! $slide->text !!}</h1>
                            @endif

                            @if( !empty( $slide->link))
                        </a>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</div>
</div>


@endif

@push('css')
    <link rel="stylesheet" href="/site/css/unslider.css">
@endpush

@push('scripts')
    <script type="text/javascript" src="/site/js/unslider-min.js"></script>
    <script type="text/javascript">

		jQuery(document).ready(function ($) {
			$('.slider').unslider({
				autoplay: true,
				infinite: true,
				nav: false,
				arrows: false,
				speed: 1500, //How fast (in milliseconds) Unslider should animate between slides.
				delay: 5000 //If autoplay is set to true, how many milliseconds should pass between moving the slides?
			});
		});

    </script>
@endpush