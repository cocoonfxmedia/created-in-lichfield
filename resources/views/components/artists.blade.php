<!-- start bootstrap grid container -->
<div class="ARTISTS artistBox container-fluid">

				<h2 class="text-center">Artists</h2>

        <!-- start bootstrap grid row-->
        <div class="row">

           @foreach ($artists as $artist)

            <div class="col-sm-4 col-md-3">

                  <div class="artist lsbox lsboxBdr">

                        <div class="artistImages">
                          @if( !empty( $artist->header_image))
                              <a href="{{ route('artists.show', $artist) }}"> <div class="lsboxheaderImg" style="background-image:url('{{$artist->header_image}}');"> </div></a>
                          @endif
                          @if( !empty( $artist->profile_image))
                            <a href="{{ route('artists.show', $artist) }}">  <img class="profileImg" src="{{$artist->profile_image}}" width="100px"/> </a>
                          @endif
                        </div>

                        <div class="lsboxInfo">

                           <h2>{{$artist->name}}</h2>

                           <div class="artistCategory">
                                <?php $categories=$artist->categories; ?>

                                <p>

                                @foreach ($categories as $category)

                                  {{$category->title}} |

                                @endforeach

                                </p>
                           </div>

                           <div class="artistSocial">
                                <a href="{{$artist->facebook_link}}" target="_blank"><img src="site/images/facebookIcon.png" width="24px"/></a>
                                <a href="{{$artist->twitter_link}}" target="_blank"><img src="site/images/twitterIcon.png" width="24px"/></a>
                                <a href="{{$artist->instagram_link}}" target="_blank"><img src="site/images/instagramIcon.png" width="24px"/></a>
                            </div>

                            <a class="lsboxMoreinfo" href="{{ route('artists.show', $artist) }}">More details</a>


                        </div>

                  </div>

            </div>

@endforeach

        <!-- end bootstrap grid row-->
        </div>


<!-- end bootstrap grid container -->
</div>
