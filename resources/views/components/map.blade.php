<div class="container-fluid trustBlock">
<div class="container">
<div class="row"> 
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h2 class="trustTitle">IFSC Group Office Locations</h2>
    		<div class="introductionBox trustBar">
<div id="map_content"></div>
                </div>
        </div>
</div> 
    </div>
</div>



	<?php
	$markers = [];

	$markerSource = explode( '|', siteConfig( 'offices' ) );

	if( count( $markerSource) > 0 )
	{
		foreach ( $markerSource as $raw )
		{
			$source = explode( ',', $raw );

			if( count($source) > 0 )
			{
				$markers[] = [ 'title' => $source[0], 'lat' => $source[1], 'lng' => $source[2] ];
			}
		}
	}

	?>


    @verbatim
        <script type="text/javascript">

			function initMap() {
				<?php foreach($markers as $i => $m ): ?>
				var pos<?php echo $i; ?>  = {lat: <?php echo $m['lat']; ?>, lng: <?php echo $m['lng']; ?>};
				<?php endforeach; ?>


				var map = new google.maps.Map(document.getElementById('map_content'), {
					zoom: 4,
					center: pos0
				});

				var bounds = new google.maps.LatLngBounds();

				<?php foreach( $markers as $i => $m ): ?>
				var marker = new google.maps.Marker({
					position: pos<?php echo $i; ?>,
					map: map,
					title: '<?php echo $m['title']; ?>'
				});
				bounds.extend({lat: <?php echo $m['lat']; ?>, lng: <?php echo $m['lng']; ?>})
				<?php endforeach; ?>

						

				map.fitBounds(bounds);
			}

        </script>
    @endverbatim
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCb3Q1tifrAO-It-i3e3OPJ6wqke6Z09I&callback=initMap"></script>

<style>
	#map_content {
		width: 100%;
		height: 350px;
	}
</style>
