<div class="container-fluid introductionBar">
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    		<div class="introductionBox">
        	<h1>Lichfield's online creative home for all things 3D and 2D</h1>
		<div class="introContent"><?php  echo Blade::compileString( $page->content ); ?></div>
    		</div>
	</div>
</div>
</div>