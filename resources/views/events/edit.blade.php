@extends('layouts.basic')

@section('content')
	<form action="{{ route('events.update', $event) }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		{{ method_field('put') }}

		@include('events.form')

		<button>Update</button>
	</form>

	<form action="{{ route('events.destroy', $event) }}" method="post">
		{{ csrf_field() }}
		{{ method_field('delete') }}

		<button>Delete</button>
	</form>
@endsection
