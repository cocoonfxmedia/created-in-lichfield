@extends('layouts.basic')

@section('content')
	<form action="{{ route('events.store') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}

		@include('events.form')

		<button>Create</button>
	</form>
@endsection
