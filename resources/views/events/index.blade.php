@extends('layouts.basic')

@section('content')

<div class="col-sm-12">
    <h1>Events</h1>
</div>



 <!-- start bootstrap grid container -->
 <div class="artistBox container-fluid">

         <!-- start bootstrap grid row-->
         <div class="row">

            @foreach ($events as $event)
            {{-- @php(dump($event->toArray())) --}}


                <div class="col-sm-4 col-md-3 eventList">
                <div class="artist lsbox lsboxBdr">

                            @if( !empty( $event->header_image))
                                <div class="lsboxheaderImg" style="background-image:url('{{$event->header_image}}');"> </div>
                            @endif


                        <div class="lsboxInfo">

                                <h2>{{$event->title}}</h2>

                                <p class="eventDate">{{$event->at}}</p>

                                <div class="eventDesc">
                                    <!-- {{$event->description}} -->
                                </div>

                                <div class="eventPrice">
                                    Price: &#163;{{$event->price}}
                                </div>

                                <div class="eventLoc">
                                    Venue:
                                    @foreach ($locations as $loc)
                                        @if($loc->id == $event->location_id)
                                            {{$loc->title}}
                                        @endif
                                    @endforeach
                                </div>

                                <div class="eventArtist">
                                        <div class="eventArtistName">Artist: <a href="../artists/{{$event->artist_id}}">
                                                @foreach ($artists as $artist)
                                                    @if($artist->id == $event->artist_id)
                                                        {{$artist->name}}
                                                    @endif
                                                @endforeach
                                            </a>
                                        </div>
                                       <!--  <a class="eventExternalLink" href="{{$event->external_link}}" target="_blank">View organiser's website</a> -->
                                </div>

                                <a class="lsboxMoreinfo" href="{{ route('events.show', $event) }}">More info</a>

                            </div>

                              <!--  <div class="eventGallery">
                                    <a href="../gallery/{{$event->gallery_id}}">View event gallery</a>
                                </div> -->





                </div>
                </div>


                @endforeach


        <!-- end bootstrap grid row-->
        </div>
<!-- end bootstrap grid container-->
</div>
@endsection
