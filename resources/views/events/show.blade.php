@extends('layouts.basic')

@section('content')
    {{-- @php(dump($event->toArray())) --}}

    <div class="event">

        @if( !empty($event->header_image))
           <div class="headerImgsingle" style="background-image:url('{{$event->header_image}}');"> <a class="back" href="/events">&#8592; Back to events</a></div>
        @endif

        <article class="container eventInfo">
            <!--- start row div -->
            <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
              <h2>{{$event->title}}</h2>
              <p class="eventDate">{{ $event->event_date }}</p>
              <div class="eventDesc">
                  {{$event->description}}
              </div>
              <div class="eventPrice">
                  Price: &#163;{{$event->price}}
              </div>
              <div class="eventLoc">
                  Venue:
                  @foreach ($locations as $loc)
                      @if($loc->id == $event->location_id)
                          {{$loc->title}}
                      @endif
                  @endforeach
                  <section class="eventArtist">
                      <div class="eventArtistName">Artist: <a href="../artist/{{$event->artist_id}}">
                              @foreach ($artists as $artist)
                                  @if($artist->id == $event->artist_id)
                                      {{$artist->name}}
                                  @endif
                              @endforeach
                          </a>
                      </div>
                      <a class="eventExternalLink" href="{{$event->external_link}}" target="_blank">Event organiser page</a>
                  </section>
              </div>
              <div class="eventGallery">
                  <a href="../gallery/{{$event->gallery_id}}">View event gallery</a>
              </div>
              <!--- end row div -->
              </div>
              </div>
        </article>
    <!--- end event div -->
    </div>
@endsection
