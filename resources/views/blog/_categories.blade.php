@if( count( $categories) > 0 )
    <div class="leftMenu">
        <h2>{{trans('site.categories')}}</h2>
        <ul class="blog-categories">
            @foreach( $categories as $cat )
                <li><a href="/blog/category/{{$cat->slug}}">{{$cat->title}}</a></li>
            @endforeach
        </ul>
    </div>
@endif