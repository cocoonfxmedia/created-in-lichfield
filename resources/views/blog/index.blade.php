@extends('site')

@section('meta_data')
    <title>
        @if( isset( $category))
            @if( !empty($category->meta_title))
                {{$category->meta_title}}
            @else
                {{$category->title}}
            @endif
            |
        @endif
        {{trans('site.news')}} | {{config('app.name')}}
    </title>

    @if( isset($category))
        @if( !empty($category->meta_description))
            <meta name="description" content="{{$category->meta_description}}"/>
        @endif
    @else
        <!--  Blog main page description -->
        <meta name="description" content=""/>
    @endif

@endsection


@section('content')

    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-8">
                    <div class="introductionBox">
                        @if( isset( $category))
                            <h1 class="defaulth1">{{trans('site.news')}}: {{$category->title}}</h1>
                        @else
                            <h1 class="defaulth1">{{trans('site.news')}}</h1>
                        @endif

                        @if( $articles->count() > 0 )
                            @foreach( $articles as $article )

                                <div class="col-md-6">
                                    <article class="news">
                                        @if( $article->featured_image)
                                            <a href="{{$article->link}}">
                                                <div class="thumb" style="background-image: url(/usr/blog_thumbs/{{$article->featured_image}});"></div>
                                            </a>
                                        @endif
                                        <h2>{{$article->title}}</h2>
                                        <div class="dateBlog">{{$article->created_at->format('F dS Y')}}</div>
                                        <p>{{$article->summary}}</p>
                                        <a class="btn btn-default" href="{{$article->link}}">{{trans('site.read_more')}}</a>
                                    </article>
                                </div>

                            @endforeach
                        @else

                            <p>{{trans('site.no_articles')}}</p>

                        @endif


                    </div>
                </div>
                <div class="col-md-4">
                    @include('blog._categories')

                    @include('blog._stories')
                    <h2>{{trans('site.tools')}}</h2>
                    <a href="https://www.fscoceans.com/customer-tools">{{trans('site.calculators')}}</a>
                    {{--@include('blog._archive')--}}
                </div>
            </div>
        </div>
    </div>

@endsection