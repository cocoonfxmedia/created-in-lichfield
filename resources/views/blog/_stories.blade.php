@if( count($newsStories) > 0 )

    <h2>{{trans('site.news_articles')}}</h2>

    <ul>
        @foreach($newsStories as $story )
            <li><a href="{{$story->link}}">{{$story->title}}</a></li>
        @endforeach
    </ul>

@endif