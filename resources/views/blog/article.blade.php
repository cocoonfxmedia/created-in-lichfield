@extends('site')

@section('meta_data')
    <title>{{$article->meta_title}}</title>
    @if( $article->meta_description )
        <meta name="description" content="{{$article->meta_description}}"/>
    @endif

@endsection


@section('content')

    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        @if( $article->featured_image)
                            <div class="thumbinner">

                                <img src="/usr/blog_thumbs/{{$article->featured_image}}" alt="{{$article->title}}">

                            </div>
                        @endif
                        <h1 class="defaulth1">{!!$article->title!!}</h1>
                        <div class="dateBlog">{{$article->created_at->format('F dS Y')}}</div>
                        {!! $article->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection