@if( count($caseStudyWidget) > 0 )

    <h2>Case Studies</h2>

    <ul>
        @foreach($caseStudyWidget as $story )
            <li><a href="/case-studies/{{$story->url}}">{{$story->title}}</a></li>
        @endforeach
    </ul>

@endif