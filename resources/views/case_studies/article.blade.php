@extends('site')

@section('header')
    <title>{{$article->meta_title}}</title>
    @if( $article->meta_description )
        <meta name="description" content="{{$article->meta_description}}"/>
    @endif
    <link rel="canonical" href="{{url('/case-studies/'.$article->url)}}"/>

@endsection


@section('content')

    <div class="yellowBar">
        <div class="row">
            <div class="small-12 medium-12 large-12 columns default">
                <h1 class="defaulth1">Case Study</h1>
            </div>
        </div>
    </div>

    <div class="row">


        <div class="col-md-8">

            <article bloginner>
                @if( $article->featured_image)
                    <div class="thumbinner">

                        <img src="/usr/case_study_thumbs/{{$article->featured_image}}" alt="{{$article->title}}">

                    </div>
                @endif
                <h2 class="blogH2">{{$article->title}}</h2>
                <div class="dateBlog">{{$article->created_at->format('F dS Y')}}</div>
                <div class="content">
                    {!! $article->content !!}
                </div>
            </article>


        </div>
        <div class="col-md-4 blogs">
            @include('case_studies._stories')
        </div>
    </div>


@endsection