@extends('site')

@section('meta_data')
    <title>
        @if( isset( $category))
            @if( !empty($category->meta_title))
                {{$category->meta_title}}
            @else
                {{$category->title}}
            @endif
            |
        @endif
        Blog | {{config('app.name')}}
    </title>

    @if( isset($category))
        @if( !empty($category->meta_description))
            <meta name="description" content="{{$category->meta_description}}"/>
        @endif
    @else
        <!-- Case Study main page description -->
        <meta name="description" content=""/>
    @endif

@endsection


@section('content')
    <div class="yellowBar">
        <div class="row">
            <div class="small-12 medium-12 large-12 columns default">
                <h1 class="defaulth1">Case Studies</h1>
            </div>
        </div>
    </div>
    <div class="row news">

        <div class="col-md-8">


            @foreach( $articles as $article )

                <div class="col-md-6">
                    <article class="news">
                        @if( $article->featured_image)
                            <div class="thumb">
                                <a href="/case-studies/{{$article->url}}">
                                    <img src="/usr/case_study_thumbs/{{$article->featured_image}}" alt="{{$article->title}}">
                                </a>
                            </div>
                        @endif
                        <h2>{{$article->title}}</h2>
                        <div class="dateBlog">{{$article->created_at->format('F dS Y')}}</div>
                        <p>{{$article->summary}}</p>
                        <a class="btn btn-default" href="/case-studies/{{$article->url}}">Read More</a>
                    </article>
                </div>

            @endforeach

        </div>


        <div class="col-md-4 blogs">
            @include('case_studies._stories')
        </div>

        @include('components.casestudyblock')

    </div>


@endsection