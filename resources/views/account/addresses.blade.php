@extends('site')

@section('content')
    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">
            <h1 class="page-heading">My addresses</h1>
            <p>Please configure your default billing and delivery addresses when placing an order. You may also add
                additional addresses, which can be useful for sending gifts or receiving an order at your office.</p>

            @if( count( $addresses) == 0 )
                <p class="alert alert-warning">No addresses are available.&nbsp;<a
                            href="/my-account/addresses/create">Add a new address</a></p>

            @else

                <div class="addresses">
                    <p><strong class="dark">Your addresses are listed below.</strong></p>
                    <p class="p-indent">Be sure to update your personal information if it has changed.</p>
                    <div class="bloc_adresses row">

                        @foreach( $addresses as $address )

                            <div class="col-xs-12 col-sm-6 address">
                                <ul class="last_item item box">
                                    <li><h3 class="page-subheading">{{$address->label}}</h3></li>
                                    <li>
                                        <span class="address_name">{{$address->first_name}} {{$address->last_name}}</span>
                                    </li>
                                    @if( !empty( $address->company))
                                        <li><span class="address_company">{{$address->company}}</span></li>
                                    @endif
                                    @if( !empty( $address->address_1))
                                        <li><span class="address_address1">{{$address->address_1}}</span></li>
                                    @endif
                                    @if( !empty( $address->address_2))
                                        <li><span class="address_address2">{{$address->address_2}}</span></li>
                                    @endif
                                    @if( !empty( $address->town_city))
                                        <li><span class="address_city">{{$address->town_city}}</span></li>
                                    @endif
                                    @if( !empty( $address->county))
                                        <li><span>{{$address->county}}</span></li>
                                    @endif
                                    @if( !empty( $address->country))
                                        <li><span>{{$address->country}}</span></li>
                                    @endif
                                    @if( !empty( $address->postcode))
                                        <li>
                                            <span class="address_phone">{{$address->postcode}}</span>
                                        </li>
                                    @endif

                                    <li class="address_update">
                                        {{--<a class="btn btn-default button button-small" href="/my-account/addresses/{{$address->id}}/edit" title="Update"><span>Update<i class="icon-chevron-right right"></i></span></a>--}}


                                        <a class="btn btn-default button button-small"
                                           href="/my-account/addresses/{{$address->id}}/delete"
                                           data-id="addresses_confirm" title="Delete"><span>Delete<i
                                                        class="icon-chevron-right right"></i></span></a>
                                    </li>
                                </ul>
                            </div>

                        @endforeach


                    </div>
                </div>


            @endif

            <div class="clearfix main-page-indent">
                <a href="/my-account/addresses/create" title="Add an address"
                   class="btn btn-default button button-medium"><span>Add a new address<i
                                class="icon-chevron-right right"></i></span></a>
            </div>
            <ul class="footer_links clearfix">
                <li><a class="btn btn-default button button-small" href="/my-account"><span><i
                                    class="icon-chevron-left"></i> Back to your account</span></a></li>
                <li><a class="btn btn-default button button-small" href="/"><span><i class="icon-chevron-left"></i> Home</span></a>
                </li>
            </ul>

        </div><!-- #center_column -->
    </div>
@endsection