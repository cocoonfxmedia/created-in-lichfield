
<div class="required text form-group">
    <label for="firstname">First name <sup>*</sup></label>
    <input class="text form-control validate" id="firstname" name="fao_first_name" data-validate="isName" value=""
           type="text">
</div>
<div class="required text form-group">
    <label for="lastname">Last name <sup>*</sup></label>
    <input class="text form-control validate" id="lastname" name="fao_last_name" data-validate="isName" value=""
           type="text">
</div>
<div class="text form-group">
    <label for="company">Company</label>
    <input class="text form-control validate" id="company" name="company" data-validate="isGenericName" value=""
           type="text">
</div>
<div class="required text form-group">
    <label for="address1">Address <sup>*</sup></label>
    <input class="text form-control validate" name="address_1" id="address_1" data-validate="isAddress" value=""
           type="text">
</div>
<div class="text is_customer_param form-group">
    <label for="address2">Address (Line 2)</label>
    <input class="text form-control validate" name="address_2" id="address_2" data-validate="isAddress" value="" type="text">
</div>
<div class="required text form-group">
    <label for="city">City <sup>*</sup></label>
    <input class="text form-control validate" name="town_city" id="city" data-validate="isCityName" value="" type="text">
</div>
<div class="required text form-group">
    <label for="County">County <sup>*</sup></label>
    <input class="text form-control validate" name="county" id="county" value="" type="text">
</div>
<div class="required postcode text form-group" style="display: block;">
    <label for="postcode">Zip/Postal code <sup>*</sup></label>
    <input class="text form-control validate uniform-input" name="postcode" id="postcode"
           data-validate="isPostCode" value="" type="text">
</div>
<div class="required select form-group" style="">
    <label for="id_country">Country <sup>*</sup></label>
    <select name="country" id="id_country" class="form-control" style="">
        <option value="GB" selected="selected">United Kingdom</option>
    </select>
</div>

<div class="form-group is_customer_param">
    <label for="other">Additional information</label>
    <textarea class="form-control" name="additional_info" id="other" cols="26" rows="7"></textarea>
</div>
