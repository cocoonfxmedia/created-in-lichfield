@extends('site')

@section('content')

    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">
            <div class="box">
                <h1 class="page-subheading">Your addresses</h1>
                <p class="info-title">
                    To add a new address, please fill out the form below.
                </p>


                <p class="required"><sup>*</sup>Required field</p>
                <form action="/my-account/addresses/create" method="post" class="std" id="add_address">
                    {{csrf_field()}}

                    @include('account._address_form')


                    <div class="clearfix"></div>
                    <div class="required form-group" id="adress_alias">
                        <label for="alias">Please assign an address title for future reference. <sup>*</sup></label>
                        <input id="alias" class="is_required validate form-control" data-validate="isGenericName"
                               name="label" value="My address" type="text">
                    </div>

                    <p class="submit2">
                        <input name="id_address" value="0" type="hidden"> <input name="back" value="order.php?step=1"
                                                                                 type="hidden"> <input
                                name="select_address" value="0" type="hidden"> <input name="token"
                                                                                      value="893be2f15508e07543a8f44a2203f33c"
                                                                                      type="hidden">
                        <button type="submit" name="submitAddress" id="submitAddress"
                                class="btn btn-default button button-medium">
				<span>
					Save
					<i class="icon-chevron-right right"></i>
				</span>
                        </button>
                    </p>
                </form>
            </div>
            <ul class="footer_links clearfix">
                <li>
                    <a class="btn btn-defaul button button-small" href="http://industrialprinterscan.uk/gb/addresses">
                        <span><i class="icon-chevron-left"></i> Back to your addresses</span>
                    </a>
                </li>
            </ul>
        </div><!-- #center_column -->
    </div>

@endsection