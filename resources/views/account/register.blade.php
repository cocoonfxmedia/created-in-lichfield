@extends('site')

@section('content')

    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">
            <div id="noSlide" style="display: block;"><h1 class="page-heading">Create an account</h1>


                <!---->
                <form action="/register" method="post" id="account-creation_form" class="std box">
                    {{csrf_field()}}

                    @include('account._registration_form'   )

                    <div class="submit clearfix">
                        <input name="email_create" value="1" type="hidden">
                        <input name="is_new_customer" value="1" type="hidden">
                        <input class="hidden" name="back" value="my-account" type="hidden">
                        <button type="submit" name="submitAccount" id="submitAccount"
                                class="btn btn-default button button-medium">
                            <span>Register<i class="icon-chevron-right right"></i></span>
                        </button>
                        <p class="pull-right required"><span><sup>*</sup>Required field</span></p>
                    </div>
                </form>
            </div>
        </div><!-- #center_column -->
    </div>

@endsection


@push('css')
<link rel="stylesheet" href="/site/themes/default-bootstrap/css/authentication.css" type="text/css" media="all"/>
@endpush


@push('scripts')
<script type="text/javascript" src="/site/themes/default-bootstrap/js/authentication.js"></script>
<script type="text/javascript" src="/site/js/validate.js"></script>
@endpush
