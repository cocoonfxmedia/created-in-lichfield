@extends('site')

@section('content')

    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">


            <h1 class="page-heading bottom-indent">Order history</h1>
            <p class="info-title">Here are the orders you've placed since your account was created.</p>
            <div class="block-center" id="block-history">
                <p class="alert alert-warning">You have not placed any orders.</p>
            </div>
            <ul class="footer_links clearfix">
                <li>
                    <a class="btn btn-default button button-small" href="my-account">
			<span>
				<i class="icon-chevron-left"></i> Back to Your Account
			</span>
                    </a>
                </li>
                <li>
                    <a class="btn btn-default button button-small" href="/">
                        <span><i class="icon-chevron-left"></i> Home</span>
                    </a>
                </li>
            </ul>
        </div><!-- #center_column -->
    </div>

@endsection