@extends('site')


@section('content')

    <div class="row" id="my-account">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">


            <h1 class="page-heading">My account</h1>
            <p class="info-account">Welcome to your account. Here you can manage all of your personal information and
                orders.</p>
            <div class="row addresses-lists">
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <ul class="myaccount-link-list">
                        <li>
                            <a href="/my-account/orders" title="Orders">
                                <i class="fa fa-bars"></i><span>Order history and details</span>
                            </a>
                        </li>
                        <li>
                            <a href="/my-account/addresses" title="Addresses">
                                <i class="fa fa-building"></i><span>My addresses</span>
                            </a>
                        </li>
                        <li>
                            <a href="/my-account/identity" title="Information">
                                <i class="icon-user"></i><span>My personal information</span>
                            </a>
                        </li>
                        <li>
                            <a href="/logout" title="Information">
                                <i class="icon-chevron-left"></i><span>Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="footer_links clearfix">
                <li>
                    <a class="btn btn-default button button-small" href="/" title="Home"><span>
                        <i class="icon-chevron-left"></i> Home</span>
                    </a>
                </li>
            </ul>
        </div><!-- #center_column -->
    </div>

@endsection


@push('css')
<link rel="stylesheet" href="/site/themes/default-bootstrap/css/my-account.css" type="text/css" media="all"/>
@endpush

@push('scripts')
<script src="https://use.fontawesome.com/0b0e2e0bfb.js"></script>
@endpush