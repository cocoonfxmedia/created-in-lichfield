<div class="account_creation">
    <h3 class="page-subheading">Your personal information</h3>
    <p class="required"><sup>*</sup>Required field</p>
    <div class="clearfix">
        <label>Title</label>
        <br>
        <div class="radio-inline">
            <label for="id_gender1" class="top">
                <input name="title" id="id_gender1" value="Mr" type="radio">
                Mr.
            </label>
        </div>
        <div class="radio-inline">
            <label for="id_gender2" class="top">
                <input name="title" id="id_gender2" value="Mrs" type="radio">
                Mrs.
            </label>
        </div>
    </div>
    <div class="required form-group">
        <label for="customer_firstname">First name <sup>*</sup></label>
        <input onkeyup="$('#firstname').val(this.value);" class="is_required validate form-control"
               data-validate="isName" id="customer_firstname" name="first_name" value=""
               type="text">
    </div>
    <div class="required form-group">
        <label for="customer_lastname">Last name <sup>*</sup></label>
        <input onkeyup="$('#lastname').val(this.value);" class="is_required validate form-control"
               data-validate="isName" id="customer_lastname" name="last_name" value=""
               type="text">
    </div>

    <div class="form-group is_customer_param">
        <label for="phone">Home phone <sup>**</sup></label>
        <input class="text form-control validate" name="landline" id="phone" data-validate="isPhoneNumber" value=""
               type="text">
    </div>
    <div class="required form-group">
        <label for="phone_mobile">Mobile phone <sup>**</sup></label>
        <input class="text form-control validate" name="mobile" id="phone_mobile" data-validate="isPhoneNumber" value=""
               type="text">
    </div>


    <div class="required form-group">
        <label for="email">Email <sup>*</sup></label>
        <input class="is_required validate form-control" data-validate="isEmail" id="email"
               name="email" value="<?php if( isset($_GETp['email'])) echo $_GET['email']; ?>" type="email">
    </div>
    <div class="required password form-group">
        <label for="passwd">Password <sup>*</sup></label>
        <input class="is_required validate form-control" data-validate="isPasswd" name="passwd"
               id="passwd" type="password">
        <span class="form_info">(Five characters minimum)</span>
    </div>

    <div class="checkbox">
        <div class="checker" id="uniform-newsletter"><span><input name="newsletter" id="newsletter"  value="1" type="checkbox"></span>
        </div>
        <label for="newsletter">Sign up for our newsletter!</label>
    </div>
</div>