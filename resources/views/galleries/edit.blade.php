@extends('layouts.basic') 


                     
                     
@section('content')


<div class="container">
 <div class="row">
    <div class="col-sm-12">
    <h1>Gallery editor</h1>  

<div class="galleryUploader">

<h3>Upload image to gallery</h3>
<form action="{{ route('artists.gallery.update', $artist) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }} {{ method_field('put') }}
    @include('galleries.form')

    <button>Update</button>
</form>
</div>

<div class="galleryThumbs">
@foreach ($gallery->images as $image)
    <div class="pull-left" style="width: 200px; height:200px; margin: 0 20px 20px 0;">
        <img src="{{$image->uri}}" class="img-responsive">
        <form action="{{ route('artists.gallery.removeImage', [$artist, $image->id]) }}" method="POST">
            {{ method_field('DELETE') }} {{ csrf_field() }}
            <button class="btn btn-xs btn-primary removeGalleryitem">Remove Image</button>
        </form>
    </div>

@endforeach
    </div>

     </div>
    </div>
</div>



@endsection