
<label for="description">Description (optional)</label>
<textarea name="description" id="description">{{ old('description', $gallery->description) }}</textarea>

<br>

<label for="header_image">Add Images</label>
<input type="file" name="images[]" multiple accept="image/*">

<br>