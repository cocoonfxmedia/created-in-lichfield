@include('admin.common._errors')

<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', old('name'),  ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email Address') !!}
    {!! Form::email('email', old('email'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('position', 'Position') !!}
    {!! Form::text('position', old('position'), ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('qualifications', 'Qualifications') !!}
    {!! Form::text('qualifications', old('qualifications'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('specialities', 'Specialities') !!}
    {!! Form::text('specialities', old('specialities'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('bio', 'Bio') !!}
    {!! Form::textarea('bio', old('bio'), ['class' => 'form-control'] ) !!}
</div>

<div class="form-group">

    <div class="col-md-3">
        {!! Form::label('image', 'Image') !!}
        {!! Form::file('image', null, ['class' => 'form-control'] ) !!}
    </div>

    @if( isset( $member ) && !is_null( $member->image))
        <div class="col-md-2">
            <img src="{{$member->image}}" alt="" class="img-responsive"/>
        </div>
    @endif

    <div class="clearfix"></div>
</div>


<div class="form-group">

    <div class="col-md-3">
        {!! Form::label('image_over', 'Image Overlay') !!}
        {!! Form::file('image_over', null, ['class' => 'form-control'] ) !!}
    </div>

    @if( isset( $member ) && !is_null( $member->image_over))
        <div class="col-md-2">
            <img src="{{$member->image_over}}" alt="" class="img-responsive"/>
        </div>
    @endif

    <div class="clearfix"></div>
</div>

<div class="form-group">
    {!! Form::label('published', 'Published') !!}
    {!! Form::checkbox('published',1, old('published')) !!}
</div>

<div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!} or <a href="/admin/team-members">Cancel</a>
</div>
