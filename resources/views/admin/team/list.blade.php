@extends('admin.app')


@section('page_title')
    <h4>Team Members</h4>
@endsection

@push('headerButtons')
    <a href="#" class="btn btn-link btn-primary btn-float has-text do_save_order"><i class="icon-list-ordered text-primary"></i><span>Save Order</span></a>

    <a href="team-members/create" class="btn btn-link btn-float has-text"><i class="icon-user-plus text-primary"></i><span>Add New Member</span></a>
@endpush

@section('content')

    <style>
        .teamlist li {
            list-style: none;
        }
    </style>

    <div class="col-md-12">

        @if( count($members) == 0 )
            <div class="alert alert-primary no-border">
                There are no team members at this time.
                <a href="/admin/team-members/create">Click here to add a new member to the team.</a>
            </div>
        @endif

        <ol class="teamlist">

            @foreach( $members as $member )

                <li data-id="{{$member->id}}">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="{{$member->image}}" alt="" class="img-responsive"/>
                        </div>
                        <div class="col-md-3">
                            {{$member->name}}
                        </div>
                        <div class="col-md-3">
                            {{$member->position}}
                        </div>
                        <div class="col-md-2">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="/admin/team-members/{{$member->id}}"><i class="icon-pencil"></i> Edit</a></li>
                                        <li><a href="/admin/team-members/{{$member->id}}/delete" class="do_delete"><i class="icon-bin"></i> Delete</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>

            @endforeach

        </ol>
    </div>


@endsection


@push('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
		$("ol.teamlist").sortable();

		$('.do_save_order').click(function () {
			var button = $(this).find('span');

			button.html('Updating...');

			var nav = [];

			//translate order into variable
			$('ol.teamlist li').each(function (index) {
				nav[index] = $(this).attr('data-id');
			});

			//submit order to server side
			$.post('/admin/team-members/order', {'nav': JSON.stringify(nav)}, function (response) {
				if (response == 'OK') {
					$('.do_save_order').find('span').html('Order Updated');
				}
			});

			return false;
		});
    </script>

@endpush
