@extends('admin.app')

@section('page_title')
    <h4>Edit Team Member</h4>
@endsection


@section('content')

    @include('admin.common._message')

    {!! Form::model($member, ['files' => true]) !!}

    @include('admin.team._form')

    {!! Form::close() !!}

@endsection