@extends('admin.app')

@section('page_title')
    <h4>New Team Member</h4>
@endsection

@section('content')

    {!! Form::open(['files' => true ]) !!}

    @include('admin.team._form')

    {!! Form::close(  ) !!}

@endsection