<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li data-nav-section="dashboard"><a href="/admin/"><i class="icon-grid3"></i> <span>Dashboard</span></a>
            </li>

            @if( Config::get('cms.components.pages'))
                @if( config('cms.components.page-blocks'))
                    <li data-nav-section="pages">
                        <a href="#"><i class="icon-files-empty"></i> <span>Pages</span></a>
                        <ul>
                            <li data-nav-section="pages-pages"><a href="/admin/pages"> <span>Manage Pages</span></a>
                            </li>
                            <li data-nav-section="pages-blocks"><a href="/admin/blocks"> <span>Page Blocks</span></a>
                            </li>
                        </ul>
                    </li>
                @else
                    <li data-nav-section="pages-pages"><a href="/admin/pages"><i class="icon-files-empty"></i>
                            <span>Pages</span></a>
                @endif

            @endif

            @if( Config::get('cms.components.blog'))
                <li data-nav-section="blog">
                    <a href="/admin/blog"><i class="icon-newspaper"></i> <span>Blog</span></a>
                    <ul>
                        <li data-nav-section="blog-articles"><a href="/admin/blog">Articles</a></li>
                        <li data-nav-section="blog-categories"><a href="/admin/blog-categories">Categories</a></li>
                    </ul>
                </li>
            @endif


            @if( Config::get('cms.components.case-studies'))
                <li data-nav-section="case-studies">
                    <a href="/admin/case-studies"><i class="icon-stars"></i> <span>Case Studies</span></a>
                    <ul>
                        <li data-nav-section="case-studies"><a href="/admin/case-studies">Studies</a></li>
                        <li data-nav-section="case-study-categories">
                            <a href="/admin/case-study-categories">Categories</a></li>
                    </ul>
                </li>
            @endif


            @if(config('cms.components.testimonials'))
                <li data-nav-section="users"><a href="/admin/testimonials"><i class="icon-bubbles"></i>
                        <span>Testimonials</span></a></li>
            @endif




            @if( Config::get('cms.components.slider'))
                <li data-nav-section="sliders"><a href="/admin/sliders"><i class="icon-images2"></i>
                        <span>Sliders</span></a></li>
            @endif

            @if( Config::get('cms.components.team-members'))
                <li data-nav-section="team"><a href="/admin/team-members"><i class="icon-people"></i> <span>Team</span></a>
                </li>
            @endif

            @if( Config::get('cms.components.gallery'))
                <li data-nav-section="galleries"><a href="/admin/gallery"><i class="icon-stack-picture"></i>
                        <span>Galleries</span></a></li>
            @endif

            @if( Config::get('cms.components.menuEditor'))
                <li data-nav-section="menus"><a href="/admin/menus"><i class="icon-direction"></i>
                        <span>Menus</span></a></li>
            @endif

            @if( Config::get('cms.components.ecommerce'))

                <li data-nav-section="products">
                    <a href="#"><i class="icon-cube4"></i> <span>Products</span></a>
                    <ul>
                        <li data-nav-section="products-catalog"><a href="/admin/products">Product Catalogue</a></li>
                        <li data-nav-section="products-categories"><a href="/admin/product-categories">Categories</a>
                        </li>
                        @if( Config::get('cms.components.ecommerce_reviews') || Config::get('cms.components.ecommerce_queries'))
                            <li class="divider"></li>
                        @endif
                        @if( Config::get('cms.components.ecommerce_reviews'))
                            <li data-nav-section="products-reviews"><a href="/admin/reviews/">Reviews</a></li>
                        @endif
                        @if( Config::get('cms.components.ecommerce_queries'))
                            <li data-nav-section="product-queries"><a href="/admin/product-queries">Queries</a></li>
                        @endif
                        @if( Config::get('cms.components.mass_update') || Config::get('cms.components.mass_import'))
                            <li class="divider"></li>
                        @endif
                        @if( Config::get('cms.components.mass_import'))
                            <li data-nav-section="products-import"><a href="/admin/import-products">Import Products</a>
                            </li>
                        @endif
                        @if( Config::get('cms.components.mass_update'))
                            <li><a href="/admin/mass-product-update">Mass Update Products</a></li>
                        @endif
                        @if( Config::get('cms.components.priceAdjust'))
                            <li data-nav-section="products-mas-price-adjust">
                                <a href="/admin/price-adjust">Mass Price Adjustment</a></li>
                        @endif
                    </ul>
                </li>

                <li data-nav-section="orders"><a href="/admin/customers"><i class="icon-cart2"></i> <span>Orders</span></a>
                </li>

                @if( Config::get('cms.components.ecommerce_vouchers'))
                    <li data-nav-section="vouchers"><a href="/admin/vouchers"><i class="icon-price-tag3"></i>
                            <span>Vouchers</span></a></li>
                @endif

                {{--<li>--}}
                {{--<a href="/"><i class="icon-stats-growth"></i> <span>Insights</span></a>--}}
                {{--<ul>--}}
                {{--<li><a href="/admin/reporting/orders">Orders</a></li>--}}
                {{--<li><a href="/admin/reporting/customers">Customers</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}

            @endif

            @if( Config::get('cms.components.newsletters'))
                <li data-nav-section="newsletter-subscriptions">
                    <a href="/admin/newsletter-subscriptions">
                        <i class="icon-mail5"></i>
                        <span>Newsletter Subscriptions</span></a>
                </li>
            @endif

            <li data-nav-section="artists"><a href="/admin/artists"><i class="icon-paint-format"></i> <span>Artists</span></a></li>

            <li data-nav-section="events"><a href="/admin/events"><i class="icon-calendar2"></i> <span>Events</span></a></li>

            <li data-nav-section="locations"><a href="/admin/locations"><i class="icon-location3"></i> <span>Locations</span></a></li>

            <li data-nav-section="categories"><a href="/admin/categories"><i class="icon-price-tag2"></i> <span>Categories</span></a></li>

            <li data-nav-section="users"><a href="/admin/users"><i class="icon-user"></i> <span>Users</span></a></li>

            <li data-nav-section="config">
                <a href="/admin/config"><i class="icon-cog"></i> <span>Settings</span></a>
            </li>

            @permission('tools')
            <li data-nav-section="tools">
                <a href="#"><i class=" icon-hammer-wrench"></i> <span>Tools</span></a>
                <ul>
                    @if( config('cms.components.multisite'))
                        <li data-nav-section="products-catalog"><a href="/admin/dupe-site">Duplicate Site</a></li>
                    @endif
                </ul>
            </li>
            @endpermission

            <!-- /main -->

        </ul>
    </div>
</div>