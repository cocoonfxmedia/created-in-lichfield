<div class="navbar-collapse collapse" id="navbar-mobile">
    <ul class="nav navbar-nav navbar-right">

        <li class="dropdown" id="hdr_notifications">
            <a href="{{siteConfig('url')}}" target="_blank" title="Got to website">
                <i class="icon-home2"></i>
                <span class="visible-xs-inline-block position-right">View Site</span>
            </a>
        </li>

        @if( config('cms.components.multisite'))
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">

                    <span>Site: {{siteConfig('site_name')}}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    @foreach( Config::get('multisite.sites') as  $siteId => $site )
                        @if( session('currentSite', 1) != $siteId && $siteId != 0 )
                            <li><a href="/admin/set-site/{{$siteId}}"> {{siteConfig('site_name', $siteId)}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
        @endif

        <li class="dropdown dropdown-user">
            <a class="dropdown-toggle" data-toggle="dropdown">

                <span>{{Auth::user()->name}}</span>
                <i class="caret"></i>
            </a>

            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="/admin/logout" id="doAuthLogout"><i class="icon-switch2"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
