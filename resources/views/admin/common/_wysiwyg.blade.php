@push('scripts')

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/redactor.min.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/imagemanager.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/alignment.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/video.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/table.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/audio.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/source.js"></script>
<script type="text/javascript">
    jQuery('.redactor_editor').redactor({
        imageUpload: '/admin/images',
        imageManagerJson: '/admin/images',
        pastePlainText: true,
        plugins: ['imagemanager', 'alignment', 'video', 'table', 'audio', 'source']
    });
</script>

@endpush

@push('css')
<link href="/backoffice/js/plugins/redactor/redactor.css" rel="stylesheet">
<link href="/backoffice/js/plugins/redactor/alignment.css" rel="stylesheet">
@endpush