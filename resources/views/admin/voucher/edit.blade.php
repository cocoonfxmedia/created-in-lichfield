@extends('admin.app')

@section('page_title')
    <h1>Edit Voucher: <span>{{$voucher->label}}</span></h1>
    @endsection

@section('content')

{!! Form::model( $voucher, ['url' => '/admin/vouchers/update/' . $voucher->id, 'method' => 'post' ]) !!}

@include('admin.voucher._form')

<button type="submit" class="btn btn-primary">Save</button>
<a href="{{url('admin/vouchers')}}">Cancel</a>

{!! Form::close() !!}

@endsection