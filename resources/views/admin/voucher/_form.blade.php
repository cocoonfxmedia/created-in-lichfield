<div class="row">

	<div class="col-sm-12 col-md-6 col-lg-8">

		@include('admin.common._errors')

		<p class="form-group">
			{!! Form::label('label' , 'Label') !!}
			{!! Form::text('label'  , old('label') , [ 'class' => 'form-control' , 'placeholder' => 'Voucher Label'] ) !!}
		</p>

		<p class="form-group">
			{!! Form::label('code' , 'Voucher Code') !!}
			{!! Form::text('code'  , old('code') , [ 'class' => 'form-control' , 'placeholder' => 'TENOFF'] ) !!}
		</p>

		<p class="form-group">
			{!! Form::label('discount_type', 'Discount Type') !!}
			<select name="type" id="discount_type" class="form-control">
				<option value="direct" <?php if( old( 'type', $voucher->type ) == 'direct' ) echo 'selected'; ?>>Direct price reduction</option>
				<option value="percent"<?php if( old( 'type', $voucher->type ) == 'percent' ) echo 'selected'; ?>>Percentage discount</option>
			</select>
		</p>

		<div class="form-group" id="discount_group">
			{!! Form::label('discount' , 'Discount') !!}
			<div class="input-group">
				<div class="input-group-addon"></div>
				{!! Form::text('discount'  , old('discount') , [ 'class' => 'form-control' , 'placeholder' => 'Discount value'] ) !!}
			</div>

		</div>

		<p class="form-group">
			{!! Form::label('expires_on' , 'Expires On') !!}
			<?php
			$expires_on = $voucher->expires_on;

			if( is_object( $expires_on ) )
			{
				if( $expires_on->format( 'Y' ) == '-0001' )
				{
					$expires_on = '';
				} else
				{
					$expires_on = $expires_on->format( 'd/m/Y' );
				}
			}

			?>
			{!! Form::text('expires_on'  , $expires_on , [ 'class' => 'form-control date-picker' , 'placeholder' => 'dd/mm/yyyy'] ) !!}
		</p>

	</div>

</div>