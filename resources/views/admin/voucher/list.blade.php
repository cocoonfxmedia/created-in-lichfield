@extends('admin.app')

@section('page_title')
    <h4>Discount Vouchers</h4>
@endsection

@push('headerButtons')
<a href="/admin/vouchers/add" class="btn btn-link btn-float has-text"><i class="icon-plus3 text-primary"></i><span> Add Voucher</span></a>
@endpush

@section('content')

    @if( count( $vouchers ) == 0 )

        <p>There are now voucher codes at the moment</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Code</td>
                <td>Label</td>
                <td>Discount</td>
                <td>Expires On</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $vouchers as $voucher )

                <tr>
                    <td>{{$voucher->code}}</td>
                    <td>{{$voucher->label}}</td>
                    <td>
                        @if( $voucher->type == 'direct')
                        &pound;
                        @else
                        &percnt;
                        @endif
                        {{$voucher->discount}}</td>
                    <td>
                        @if( $voucher->expires_on->format('Y') == '-0001' )
                            Never
                        @else
                            {{$voucher->expires_on->format('d/m/Y')}}
                        @endif
                    </td>
                    <td>
                        <a href="{{url('admin/vouchers/edit/' . $voucher->id)}}" class="btn btn-primary">Edit</a>
                        <a href="{{url('admin/vouchers/delete/' . $voucher->id) }}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection