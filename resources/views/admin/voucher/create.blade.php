@extends('admin.app')


@section('page_title')
    <h4>New Voucher Code</h4>
@endsection

@section('content')

    {!! Form::open( ['url' => '/admin/vouchers/create', 'method' => 'post' ]) !!}

    @include('admin.voucher._form')

    <button type="submit" class="btn btn-primary">Create</button>
    <a href="{{url('admin/vouchers')}}">Cancel</a>

    {!! Form::close() !!}

@endsection