<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel for {{config('settings.company')}}</title>

    <!-- Global stylesheets -->
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/backoffice/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/backoffice/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script type="text/javascript" src="/backoffice/js/core/app.js"></script>
    <!-- /theme JS files -->

@stack('scripts')
<!-- /theme JS files -->

</head>

<body class="login-container">

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="/admin">Admin Panel for {{config('settings.company')}}</a>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

            @yield('main')

            <!-- Footer -->
                <div class="footer text-muted text-center">
                    Copyright {{date('Y')}} Cocoonfxmedia Limited.
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>