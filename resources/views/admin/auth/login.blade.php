@extends('admin.auth.layout')

@section('main')

	<form method="POST" action="{{ url('/admin/login') }}" role="form" class="form-validate">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="panel panel-body login-form">
			<div class="text-center">
				<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
				<h5 class="content-group">Login to the website admin panel
					<small class="display-block">Please enter your email address and password to login.</small>
				</h5>
			</div>

			@if ( Session::get('msg') )
				<div class="alert alert-danger">
					{{Session::get('msg')}}
				</div>
			@endif

			<div class="form-group has-feedback has-feedback-left">
				<input type="email" name="email" class="form-control" placeholder="Your email address" value="{{ old('email') }}" required="required">

				<div class="form-control-feedback">
					<i class="icon-envelope text-muted"></i>
				</div>
			</div>

			<div class="form-group has-feedback has-feedback-left">
				<input type="password" name="password" class="form-control" placeholder="Your password" required="required">

				<div class="form-control-feedback">
					<i class="icon-lock2 text-muted"></i>
				</div>
			</div>


			<div class="form-group">
				<div class="checkbox checkbox-switchery switchery-xs">
					<label>
						<input type="checkbox" name="remember" id="remember" class="switchery" checked="checked"> Remember me
					</label>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block">Login
					<i class="icon-circle-right2 position-right"></i></button>
			</div>

			<div class="text-center">
				<a href="/auth/password/email" style="display: none;">Forgot your password?</a>
			</div>
		</div>
	</form>

@endsection


@push('scripts')
<script type="text/javascript" src="/backoffice/js/plugins/forms/validation/validate.min.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="/backoffice/js/pages/login.js"></script>
@endpush
