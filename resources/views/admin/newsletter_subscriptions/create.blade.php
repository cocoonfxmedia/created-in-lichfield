@extends('admin.app')


@section('page_title')
    <h4>Create New Newsletter Subscription</h4>
@endsection


@section('content')


    {!! Form::open(['url' => '/admin/newsletter-subscriptions/create']) !!}

    @include('admin.newsletter_subscriptions._form')

    {!! Form::close() !!}

@endsection