@extends('admin.app')



@section('page_title')
    <h4>Create New Newsletter Subscription</h4>
@endsection



@section('content')


    {!! Form::model($subscription, ['url' => '/admin/newsletter-subscriptions/' . $subscription->id . '/edit' ]) !!}

    @include('admin.newsletter_subscriptions._form')

    {!! Form::close() !!}

@endsection