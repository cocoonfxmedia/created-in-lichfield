@extends('admin.app')

@section('page_title')
    <h4>News Letter Subscription Manager</h4>
@endsection

@push('headerButtons')
<a href="/admin/newsletter-subscriptions/create" class="btn btn-link btn-float has-text"><i class="icon-user-plus text-primary"></i><span> Create Subscriber</span></a>
@endpush


@section('content')

    <table class="table datatable-basic">
        <thead>
        <tr>
            <th>Email</th>
            <th>Subscribed On</th>
            <th>Unsubscribed On</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach( $subscriptions as $user )
            <tr>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at->format('d/m/Y')}}</td>
                <td>
                    @if( !is_null( $user->unsubscribed_at))
                        {{$user->unsubscribed_at->format('d/m/Y')}}
                    @endif
                </td>
                <dt></dt>
                <td>
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="/admin/newsletter-subscriptions/{{$user->id}}/edit"><i class="icon-pencil"></i> Edit</a>
                                </li>
                                <li>
                                    @if( is_null( $user->unsubscribed_at))
                                        <a href="/admin/newsletter-subscriptions/{{$user->id}}/unsubscribe"><i class="icon-user-minus"></i> Unsubscribe</a>
                                    @else
                                        <a href="/admin/newsletter-subscriptions/{{$user->id}}/resubscribe"><i class="icon-user-plus"></i> Resubscribe</a>
                                    @endif
                                </li>
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

    @include('admin.common._datatable')

@endsection