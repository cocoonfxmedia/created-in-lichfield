@include('admin.common._errors')


<div class="form-group">
    {!! Form::label('email', 'Email Address') !!}

    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">

</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="/admin/newsletter-subscriptions">Cancel</a>
</div>