@extends('admin.app')

@section('page_title')
	<h4>Product Reviews</h4>
	@endsection

@section('content')

	@if( !$reviews )

		<p>There are no product reviews available</p>

	@else

		<table class="table table-striped table-bordered data-table product-reviews">

			<thead>
			<tr>
				<th>Date</th>
				<th>Product</th>
				<th>Name</th>
				<th>Email</th>
				<th>Rating</th>
				<th>Status</th>
				<th></th>
			</tr>
			</thead>

			<tbody>

			@foreach( $reviews as $review )



				<tr>
					<td>{{$review->created_at->format('d/m/Y')}}</td>
					<td>{{$review->product->name}}</td>
					<td>{{$review->name}}</td>
					<td>{{$review->email}}</td>
					<td>{{$review->rating}}</td>
					<td>
						@if( $review->approved_at )
							<span class="label label-success">Approved</span>
						@endif

						@if( $review->deleted_at )
							<span class="label label-danger">Rejected</span>
						@endif

						@if( !$review->deleted_at && !$review->approved_at )
							<a href="{{$review->id}}" class="approve_review"><span class="label label-warning">Pending</span></a>
						@endif
					</td>
					<td>
						<a class="btn btn-sm btn-primary view-review" href="{{$review->id}}"><i class="fa fa-search"></i></a>
					</td>
				</tr>

			@endforeach

			</tbody>
		</table>

		<p>Review rating are from 1 to 5</p>
		<p>Only approved reviews will appear on the website</p>



		<div id="review-modal" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Product Review Details</h4>
					</div>
					<div class="modal-body">
						<table>
							<tr>
								<th class="pull-right">Date</th>
								<td style="width: 20px;"> </td>
								<td id="review-details-date"></td>
							</tr>
							<tr>
								<th class="pull-right">Status</th>
								<td></td>
								<td id="review-details-status"></td>
							</tr>
							<tr>
								<th class="pull-right">Name</th>
								<td></td>
								<td id="review-details-name"></td>
							</tr>
							<tr>
								<th class="pull-right">Email</th>
								<td></td>
								<td id="review-details-email"></td>
							</tr>
							<tr>
								<th class="pull-right">Product</th>
								<td></td>
								<td id="review-details-product"></td>
							</tr>
							<tr>
								<th class="pull-right">Rating</th>
								<td></td>
								<td id="review-details-rating"></td>
							</tr>
							<tr>
								<th class="pull-right">Comment</th>
								<td></td>
								<td id="review-details-comment"></td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div><!-- /.modal -->

	@endif

@endsection
