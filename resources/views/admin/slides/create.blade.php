@extends('admin.app')


@section('page_title')
    <h4><span>New</span> Slide</h4>
@endsection

@section('content')

    {!! Form::open( [ 'files' => true ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            <p class="form-group">
                {!! Form::label('name' , 'Name') !!}
                {!! Form::text('name'  , null , [ 'class' => 'form-control' , 'placeholder' => 'The name of this slide'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('link' , 'Link URL') !!}
                {!! Form::text('link'  , null , [ 'class' => 'form-control' , 'placeholder' => 'URL this slide will direct customers too'] ) !!}
            </p>
            <p class="form-group">
                {!! Form::label('text', 'Text') !!}
                {!! Form::textarea('text', old('text'), [ 'class' => 'form-control redactor_editor'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('image' , 'Image') !!}
                {!! Form::file('image', null , array( 'class' => 'form-control' ) ) !!}
            </p>


            <p class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="/admin/sliders" class='btn'>Cancel</a>
            </p>

        </div>

    </div>


    {!! Form::close() !!}


    @include('admin.common._errors')


@endsection

@push('scripts')


<script type="text/javascript" src="/backoffice/js/plugins/redactor/redactor.min.js"></script>

<script type="text/javascript">
    jQuery('.redactor_editor').redactor();
</script>

@endpush

@push('css')
<link href="/backoffice/js/plugins/redactor/redactor.css" rel="stylesheet">
@endpush