@extends('admin.app')

@section('page_title')
<h4>{{$slider->name}} Slides</h4>
@endsection

@push('headerButtons')
<a href="/admin/sliders/{{$slider->id}}/slides/create" class="btn btn-link btn-float has-text"><i class="icon-plus3 text-primary"></i><span> Add Slide</span></a>
@endpush

@section('content')

    @if( count( $slides ) == 0 )

        <p>There are no slides set up at the moment</p>

    @else

        <table class="table table-striped table-bordered data-table">

            <thead>
            <tr>
                <td>Name</td>
                <td>Preview</td>
                <td>Link</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $slides as $slide )

                <tr>
                    <td>{{$slide->name}}</td>
                    <td><img src="/usr/dip/{{$slide->image}}" alt="{{$slide->name}}" class="thumbnail" style="max-width: 200px;"/></td>
                    <td><a href="{{$slide->link}}" target="_blank">{{$slide->link}}</a></td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{url('admin/sliders/'.$slider->id.'/slides/edit/' . $slide->id)}}"><i class="icon-pencil7"></i> Edit Slide</a>
                                    </li>

                                    <li class="divider"></li>
                                    <li><a href="{{url('admin/sliders/'.$slider->id.'/slides/delete/' . $slide->id) }}" class="do_delete"><i class="icon-bin"></i> Delete Slide</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

    @endif

@endsection



@push('scripts')
    <script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete Slide",
					text: "Are you sure you want to delete this slide?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
    </script>
@endpush