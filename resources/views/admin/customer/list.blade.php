@extends('admin.app')

@section('page_title')
    <h4>Customers</h4>
@endsection

@section('content')

    @if( count( $customers ) )

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Registered On</td>
                <td># Orders</td>
                <td></td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Registered On</td>
                <td># Orders</td>
                <td></td>
            </tr>
            </tfoot>

            <tbody>

            @foreach( $customers as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at->format('d/m/Y') }}</td>
                    <td>{{ count( $user->orders ) }}</td>
                    <td>
                        <a href="/admin/customers/{{$user->id}}/edit" title="Edit customer detailt"
                           class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                        @if( count( $user->orders ) > 0 )
                            <a href="/admin/customers/{{$user->id}}/orders" class="btn-primary btn-sm">Orders</a>
                        @endif
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @else
        <h2>There are no registered customer accounts</h2>
    @endif

@endsection