@extends('admin.app')

@section('page_title')
    <h4>Update Customer's Profile</h4>
@endsection

@section('content')

    {!! Form::model($customer, ['url' => '/admin/customers/'.$customer->id.'/update', $customer->id , 'files' => true ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.common._errors')

            <h2>Contact Information</h2>

            <p class="form-group">
                {!! Form::label('name' , 'Name') !!}
                {!! Form::text('name'  , old('name') , [ 'class' => 'form-control' , 'placeholder' => 'Enter custom\'s name'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('landline' , 'Telephone No.') !!}
                {!! Form::text('landline'  , old('landline') , [ 'class' => 'form-control' , 'placeholder' => 'Customer\'s telephone number'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('mobile' , 'Mobile No.') !!}
                {!! Form::text('mobile'  , old('mobile') , [ 'class' => 'form-control' , 'placeholder' => 'Customer\'s mobile number'] ) !!}
            </p>

            <p class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="/admin/customers" class='btn'>Cancel</a>
            </p>

        </div>

    </div>

    {!! Form::close() !!}


    {!! Form::model($customer, ['url' => '/admin/customers/'.$customer->id.'/update', $customer->id , 'files' => true ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            <h2>Login Password</h2>

            <p class="form-group">
                {!! Form::label('password' , 'New Password', ['style' => 'min-width: 107px;']) !!}
                {!! Form::password('password', '', [ 'class' => 'form-control' ] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('passConfirm' , 'Confirm Password') !!}
                {!! Form::password('passConfirm', '', [ 'class' => 'form-control'] ) !!}
            </p>

            <p class="form-group">
                <button type="submit" class="btn btn-primary">Change Password</button>
            </p>

        </div>

    </div>

    {!! Form::close() !!}

@endsection
