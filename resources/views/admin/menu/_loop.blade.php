<li data-id="{{$li->id}}">
	<div class="panel-group" id="menuitem-{{$li->id}}" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading{{$li->id}}">
				<h6 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#menuitem-{{$li->id}}" href="#collapse{{$li->id}}" aria-expanded="true" aria-controls="collapse{{$li->id}}">
						@if( empty($li->label) && $li->type == 'page')
							@if( is_null( $li->page ))
								ERROR: UNKNOWN PAGE!!!!
							@else
								{{$li->page->title}}
							@endif
						@elseif($li->type == 'product_category')
							@if( is_null( $li->productCategory))
								ERROR: UNKNOWN PRODUCT CATEGORY!!!
							@else
								{{$li->productCategory->name}}
							@endif
						@else
							{{$li->label}}
						@endif
					</a>
					<div class="pull-right">
						@if( $li->type == 'link')
							<i class="fa fa-link"></i>
						@elseif($li->type == 'product_category')
							<i class="fa fa-shopping-cart"></i>
						@else
							<i class="fa fa-file-text-o"></i>
						@endif
					</div>
				</h6>
			</div>
			<div id="collapse{{$li->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$li->id}}">
				<div class="panel-body">
					{!! Form::open() !!}

					<p>
						@if( $li->type == 'link')
							{!! Form::label('destination', 'URL') !!}
							{!! Form::text('destination', $li->destination, ['class' => 'form-control'] ) !!}
						@elseif( $li->type == 'product_category')
							{!! Form::label('destination', 'Product Category') !!}
							{!! Form::select('destination', $product_categories, $li->destination, ['class' => 'form-control']) !!}
						@else
							{!! Form::label('destination', 'Page') !!}
							{!! Form::select('destination', $pages, $li->destination, ['class' => 'form-control']) !!}
						@endif
					</p>

					<p>
						{!! Form::label('label', 'Label') !!}
						{!! Form::text('label', $li->label,  ['class' => 'form-control']) !!}
					</p>

					<p>
						<a href="#" class="btn pull-right btn-danger do_delete" style="margin-left: 5px;" data-id="{{$li->id}}">Remove</a>
						{!! Form::Submit('Save', ['class' => 'pull-right btn btn-success do_change']) !!}
					</p>

					<input type="hidden" name="type" value="{{$li->type}}"/>
					<input type="hidden" name="id" value="{{$li->id}}"/>


					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

	<ol>
		@foreach( $li->children as $cli)
			@include('admin.menu._loop', ['li' => $cli])
		@endforeach
	</ol>


</li>