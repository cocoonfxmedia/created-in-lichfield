@extends('admin.app')


@section('page_title')
    <h4>Menu Editor</h4>
@endsection

@section('content')

    <style>
        body.dragging, body.dragging * {
            cursor: move !important;
        }

        .ml {
            max-height: 99999px !important;
        }
        ol li {
            list-style: none;
        }
        .dragged {
            position: absolute;
            opacity: 0.7;
            z-index: 2000;
            display: block;
        }
        .dragged .panel-group {
            margin-top: -30px;
        }

        ol.ml li.placeholder {
            position: relative;
            padding: 25px 20px;
            height: 33px;
            disply: block;
            border: 1px dashed #ddd;
            /** More li styles **/
        }
        ol.ml li.placeholder:before {
            position: absolute;
            /** Define arrowhead **/
        }
    </style>


    <div class="col-md-4">

        <div class="well well-lg">
            {!! Form::open(['id' => 'menuSelector']) !!}

            {!! Form::label('menu', 'Select Menu') !!}
            {!! Form::select('menu', $menuList, $selectedMenu, ['class' => 'form-control'] ) !!}

            {!! Form::close() !!}
        </div>

        @include('admin.menu._add_item')

    </div>

    <div class="col-md-8">


        <a href="#" class="pull-right btn btn-success do_saveMenu" style="margin-top:25px;">Save Menu</a>
        <h2>Menu Items</h2>

        <div id="nested">
            <ol class="ml">
                @foreach( $menu as $menuSegment )
                    @include('admin.menu._loop', ['li' => $menuSegment])
                @endforeach
            </ol>
        </div>
    </div>

@endsection

@push('scripts')
    <script type="text/javascript" src="/backoffice/js/plugins/sortable.js"></script>
    <script type="text/javascript">

		var oldContainer;
		$("ol.ml").sortable({
			group: 'nested',
			afterMove: function (placeholder, container) {
				if (oldContainer != container) {
					if (oldContainer)
						oldContainer.el.removeClass("active");
					container.el.addClass("active");

					oldContainer = container;
				}
			},
			onDrop: function ($item, container, _super) {
				container.el.removeClass("active");
				_super($item, container);
			}
		});

		$(document).on("click", ".do_delete", function () {
			var target = $(this).attr('data-id');


			swal({
					title: "Delete Menu Item",
					text: "Are you sure you want to delete this item from the menu?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					$('.ml li[data-id=' + target + ']').remove();

					$.get('/admin/menus/delete/' + target);
				});


			return false;
		});

		$('.do_saveMenu').click(function () {
			var button = $(this);

			button.html('<i class="fa fa-circle-o-notch  fa-spin"></i> Saving');

			var nav = [];

			// attach parent information to the nav items
			$('ol.ml li').attr('data-parent', 0);
			$('ol.ml li ol li').each(function (index) {
				$(this).attr('data-parent', $(this).parent().parent().attr('data-id'));
			});

			//translate order into variable
			$('ol.ml li').each(function (index) {
				nav[index] = {id: $(this).attr('data-id'), parent: $(this).attr('data-parent')};
			});

			//submit order to server side
			$.post('/admin/menus/order', {'nav': JSON.stringify(nav)}, function (response) {
				if (response == 'OK') {
					$('.do_saveMenu').html('<i class="fa fa-check"></i> Saved');
				}
			});

			return false;
		});

		$(document).on("click", ".do_change", function () {

			var form = $(this).parent().parent();
			var id = form.children('input[name=id]').val();
			var button = $(this);

			button.val('Saving...');

			$.post('/admin/menus/update', form.serialize(), function (response) {
				if (response == 'OK') {

					button.val('Saved');
				}
			});

			return false;
		});

		$('select#menu').change(function () {
			$('#menuSelector').submit();
		});


    </script>
@endpush


