<h2>New Menu Item</h2>

<div class="panel-group" id="add-item" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingSysPage">
            <h6 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#add-item" href="#collapseSysPage" aria-expanded="false" aria-controls="collapseSysPage">
                    System Page
                </a>
            </h6>
        </div>
        <div id="collapseSysPage" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSysPage">
            <div class="panel-body">
                {!! Form::open(['url' => '/admin/menus/create', 'id' => 'sys-page-frm']) !!}

                <p>
                    {!! Form::label('destination', 'Page') !!}
                    {!! Form::select('destination', $syspages, old('destination'), ['class' => 'form-control']) !!}
                </p>

                <p>
                    {!! Form::label('label', 'Label') !!}
                    {!! Form::text('label', old('label'),  ['class' => 'form-control']) !!}
                </p>

                <p>
                    {!! Form::Submit('Add', ['class' => 'pull-right btn btn-success do_add_nav']) !!}
                </p>

                <input type="hidden" name="type" value="sys_page"/>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h6 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#add-item" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Page
                </a>
            </h6>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                {!! Form::open(['url' => '/admin/menus/create']) !!}

                <p>
                    {!! Form::label('destination', 'Page') !!}
                    {!! Form::select('destination', $pages, old('destination'), ['class' => 'form-control']) !!}
                </p>

                <p>
                    {!! Form::label('label', 'Label') !!}
                    {!! Form::text('label', old('label'),  ['class' => 'form-control']) !!}
                </p>

                <p>
                    {!! Form::Submit('Add', ['class' => 'pull-right btn btn-success do_add_nav']) !!}
                </p>

                <input type="hidden" name="type" value="page"/>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h6 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#add-item" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Link
                </a>
            </h6>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                {!! Form::open(['url' => '/admin/menus/create']) !!}

                <p>
                    {!! Form::label('destination', 'URL') !!}
                    {!! Form::text('destination', old('destination'), ['class' => 'form-control', 'required'=>'required'] ) !!}
                </p>

                <p>
                    {!! Form::label('label', 'Label') !!}
                    {!! Form::text('label', old('label'),  ['class' => 'form-control', 'required'=>'required']) !!}
                </p>

                <p>
                    {!! Form::Submit('Add', ['class' => 'pull-right btn btn-success do_add_nav']) !!}
                </p>

                <input type="hidden" name="type" value="link"/>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

    @if( Config::get('cms.components.ecommerce'))
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h6 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#add-item" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Product Category
                    </a>
                </h6>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/menus/create']) !!}

                    <p>
                        {!! Form::label('destination', 'Product Category') !!}
                        {!! Form::select('destination', $product_categories, null, ['class' =>'form-control'] ) !!}
                    </p>

                    <p>
                        {!! Form::label('label', 'Label') !!}
                        {!! Form::text('label', old('label'),  ['class' => 'form-control']) !!}
                    </p>

                    <p>
                        {!! Form::Submit('Add', ['class' => 'pull-right btn btn-success do_add_nav']) !!}
                    </p>

                    <input type="hidden" name="type" value="product_category"/>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endif

</div>


@push('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
			$('#sys-page-frm select').change(function(){
				$('#sys-page-frm #label').val($('#sys-page-frm select option:selected').text());
            });

			$('#sys-page-frm #label').val($('#sys-page-frm select option:selected').text());
        });

    </script>
@endpush