@extends('admin.app')

@section('page_title')
    <h4>Gallery Images</h4>
@endsection

@push('headerButtons')
<a id="do_saveOrder" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Save Order</span></a>
@endpush


@section('content')

    <!-- Multiple file upload -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Add images</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse" class=""></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <p class="content-group">Drag and drop images onto the box below to add them to this gallery
            </p>

            <form action="#" class="dropzone" id="dropzone_multiple"></form>
        </div>
    </div>
    <!-- /multiple file upload -->




    <div class="row galleryContainer">

        @foreach( $images as $img )

            <div class="col-lg-3 col-sm-6 sortable" data-id="{{$img->id}}">
                <div class="thumbnail">
                    <div class="thumb">
                        <img src="/usr/gallery/{{$img->gallery}}/{{$img->filename}}" alt="{{$img->title}}">
                        <div class="caption-overflow">
                                <span>
                                    <a href="/usr/gallery/{{$img->gallery}}/{{$img->filename}}" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                                </span>
                        </div>
                    </div>

                    <div class="caption">
                        <h6 class="no-margin" style="min-height: 23px;">
                            <a href="#" class="text-default">{{$img->title}}</a>
                            <a data-id="{{$img->id}}" class="text-muted delete"><i class="icon-cross2 pull-right" style="margin-left: 10px;"></i></a>

                            <a
                                data-id="{{ $img->id }}"
                                data-title="{{ $img->title }}"
                                data-latitude="{{ $img->latitude }}"
                                data-longitude="{{ $img->longitude }}"
                                data-category="{{ $img->category_id }}"
                                class="text-muted editName"
                                data-toggle="modal"
                                data-target="#modal_name"
                            >
                                <i class="icon-pencil pull-right"></i>
                            </a>
                        </h6>
                    </div>
                </div>
            </div>
        @endforeach

    </div>




    <div id="modal_name" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Image properties</h5>
                </div>

                <form action="/admin/gallery/image-properties/testing" method="POST" class="form-horizontal" id="properties-frm">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">Image Title</label>
                            <div class="col-sm-9">
                                <input type="text" name="title" id="image_title" placeholder="Image title" class="form-control">
                            </div>
                        </div>

                        @if (config('services.google-maps.key'))
                            <div class="form-group">
                                <label class="control-label col-sm-3">Address</label>
                                <div class="col-sm-9">
                                    <textarea placeholder="Address" class="form-control" id="address"></textarea>
                                    <br>
                                    <button class="btn btn-info" id="geocode">Geocode</button>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-sm-3">Latitude</label>
                            <div class="col-sm-9">
                                <input type="number" step="0.00000001" name="latitude" id="image_latitude" placeholder="Latitude" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3">Longitude</label>
                            <div class="col-sm-9">
                                <input type="number" step="0.00000001" name="longitude" id="image_longitude" placeholder="Longitude" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3">Category</label>
                            <div class="col-sm-9">
                                <select name="category_id" id="image_category" class="form-control">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="image" id="image_id" value="">


                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /horizontal form modal -->



@endsection


@push('scripts')
<script type="text/javascript" src="/backoffice/js/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/uploaders/dropzone.min.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/media/fancybox.min.js"></script>
<script>
	$(function () {

		// Defaults
		Dropzone.autoDiscover = false;


		// Single file
		$("#dropzone_single").dropzone({
			paramName: "file", // The name that will be used to transfer the file
			maxFilesize: 6, // MB
			maxFiles: 1,
			dictDefaultMessage: 'Drop file to upload <span>or CLICK</span>',
			autoProcessQueue: false,
			init: function () {
				this.on('addedfile', function (file) {
					if (this.fileTracker) {
						this.removeFile(this.fileTracker);
					}
					this.fileTracker = file;
				});
			}
		});


		// Multiple files
		$("#dropzone_multiple").dropzone({
			paramName: "file", // The name that will be used to transfer the file
			dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
			maxFilesize: 6 // MB
		});


		// Initialize lightbox
		$('[data-popup="lightbox"]').fancybox({
			padding: 3
		});

		$('.editName').click(function () {
			$('#image_id').val($(this).attr('data-id'));
			$('#image_title').val($(this).attr('data-title'));
			$('#image_latitude').val($(this).attr('data-latitude'));
			$('#image_longitude').val($(this).attr('data-longitude'));
			$('#image_category').val($(this).attr('data-category'));
		});

		$('.delete').click(function () {

			var target = $(this);

			swal({
				title: "Are you sure?",
				text: "This will delete this image from the gallery and cannot be undone.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#FF7043",
				confirmButtonText: "Yes, delete it!",

			}, function () {
				$.get('/admin/gallery/remove-image/' + target.attr('data-id'), function () {
					$('.ui-sortable-handle[data-id=' + target.attr('data-id') + ']').remove();
				});
			});
			return false;
		});

		// Sortable panel
		$(".galleryContainer").sortable({
			items: '.sortable'
		});

		$('#do_saveOrder').click(function () {
			var nav = [];


			//translate order into variable
			$('.sortable').each(function (index) {
				nav[index] = {id: $(this).attr('data-id')};
			});

			//submit order to server side
			$.post('/admin/gallery/{{$gallery->id}}/order', {'nav': JSON.stringify(nav)}, function (response) {
				if (response == 'OK') {
					new PNotify({
						title: 'Saved',
						text: 'The gallery order was saved',
						addclass: 'bg-success'
					});
				}
			});
		});


	});
</script>

@if (config('services.google-maps.key'))
	<script>
		function initMap() {
			var geocoder = new google.maps.Geocoder();
			var $address = document.getElementById('address');
			var $latitude = document.getElementById('image_latitude');
			var $longitude = document.getElementById('image_longitude');

			document.getElementById('geocode').addEventListener('click', function (event) {
				event.preventDefault();

				geocoder.geocode({
					address: $address.value,
					region: 'gb',
				}, function (results, status) {
					if (status !== 'OK') {
						console.warn(status);
						return;
					}

					$latitude.value = results[0].geometry.location.lat().toFixed(8);
					$longitude.value = results[0].geometry.location.lng().toFixed(8);
				});
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google-maps.key') }}&callback=initMap" async defer></script>
@endif
@endpush