@extends('admin.app')

@section('page_title')
    <h4>Galleries</h4>
@endsection

@push('headerButtons')
<a href="/admin/gallery/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Gallery</span></a>
@endpush


@section('content')

    @if( count( $galleries ) == 0 )

        <p>There are no galleries</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <th>Name</th>
                <th>Slug</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>

            <tbody>

            @foreach ( $galleries as $gallery)

                <tr>
                    <td><a href="#">{{$gallery->name}}</a></td>
                    <td><a href="#">{{$gallery->slug}}</a></td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="/admin/gallery/{{$gallery->id}}/edit"><i class="icon-pencil7"></i> Edit gallery details</a>
                                    </li>
                                    <li>
                                        <a href="/admin/gallery/{{$gallery->id}}">
                                            <i class="icon-images3"></i> Manage Gallery
                                        </a>
                                    </li>
                                    @if( $gallery->published == 1 )
                                        <li>
                                            <a href="/admin/gallery/{{$gallery->id}}/unpublish"><i class="icon-eye-blocked"></i> Unpublish</a>
                                        </li>
                                    @endif
                                    @if( $gallery->published == 0 )
                                        <li>
                                            <a href="/admin/gallery/{{$gallery->id}}/publish"><i class="icon-eye"></i> Publish</a>
                                        </li>
                                    @endif
                                    <li class="divider"></li>
                                    <li><a href="/admin/gallery/{{$gallery->id}}/delete" class="do_delete"><i class="icon-bin"></i> Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection



@push('scripts')
    <script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete Gallery",
					text: "Are you sure you want to delete this gallery?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
    </script>
@endpush