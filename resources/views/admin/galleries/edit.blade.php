@extends('admin.app')

@section('page_title')
    <h4>Edit Gallery</h4>
@endsection


@section('content')

    {!! Form::model($gallery, ['url' => '/admin/gallery/'.$gallery->id . '/update' , $gallery->id ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">


            @include('admin.galleries._form')

        </div>

    </div>


    {!! Form::close() !!}

@endsection