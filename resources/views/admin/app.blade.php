<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin Panel for {{siteConfig('site_name')}}</title>
    <link rel="shortcut icon" href="/favicon.png">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/backoffice/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/backoffice/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet" type="text/css">
    @stack('css')

    <script type="text/javascript" src="/backoffice/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/ui/fab.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="/backoffice/js/plugins/notifications/sweet_alert.min.js"></script>
    @stack('lib_scripts')
    <script type="text/javascript" src="/backoffice/js/core/app.js"></script>


</head>

<body class="{{session('main-nav-view', '')}}">

<div id="app">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="/admin">Admin Panel {{siteConfig('site_name')}}</a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>
        @include('admin.common._nav_top')
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">
                    @include('admin.common._nav_left')
                </div>
            </div>
            <!-- /main sidebar -->


            <!-- Main content -->
            <div class="content-wrapper">

                @if( config('cms.components.multisite') )
                    <div class="alert alert-primary no-border">
                        <p>You are currently editing the
                            <b>{{siteConfig('site_name')}}</b> site.</p>
                    </div>
            @endif

            <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            @yield('page_title')
                        </div>

                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                @stack('headerButtons')
                            </div>
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        {!! Breadcrumbs::render() !!}
                        @yield('breadcrumb_elements')
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    @yield('content')

                    <div class="footer text-muted">
                        &copy;Cocoonfxmedia Ltd
                    </div>

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->


        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->
</div>
<!-- vue app container -->


<script>
	var navController = <?php echo json_encode($navController); ?>;
	$.each(navController, function (index, value) {
		$('ul.navigation-main li[data-nav-section=' + value + ']').addClass('active');
	});
</script>

@stack('scripts')

@if( Session::has('msg') )
    <script type="text/javascript">

		$(document).ready(function () {
			new PNotify({
				title: '{{Session::get('msg.title')}}',
				text: '{{Session::get('msg.text')}}',
				addclass: 'bg-{{Session::get('msg.state')}}'
			});
		});

    </script>
@endif
<script src="/js/app.js" type="text/javascript"></script>

</body>
</html>