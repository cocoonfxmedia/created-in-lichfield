@extends('admin.app')

@section('page_title')
    <h4>Edit Slider</h4>
@endsection


@section('content')

    {!! Form::model($slider ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.sliders._form')

        </div>

    </div>


    {!! Form::close() !!}

@endsection