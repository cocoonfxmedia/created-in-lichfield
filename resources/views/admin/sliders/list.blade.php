@extends('admin.app')

@section('page_title')
<h4>Slider Manager</h4>
@endsection

@push('headerButtons')
<a href="/admin/sliders/create" class="btn btn-link btn-float has-text"><i class="icon-plus3 text-primary"></i><span> Add New Slider</span></a>
@endpush

@section('content')

    @if( count( $sliders ) == 0 )

        <p>There are no sliders configured for this site at the moment</p>

    @else

        <table class="table table-striped table-bordered data-table">

            <thead>
            <tr>
                <td>Homepage</td>
                <td>Name</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $sliders as $slider )

                <tr>
                    <td width="40">
                        @if( $slider->homepage)
                        <i class="icon-star-full2"></i>
                        @endif
                    </td>
                    <td>{{$slider->name}}</td>
                    <td>

                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{url('admin/sliders/edit/' . $slider->id)}}"><i class="icon-pencil7"></i> Edit Slider</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/sliders/' . $slider->id)}}/slides"><i class="icon-images3"></i> Manage Slides</a>
                                    </li>

                                    <li class="divider"></li>
                                    <li><a href="{{url('admin/sliders/delete/' . $slider->id) }}" class="do_delete"><i class="icon-bin"></i> Delete Slider</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

    @endif

@endsection


@push('scripts')
    <script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete Slider",
					text: "Are you sure you want to delete this slider?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
    </script>
@endpush