@include('admin.common._message')

@include('admin.common._errors')

<?php
$attrs = [ 'class' => 'form-control' ];
?>


<div class="form-group">
    {!! Form::label('homepage', 'Display this slider on the homepage') !!}
    {!! Form::checkbox('homepage', 1, old('homepage', ((isset($slider)) ? $slider->homepage : false) )) !!}
</div>

<div class="form-group">
    {!! Form::label('name', 'Slider Name') !!}
    {!! Form::text('name', old('name'), $attrs) !!}
</div>

<button class="btn btn-primary">Save</button> or <a href="{{url('admin/sliders')}}">Cancel</a>