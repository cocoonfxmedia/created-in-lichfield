@include('admin.common._errors')

<p class="form-group">
	{!! Form::label('title', 'Title') !!}
	{!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="/admin/categories" class="btn">Cancel</a>
</p>
