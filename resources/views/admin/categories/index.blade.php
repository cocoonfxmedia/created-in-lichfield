@extends('admin.app')

@section('page_title')
	<h4>Categories</h4>
@endsection

@push('headerButtons')
	<a href="/admin/categories/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Category</span></a>
@endpush


@section('content')
	@if ($categories->isEmpty())
		<p>There are no categories at the moment</p>
	@else
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>Title</th>
					<th data-orderable="false"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($categories as $category)
					<tr class="{{ $category->trashed() ? 'danger' : '' }}">
						<td>{{ $category->title }}</td>
						<td>
							@if ($category->trashed())
								Deleted
							@else
								<ul class="icons-list">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<i class="icon-menu9"></i>
										</a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a href="{{ url("admin/categories/edit/{$category->id}") }}"><i class="icon-pencil"></i> Edit</a>
											</li>
											<li>
												<a href="{{ url("admin/categories/delete/{$category->id}") }}" class="do_delete"><i class="icon-bin"></i> Delete</a>
											</li>
										</ul>
									</li>
								</ul>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		@include('admin.common._datatable')
	@endif
@endsection

@push('scripts')
	<script>
		$('.do_delete').click(function () {
			var target = $(this).attr('href');

			swal({
				title: 'Delete Category',
				text: 'Are you sure you want to delete this category?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#ff7043',
				confirmButtonText: 'Yes, delete it!',
			}, function () {
				window.location = target;
			});

			return false;
		});
	</script>
@endpush
