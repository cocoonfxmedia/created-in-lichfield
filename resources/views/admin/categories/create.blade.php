@extends('admin.app')

@section('page_title')
	<h4>New Category</h4>
@endsection

@section('content')
	{!! Form::open(['url' => '/admin/categories/create']) !!}

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			@include('admin.categories._form')
		</div>
	</div>

	{!! Form::close() !!}
@endsection
