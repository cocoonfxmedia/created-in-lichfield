@extends('admin.app')

@section('page_title')
	<h4>User Manager</h4>
@endsection

@push('headerButtons')
<a href="/admin/users/create" class="btn btn-link btn-float has-text"><i class="icon-user-plus text-primary"></i><span> Create User</span></a>
@endpush


@section('content')

	<table class="table datatable-basic">
		<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Actions</th>
		</tr>
		</thead>
		<tbody>

		@foreach( $users as $user )
			<tr>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
				<td>{{$user->roles()->first()->display_name}}</td>
				<td>
					<ul class="icons-list">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-menu9"></i>
							</a>

							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="/admin/users/{{$user->id}}/edit"><i class="icon-pencil"></i> Edit</a></li>
								<li><a href="/admin/users/{{$user->id}}/delete" class="do_delete"><i class="icon-bin"></i> Delete</a></li>
							</ul>
						</li>
					</ul>
				</td>
			</tr>
		@endforeach

		</tbody>
	</table>

	@include('admin.common._datatable')

@endsection




@push('scripts')
	<script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete User Account",
					text: "Are you sure you want to delete this user's account?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
	</script>
@endpush