@extends('admin.app')

@section('page_title')
    <h4>Update Newsletter Subscription</h4>
@endsection

@section('content')

    {!! Form::model( $user, ['url' => '/admin/users/' . $user->id , 'method' => 'PATCH' ]) !!}

    @include('admin.users._form')

    {!! Form::close() !!}

@endsection