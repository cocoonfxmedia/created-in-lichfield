@extends('admin.app')

@section('page_titlte')
    <h4>Create New User</h4>
@endsection

@section('content')

    {!! Form::open(['url' => '/admin/users']) !!}
    @include('admin.users._form')
    {!! Form::close() !!}

@endsection