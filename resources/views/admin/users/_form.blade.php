@include('admin.common._errors')

<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', old('first_name'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', old('last_name'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email Address:') !!}
    {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('role', 'User Role') !!}
    {!! Form::select('role', $roles, old('role', $roleId ) , [ 'class' => 'form-control bootstrap-select']) !!}
</div>

<div class="form-group">
    {!! Form::label('password', 'New Password') !!}
    {!! Form::password('password',  ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pass_confirm', 'Confirm New Password') !!}
    {!! Form::password('pass_confirm',  ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="/admin/users">Cancel</a>
</div>


@push('scripts')
<script type="text/javascript" src="/backoffice/js/plugins/forms/selects/bootstrap_select.min.js"></script>
<script type="text/javascript">
    $('.bootstrap-select').selectpicker();
</script>
@endpush