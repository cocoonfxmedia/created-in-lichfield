@extends('admin.app')

@section('page_title')
	<h4>New Case Study Category</h4>
@endsection

@section('content')

{!! Form::open( ['url' => '/admin/case-study-categories/store' ] ) !!}

<div class="row">

	<div class="col-sm-12 col-md-12 col-lg-12">

		@include('admin.case_studies.category.form')

	</div>

</div>


{!! Form::close() !!}


@include('admin.common._errors')


@endsection
