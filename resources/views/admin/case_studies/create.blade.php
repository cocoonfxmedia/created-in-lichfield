@extends('admin.app')

@section('page_title')
    <h4>New Case Study</h4>
@endsection


@section('content')
    {!! Form::open( ['url' => '/admin/case-studies/store', 'files' => true ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.case_studies._form')

        </div>

    </div>

    {!! Form::close() !!}

@endsection
