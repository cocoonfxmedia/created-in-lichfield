@extends('admin.app')

@section('page_title')
    <h4>Edit Case Study</h4>
@endsection

@section('content')

    {!! Form::model($article, ['url' => '/admin/case-studies/update/'.$article->id , $article->id, 'files' => true  ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.case_studies._form')

        </div>

    </div>

    {!! Form::close() !!}

@endsection