@extends('admin.app')

@section('page_title')
    <h4><span>Case Study</span> (Recycle Bin)</h4>
@endsection

@push('headerButtons')

@if( count( $articles ) > 0 )
    <a href="/admin/case-studies/empty-trash" class="btn btn-link btn-float has-text">
        <i class=" icon-trash-alt text-primary"></i><span>Empty Recycle Bin</span>
    </a>
@endif

@endpush

@section('content')

    @if( count( $articles ) == 0 )

        <p>There are no case studies on the website at the moment</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Date</td>
                <td>Title</td>
                <td>URL</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $articles as $p )

                <tr>
                    <td>{{$p->created_at->format('d/m/Y')}}</td>
                    <td>{{$p->title}}</td>
                    <td><a href="/{{$p->url}}" target="_blank">{{$p->url}}</a></td>
                    <td>
                        <a href="{{url('admin/case-studies/restore/' . $p->id)}}" class="btn btn-primary"><i
                                    class="fa fa-refresh"></i> Retsore</a>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection