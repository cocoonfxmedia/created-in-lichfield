@extends('admin.app')

@section('page_title')
	<h4>New Artist</h4>
@endsection

@section('content')
	{!! Form::open(['url' => '/admin/artists/create', 'files' => true]) !!}

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			@include('admin.artists._form')
		</div>
	</div>

	{!! Form::close() !!}
@endsection
