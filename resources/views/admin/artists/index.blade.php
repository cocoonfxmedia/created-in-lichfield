@extends('admin.app')

@section('page_title')
	<h4>Artists</h4>
@endsection

@push('headerButtons')
	<a href="/admin/artists/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Artist</span></a>
	<a href="/admin/artists/trash" class="btn btn-link btn-float has-text"><i class="icon-trash text-primary"></i><span>Recycle Bin</span></a>
@endpush


@section('content')
	@if ($artists->isEmpty())
		<p>There are no artists at the moment</p>
	@else
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Email</th>
					<th>Status</th>
					<th>Can manage events?</th>
					<th>Can manage locations?</th>
					<th data-orderable="false"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($artists as $artist)
					<tr class="{{ 'inactive' === $artist->status ? 'danger' : '' }} {{ 'pending' === $artist->status ? 'warning' : '' }}">
						<td>{{ $artist->id }}</td>
						<td>{{ $artist->name }}</td>
						<td>{{ $artist->email }}</td>
						<td>{{ $artist->status }}</td>
						<td><i class="icon-{{ $artist->can_manage_events ? 'checkmark' : 'cross2' }}"></i></td>
						<td><i class="icon-{{ $artist->can_manage_locations ? 'checkmark' : 'cross2' }}"></i></td>
						<td>
							<ul class="icons-list">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="icon-menu9"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a href="{{ url("admin/artists/edit/{$artist->id}") }}"><i class="icon-pencil"></i> Edit</a>
										</li>
										<li>
											<a href="{{ url("admin/artists/delete/{$artist->id}") }}" class="do_delete"><i class="icon-bin"></i> Delete</a>
										</li>
									</ul>
								</li>
							</ul>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		@include('admin.common._datatable')

		<button class="btn js-toggle-perm js-toggle-event-perm" disabled>Toggle permission to manage events</button>
		<button class="btn js-toggle-perm js-toggle-location-perm" disabled>Toggle permission to manage locations</button>
	@endif
@endsection

@push('scripts')
	<script>
		$('.do_delete').click(function () {
			var target = $(this).attr('href');

			swal({
				title: 'Delete Artist',
				text: 'Are you sure you want to delete this artist?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#ff7043',
				confirmButtonText: 'Yes, delete them!',
			}, function () {
				window.location = target;
			});

			return false;
		});

		$('.datatable-basic tbody').on('click', 'tr', function () {
			$(this).toggleClass('selected-row');
			$('.js-toggle-perm').prop('disabled', table.rows('.selected-row').data().length === 0);
		});

		$('.js-toggle-event-perm').click(function () {
			var selected = table.rows('.selected-row');

			var ids = selected.data().toArray().map(function (row) {
				return row[0];
			});

			$.ajax({
				type: 'post',
				url: '/admin/artists/toggle-event-permissions',
				data: JSON.stringify(ids),
				contentType: 'application/json',
				success: function () {
					location.reload();
				},
			});
		});

		$('.js-toggle-location-perm').click(function () {
			var selected = table.rows('.selected-row');

			var ids = selected.data().toArray().map(function (row) {
				return row[0];
			});

			$.ajax({
				type: 'post',
				url: '/admin/artists/toggle-location-permissions',
				data: JSON.stringify(ids),
				contentType: 'application/json',
				success: function () {
					location.reload();
				},
			});
		});
	</script>

	<style>
		.selected-row {
			background-color: #999;
			color: white;
		}

		.selected-row td {
			background-color: #999 !important;
			color: white;
		}
	</style>
@endpush
