@extends('admin.app')

@section('page_title')
	<h4>Edit Artist</h4>
@endsection

@push('headerButtons')
<a href="/admin/artists/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Artist</span></a>
@endpush

@section('content')
	{!! Form::model($artist, ['url' => "/admin/artists/update/{$artist->id}", 'artist-id' => $artist->id, 'files' => true]) !!}

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			@include('admin.artists._form')
		</div>
	</div>

	{!! Form::close() !!}
@endsection
