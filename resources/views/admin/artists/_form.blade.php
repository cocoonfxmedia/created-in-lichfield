@include('admin.common._errors')

<p class="form-group">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('status', 'status') !!} 
	{!! Form::select('status', ['pending' => 'pending', 'active' => 'active', 'inactive' => 'inactive'], old('status'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('website', 'Website') !!}
	{!! Form::text('website', old('website'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('bio', 'Bio') !!}
	{!! Form::textArea('bio', old('bio'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('facebook_link', 'Facebook Link') !!}
	{!! Form::text('facebook_link', old('facebook_link'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('twitter_link', 'Twitter Link') !!}
	{!! Form::text('twitter_link', old('twitter_link'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('instagram_link', 'Instagram Link') !!}
	{!! Form::text('instagram_link', old('instagram_link'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	@if( isset($artist) ) @if( !is_null($artist->header_image) )
	<div class="pull-left" style="width: 300px; margin: 0 20px 20px 0;">
		<img src="{{$artist->header_image}}" class="img-responsive header_thumb">
		<input type="hidden" name="remove_header_image" id="remove_header_image" value="0">
		<button id="do_remove_header_image" class="btn btn-xs btn-primary">Remove Image</button>
	</div>
	@endif @endif
	{!! Form::label('header_image', 'Header Image') !!}
	{!! Form::file('header_image', old('header_image'), ['class' => 'form-control']) !!}
	{{-- {!! Form::file('thumbnail', [ 'class' => 'file-styled' ] ) !!} --}}
</p>

<p class="form-group" style="width:100%; clear:both">
	
	@if( isset($artist) && !is_null($artist->profile_image) )
	<div class="pull-left" style="width: 300px; margin: 0 20px 20px 0;">
		<img src="{{$artist->profile_image}}" class="img-responsive profile_thumb">
		<input type="hidden" name="remove_profile_image" id="remove_profile_image" value="0">
		<button id="do_remove_profile_image" class="btn btn-xs btn-primary">Remove Image</button>
	</div>
	@endif
	{!! Form::label('profile_image', 'Profile Image') !!}
	{!! Form::file('profile_image', old('profile_image'), ['class' => 'form-control']) !!}
</p>
<br>

<p class="form-group" style="width:100%; clear:both">
	{!! Form::label('email', 'Email') !!}
	{!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('can_manage_events', 'Can manage events?') !!}
	<input type="hidden" name="can_manage_events" value="0">
	{!! Form::checkbox('can_manage_events', old('can_manage_events', true)) !!}
</p>

<p class="form-group">
	{!! Form::label('can_manage_locations', 'Can manage locations?') !!}
	<input type="hidden" name="can_manage_locations" value="0">
	{!! Form::checkbox('can_manage_locations', old('can_manage_locations', true)) !!}
</p>

<p class="form-group">
	{!! Form::label('category_id', 'Category') !!}
	<tags-input :whitelist="{{ \App\Models\ArtCategory::all()->toJson() }}" :default="{{ old( 'categories', $artist_categories->toJson()) }}" name="categories"></tags-input>
</p>

<p class="form-group">
	{!! Form::label('password', 'Password') !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="/admin/artists" class="btn">Cancel</a>
</p>

@push('scripts')
<script type="text/javascript">
	$(document).ready(function () {	
		$('#do_remove_profile_image').click(function () { $('#remove_profile_image').val('1'); $(this).parent().hide(); return false; });
		$('#do_remove_header_image').click(function () { $('#remove_header_image').val('1'); $(this).parent().hide(); return false; });
	});
	</script>	
@endpush
