@extends('admin.app')

@section('page_title')
	<h4>Artists</h4>
@endsection

@push('headerButtons')
	<a href="/admin/artists/empty-trash" class="btn btn-link btn-float has-text"><i class="icon-trash-alt text-primary"></i><span>Empty Recycle Bin</span></a>
@endpush


@section('content')
	@if ($artists->isEmpty())
		<p>There are no deleted artists at the moment</p>
	@else
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Email</th>
					<th>Status</th>
					<th>Can manage events?</th>
					<th>Can manage locations?</th>
					<th data-orderable="false"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($artists as $artist)
			 {{-- $artist->trashed() --}}
					<tr class="{{ 'inactive' === $artist->status ? 'danger' : '' }} {{ 'pending' === $artist->status ? 'warning' : '' }}">
						<td>{{ $artist->id }}</td>
						<td>{{ $artist->name }}</td>
						<td>{{ $artist->email }}</td>
						<td>{{ $artist->status }}</td>
						<td><i class="icon-{{ $artist->can_manage_events ? 'checkmark' : 'cross2' }}"></i></td>
						<td><i class="icon-{{ $artist->can_manage_locations ? 'checkmark' : 'cross2' }}"></i></td>
						<td>
							<ul class="icons-list">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="icon-menu9"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a href="{{ url("admin/artists/restore/{$artist->id}") }}"><i class="icon-history"></i> Restore</a>
										</li>
										<li>
											<a href="{{ url("admin/artists/destroy/{$artist->id}") }}" class="do_delete"><i class="icon-warning"></i> Destroy</a>
										</li>
									</ul>
								</li>
							</ul>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		@include('admin.common._datatable')
	@endif
@endsection

@push('scripts')
	<script>


		$('.datatable-basic tbody').on('click', 'tr', function () {
			$(this).toggleClass('selected-row');
			$('.js-toggle-perm').prop('disabled', table.rows('.selected-row').data().length === 0);
		});

		$('.js-toggle-event-perm').click(function () {
			var selected = table.rows('.selected-row');

			var ids = selected.data().toArray().map(function (row) {
				return row[0];
			});

			$.ajax({
				type: 'post',
				url: '/admin/artists/toggle-event-permissions',
				data: JSON.stringify(ids),
				contentType: 'application/json',
				success: function () {
					location.reload();
				},
			});
		});

		$('.js-toggle-location-perm').click(function () {
			var selected = table.rows('.selected-row');

			var ids = selected.data().toArray().map(function (row) {
				return row[0];
			});

			$.ajax({
				type: 'post',
				url: '/admin/artists/toggle-location-permissions',
				data: JSON.stringify(ids),
				contentType: 'application/json',
				success: function () {
					location.reload();
				},
			});
		});
	</script>

	<style>
		.selected-row {
			background-color: #999;
			color: white;
		}

		.selected-row td {
			background-color: #999 !important;
			color: white;
		}
	</style>
@endpush
