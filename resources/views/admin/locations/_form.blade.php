@include('admin.common._errors')

<p class="form-group">
	{!! Form::label('title', 'Title') !!}
	{!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('external_link', 'External Link') !!}
	{!! Form::text('external_link', old('external_link'), ['class' => 'form-control']) !!}
</p>


<p class="form-group">
	{!! Form::label('categories', 'Category') !!}
	<tags-input name="categories" placeholder="Categories..." :whitelist='{!! $categories->toJson() !!}' :default='{!! isset($location) ? $location->categories->toJson():"[]" !!}'></tags-input>
</p>

<p class="form-group">
	{!! Form::label('address', 'Address') !!}
	{!! Form::textArea('address', old('address'), ['class' => 'form-control', 'rows' => 5]) !!}
</p>

@if (config('services.google-maps.key'))
	<p class="form-group">
		<button class="btn btn-info" id="geocode">Geocode</button>
	</p>
@endif

<p class="form-group">
	{!! Form::label('latitude', 'Latitude') !!}
	{!! Form::number('latitude', old('latitude'), ['class' => 'form-control', 'step' => '0.00000001']) !!}
</p>

<p class="form-group">
	{!! Form::label('longitude', 'Longitude') !!}
	{!! Form::number('longitude', old('longitude'), ['class' => 'form-control', 'step' => '0.00000001']) !!}
</p>

<p class="form-group">
	{!! Form::label('artists', 'Artist') !!}
	<tags-input name="artists" placeholder="Artists..." :whitelist='{!! $artists->toJson() !!}' :default='{!! $location_artists !!}' tag-text='name'></tags-input>
</p>

<p class="form-group">
	{!! Form::label('gallery_id', 'Gallery') !!}
	{!! Form::select('gallery_id', $galleries, old('gallery_id'), ['class' => 'form-control', 'placeholder' => 'None']) !!}
</p>

<p class="form-group">
	@if( isset($location) ) @if( !is_null($location->header_image) )
		<div class="pull-left" style="width: 300px; margin: 0 20px 20px 0;">
			<img src="{{$location->header_image}}" class="img-responsive header_thumb">
			<input type="hidden" name="remove_header_image" id="remove_header_image" value="0">
			<button id="do_remove_header_image" class="btn btn-xs btn-primary">Remove Image</button>
		</div>
	@endif @endif
	{!! Form::label('header_image', 'Header Image') !!}
	{!! Form::file('header_image', old('header_image'), ['class' => 'form-control']) !!}
</p>

<p class="form-group" style="width:100%; clear:both">
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="/admin/locations" class="btn">Cancel</a>
</p>

@push('scripts')
	<script>
		$(document).ready(function () { 
				$('#do_remove_header_image').click(function () { 
					$('#remove_header_image').val('1'); 
					$(this).parent().hide();
					return false; 
				}); 
			});
	</script>
@endpush

@if (config('services.google-maps.key'))
	@push('scripts')
	<script>
		function initMap() {
			var geocoder = new google.maps.Geocoder();
			var $address = document.getElementById('address');
			var $latitude = document.getElementById('latitude');
			var $longitude = document.getElementById('longitude');

			document.getElementById('geocode').addEventListener('click', function (event) {
				event.preventDefault();

				geocoder.geocode({
					address: $address.value,
					region: 'gb',
				}, function (results, status) {
					if (status !== 'OK') {
						console.warn(status);
						return;
					}

					$latitude.value = results[0].geometry.location.lat().toFixed(8);
					$longitude.value = results[0].geometry.location.lng().toFixed(8);
				});
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google-maps.key') }}&callback=initMap" async defer></script>
	@endpush
@endif