@extends('admin.app')

@section('page_title')
	<h4>Locations</h4>
@endsection

@push('headerButtons')
	<a href="/admin/locations/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Location</span></a>
@endpush


@section('content')
	@if ($locations->isEmpty())
		<p>There are no locations at the moment</p>
	@else
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>Title</th>
					<th data-orderable="false"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($locations as $location)
					<tr class="{{ $location->trashed() ? 'danger' : '' }}">
						<td>{{ $location->title }}</td>
						<td>
							@if ($location->trashed())
								Deleted
							@else
								<ul class="icons-list">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<i class="icon-menu9"></i>
										</a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a href="{{ url("admin/locations/edit/{$location->id}") }}"><i class="icon-pencil"></i> Edit</a>
											</li>
											<li>
												<a href="{{ url("admin/locations/delete/{$location->id}") }}" class="do_delete"><i class="icon-bin"></i> Delete</a>
											</li>
										</ul>
									</li>
								</ul>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		@include('admin.common._datatable')
	@endif
@endsection

@push('scripts')
	<script>
		$('.do_delete').click(function () {
			var target = $(this).attr('href');

			swal({
				title: 'Delete Location',
				text: 'Are you sure you want to delete this location?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#ff7043',
				confirmButtonText: 'Yes, delete it!',
			}, function () {
				window.location = target;
			});

			return false;
		});
	</script>
@endpush
