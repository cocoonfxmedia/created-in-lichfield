@extends('admin.app')

@section('page_title')
	<h4>Edit Location</h4>
@endsection

@section('content')
	{!! Form::model($location, ['url' => "/admin/locations/update/{$location->id}", 'files' => true]) !!}
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			@include('admin.locations._form')
		</div>
	</div>

	{!! Form::close() !!}
@endsection
