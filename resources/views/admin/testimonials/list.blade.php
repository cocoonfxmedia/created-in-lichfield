@extends('admin.app')

@section('page_title')
    <h4>Testimonials</h4>
@endsection

@push('headerButtons')
<a href="/admin/testimonials/create" class="btn btn-link btn-float has-text"><i class="icon-file-plus text-primary"></i><span>Add Testimonial</span></a>

<a href="/admin/testimonials/trash" class="btn btn-link btn-float has-text"><i class="icon-trash text-primary"></i><span>Recycle Bin</span></a>
@endpush


@section('content')

    @if( count( $articles ) == 0 )

        <p>There are no testimonials on the website at the moment</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Date</td>
                <td>Title</td>
                <td>URL</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $articles as $p )

                <tr>
                    <td>{{$p->created_at->format('d/m/Y')}}</td>
                    <td>{{$p->title}}</td>
                    <td><a href="/{{$p->url}}" target="_blank">{{$p->url}}</a></td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{url('admin/testimonials/edit/' . $p->id)}}"><i class="icon-pencil"></i> Edit</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/testimonials/delete/' . $p->id)}}" class="do_delete"><i class="icon-bin"></i> Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection