@extends('admin.app')

@section('page_title')
    <h4>New Testimonial</h4>
@endsection


@section('content')
    {!! Form::open( ['url' => '/admin/testimonials/store', 'files' => true ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.testimonials._form')

        </div>

    </div>

    {!! Form::close() !!}

@endsection
