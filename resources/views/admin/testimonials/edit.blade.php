@extends('admin.app')

@section('page_title')
    <h4>Edit Testimonial</h4>
@endsection

@section('content')

    {!! Form::model($article, ['url' => '/admin/testimonials/update/'.$article->id , $article->id, 'files' => true  ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.testimonials._form')

        </div>

    </div>

    {!! Form::close() !!}

@endsection