@extends('admin.app')

@section('page_title')
    <h4><span>Pages</span> (Recycle Bin)</h4>
@endsection

@push('headerButtons')
@if( count( $pages ) > 0 )
    <a href="/admin/pages/empty-trash" class="btn btn-link btn-float has-text">
        <i class=" icon-trash-alt text-primary"></i><span>Empty Recycle Bin</span>
    </a>
@endif
@endpush

@section('content')

    @include('admin.common._message')


    @if( count( $pages ) == 0 )

        <p>There are no pages in the recycle bin</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Title</td>
                <td>URL</td>
                <td>Template</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $pages as $p )

                <tr>
                    <td>{{$p->title}}</td>
                    <td>{{$p->url}}</td>
                    <td>
                        {{ucwords( str_replace( "_", ' ', $p->template ) )}}
                    </td>
                    <td>
                        <a href="{{url('admin/pages/restore/' . $p->id)}}" class="btn btn-primary"><i
                                    class="fa fa-refresh"></i> Restore</a>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection