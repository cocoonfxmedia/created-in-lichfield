@extends('admin.app')

@section('page_title')
    <h4>Pages</h4>
@endsection

@push('headerButtons')
    <a href="/admin/pages/create" class="btn btn-link btn-float has-text"><i class="icon-file-plus text-primary"></i><span>Add Page</span></a>

    <a href="/admin/pages/trash" class="btn btn-link btn-float has-text"><i class="icon-trash text-primary"></i><span>Recycle Bin</span></a>
@endpush


@section('content')

    @if( count( $pages ) == 0 )

        <p>There are no pages</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <th>Title</th>
                <th>Homepage</th>
                <th>URL</th>
                <th>Template</th>
                <th></th>
            </tr>
            </thead>

            <tbody>

            @foreach ( $pages as $p )

                <tr>
                    <td>{{$p->title}}</td>
                    <td>
                        @if( $p->is_homepage )
                            <i class="icon-star-full2" style="color: gold;"></i>
                        @endif
                    </td>
                    <td><a href="{{siteConfig('url') .'/' . $p->path}}" target="_blank">{{$p->url}}</a></td>
                    <td>
                        {{ucwords( str_replace( "_", ' ', $p->template ) )}}
                    </td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{url('admin/pages/edit/' . $p->id)}}"><i class="icon-pencil"></i> Edit</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/pages/delete/' . $p->id)}}" class="do_delete"><i class="icon-bin"></i> Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection



@push('scripts')
    <script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete Page",
					text: "Are you sure you want to delete this page?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
    </script>
@endpush