@extends('admin.app')

@section('page_title')
    <h4>Edit Page</h4>
@endsection


@section('content')

    {!! Form::model($page, ['files' => true, 'url' => '/admin/pages/update/'.$page->id , $page->id ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            <p>Link: <a href="{{siteConfig('url') . '/' . $page->path}}" target="_blank">/{{$page->path}}</a></p>

            @include('admin.pages._form')

        </div>

    </div>


    {!! Form::close() !!}

@endsection