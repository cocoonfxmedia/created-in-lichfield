@include('admin.common._message')

@include('admin.common._errors')

<div class="form-group">
    {!! Form::label('title' , 'Title') !!}
    {!! Form::text('title' , old('title') , [ 'class' => 'form-control' , 'placeholder' => 'Title of the page'] ) !!}
</div>

<div class="form-group">
    {!! Form::label('url' , 'Page URL') !!}
    {!! Form::text('url' , old('url') , [ 'class' => 'form-control' , 'placeholder' => 'URL for this page - leave this blank and we\'ll generate it for you based on the page title'] ) !!}
</div>

<div class="form-group">
    {!! Form::label('template' , 'Template') !!}
    {!! Form::select('template' , $templates, old('template') , [ 'class' => 'form-control' , 'placeholder' => 'Select page template'] ) !!}
</div>

<div class="form-group">
    {!! Form::label( 'slider', 'Slider for this page') !!}
    {!! Form::select('slider', $sliderList, old('slider'), ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    @if( isset( $page))
        @if( !is_null( $page->masthead))
            <div class="pull-left" style="width: 300px; margin: 0 20px 20px 0;">
                <img src="{{$page->masthead}}" class="img-responsive masthead_thumb">
                <input type="hidden" name="remove_masthead" id="remove_masthead" value="0">
                <button id="do_remove_masthead" class="btn btn-xs btn-primary">Remove Masthead</button>
            </div>
        @endif
    @endif
    {!! Form::label('masthead', 'Masthead Image') !!}
    {!! Form::file('masthead') !!}

</div>
<div class="clearfix"></div>


<div class="form-group">
    {!! Form::label('parent_id' , 'Parent page') !!}
    {!! Form::select('parent_id' , $page_list, old('parent_id') , [ 'class' => 'form-control' , 'placeholder' => 'Select page template'] ) !!}
</div>

<div class="form-group">
    {!! Form::label('content' , 'Content') !!}

    @if( config('cms.components.gallery'))
        <select id="insert_gallery">
            <option value="#">Select a gallery to insert</option>
            @foreach( $gallery_list as $gallery )
                <option value="{{$gallery->slug}}">{{$gallery->name}}</option>
            @endforeach
        </select>
    @endif

    @if(config('cms.components.page-blocks'))
        <select id="insert_block">
            <option value="#">Select a content block to insert</option>
            @foreach( $block_list as $block )
                <option value="{{$block->slug}}">{{$block->name}}</option>
            @endforeach
        </select>
    @endif

    {!! Form::textArea('content', old('content') , [ 'class' => 'form-control redactor_editor' , 'placeholder' => 'Place your content here'] ) !!}
</div>


<div class="form-group">
    {!! Form::label('meta_title' , 'Meta Title') !!}
    {!! Form::text('meta_title' , old('meta_title') , [ 'class' => 'form-control' , 'placeholder' => 'Meta title of the page'] ) !!}
</div>

<div class="form-group">
    {!! Form::label('meta_description' , 'Meta Description') !!}
    {!! Form::textArea('meta_description' , old('meta_description') , [ 'class' => 'form-control' , 'placeholder' => 'Meta description for the page', 'rows' => '2'] ) !!}
</div>


<div class="form-group">
    {!! Form::label('block_link', 'Page link to homepage block') !!}
    {!! Form::select('block_link', $block_places, old('block_link'), ['class' => 'form-control']  ) !!}
</div>
<div id="homepage_block" class="hidden">
    <div class="form-group">
        {!! Form::label('block_image', 'Image for this block') !!}
        {!! Form::file('block_image') !!}
    </div>
    <div class="form-group">
        {!! Form::label('block_content', 'Content') !!}
        {!! Form::textarea('block_content', old('block_content'), [ 'class' => 'form-control redactor_editor' ]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('is_homepage' , 'Is this the homepage') !!}
    {!! Form::checkbox('is_homepage', '1', old('is_homepage', ((isset($page)) ? $page->is_homepage : false)) ) !!}
</div>


<div class="form-group">
    {!! Form::label('published' , 'Published') !!}
    {!! Form::checkbox('published', '1', old('published', ((isset($page)) ? $page->published : true)) ) !!}
</div>


<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="/admin/pages" class='btn'>Cancel</a>
</div>

@include('admin.common._wysiwyg')


@push('scripts')
    <script type="text/javascript">
		$(document).ready(function () {

			$('#insert_block').change(function () {
				var block = $(this).find("option:selected").attr('value');
				if (block != '#') {
					//fix to update content properly
					var output = "@@block('" + block + "')";

					$('#content').redactor('insert.html', output);
				}
			});

			$('#insert_gallery').change(function () {
				var gallery = $(this).find("option:selected").attr('value');
				if (gallery != '#') {
					//fix to update content properly
					$('#content').redactor('insert.html', "@@gallery('" + gallery + "')");
				}

			});

			if ($('#block_link').val() != '') {
				$('#homepage_block').removeClass('hidden');
			}

			$('#block_link').change(function () {
				if ($('#block_link').val() == '') {
					$('#homepage_block').addClass('hidden');
				}
				else {
					$('#homepage_block').removeClass('hidden');
				}
			});

			$('#do_remove_masthead').click(function () {
				$('#remove_masthead').val('1');
				$(this).parent().hide();
				return false;
			});

		});
    </script>
@endpush