@extends('admin.app')


@push('headerButtons')
<a href="javascript:history.back();" class="btn btn-link btn-float has-text">
    <i class="icon-backward text-primary"></i><span>Back</span>
</a>
@endpush


@section('page_title')
    <h4>Order No {{ $order->id }}</h4>
@endsection

@section('content')


    @if( count( $order ) )

        <h3>Order Detail</h3>

        <table class="table datatable-basic">

            <tbody>

            <tr>
                <td>Order ID</td>
                <td>{{ $order->id }}</td>
            </tr>
            <tr>
                <td>Order Status</td>
                <td>{{ ucwords($order->status) }}
                    <a href="/admin/orders/{{ $order->id }}/status/pending" class="btn btn-xs btn-warning">Mark
                        Pending</a>
                    <a href="/admin/orders/{{ $order->id }}/status/shipped" class="btn btn-xs btn-primary">Mark
                        Shipped</a>
                    <a href="/admin/orders/{{ $order->id }}/status/complete" class="btn btn-xs btn-success">Mark
                        Complete</a>
                </td>
            </tr>
            <tr>
                <td>Order Value</td>
                <td>&pound;{{ $order->order_value }}</td>
            </tr>
            <tr>
                <td>Payment Method</td>
                <td>
                    {{ucwords($order->pay_method)}}

                    @if( $order->pay_method == 'cash')
                        taken by {{$order->cashierInfo->name}}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Order Placed at</td>
                <td>{{ $order->created_at->format('d/m/Y H:i') }}</td>
            </tr>

            </tbody>

        </table>

        <h3>Order Lines</h3>

        <table class="table table-striped table-bordered">

            <thead>
            <tr>
                <td>Product Name</td>
                <td>Quantity</td>
                <td>Price</td>
                <td>Colour</td>
                <td>Size</td>
                <td>Custom Design</td>
            </tr>
            </thead>

            <tfoot>
            <tr>
                <td>Product Name</td>
                <td>Quantity</td>
                <td>Price</td>
                <td>Colour</td>
                <td>Size</td>
                <td>Custom Design</td>
            </tr>
            </tfoot>

            <tbody>
            @foreach( $order->lineItems as $line )
                <tr>
                    <td>{{$line->product_name}}</td>
                    <td>{{$line->quantity}}</td>
                    <td>{{$line->price}}</td>
                    <td>
                        @if( $line->colourOption )
                            {{ucwords( $line->colourOption->name )}}
                        @endif
                    </td>
                    <td>
                        @if( $line->sizeOption )
                            {{ucwords( $line->sizeOption->size )}}
                        @endif
                    </td>
                    <td>
                        @if( $line->custom_design )
                            @if( $line->custom_design == 'error')
                                <span style="color: red;">Unable to locate custom design file!</span>
                            @else
                                <a href="/pdf/{{$line->getOutputFile()}}" target="_blank">
                                    <img src="/backoffice/images/customer_products/{{$line->custom_design}}" alt=""
                                         class="thumbnail"/>
                                </a>
                            @endif
                        @else
                            No custom design uploaded
                        @endif

                        @if( $line->custom_text )
                            <p>{{$line->custom_text}}</p>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>


        @if( count($vouchers) > 0 )
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h4>Vouchers</h4>

                    <table class="table table-striped table-bordered">

                        <tr>
                            <td>Code</td>
                            <th>Description</th>
                            <th>Discount</th>
                        </tr>

                        @foreach( $vouchers as $code )

                            <tr>
                                <td>{{$code->code}}</td>
                                <td>{{$code->label}}</td>
                                <td>
                                    @if( $code->type == 'direct')
                                    &pound;
                                    @endif
                                    {{$code->discount}}
                                    @if( $code->type == 'percent')
                                    &percnt;
                                    @endif
                                </td>
                            </tr>

                        @endforeach

                    </table>
                </div>
            </div>
        @endif


        @if( $order->customer )
            <div class="row">

                <div class="col-lg-6 col-md-6">

                    <h4>Customer Contact Information</h4>

                    <table class="table table-striped table-bordered">

                        <tr>
                            <td>Name</td>
                            <td>{{$order->customer->name}}
                        </tr>

                        <tr>
                            <td>Email address:</td>
                            <td>{{$order->customer->email}}</td>
                        </tr>


                        @if( $order->customer->mobile )
                            <tr>
                                <td>Mobile No:
                                <td>{{$order->customer->mobile}}</td>
                            </tr>
                        @endif


                        @if( $order->customer->landline )
                            <tr>
                                <td>Telephone No:
                                <td>{{$order->customer->landline}}</td>
                            </tr>
                        @endif

                    </table>

                </div>

            </div>
        @endif


        <div class="row">


            <div class="col-lg-6 col-md-6">

                <h4>Shipping Address</h4>

                @if( $order->click_and_collect )

                    <p>This is a click and collect order</p>
                    <p>To be collected from the <b>{{$order->store}}</b> store</p>

                @else

                    <p>{{ $order->shippingAddress->name }}</p>
                    <p>{{ $order->shippingAddress->fullname }}</p>
                    <p>{{ $order->shippingAddress->address_1 }}</p>
                    <p>{{ $order->shippingAddress->address_2 }}</p>
                    <p>{{ $order->shippingAddress->town_city }}</p>
                    <p>{{ $order->shippingAddress->county }}</p>
                    <p>{{ $order->shippingAddress->postcode }}</p>

                @endif

            </div>

            <div class="col-lg-6 col-md-6">

                @if( $order->pay_method != 'cash' )

                    <h4>Billing Address</h4>

                    <p>{{ $order->billingAddress->name }}</p>

                    <p>{{ $order->billingAddress->fullname }}</p>

                    <p>{{ $order->billingAddress->address_1 }}</p>

                    <p>{{ $order->billingAddress->address_2 }}</p>

                    <p>{{ $order->billingAddress->town_city }}</p>

                    <p>{{ $order->billingAddress->county }}</p>

                    <p>{{ $order->billingAddress->postcode }}</p>

                @endif

            </div>

        </div>

        @include('admin.common._datatable')

    @else
        <h2>Invalid Order</h2>
    @endif

@endsection