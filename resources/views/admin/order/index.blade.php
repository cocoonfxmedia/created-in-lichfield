@extends('admin.app')

@section('page_title')

    @if( isset($customer) )
        <h4>Orders for {{$customer->name}}</h4>
    @else
        <h4>Orders</h4>
    @endif

@endsection

@push('headerButtons')
<a href="javascript:history.back();" class="btn btn-link btn-float has-text">
    <i class="icon-backward text-primary"></i><span>Back</span>
</a>
@endpush


@section('content')

    @if( count( $orders ) )

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>#</td>
                <td>T</td>
                <td>Status</td>
                <td>Value</td>
                <td>Placed At</td>
                <td>View</td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td>#</td>
                <td>T</td>
                <td>Status</td>
                <td>Value</td>
                <td>Placed At</td>
                <td>View</td>
            </tr>
            </tfoot>

            <tbody>

            @foreach( $orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>
                        @if( $order->click_and_collect)
                            <i class="fa fa-male" title="This order is for pickup"></i>
                            {{$order->store}}
                        @else
                            <i class="fa fa-truck" title="This order is for delivery"></i>
                        @endif
                    </td>
                    <td>{{ $order->status }}</td>
                    <td>&pound;{{ $order->order_value }}</td>
                    <td>{{ $order->created_at->format('d/m/Y H:i') }}</td>
                    <td><a href="/admin/orders/{{$order->id}}" class="btn-primary btn-sm">View</a></td>
                </tr>
            @endforeach

            </tbody>

        </table>

        @import('admin.common._datatable')

        <p style="margin-top: 15px;">
            <i class="fa fa-truck" title="This order is for delivery"></i> = Order for delivery |
            <i class="fa fa-male" title="This order is for pickup"></i> = Click & Collect Order
        </p>


    @else
        <h2>There are no orders</h2>
    @endif


@endsection

@push( 'sctipts')

<script type="text/javascript">
    $(document).ready(function () {
        $('table').DataTable({
            "order": [[1, "desc"]]
        });
    });

</script>

@endpush