@extends('admin.app')

@section('page_title')
	<h4>Edit Blog Category</h4>
@endsection

@section('content')
	{!! Form::model($cat, ['url' => '/admin/blog-categories/update/'.$cat->id , $cat->id ] ) !!}

	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12">

			@include('admin.blog.category.form')

		</div>

	</div>


	{!! Form::close() !!}


	@include('admin.common._errors')


@endsection
