@if( session('msg') )
	<div class="alert alert-success">
		{{session('msg')}}
	</div>
@endif

<p class="form-group">
	{!! Form::label('title' , 'Title') !!}
	{!! Form::text('title' , old('title') , [ 'class' => 'form-control' , 'placeholder' => 'Title of the blog category'] ) !!}
</p>

<p class="form-group">
	{!! Form::label('slug' , 'Category URL') !!}
	{!! Form::text('slug' , old('slug') , [ 'class' => 'form-control' , 'placeholder' => 'URL slug for this page'] ) !!}
</p>


<p class="form-group">
	{!! Form::label('active' , 'Active') !!}
	{!! Form::checkbox('active', 1, old('active', 1) ) !!}
</p>

<p class="form-group">
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="/admin/blog-categories" class='btn'>Cancel</a>
</p>

@section('javascript')

	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="/lib/redactor/redactor.min.js"></script>

	<script type="text/javascript">
		jQuery('.redactor_editor').redactor();
	</script>

@endsection

@section('css')
	<link href="/lib/redactor/redactor.css" rel="stylesheet">
@endsection