@extends('admin.app')

@section('page_title')
    <h4>Blog Categories</h4>
@endsection

@push('headerButtons')
    <a href="/admin/blog-categories/create" class="btn btn-link btn-float has-text"><i class="icon-folder-plus text-primary"></i><span>Add Category</span></a>
@endpush

@section('content')


    @if( count( $categories ) == 0 )

        <p>There are no blog categories on the website at the moment</p>

    @else

        <table class="table table-striped table-bordered data-table">

            <thead>
            <tr>
                <td>Title</td>
                <td>Slug</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $categories as $cat )

                <tr>
                    <td>{{$cat->title}}</td>
                    <td>{{$cat->slug}}</td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{url('admin/blog-categories/edit/' . $cat->id)}}"><i class="icon-pencil"></i> Edit</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/blog-categories/delete/' . $cat->id)}}" class="do_delete"><i class="icon-bin"></i> Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

    @endif

@endsection



@push('scripts')
    <script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete Category",
					text: "Are you sure you want to delete this category?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
    </script>
@endpush