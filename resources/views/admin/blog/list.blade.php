@extends('admin.app')

@section('page_title')
    <h4>Blog</h4>
@endsection

@push('headerButtons')
    <a href="/admin/blog/create" class="btn btn-link btn-float has-text"><i class="icon-file-plus text-primary"></i><span>Add Article</span></a>

    <a href="/admin/blog/trash" class="btn btn-link btn-float has-text"><i class="icon-trash text-primary"></i><span>Recycle Bin</span></a>
@endpush


@section('content')

    @if( count( $articles ) == 0 )

        <p>There are no blog articles on the website at the moment</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Date</td>
                <td>Title</td>
                <td>URL</td>
                <td>Category</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $articles as $p )

                <tr>
                    <td>{{$p->created_at->format('d/m/Y')}}</td>
                    <td>{{$p->title}}</td>
                    <td><a href="{{$p->link}}" target="_blank">{{$p->url}}</a></td>
                    <td>
                        @if( is_object( $p->category ) )
                            {{$p->category->title}}
                        @endif
                    </td>
                    <td>
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{url('admin/blog/edit/' . $p->id)}}"><i class="icon-pencil"></i> Edit</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/blog/delete/' . $p->id)}}" class="do_delete"><i class="icon-bin"></i> Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif

@endsection


@push('scripts')
    <script type="text/javascript">
		$('.do_delete').click(function () {
			var target = $(this).attr('href');
			swal({
					title: "Delete Article",
					text: "Are you sure you want to delete this article?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#FF7043",
					confirmButtonText: "Yes, delete it!"
				},
				function () {
					window.location = target;
				});
			return false;

		});
    </script>
@endpush