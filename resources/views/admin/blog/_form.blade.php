@include('admin.common._errors')

<p class="form-group">
    {!! Form::label('title' , 'Title') !!}
    {!! Form::text('title' , old('title') , [ 'class' => 'form-control' , 'placeholder' => 'Title of the page'] ) !!}
</p>

<p class="form-group">
    {!! Form::label('url' , 'Article URL') !!}
    {!! Form::text('url' , old('url') , [ 'class' => 'form-control' , 'placeholder' => 'URL for this page - You can leave this blank and we\'ll automatically create the page URL based on the title you entered above' ] ) !!}
</p>

<p class="form-group">
    {!! Form::label('category_id' , 'Category') !!}
    {!! Form::select('category_id' , $categories, old('category_id') , [ 'class' => 'form-control' , 'placeholder' => 'Select page category'] ) !!}
</p>

<p class="form-group">
    {!! Form::label('content' , 'Content') !!}
    {!! Form::textArea('content', old('content') , [ 'class' => 'form-control redactor_editor' , 'placeholder' => 'Place your content here'] ) !!}
</p>


<p class="form-group">
    {!! Form::label('meta_title' , 'Meta Title') !!}
    {!! Form::text('meta_title' , old('meta_title') , [ 'class' => 'form-control' , 'placeholder' => 'Meta title of the page'] ) !!}
</p>

<p class="form-group">
    {!! Form::label('meta_description' , 'Meta Description') !!}
    {!! Form::textArea('meta_description' , old('meta_description') , [ 'class' => 'form-control' , 'placeholder' => 'Meta description for the page', 'rows' => '2'] ) !!}
</p>
<p class="form-group">
    {!! Form::label('thumbnail', 'Thumbnail') !!}
    {!! Form::file('thumbnail', [ 'class' => 'file-styled' ] ) !!}
</p>

<p class="form-group">
    {!! Form::label('published' , 'Published') !!}
    {!! Form::checkbox('published', 1,  old('published', ((isset($article)) ? $article->published : 1)) ) !!}
</p>

<p class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="/admin/blog" class='btn'>Cancel</a>
</p>

@include('admin.common._wysiwyg')