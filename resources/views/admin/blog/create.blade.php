@extends('admin.app')

@section('page_title')
    <h4>New Blog Article</h4>
@endsection


@section('content')
    {!! Form::open( ['url' => '/admin/blog/store', 'files' => true ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.blog._form')

        </div>

    </div>

    {!! Form::close() !!}

@endsection
