@extends('admin.app')

@section('page_title')
    <h4>Edit Blog Article</h4>
@endsection

@section('content')

    {!! Form::model($article, ['url' => '/admin/blog/update/'.$article->id , $article->id, 'files' => true  ] ) !!}

    {!! Form::hidden('id', $article->id ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">

            @include('admin.blog._form')

        </div>

    </div>

    {!! Form::close() !!}

@endsection