@extends('admin.app')

@section('page_title')
    <h4>Currency Exchange Rates</h4>
@endsection


@section('content')

    <div class="chart-container">
        <div class="chart" id="google-line"></div>
    </div>

@endsection


@push('scripts')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script>
		/* ------------------------------------------------------------------------------
 *
 *  # Google Visualization - lines
 *
 *  Google Visualization line chart demonstration
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */


		// Line chart
		// ------------------------------

		// Initialize chart
		google.load("visualization", "1", {packages: ["corechart"]});
		google.setOnLoadCallback(drawLineChart);


		// Chart settings
		function drawLineChart() {

			// Data
			var data = google.visualization.arrayToDataTable([
				<?php echo $dataset; ?>
			]);

			// Options
			var options = {
				fontName: 'Roboto',
				height: 400,
				curveType: 'function',
				fontSize: 12,
				chartArea: {
					left: '5%',
					width: '90%',
					height: 350
				},
				pointSize: 4,
				tooltip: {
					textStyle: {
						fontName: 'Roboto',
						fontSize: 13
					}
				},
				vAxis: {
					title: 'Exchange rate against GBP',
					titleTextStyle: {
						fontSize: 13,
						italic: false
					},
					gridlines: {
						color: '#e5e5e5',
						count: 10
					},
					minValue: 0
				},
				legend: {
					position: 'top',
					alignment: 'center',
					textStyle: {
						fontSize: 12
					}
				}
			};

			// Draw chart
			var line_chart = new google.visualization.LineChart($('#google-line')[0]);
			line_chart.draw(data, options);
		}


		// Resize chart
		// ------------------------------

		$(function () {

			// Resize chart on sidebar width change and window resize
			$(window).on('resize', resize);
			$(".sidebar-control").on('click', resize);

			// Resize function
			function resize() {
				drawLineChart();
			}
		});

    </script>
@endpush
