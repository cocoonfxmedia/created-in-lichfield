@extends('admin.app')

@section('page_title')
    <h4>Settings</h4>
@endsection

@section('content')

    @include('admin.common._message')

    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#general-tab" data-toggle="tab">General</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="general-tab">
                <div class="col-md-9">
                    {!! Form::open(['url' => '/admin/config/general']) !!}

                    @foreach( $settings as $key => $value )
                        <div class="form-group">
                            @if( Auth::user()->can('advanced-config'))
                                {!! Form::label( $key,$key) !!}
                            @else
                                {!! Form::label( $key, str_replace('_', ' ' , $key)) !!}
                            @endif

                            @if( is_bool($value))
                                {!! Form::checkbox($key,1, old($key, $value)) !!}
                            @else
                                {!! Form::text($key, old($key, $value), ['class' => 'form-control']) !!}
                            @endif
                        </div>
                    @endforeach

                    <div class="form-group">
                        <button class="btn btn-primary">Save</button>
                    </div>

                    {!! Form::close() !!}
                </div>

                @if( Auth::user()->can('advanced-config'))
                    <div class="col-md-3">
                        <h4>Settings to templates</h4>
                        Use the following code with the
                        <em>setting_name</em> replace with the label for the relevant setting you want to add
                        <code>{{siteConfig('setting_name')}}</code>

                        <br><br>

                        <h4>Adding new settings</h4>
                        <p>All of the setting values are stored in the /config/settings.php file.</p>

                        <p>Settings values are stored in a PHP array.</p>

                        <p>The setting name must be all lower case and not contain spaces. <code>_</code> characters in the name will be replaced with spaces on this page</p>


                        <p>Copy the following line and add it to the settings.php file.</p>
                        <code>'setting_name' => 'setting value',</code>



                    </div>
                @endif
            </div>
        </div>

    </div>


@endsection