<a id="do_new_course" class="btn btn-success pull-right"><i class="icon-plus3"></i> New Course</a>

@if( count($courses) > 0 )

    {!! Form::open() !!}
    <button id="do_saveOrder" class="btn btn-primary pull-right" style="margin-right: 10px;">Save Order</button>
    {!! Form::close() !!}

    <div class="col-md-6">
        <ul class="courseList">

            @foreach( $courses as  $c )
                <li data-id="{{$c->id}}">{{$c->name}} <a href="#" class="do_delete"><i class="pull-right icon-cancel-square text-danger"></i></a></li>
            @endforeach
        </ul>
    </div>

@else

    <p>There are no courses currently configured for this site.</p>
    <p>The course form will be hidden from view on the site until one or more course has been added.</p>

@endif


<div id="new_course_form" style="display: none;">
    {!! Form::open(['url' => '/admin/config/course/store']) !!}
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-heading">
                <h6 class="panel-title">New Course</h6>
            </div>
            <div class="panel-body">


                <div class="form-group">
                    {!! Form::label('name', 'Course Name') !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required']) !!}
                </div>

                <button class="btn btn-primary">Save</button>
                or <a class="do_cancel">Cancel</a>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
</div>


@push('scripts')
<script type="text/javascript" src="/backoffice/js/plugins/sortable.js"></script>
<script type="text/javascript">

	$(document).ready(function () {

		//open new course form
		$('#do_new_course').click(function () {
			$('#new_course_form').slideDown('slow');
			return false;
		});

		//close new course form
		$('.do_cancel').click(function () {
			$('#new_course_form').slideUp('slow');
			$('#new_course_form #name').val('');
			return false;
		});

		//save the course list order
		$('#do_saveOrder').click(function () {
			var nav = [];

			//translate order into variable
			$('.courseList li').each(function (index) {
				nav[index] = {id: $(this).attr('data-id')};
			});

			//submit order to server side
			$.post('/admin/config/course/order', {'nav': JSON.stringify(nav)}, function (response) {
				if (response == 'OK') {
					new PNotify({
						title: 'Saved',
						text: 'The course list order was saved',
						addclass: 'bg-success'
					});
				}
			});
			return false;
		});

		//make the course list sortable
		$('.courseList').sortable();

		//delete course from list
		$('.do_delete').click(function(){
			var target = $(this).parent().attr('data-id');
			$(this).parent().remove();
			$.get('/admin/config/course/delete/' + target );
			return false;
        });

	});

</script>

<style>
    .courseList li {
        padding: 10px;
        margin: 5px 0;
        border: 1px solid #ccc;
        list-style: none;
    }
</style>


@endpush

