@extends('admin.app')


@section('page_title')
    <h4>Duplicate Site Content</h4>
@endsection



@section('content')

    <p class="alert alert-danger" id="no_same" style="display: none;">You cannot copy content to the same site! <br>Select a different destination site.</p>

    <p class="alert alert-warning">This process will duplicate the content from one site to another. Once started, this cannot be undone!</p>

    <p>To prevent conflicts, the desination site should be clean.</p>

    {!! Form::open() !!}

    <div class="form-group">
        {!! Form::label('from', 'Source Site - content from this site will be copied') !!}
        {!! Form::select('from', $siteList, old('from'), ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('to', 'Destination Site - the copied content will be inserted to this site') !!}
        {!! Form::select('to', $siteList, old('to'), ['class' => 'form-control']) !!}
    </div>

    <button class="btn btn-success">Start Duplicating</button>

    {!! Form::close() !!}

@endsection

@push('scripts')
<script type="text/javascript">
	$('form').submit(function () {
		var from = $('#from :selected').val();
		var to = $('#to :selected').val();

        if (from == to) {
            $('#no_same').show();
			return false;
		}

		$('#no_same').hide();
	});
</script>
@endpush