@extends('admin.app')


@section('page_title')
    <h4>Power Tools</h4>
@endsection


@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                    <p><b><i class="icon-power2"></i> These tools are powerful.</b> They should be used with caution. Backup the site before using them just in case.</p>
            </div>
        </div>
    </div>


@endsection