@extends('admin.app')

@section('page_title')
    <h4><i class="icon-grid3 position-left"></i> <span class="text-semibold">Your</span> - Dashboard</h4>
@endsection


@section('content')

    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Welcome</h6>
            </div>

            <div class="panel-body">
                <p>Welcome to admin panel for <b>{{siteConfig('site_name')}}</b> site.</p>
                <p>This is the place to manage your website.</p>
            </div>
        </div>
    </div>

@endsection