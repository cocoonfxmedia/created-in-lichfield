@extends('admin.app')

@section('page_title')
    <h4>Edit Content Block</h4>
@endsection


@section('content')

    {!! Form::model($block, ['url' => '/admin/blocks/'.$block->id . '/update' , $block->id ] ) !!}

    <div class="row">

        <div class="col-sm-12 col-md-12 col-lg-12">


            @include('admin.blocks._form')

        </div>

    </div>


    {!! Form::close() !!}

@endsection