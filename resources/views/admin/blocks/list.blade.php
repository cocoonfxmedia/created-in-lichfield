@extends('admin.app')

@section('page_title')
    <h4>Content Blocks</h4>
@endsection

@push('headerButtons')
<a href="/admin/blocks/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Block</span></a>
@endpush


@section('content')

    @if( count( $blocks ) == 0 )

        <p>There are no blocks configured yet</p>

    @else

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Name</td>
                <td>Description</td>
                <td>Slug</td>
                <td></td>
            </tr>
            </thead>

            <tbody>

            @foreach ( $blocks as $block )

                <tr>
                    <td>{{$block->name}}</td>
                    <td>{{$block->description}}</td>
                    <td>{{$block->slug}}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="/admin/blocks/{{$block->id}}/edit"><i class="icon-pencil7"></i> Edit block details</a>
                                    </li>
                                    <li>
                                        <a href="/admin/blocks/{{$block->id}}">
                                            <i class="icon-images3"></i> Manage Content
                                        </a>
                                    </li>
                                    @if( $block->published == 1 )
                                        <li>
                                            <a href="/admin/blocks/{{$block->id}}/unpublish"><i class="icon-eye-blocked"></i> Unpublish</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="/admin/blocks/dupe" class="do_dupe" data-toggle="modal" data-target="#dupeForm" data-id="{{$block->id}}" data-name="{{$block->name}}"><i class="icon-copy4"></i> Duplicate</a>
                                    </li>
                                    @if( $block->published == 0 )
                                        <li>
                                            <a href="/admin/blocks/{{$block->id}}/publish"><i class="icon-eye"></i> Publish</a>
                                        </li>
                                    @endif
                                    <li class="divider"></li>
                                    <li>
                                        <a href="/admin/blocks/{{$block->id}}/delete"><i class="icon-bin"></i> Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @endif



    <!-- Vertical form modal -->
    <div id="dupeForm" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Create new block from an existing one</h5>
                    <p>This will allow you to create a new block and copy all of the existing content from the selected block to your newly created block.</p>
                </div>

                <form action="/admin/blocks/dupe">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>New block name</label>
                            <input type="text" name="newName" id="newName" value="" class="form-control">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="hidden" name="blockId" id="blockId" value="">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Duplciate Block</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /vertical form modal -->

@endsection


@push('scripts')
<script type="text/javascript" src="/backoffice/js/plugins/notifications/bootbox.min.js"></script>
<script>
	$('.do_dupe').mouseup(function () {
		$('#blockId').val( $(this).attr('data-id') );
		$('#newName').val( $(this).attr('data-name') + ' 2' );
	});
</script>
@endpush