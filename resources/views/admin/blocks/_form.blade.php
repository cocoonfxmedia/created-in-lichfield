@include('admin.common._message')

@include('admin.common._errors')

<p class="form-group">
	{!! Form::label('name' , 'Name') !!}
	{!! Form::text('name' , old('name') , [ 'class' => 'form-control' , 'placeholder' => 'Name for the gallery'] ) !!}
</p>

<p class="form-group">
	{!! Form::label('description' , 'Description') !!}
	{!! Form::textArea('description' , old('description') , [ 'class' => 'form-control' , 'placeholder' => 'Name for the content block'] ) !!}
</p>

<p class="form-group">
	{!! Form::label('slug' , 'Slug') !!}
	{!! Form::text('slug' , old('slug') , [ 'class' => 'form-control' , 'placeholder' => 'Code used to access the gallery'] ) !!}
</p>

<p class="form-group">
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="/admin/blocks" class='btn'>Cancel</a>
</p>
