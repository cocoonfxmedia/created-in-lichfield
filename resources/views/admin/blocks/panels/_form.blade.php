@include('admin.common._message')

@include('admin.common._errors')

<p class="form-group">
    {!! Form::label('title' , 'Title') !!}
    {!! Form::text('title' , old('title') , [ 'class' => 'form-control' , 'placeholder' => 'Title of the content panel'] ) !!}
</p>

<p class="form-group">
    {!! Form::label('content' , 'Content') !!}
    {!! Form::textArea('content', old('content') , [ 'class' => 'form-control redactor_editor' , 'placeholder' => 'Place your content for this panel here'] ) !!}
</p>


<p class="form-group">
    {!! Form::label('published' , 'Published') !!}
    {!! Form::checkbox('published', '1', old('published', 1) ) !!}
</p>

<p class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
    <a href="/admin/blocks/{{$block->id}}" class='btn'>Cancel</a>
</p>


@include('admin.common._wysiwyg')


@push('scripts')
<script type="text/javascript" src="/backoffice/js/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/uploaders/dropzone.min.js"></script>
<script>
    $(function () {

        // Defaults
        Dropzone.autoDiscover = false;


        // Multiple files
        $("#dropzone_multiple").dropzone({
            paramName: "file", // The name that will be used to transfer the file
            dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
            maxFilesize: 3, // MB,
            acceptedFiles: '.pdf, .doc, .docx, .xlsx, .ppt, .pptx, .ppsx',
            init: function(){
                this.on("success", function(file, data) {
                   output = '<a href="'+data.url+'" class="doclink"><i class="'+data.type+'"></i> '+data.name+'</a><br />';

//                    $('#redactor-uuid-0').append( output );

                    $('.redactor_editor').redactor('insert.html', output);
                });
            },
        });


    });
</script>
@endpush