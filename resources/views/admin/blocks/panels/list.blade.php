@extends('admin.app')

@section('page_title')
    <h4>Content Block: {{$block->name}}</h4>
@endsection

@push('headerButtons')
<a href="/admin/blocks/{{$block->id}}/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Panel</span></a>
<a id="do_saveOrder" class="btn btn-link btn-float has-text"><i class="icon-list text-primary"></i><span>Save Order</span></a>
@endpush

@section('content')

    <div class="row">

        @if( count( $panels) == 0 )

            <p>There are no panels for this block</p>

        @else

            <div class="panelContainer">

                @foreach( $panels as $panel )

                    <div class="col-md-12 sortable" data-id="{{$panel->id}}">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">{{$panel->title}}</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li>
                                            <a href="/admin/blocks/{{$block->id}}/edit/{{$panel->id}}"><i class="icon-pencil7"></i></a>
                                        </li>
                                        <li>
                                            <a href="/admin/blocks/{{$block->id}}/delete/{{$panel->id}}"><i class="icon-trash"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                {!!  $panel->content !!}
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>

        @endif

    </div>

@endsection


@push('scripts')
<script type="text/javascript" src="/backoffice/js/core/libraries/jquery_ui/interactions.min.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/uploaders/dropzone.min.js"></script>

<script>
    $(function () {

        // Sortable panel
        $(".panelContainer").sortable({
            items: '.sortable'
        });

        $('#do_saveOrder').click(function(){
            var nav = [];


            //translate order into variable
            $('.sortable').each(function (index) {
                nav[index] = {id: $(this).attr('data-id')};
            });

            //submit order to server side
            $.post('/admin/blocks/{{$block->id}}/order', {'nav': JSON.stringify(nav)}, function (response) {
                if (response == 'OK') {
                    new PNotify({
                        title: 'Saved',
                        text: 'The content panels order was saved',
                        addclass: 'bg-success'
                    });
                }
            });
        });

    });
</script>
@endpush