@extends('admin.app')

@section('page_title')
    <h4>Edit Content Panel</h4>
@endsection


@section('content')
    <div class="row">

        {!! Form::model($panel, ['url' => '/admin/blocks/'.$block->id.'/update/'. $panel->id , $panel->id ] ) !!}


        <div class="col-sm-12 col-md-8">

            @include('admin.blocks.panels._form')

        </div>

        {!! Form::close() !!}

        <div class="col-sm-12 col-md-4">
            <!-- Multiple file upload -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Add files</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <p class="content-group">Drag and drop files onto the box below to add them to this content panel
                    </p>

                    <form action="/admin/blocks/fileupload" class="dropzone" id="dropzone_multiple"></form>
                </div>
            </div>
            <!-- /multiple file upload -->
        </div>


    </div>
@endsection