@extends('admin.app')

@section('page_title')
    <h4>Products</h4>
@endsection

@push('headerButtons')
<a href="/admin/products/create" class="btn btn-link btn-float has-text"><i class="icon-plus3 text-primary"></i><span> Add Product</span></a>
<a href="/admin/products/order" class="btn btn-link btn-float has-text"><i class="icon-list-ordered text-primary"></i><span> Manager Order</span></a>
@endpush

@section('content')

    @if( count( $products ) )

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>#</td>
                <td>Product Code</td>
                <td>Name</td>
                <td>Category</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td>#</td>
                <td>Product Code</td>
                <td>Name</td>
                <td>Category</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </tfoot>

            <tbody>

            @foreach( $products as $product)
                <tr @if( ! $product->active ) class="danger" @endif>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->product_code }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->category['name'] }}</td>
                    <td><a href="/admin/products/{{$product->id}}/edit" class="btn-primary btn-sm">Edit</a></td>
                    <td><a href="/admin/products/{{$product->id}}/delete" class="btn-danger btn-sm">Delete</a></td>
                </tr>
            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @else
        <h2>There are no products</h2>
    @endif

@endsection