@extends('admin.app')

@section('page_title')
    <h1><span>Product Query</span> - # {{$query->id}}</h1>
@endsection


@section('content')

    <div class="row">

        <div class="col-md-6">
            <table class="table">

                <tr>
                    <th style="text-align: right;">Received:</th>
                    <td>{{$query->created_at->format('d/m/Y G:i	')}}</td>
                </tr>

                <tr>
                    <th style="text-align: right;">Status:</th>
                    <td class="status">
                        @if( $query->replied == 0)
                            Pending
                        @else
                            Replied
                        @endif
                    </td>
                </tr>

                <tr>
                    <th style="text-align: right;">Name</th>
                    <td>{{$query->name}}</td>
                </tr>

                <tr>
                    <th style="text-align: right;">Email Address</th>
                    <td><a href="mailto:{{$query->email}}">{{$query->email}}</a></td>
                </tr>

            </table>
        </div>

        <div class="col-md-6">
            <table class="table">
                <tr>
                    <th>Product</th>
                    <td>{{$query->product->name}}</td>
                </tr>

                <tr>
                    <td colspan="2"><img src="/usr/products/{{$query->product->image}}"></td>
                </tr>
                <tr>
                    <th>SKU</th>
                    <td>{{$query->product->product_code}}</td>
                </tr>
                <tr>
                    <td colspan="2"><a href="/shop/product/{{$query->product->slug}}" target="_blank">View product</a>
                    </td>

                </tr>
            </table>
        </div>

    </div>

    <h4>Query:</h4>
    <p>{{$query->message}}</p>

    @if( $query->replied == 0 )
        <p style="margin-top: 20px;"><a class="btn btn-success" id="do_markAsReplied">Mark as replied</a></p>
    @endif

@endsection


@push('scripts')

<script type="text/javascript">

    $('#do_markAsReplied').click(function () {
        $.get('/admin/product-queries/replied/{{$query->id}}', function (response) {
            $('#do_markAsReplied').hide();
            $('.status').html('Replied');
        });

        return false;
    });

</script>

@endpush