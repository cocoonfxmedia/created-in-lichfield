@extends('admin.app')

@section('page_title')
    <h4>Product Queries</h4>
@endsection

@section('content')

    @if( count( $queries ) )

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>Date</td>
                <td>Name</td>
                <td>Email</td>
                <td>Product</td>
                <td>Replied to</td>
                <td></td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td>Date</td>
                <td>Name</td>
                <td>Email</td>
                <td>Product</td>
                <td>Replied to</td>
                <td></td>
            </tr>
            </tfoot>

            <tbody>

            @foreach( $queries as $q)
                <tr>
                    <td>{{$q->created_at->format('d/m/Y G:i')}}</td>
                    <td>{{$q->name}}</td>
                    <td><a href="mailto:{{$q->email}}">{{$q->email}}</a></td>
                    <td><a href="/shop/product/{{$q->product->slug}}" target="_blank">{{$q->product->name}}</a></td>
                    <td>
                        @if( $q->replied)
                            <i class="fa fa-check" aria-hidden="true" style="color: green;"></i>
                        @else
                            <i class="fa fa-times" aria-hidden="true" style="color: red;"></i>
                        @endif
                    </td>
                    <td>
                        <a href="/admin/product-queries/{{$q->id}}"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @else
        <h3>There are no product queries</h3>
    @endif


@endsection