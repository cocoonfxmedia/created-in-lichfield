@extends('admin.app')

@section('page_title')
    <h4>Mass Price Adjustment</h4>
@endsection

@section('content')

    @include('admin.common._errors')


    {!! Form::open() !!}

    <p>
        This tool is used to adjust all prices in the product catalogue at the same time be a specified percentage of the current price.
    </p>
    <p>Once this operation has been started, it cannot be undone.</p>

    <p class="form-group">
        {!! Form::label('amount', '% to adjust prices by') !!}
        {!! Form::text('amount', old('amount'), ['class' => 'form-control']) !!}
    </p>

    <p class="form-group">
        {!! Form::label('direction', 'Adjustment type') !!}
        <select name="direction" id="direction" class="form-control">
            <option value="up">Increase</option>
            <option value="down">Decrease</option>
        </select>
    </p>

    <p class="form-group">
        {!! Form::submit('Adjust Prices', ['class' => 'form-control btn btn-warning'] ) !!}
    </p>

    {!! Form::close() !!}

@endsection