@extends('admin.app')

@section('page_title')
    <h4>Product Display Order</h4>
@endsection

@push('headerButtons')
{!! Form::open( [ 'url' => 'admin/products/order/save', 'style' => 'display: inline;'] )!!}
<input type="hidden" name="product_order" id="product_order" value=""/>
<button type="submit" id="Ordersave" class="btn btn-primary">Save Changes</button>
{!! Form::close() !!}

<a href="/admin/products/order" class="btn btn-danger" style="margin-left: 15px;">Discard Changes</a>
@endpush

@section('content')

    <p>This page allows you to reorder the products as they appear on the web site.</p>
    <p>Drag and drop the products to reorder them and don&rsquo;t forget to click <b>Save changes</b> at the bottom of
        the page.</p>
    <p>Click <b>Discard Changes</b> to reload the saved order without saving any changes you have made.</p>


    @if( Session::has("status") )
        <div class="alert alert-success alert-dismissible" role="alert">
            {{Session::get('status')}}
        </div>
    @endif

    <ul class="list-group" id="productOrder">
        @foreach( $products as $p )
            <li class="list-group-item" data-pid="{{$p->id}}">{{$p->name}}</li>
        @endforeach
    </ul>


@endsection