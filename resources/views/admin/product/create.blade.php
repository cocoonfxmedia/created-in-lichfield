@extends('admin.app')

@section('page_title')
    <h4>Create Product</h4>
@endsection



@section('content')

    {!! Form::open( ['url' => '/admin/products/create' , 'method' => 'post' ]) !!}

    <p class="form-group">
        {!! Form::label('name' , 'Product Name') !!}
        {!! Form::text('name'  , old('name') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a product name'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('product_code' , 'Product Code') !!}
        {!! Form::text('product_code'  , old('product_code') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a product code'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('description' , 'Product Description') !!}
        {!! Form::textarea('description'  , old('description') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a product description'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('category_id' , 'Category') !!}
        {!! Form::select('category_id', $categories , null , array( 'class' => 'form-control' ) ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('meta_title' , 'Page Title') !!}
        {!! Form::text('meta_title'  , old('meta_title') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a custom page title'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('meta_description' , 'Meta Description') !!}
        {!! Form::textarea('meta_description'  , old('meta_description') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a description for the category', 'rows' => 2] ) !!}
    </p>

    <p class="form-group dimensions">
        {!! Form::label('dimension_w' , 'Dimensions') !!}<br/>

        W: {!! Form::text('dimension_w'  , old('dimension_w') , [ 'class' => 'form-control' , 'placeholder' => 'width'] ) !!}
        cm

        H: {!! Form::text('dimension_h'  , old('dimension_h') , [ 'class' => 'form-control' , 'placeholder' => 'height'] ) !!}
        cm

        L: {!! Form::text('dimension_l'  , old('dimension_l') , [ 'class' => 'form-control' , 'placeholder' => 'length'] ) !!}
        cm
    </p>

    <p class="form-group">
        {!! Form::label('weight' , 'Weight') !!}
        {!! Form::text('weight'  , old('weight') , [ 'class' => 'form-control' , 'placeholder' => 'Enter product weight in Kg'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('package_type' , 'Packaging Note') !!}
        {!! Form::text('package_type'  , old('package_type') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a note about the product packaging'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::label('featured' , 'Featured') !!}
        {!! Form::checkbox('featured'  , old('featured') , [ 'class' => 'form-control'] ) !!}
    </p>

    <p class="form-group">
        {!! Form::Label('cf_enabled', 'Enable Custom Text Field') !!}
        {!! Form::checkbox('cf_enabled', old('cf_enabled'), [ 'class' => 'form-control'] ) !!}
    </p>

    <div id="custom_field_subform">

        <p class="form-group">
            {!! Form::label('cf_label', 'Custom Text Field Label') !!}
            {!! Form::text('cf_label', old('cf_label'), [ 'class' => 'form-control'] ) !!}
        </p>

        <p class="form-group">
            {!! Form::label('cf_char', 'No of characters customer can enter') !!}
            {!! Form::text('cf_char', old('cf_char'), [ 'class' => 'form-control']) !!}
        </p>

        <p class="form-group">
            {!! Form::label('cf_lines', 'No. lines for custom text field') !!}
            {!!  Form::text('cf_lines', old('cf_lines'), ['class' => 'form-control'] )!!}
        </p>

    </div>

    <p class="form-group">
        <button type="submit" class="btn btn-primary">Create</button>
    </p>

    @include('admin.common._errors')

    {!! Form::close() !!}

@endsection


@push('scripts')

<script type="text/javascript">
    jQuery('#cf_enabled').click(function () {
        jQuery('#custom_field_subform').slideToggle();
    });

    if (jQuery('#cf_enabled:checked').length == 0) {
        jQuery('#custom_field_subform').hide();
    }
</script>

@endpush
