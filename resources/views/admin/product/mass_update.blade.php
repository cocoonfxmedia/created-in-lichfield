@extends('admin.layout')

@section('content')

<h1>Mass Update Products</h1>
<div class="alert alert-info" role="alert"> <strong>Important!</strong> Do not include commas in fields within the CSV. </div>

<a href="/admin/products/mass-update/download">Download CSV Catalogue &rtrif;</a>


{!! Form::open([ 'url' => '/admin/products/mass-update/processor', 'files' => true] ) !!}

<div class="row" style="margin-top: 20px;">

	<div class="col-sm-12 col-md-6 col-lg-8">

		<p class="form-group">
			{!! Form::label('catalog' , 'CSV Product Catalogue') !!}
			{!! Form::file('catalog'  , null , [ 'class' => 'form-control'] ) !!}
		</p>

		<p class="form-group">
			<button type="submit" class="btn btn-primary">Process Catalogue File</button>
		</p>

		@include('admin.partials.errors')

	</div>

</div>

{!! Form::close() !!}


@if( $messages ) 

<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-12">
		
		<h2>Mass Update Results</h2>
		
		@foreach( $messages as $msg ) 
		
		<p>{{$msg}}</p>
		
		@endforeach
		
	</div>
</div>

@endif

@endsection