<div class="row">

    <div class="col-md-12">
        @include('admin.common._errors')
    </div>

    <div class="col-sm-12 col-md-6 col-lg-8">
        <p class="form-group">
            {!! Form::label('name' , 'Category Name') !!}
            {!! Form::text('name'  , old('name') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a category name'] ) !!}
        </p>

        <p class="form-group">
            {!! Form::label('description' , 'Description') !!}
            {!! Form::textarea('description'  , old('description') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a description for the category', 'rows' => 2] ) !!}
        </p>

        <p class="form-group">
            {!! Form::label('meta_title' , 'Page Title') !!}
            {!! Form::text('meta_title'  , old('meta_title') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a custom page title for this category'] ) !!}
        </p>

        <p class="form-group">
            {!! Form::label('meta_description' , 'Meta Description') !!}
            {!! Form::textarea('meta_description'  , old('meta_description') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a description for the category', 'rows' => 2] ) !!}
        </p>

    </div>

    <div class="col-sm-12 col-md-6 col-lg-4">

        <div class="well">
            <h3 class="panel-title">Category Details</h3>
            <div class="panel-body">

                <p class="form-group">
                    {!! Form::label('active' , 'Is the category active') !!}
                    {!! Form::checkbox('active',1, old('active') ) !!}
                </p>

                <p class="form-group">
                    <?php $no_image = '/backoffice/images/no_image.jpg'; ?>
                    @if( isset( $category))
                        @if( strlen( $category->image ) )
                            <img src="{{ $filename }}" alt="Product Image" class="admin-product-image"/>
                        @else
                            <img src="{{ $no_image }}" alt="Product Image" class="admin-product-image"/>
                        @endif
                    @endif

                    {!! Form::label('image' , 'Category Image') !!}
                    {!! Form::file('image', old('image') , array( 'class' => 'form-control' ) ) !!}
                </p>

                <p class="form-group">
                    {!! Form::label('parent_id', 'Parent Category') !!}
                    {!! Form::select('parent_id', $categories , old('parent_id') , array( 'class' => 'form-control' ) ) !!}
                </p>

                <p class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </p>

            </div>

        </div>

    </div>

</div>