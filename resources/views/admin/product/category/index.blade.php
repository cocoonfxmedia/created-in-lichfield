@extends('admin.app')

@section('page_title')
    <h4>Product Categories</h4>
@endsection

@push('headerButtons')
<a href="/admin/product-categories/create" class="btn btn-link btn-float has-text"><i class="icon-folder-plus2 text-primary"></i><span>Add Product Category</span></a>
@endpush

@section('content')



    @if( count( $categories ) )

        <table class="table datatable-basic">

            <thead>
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Parent</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Parent</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </tfoot>

            <tbody>

            @foreach( $categories as $cat)
                <tr @if( ! $cat->active ) class="danger" @endif>
                    <td>{{ $cat->id }}</td>
                    <td>{{ $cat->name }}</td>
                    <td>
                        @if( $cat->parent )
                            {{$cat->parent->name}}
                        @endif
                    </td>
                    <td><a href="/admin/product-categories/{{$cat->id}}/edit" class="btn-primary btn-sm">Edit</a></td>
                    <td><a href="/admin/product-categories/{{$cat->id}}/delete" class="btn-danger btn-sm">Delete</a></td>
                </tr>
            @endforeach

            </tbody>

        </table>

        @include('admin.common._datatable')

    @else
        <h2>There are no categories</h2>
    @endif





@endsection