@extends('admin.app')

@section('page_title')
    <h4>Create Product Category</h4>
@endsection

@section('content')

    {!! Form::open( ['url' => '/admin/product-categories/create' , 'method' => 'post' , 'files' => true ]) !!}

    @include('admin.product.category._form')

    {!! Form::close() !!}

@endsection