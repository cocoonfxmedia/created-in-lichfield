@extends('admin.app')

@section('page_title')
    <h4>Edit Producut Category</h4>
@endsection

@section('content')

    {!! Form::model($category, ['url' => '/admin/product-categories/'.$category->id.'/update/', $category->id, 'files' => true ] ) !!}

    @include('admin.product.category._form')

    {!! Form::close() !!}

@endsection