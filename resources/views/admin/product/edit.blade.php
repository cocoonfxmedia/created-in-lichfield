@extends('admin.app')

@section('page_title')
    <h4>Update Product</h4>
@endsection

@section('content')


    {!! Form::model($product, ['url' => '/admin/products/update', $product->id , 'files' => true ] ) !!}

    <div style="position: relative; top: -39px;">


    </div>

    <div class="row">

        <div class="col-sm-12 col-md-6 col-lg-8">

            {!! Form::hidden('id'  , $product->id  ) !!}
            {!! Form::hidden('_token'  , csrf_token()  ) !!}

            <p class="form-group">
                {!! Form::label('name' , 'Product Name') !!}
                {!! Form::text('name'  , old('name') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a product name'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('description' , 'Product Description') !!}
                {!! Form::textarea('description'  , old('description') , [ 'class' => 'form-control redactor_editor' , 'placeholder' => 'Enter a product description'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('meta_title' , 'Page Title') !!}
                {!! Form::text('meta_title'  , old('meta_title') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a custom page title'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('meta_description' , 'Meta Description') !!}
                {!! Form::textarea('meta_description'  , old('meta_description') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a description for the product', 'rows' => 2] ) !!}
            </p>

            <p class="form-group dimensions">
                {!! Form::label('dimension_w' , 'Dimensions') !!}<br/>

                W: {!! Form::text('dimension_w'  , old('dimension_w') , [ 'class' => 'form-control' , 'placeholder' => 'width'] ) !!}
                cm

                H: {!! Form::text('dimension_h'  , old('dimension_h') , [ 'class' => 'form-control' , 'placeholder' => 'height'] ) !!}
                cm

                L: {!! Form::text('dimension_l'  , old('dimension_l') , [ 'class' => 'form-control' , 'placeholder' => 'length'] ) !!}
                cm
            </p>

            <p class="form-group">
                {!! Form::label('weight' , 'Weight') !!}
                {!! Form::text('weight'  , old('weight') , [ 'class' => 'form-control' , 'placeholder' => 'Enter product weight in Kg'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('package_type' , 'Packaging Note') !!}
                {!! Form::text('package_type'  , old('package_type') , [ 'class' => 'form-control' , 'placeholder' => 'Enter a note about the product packaging'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::label('featured' , 'Featured') !!}
                {!! Form::checkbox('featured'  , old('featured') , [ 'class' => 'form-control'] ) !!}
            </p>

            <p class="form-group">
                {!! Form::Label('cf_enabled', 'Enable Custom Text Field') !!}
                {!! Form::checkbox('cf_enabled', old('cf_enabled'), [ 'class' => 'form-control'] ) !!}
            </p>

            <div id="custom_field_subform">

                <p class="form-group">
                    {!! Form::label('cf_label', 'Custom Text Field Label') !!}
                    {!! Form::text('cf_label', old('cf_label'), [ 'class' => 'form-control'] ) !!}
                </p>

                <p class="form-group">
                    {!! Form::label('cf_char', 'No of characters customer can enter') !!}
                    {!! Form::text('cf_char', old('cf_char'), [ 'class' => 'form-control']) !!}
                </p>

                <p class="form-group">
                    {!! Form::label('cf_lines', 'No. lines for custom text field') !!}
                    {!!  Form::text('cf_lines', old('cf_lines'), ['class' => 'form-control'] )!!}
                </p>

            </div>

            @include('admin.common._errors')

        </div>

        <div class="col-sm-12 col-md-6 col-lg-4">

            <div class="well">
                <h3 class="panel-title">Product Details
                    <small class="pull-right">Product Code <strong>{{ $product->product_code }}</strong></small>
                </h3>
                <div class="panel-body">

                    <p class="form-group">
                        {!! Form::label('product_code' , 'Product Code') !!}
                        {!! Form::text('product_code', $product->product_code ,  array( 'class' => 'form-control' ) ) !!}
                    </p>

                    <p class="form-group">
                        {!! Form::label('category_id' , 'Category') !!}
                        {!! Form::select('category_id', $categories , null , array( 'class' => 'form-control' ) ) !!}
                    </p>

                    <p class="form-group">
                        {!! Form::label('active' , 'Is the product on sale') !!}
                        {!! Form::checkbox('active', 1 , old('active')) !!}
                    </p>

                    <p class="form-group">
                        {!! Form::label('price' , 'Price') !!}
                        {!! Form::text('price', old('price') , array( 'class' => 'form-control' ) ) !!}
                    </p>


                    <p class="form-group">
                        {!! Form::label('sale_price' , 'Sale Price') !!}
                        {!! Form::text('sale_price', old('sale_price') , array( 'class' => 'form-control' ) ) !!}
                    </p>

                    <p class="form-group">

                        <?php
                        $filename = \Config::get( 'elem.product_images_folder_views' ) . $product->image;
                        $no_image = '/backoffice/images/no_image.jpg';
                        ?>
                        @if( strlen( $product->image ) )
                            <img src="{{ $filename }}" alt="Product Image" class="admin-product-image"/>
                        @else
                            <img src="{{ $no_image }}" alt="Product Image" class="admin-product-image"/>
                        @endif

                        {!! Form::label('image' , 'Image') !!}
                        {!! Form::file('image', old('image') , array( 'class' => 'form-control' ) ) !!}
                    </p>

                    <p class="form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="submit" class="btn btn-primary" name="save_and_new" value="yes">Save & create
                            new
                        </button>
                    </p>
                </div>
            </div>


        </div>


    </div>



    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Additional Images</h3>

                </div>
                <div class="panel-body">

                    <div class="col-md-4">
                        <p class="form-group">

                            <?php
                            $filename = \Config::get( 'elem.product_images_folder_views' ) . $product->image_1;
                            ?>
                            @if( !strlen( $product->image_1 ) )
                                <?php $filename = '/backoffice/images/no_image.jpg'; ?>
                            @endif

                            <img src="{{ $filename }}" alt="Product Image" class="admin-product-image"
                                 style="margin-bottom: 10px;"/>

                            {!! Form::file('image_1', old('image_1') , array( 'class' => 'form-control' ) ) !!}
                        </p>
                    </div>

                    <div class="col-md-4">
                        <p class="form-group">

                            <?php
                            $filename = \Config::get( 'elem.product_images_folder_views' ) . $product->image_2;
                            ?>
                            @if( strlen( !$product->image_2 ) )
                                <?php $filename = '/backoffice/images/no_image.jpg'; ?>
                            @endif

                            <img src="{{ $filename }}" alt="Product Image" class="admin-product-image"
                                 style="margin-bottom: 10px;"/>

                            {!! Form::file('image_2', old('image_2') , array( 'class' => 'form-control' ) ) !!}
                        </p>
                    </div>

                    <div class="col-md-4">
                        <p class="form-group">

                            <?php
                            $filename = \Config::get( 'elem.product_images_folder_views' ) . $product->image_3;
                            ?>
                            @if( !strlen( $product->image_3 ) )
                                <?php $filename = '/backoffice/images/no_image.jpg'; ?>
                            @endif

                            <img src="{{ $filename }}" alt="Product Image" class="admin-product-image"
                                 style="margin-bottom: 10px;"/>

                            {!! Form::file('image_3', old('image_3') , array( 'class' => 'form-control' ) ) !!}
                        </p>
                    </div>

                </div>
            </div>


        </div>
    </div>

    {!! Form::close() !!}

    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Colours</h3>
                </div>
                <div class="panel-body">

                    {!! Form::open( ['method' => 'post' , 'url' => '/admin/products/colour/create', 'files' => true ] ) !!}
                    <input type="hidden" name="product_id" value="{{ $product->id }}"/>

                    <input type="text" name="name" class="form-control" placeholder="Colour Name"/>


                    <div class="input-group">
                        <div class="input-group-addon">#</div>
                        <input type="text" name="hex" class="form-control" placeholder="e.e. ffffff"/>
                    </div>

                    <input type="submit" value="Add Colour" class="btn btn-primary btn-sm btn-block"/>
                    {!! Form::close() !!}

                    <div class="row">
                        <table class="table">
                            @foreach( $colours as $colour )

                                <tr>
                                    <td>
                                        {{ $colour->name }}

                                    </td>
                                    <td>
                                        <span style="background-color: #{{ $colour->hex }}" class="colour-block"></span>
                                    </td>
                                    <td>
                                        <a href="/admin/products/colour/delete/{{ $colour->id }}"
                                           class="btn btn-primary btn-xs">Delete</a>
                                    </td>
                                </tr>


                            @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sizes</h3>
                </div>
                <div class="panel-body">

                    {!! Form::open( ['method' => 'post' , 'url' => 'admin/products/size/create']) !!}
                    <input type="hidden" name="product_id" value="{{ $product->id }}"/>

                    <input type="text" name="size" class="form-control" placeholder="e.g. medium"/>
                    <input type="submit" value="Add Size" class="btn btn-primary btn-sm btn-block"/>
                    {!! Form::close() !!}

                    <div class="row">
                        <table class="table">
                            @foreach( $sizes as $size )

                                <tr>
                                    <td>
                                        {{ $size->size }}
                                    </td>
                                    <td>
                                        <a href="/admin/products/size/delete/{{ $size->id }}"
                                           class="btn btn-primary btn-xs">Delete</a>
                                    </td>
                                </tr>


                            @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <a href="#" class="pull-right btn btn-xs btn-success do_show_rproduct">New Related Product</a>

                    <h3 class="panel-title">Related Products</h3>
                </div>

                <div class="panel-body">

                    <div class="panel panel-default" id="rproduct-frm">
                        <div class="panel-body">
                            @if( Session::has('relation_msg') )

                                <div class="alert alert-danger" role="alert">
                                    {{ Session::get('relation_msg') }}
                                </div>

                            @endif

                            {!!Form::open(['url' => '/admin/products/'.$product->id.'/related_products'])!!}

                            <label for="rproduct_name">Product Name</label>
                            <input type="text" name="rproduct_name" id="rproduct_name"/>

                            <button class="btn btn-primary">Attach Product</button>
                            {!!Form::close()!!}
                        </div>
                    </div>

                    @if( !$product->relatedProducts->count() )

                        <p>There are no product related to this one</p>
                        <p>Add a <a href="#" class="btn btn-xs btn-success do_show_rproduct">New Related Product</a></p>

                    @else

                        <table class="table">

                            <thead>
                            <tr>
                                <td>SKU</td>
                                <td>Name</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($product->relatedProducts as $p)

                                <tr>
                                    <td>{{$p->product_code}}</td>
                                    <td>{{$p->name}}</td>
                                    <td>
                                        <a href="/admin/products/{{$product->id}}/related/{{$p->id}}/delete"
                                           class="btn btn-xs btn-danger">Remove</a>
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>

                    @endif
                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/redactor/redactor.min.js"></script>

<script type="text/javascript">
    jQuery('.do_show_new_faceFrm').click(function () {
        jQuery('#new_faceFrm').slideDown('slow');
        return false;
    });

    jQuery('.do_show_rproduct').click(function () {
        jQuery('#rproduct-frm').slideDown();
        return false;
    });

    <?php if ( Session::has( 'relation_msg' ) ): ?>
        jQuery('#rproduct-frm').slideDown();
    <?php endif; ?>


    jQuery(function () {
        var availableTags = [
            <?php foreach ( $productTitles as $p ): ?>
            <?php echo json_encode( $p->name ) ?>,
            <?php endforeach; ?>
        ];

        jQuery("#rproduct_name").autocomplete({
            source: availableTags
        });

        jQuery('.redactor_editor').redactor();

        jQuery('#cf_enabled').click(function () {
            jQuery('#custom_field_subform').slideToggle();
        });

        if (jQuery('#cf_enabled:checked').length == 0) {
            jQuery('#custom_field_subform').hide();
        }


    });

    var originalProductSubcategory = "<?php echo $product->subcategory_id; ?>";

</script>

@endpush


@push('css')
<link href="/backoffice/js/plugins/redactor/redactor.css" rel="stylesheet">
@endpush