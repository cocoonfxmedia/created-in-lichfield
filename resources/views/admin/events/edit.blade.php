@extends('admin.app')

@section('page_title')
	<h4>Edit Event</h4>
@endsection

@push('headerButtons')
<a href="/admin/events/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Event</span></a>
@endpush

@section('content')
	{!! Form::model($event, ['url' => "/admin/events/update/{$event->id}", 'files' => true]) !!}

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			@include('admin.events._form')
		</div>
	</div>

	{!! Form::close() !!}
@endsection
