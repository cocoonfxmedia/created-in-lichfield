@extends('admin.app')

@section('page_title')
	<h4>Events</h4>
@endsection

@push('headerButtons')
	<a href="/admin/events/create" class="btn btn-link btn-float has-text"><i class="icon-plus2 text-primary"></i><span>Add Event</span></a>
@endpush


@section('content')
	@if ($events->isEmpty())
		<p>There are no events at the moment</p>
	@else
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>Title</th>
					<th>Status</th>
					<th>At</th>
					<th data-orderable="false"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($events as $event)
					<tr class="{{ $event->trashed() ? 'danger' : '' }}">
						<td>{{ $event->title }}</td>
						<td>{{ $event->status }}</td>
						<td>{{ $event->at->format('d/m/Y H:i:s') }}</td>
						<td>
							@if ($event->trashed())
								Deleted
							@else
								<ul class="icons-list">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<i class="icon-menu9"></i>
										</a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a href="{{ url("admin/events/edit/{$event->id}") }}"><i class="icon-pencil"></i> Edit</a>
											</li>
											<li>
												<a href="{{ url("admin/events/delete/{$event->id}") }}" class="do_delete"><i class="icon-bin"></i> Delete</a>
											</li>
										</ul>
									</li>
								</ul>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		@include('admin.common._datatable')
	@endif
@endsection

@push('scripts')
	<script>
		$('.do_delete').click(function () {
			var target = $(this).attr('href');

			swal({
				title: 'Delete Event',
				text: 'Are you sure you want to delete this event?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#ff7043',
				confirmButtonText: 'Yes, delete it!',
			}, function () {
				window.location = target;
			});

			return false;
		});
	</script>
@endpush
