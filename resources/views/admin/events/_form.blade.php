@include('admin.common._errors')

@push('lib_scripts')
<script type="text/javascript" src="/backoffice/js/plugins/pickers/pickadate/picker.js"></script>	
<script type="text/javascript" src="/backoffice/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript" src="/backoffice/js/plugins/pickers/pickadate/picker.time.js"></script>
@endpush

<p class="form-group">
	{!! Form::label('title', 'Title') !!}
	{!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('at', 'At') !!}
	{!! Form::text('date', old('date'), ['class' => 'form-control datepicker', 'data-date-format' =>'dd-mm-yyyy']) !!}
	{!! Form::text('time', old('time'), ['class' => 'form-control timepicker', 'data-date-format' =>'dd-mm-yyyy']) !!}
</p>

<p class="form-group">
	{!! Form::label('status', 'status') !!} 
	{!! Form::select('status', ['pending' => 'pending', 'active' => 'active', 'inactive' => 'inactive'], old('status'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('price', 'Price') !!}
	{!! Form::number('price', old('price'), ['class' => 'form-control', 'step' => 0.01]) !!}
</p>

<p class="form-group">
	{!! Form::label('description', 'Description') !!}
	{!! Form::textArea('description', old('description'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('external_link', 'External Link') !!}
	{!! Form::text('external_link', old('external_link'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('header_image', 'Header Image') !!}
	{!! Form::file('header_image', old('header_image'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('event_code', 'Event Code') !!}
	{!! Form::text('event_code', old('event_code'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('location_id', 'Location') !!}
	{!! Form::select('location_id', $locations, old('location_id'), ['class' => 'form-control']) !!}
</p>

<p class="form-group">
	{!! Form::label('categories', 'Category') !!}
	<tags-input name="categories" placeholder="Categories..." :whitelist='{!! $categories->toJson() !!}' :default='{!! isset($event) ? $event->categories->toJson():"[]" !!}'></tags-input>
</p>

@if (config('services.google-maps.key'))
	<p class="form-group">
		{!! Form::label('address', 'Address') !!}
		{!! Form::textArea('address', old('address'), ['class' => 'form-control', 'rows' => 5]) !!}
	</p>

	<p class="form-group">
		<button class="btn btn-info" id="geocode">Geocode</button>
	</p>
@endif

<p class="form-group">
	{!! Form::label('latitude', 'Latitude') !!}
	{!! Form::number('latitude', old('latitude'), ['class' => 'form-control', 'step' => '0.00000001']) !!}
</p>

<p class="form-group">
	{!! Form::label('longitude', 'Longitude') !!}
	{!! Form::number('longitude', old('longitude'), ['class' => 'form-control', 'step' => '0.00000001']) !!}
</p>

<p class="form-group">
	{!! Form::label('artists', 'Artists') !!}
	<tags-input name="artists" placeholder="Artists..." 
		:whitelist='{!! $artists->toJson() !!}' 
		:default='{!! $event_artists !!}' 
		tag-text='name' 
		selected="{{ old('artists') }}"></tags-input>
</p>

<p class="form-group">
	{!! Form::label('gallery_id', 'Gallery') !!}
	{!! Form::select('gallery_id', $galleries, old('gallery_id'), ['class' => 'form-control', 'placeholder' => 'None']) !!}
</p>

<p class="form-group">
	<button type="submit" class="btn btn-primary">Save</button>
	<a href="/admin/events" class="btn">Cancel</a>
</p>

@push('scripts')
	<script>
		$(document).ready(function () {
			$('.datepicker').pickadate({
				formatSubmit: 'yyyy-mm-dd',
			});	
			$('.timepicker').pickatime();
		});
	</script>
@endpush

@if (config('services.google-maps.key'))
	@push('scripts')
	<script>
		function initMap() {
			var geocoder = new google.maps.Geocoder();
			var $address = document.getElementById('address');
			var $latitude = document.getElementById('latitude');
			var $longitude = document.getElementById('longitude');

			document.getElementById('geocode').addEventListener('click', function (event) {
				event.preventDefault();

				geocoder.geocode({
					address: $address.value,
					region: 'gb',
				}, function (results, status) {
					if (status !== 'OK') {
						console.warn(status);
						return;
					}

					$latitude.value = results[0].geometry.location.lat().toFixed(8);
					$longitude.value = results[0].geometry.location.lng().toFixed(8);
				});
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google-maps.key') }}&callback=initMap" async defer></script>
	@endpush
@endif
