<?php echo '<?xml version = "1.0" encoding = "UTF-8"?>'; ?>
<urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <url>
        <loc>{{url('/')}}</loc>
    </url>
    @foreach( $pages as $page)
        <url>
            <loc>{{url($page->url)}}</loc>
        </url>
    @endforeach
    <url>
        <loc>{{url('blog')}}</loc>
    </url>
    @foreach( $blogArticles as $page)
        <url>
            <loc>{{url($page->link)}}</loc>
        </url>
    @endforeach
    @foreach( $blogCategories as $page)
        <url>
            <loc>{{url('blog/catgeory/'.$page->slug)}}</loc>
        </url>
    @endforeach


    @if( config('cms.components.ecommerce'))
        @foreach( $categories as $cat )
            <url>
                <loc>{{url('/category/' . $cat->slug)}}</loc>
            </url>
            @foreach( $cat->products as $product )
                <url>
                    <loc>{{url('/product/' . $product->slug)}}</loc>
                </url>
            @endforeach
        @endforeach
        <url>
            <loc>{{url('login')}}</loc>
        </url>
        <url>
            <loc>{{url('register')}}</loc>
        </url>
        <url>
            <loc>{{url('checkout')}}</loc>
        </url>
    @endif
    <url>
        <loc>{{url('contact-us')}}</loc>
    </url>
</urlset>