@extends('site')

@section('meta_data')
    <title>Sitemap - {{config('app.name')}}</title>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        <h1 class="defaulth1">Sitemap</h1>

                        <div class="col-xs-12 col-sm-6">
                            <div class="sitemap_block box">
                                <h3 class="page-subheading">Pages</h3>
                                <ul>
                                    <li>
                                        <a href="/" title="Home">
                                            Home
                                        </a>
                                    </li>
                                    @foreach( $pages as $page )
                                        <li>
                                            <a href="{{url($page->url)}}" title="{{$page->title}}">
                                                {{$page->title}}
                                            </a>
                                        </li>
                                    @endforeach
                                    <li>
                                        <a href="/contact-us" title="Contact">
                                            Contact
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <div class="sitemap_block box">
                                <h3 class="page-subheading">Blog</h3>
                                <ul>
                                    <li><a href="/blog">Blog</a></li>
                                    @foreach($blogCategories as $cat )
                                        <li><a href="/blog/category/{{$cat->slug}}">{{$cat->title}}</a></li>
                                    @endforeach
                                    @foreach( $blogArticles as $article )
                                        <li><a href="{{$article->link}}">{{$article->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('css')
    <link rel="stylesheet" href="/site/themes/default-bootstrap/css/sitemap.css" type="text/css" media="all"/>
@endpush