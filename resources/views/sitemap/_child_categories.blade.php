@foreach ($catalog as $cat )

	<li><a href="{{url('category/' . $cat->slug)}}">{{$cat->name}}</a></li>

	<ul>
		@include('sitemap._child_categories', ['catalog' => $cat->children ] )
	</ul>

@endforeach