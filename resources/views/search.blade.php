@extends('site')

@section('meta_data')
    <title>{{config('app.name')}}</title>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container search-results">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        <h1 class="defaulth1">Search : {{$searchString}}
                            <span class="heading-counter">{{$resultCount}} results have been found.</span></h1>


                        @if( $resultCount == 0 )

                            <p class="alert alert-warning">
                                No results were found for your search&nbsp;"{{$searchString}}"
                            </p>

                            <p>Please try searching for something else</p>

                        @else

                            @if( count( $pages ) > 0)
                            <!-- Subcategories -->
                                <div id="subcategories">
                                    <h2>Pages</h2>
                                    @foreach( $pages as $page )
                                        <article>
                                            <h3>
                                                <a class="subcategory-name" href="/{{$page->path}}">{{$page->title}}</a>
                                            </h3>
                                            <p>
                                                <a href="/{{$page->path}}">{{str_limit(strip_tags($page->content), 300)}}.... </a>
                                            </p>

                                        </article>
                                    @endforeach
                                </div>
                            @endif


                            @if( count( $articles ) > 0)
                            <!-- Subcategories -->
                                <div id="subcategories">
                                    <h2>News</h2>
                                    @foreach( $articles as $page )
                                        <article>
                                            <h5>
                                                <a class="subcategory-name" href="/{{$page->path}}">{{$page->title}}</a>
                                            </h5>
                                            <p>
                                                <a href="/{{$page->path}}">{{str_limit(strip_tags($page->content), 300)}}.... </a>
                                            </p>
                                        </article>
                                    @endforeach
                                </div>
                            @endif

                            @if( count( $categoryList ) > 0)
                            <!-- Subcategories -->
                                <div id="subcategories">
                                    <h2>Categories</h2>
                                    @foreach( $categoryList as $cat )
                                        <article>
                                            <div class="subcategory-image">
                                                <a href="/category/{{$cat->slug}}" title="{{$cat->name}}"
                                                   class="img">
                                                    <img class="replace-2x"
                                                         src="{{$cat->image}}"
                                                         alt="{{$cat->name}}" width="125" height="125"/>
                                                </a>
                                            </div>
                                            <h5><a class="subcategory-name"
                                                   href="/category/{{$cat->slug}}">{{$cat->name}}</a>
                                            </h5>
                                        </article>
                                    @endforeach
                                </div>
                            @endif


                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@push('css')
    <link rel="stylesheet" href="/site/themes/default-bootstrap/css/category.css" type="text/css" media="all"/>
    <style>
        .search-results article a {
            color: #000;
        }
    </style>
@endpush