@extends('site')

@include('common._cms_page_meta')

@section('content')

    @if( $page->slider != 0 )
        @include('components.slider')
    @endif

    @if( !is_null( $page->masthead ))
        <div class="masthead" style="background-image: url({{$page->masthead}});">
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        <h1 class="defaulth1">{!!$page->title!!}</h1>
						<!-- <?php  echo Blade::compileString( $page->content ); ?> -->



					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/automotiveth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Automotive</h2></div>
							<div class="sectorDesc"><p>The automotive supply chain is driven by deadline and costs.</p>

<p>Most manufacturers and tier 1, 2 and 3 suppliers all require supply chain solutions that enable inventory management in order to fulfil their commitments.</p>

<p>Our focus on inventory management for our customers enables fast and effective services to the changing needs of this sector.</p></div>
						</div>
					</div>

					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/drinksth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Drinks</h2></div>
							<div class="sectorDesc"><p>The drinks market is a very competitive industry and at IFSC Group we understand that services developed need to provide an innovative, tailored solution.</p>

<p>The product is often very sensitive to temperature and humidity and requires constant monitoring and management.</p></div>
						</div>
					</div>


					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/artwork.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Fine Art Work</h2></div>
							<div class="sectorDesc"><p>IFSC Group provides comprehensive Fine Art Transportation service that includes all modes of transport. Artwork from a gallery to a client's residence, for museums, major exhibitions, to a world-wide touring exhibitions. Whether by Air, Sea or road, our dedicated shipping team can handle it and ensure that our Customers have piece of mind.</p></div>
						</div>
					</div>


					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/fmcgth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>FMCG</h2></div>
							<div class="sectorDesc"><p>The Fast Moving Consumer Goods (FMCG) sector is known for high-frequency purchasing, constant change and large volumes.</p>

<p>Having an agile and flexible logistics provider is essential.</p>

<p>At IFSC Group we offer our customers bespoke added-value solutions and services to create value in your supply chain with the flexibility to support change.</p></div>
						</div>
					</div>

					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/hitechth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>High Tech</h2></div>
							<div class="sectorDesc"><p>The High-Tech sector is constantly developing and evolving and as new products reach us, there are more challenges within the supply chain in order to reduce inventory and real cost.</p> 

<p>At IFSC Group we understand that these supply chain requirements need to be managed effectively and while speed to market is important, we provide a pre planning service that is focused on speed combined with inventory management.</p></div>
						</div>
					</div>


					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/industrialth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Industrial</h2></div>
							<div class="sectorDesc"><p>The industrial sector has diverse logistics challenges due to global sourcing including agility, security and strong technical competence.</p>

<p>We understand the design requirement to execute supply chains that enables our customers to have a competitive advantage.</p>

<p>Our cross trade services support effective supply chain redesign to overcome these challenges.</p></div>
						</div>
					</div>

					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/marineth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Marine Logistics</h2></div>
							<div class="sectorDesc"><p>Marine Logistics requires management to tight schedules.</p>

<p>At IFSC Group we offer a customised solution for our customers to ensure all schedules are managed and delivered within the requirements.</p>

<p>We understand the downtime of cost for ships that must stay on the move, which means that our focus is  on making sure  the supply chain runs efficiently and effectively.</p></div>
						</div>
					</div>

					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/oilgasth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Oil & Gas</h2></div>
							<div class="sectorDesc"><p>The oil and gas industry has survived an especially tough few years with weak demand and low fuel prices.</p>

<p>It has been difficult to make strategic decisions and plan for the future. Only now is the sector beginning to emerge from the economic downside.</p> 

<p>At IFSC Group we value the loyalty of our customers and in order to support the change in market conditions, we regularly review supply chains with our customers to reduce real costs.</p></div>
						</div>
					</div>

					<div class="singleSector">
						<div class="sectorPhoto sect"><img src="/site/images/retailth.jpg"/></div>
						<div class="sectorBio">
							<div class="sectorName"><h2>Retail</h2></div>
							<div class="sectorDesc"><p>UK retailers are looking to reduce cost and inventory, with the added pressure of demand for in-store product availability and e-commerce increasing.</p>

<p>The IFSC Group sector has years of experience in the retail sector in customised solutions, working closely with customers to enhance performance and optimise supply chains.</p></div>
						</div>
					</div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection