@extends('site')

@include('common._cms_page_meta')

@section('content')

    @if( $page->slider != 0 )
        @include('components.slider')
    @endif

    @if( !is_null( $page->masthead ))
        <div class="masthead" style="background-image: url({{$page->masthead}});">
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>



    <div class="container-fluid standardPage">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12  col-md-10  col-md-push-1 col-lg-8 col-lg-push-2">
                    <div class="introductionBox">
                        <h1 class="defaulth1">{!!$page->title!!}</h1>
						<?php  echo Blade::compileString( $page->content ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection