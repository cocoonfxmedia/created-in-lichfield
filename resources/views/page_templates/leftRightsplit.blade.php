@extends('site')

@include('common._cms_page_meta' )

@section('content')

    @if( $page->slider != 0 )
        @include('components.slider')
    @endif

    <div class="yellowBar">
        <div class="row">
            <div class="small-12 medium-12 large-12 columns">

                <h1 class="lefth1">{!!$page->title!!}</h1>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>


    <div class="row">

        <div class="small-12 medium-8 large-8 columns leftRight">
            <?php  echo Blade::compileString( $page->content ); ?>
        </div>

        <div class="small-12 medium-4 large-4 columns">
            <div class="childMenus">
                <h2>In this section</h2>
            <nav>
                @include('common._section_nav', ['subnav' => $sectionNav])
            </nav>
            </div>
        </div>


    </div>
@endsection