@extends('site')


@section('meta_data')
    <title>{{config('app.name')}}</title>
@endsection


@section('content')

@include('components.introduction')

@include('components.artists')

@include('components.events')

@include('components.locations')

@endsection
