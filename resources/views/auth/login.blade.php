@extends('site')

@section('content')

    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">
            <h1 class="page-heading">Authentication</h1>

            <!---->
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <form action="/register" method="get" id="create-account_form"
                          class="box">
                        <h3 class="page-subheading">Create an account</h3>
                        <div class="form_content clearfix">
                            <p>Please enter your email address to create an account.</p>
                            <div class="alert alert-danger" id="create_account_error" style="display:none"></div>
                            <div class="form-group">
                                <label for="email_create">Email address</label>
                                <input type="email" class="is_required validate account_input form-control" data-validate="isEmail" id="email_create" name="email" value=""/>
                            </div>
                            <div class="submit">
                                <button class="btn btn-default button button-medium exclusive" type="submit">
							<span>
								<i class="icon-user left"></i>
								Create an account
							</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <form action="login" method="post" id="login_form" class="box">
                        {{csrf_field()}}
                        <h3 class="page-subheading">Already registered?</h3>
                        @include('common._errors')
                        <div class="form_content clearfix">
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="is_required validate account_input form-control" data-validate="isEmail"
                                       type="email" id="email" name="email" value=""/>
                            </div>
                            <div class="form-group">
                                <label for="passwd">Password</label>
                                <input class="is_required validate account_input form-control" type="password"
                                       data-validate="isPasswd" id="passwd" name="password" value=""/>
                            </div>
                            <!--<p class="lost_password form-group"><a
                                        href="/password/reset"
                                        title="Recover your forgotten password" rel="nofollow">Forgot your password?</a>
                            </p> -->
                            <p class="submit" style="margin-top: 10px;">
                                <input type="hidden" class="hidden" name="back" value="my-account"/>
                                <button type="submit" id="SubmitLogin"
                                        class="button btn btn-default button-medium">
							<span>
								<i class="icon-lock left"></i>
								Sign in
							</span>
                                </button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- #center_column -->
    </div><!-- .row -->

@endsection

@push('css')
<link rel="stylesheet" href="/site/themes/default-bootstrap/css/authentication.css" type="text/css" media="all"/>
@endpush
