@extends('site')

@section('content')

    <div class="row">
        <div id="center_column" class="center_column col-xs-12 col-sm-12">

            <div class="box">
                <h1 class="page-subheading">Forgot your password?</h1>


                <p>Please enter the email address you used to register. We will then send you a new password. </p>
                <form action="/password/reset" method="post" class="std" id="form_forgotpassword">
                    {{csrf_field()}}
                    <fieldset>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input class="form-control" type="email" id="email" name="email" value=""/>
                        </div>
                        <p class="submit">
                            <button type="submit" class="btn btn-default button button-medium"><span>Retrieve Password
                                    <i class="icon-chevron-right right"></i></span>
                            </button>
                        </p>
                    </fieldset>
                </form>
            </div>
            <ul class="clearfix footer_links">
                <li><a class="btn btn-default button button-small" href="/login"
                       title="Back to Login" rel="nofollow"><span><i class="icon-chevron-left"></i>Back to Login</span></a>
                </li>
            </ul>
        </div><!-- #center_column -->
    </div><!-- .row -->

@endsection


