@extends('layouts.basic')

@section('content')

<div class="container-fluid artistLogin artistReg">
	<div class="container">
	<div class="row">
		<div class=" col-xs-12 col-sm-8 col-sm-push-2">
		<div class="loginBox registerBox">


	<form action="{{ route('artists.update', $artist) }}" method="post" enctype="multipart/form-data">

		{{ csrf_field() }}
		{{ method_field('put') }}

		<label for="name">Name</label>
		<input type="text" name="name" id="name" value="{{ old('name', $artist->name) }}">

		<br>

		<label for="website">Website</label>
		<input type="url" name="website" id="website" value="{{ old('website', $artist->website) }}">

		<br>

		<label for="bio">Bio</label>
		<textarea name="bio" id="bio">{{ old('bio', $artist->bio) }}</textarea>

		<br>

		<label for="facebook_link">Facebook Link</label>
		<input type="url" name="facebook_link" id="facebook_link" value="{{ old('facebook_link', $artist->facebook_link) }}">

		<br>

		<label for="twitter_link">Twitter Link</label>
		<input type="url" name="twitter_link" id="twitter_link" value="{{ old('twitter_link', $artist->twitter_link) }}">

		<br>

		<label for="instagram_link">Instagram Link</label>
		<input type="url" name="instagram_link" id="instagram_link" value="{{ old('instagram_link', $artist->instagram_link) }}">

		<br>

		<label for="header_image">Header Image</label>
		@if( isset($artist) && !is_null($artist->header_image) )
			<div class="pull-left" style="width: 300px; margin: 0 20px 20px 0;">
				<img src="{{$artist->header_image}}" class="img-responsive header_thumb">
				<input type="hidden" name="remove_header_image" id="remove_header_image" value="0">
				<button id="do_remove_header_image" class="btn btn-xs btn-primary">Remove Image</button>
			</div>
			@endif
		<input type="file" name="header_image" id="header_image" accept="image/*">

		<br>
		@if( isset($artist) && !is_null($artist->profile_image) )
		<div class="pull-left" style="width: 300px; margin: 0 20px 20px 0;">
			<img src="{{$artist->profile_image}}" class="img-responsive header_thumb">
			<input type="hidden" name="remove_profile_image" id="remove_profile_image" value="0">
			<button id="do_remove_profile_image" class="btn btn-xs btn-primary">Remove Image</button>
		</div>
		@endif
		<label for="profile_image">Profile Image</label>
		<input type="file" name="profile_image" id="profile_image" accept="image/*">

		<br>

		<label for="categories">Category</label>	
			<tags-input name="categories" placeholder="Categories..." :whitelist='{{ $categories->toJson() }}' :default='{{ $artist->categories->toJson() }}'></tags-input>
		<br>

		<button>Update</button>
	</form>
            
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('scripts')
	<script>
		$(document).ready(function () { 
			$('#do_remove_header_image').click(function () { 
				$('#remove_header_image').val('1'); $(this).parent().hide(); return false; 
			}); 

			$('#do_remove_profile_image').click(function () { 
				$('#remove_profile_image').val('1'); $(this).parent().hide(); return false;
			});
		});
	</script>
@endpush