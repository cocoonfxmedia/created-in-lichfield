@extends('layouts.basic')

@section('content')


    @if( !empty( $artist->header_image))
        <div class="headerImgsingle" style="background-image:url('{{$artist->header_image}}');"> <a class="back" href="/artists">&#8592; Back to artists</a></div>
    @endif



    <!--- start container div -->
    <div class="container artistSingle">
    <!--- start row div -->
    <div class="row">


          <!--- start artist div -->
  	     <div class="artist col-sm-12 col-md-8 col-md-offset-2">

              <div class="artistImages">
          			@if( !empty( $artist->profile_image))
          				<img class="profileImg" src="{{$artist->profile_image}}" width="100px"/>
          			@endif
          		</div>

        		  <div class="artistInfo">

      			      <h2>{{$artist->name}}</h2>

                  <div class="artistCategory">
                    <?php $categories=$artist->categories; ?>

                    <p>

                    @foreach ($categories as $category)

                      {{$category->title}} |

                    @endforeach

                    </p>
            			</div>

            			@if ($artist->canEdit())
            				<a class="editProfilebtn" href="{{ route('artists.edit', $artist) }}">Edit Profile</a>
            				<a class="editProfilebtn" href="{{ route('artists.gallery.edit', $artist) }}">Edit Gallery</a>
            			@endif


                  <div class="artistSocial">
        			    <a href="{{$artist->facebook_link}}" target="_blank"><img src="/site/images/facebookIcon.png" width="24px" /></a>
        			    <a href="{{$artist->twitter_link}}" target="_blank"><img src="/site/images/twitterIcon.png" width="24px" /></a>
        			    <a href="{{$artist->instagram_link}}" target="_blank"><img src="/site/images/instagramIcon.png" width="24px" /></a>
                  </div>

                  <div class="artistContact">
            				Email: {{$artist->email}} |
            				Website: <a href="{{$artist->website}}" target="_blank">{{$artist->website}}</a>
            			</div>

                  <div class="artistBio">
          				   <p>{{$artist->bio}}</p>
                  </div>




        		</div>
        <!--- end artist div -->
        </div>
  <!--- end row div -->
  </div>
  <!--- end container div -->
  </div>
@endsection
