<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
*/

include( '_admin.php' );

require __DIR__.'/created-in-lichfield.php';

/*
 * Prestashop product imports
 */
Route::get( 'import/prestashop', [ 'uses' => 'ImportPrestashopController@index' ] );


/*
|--------------------------------------------------------------------------
| Webhook Routes Routes
|--------------------------------------------------------------------------
|
| These routes are used by 3rd party services to send messages to the installation for processing
|
*/


Route::post( 'hook/paypal-ipn', [ 'uses' => 'webhooks\PaypalIPNController@handle' ] );
Route::post( 'hook/worldpay', [ 'uses' => 'webhooks\WorldpayPaymentMessageController@handle' ] );


/*
|--------------------------------------------------------------------------
| Front End Routes
|--------------------------------------------------------------------------
|
| These routs control front end functionality for the website
|
*/

Route::post( '/quote', [ 'uses' => 'QuoteController@submit' ] );
Route::post( '/quote-request', [ 'uses' => 'QuoteController@submit2' ] );

Route::get( 'sitemap.xml', [ 'uses' => 'SitemapController@xml' ] );


Route::get( 'calendar-pull-manual', [ 'uses' => 'CalendarPullController@index' ] );

/*
 * newsletter subscription
 */
Route::post( '/newsletter', [ 'uses' => 'NewsletterController@index' ] );

Route::post( '/currency-conversion', [ 'uses' => 'CurrencyConverterController@index' ] );

/*
 * Other Routes which include breadcrumbs
 */
Route::group( [ 'middleware' => [ 'breadcrumbs', 'news.latest' ] ], function () {
	/**
	 * Homepage
	 */

	Route::get( '/', [ 'uses' => 'HomeController@index' ] );

	/*
	 * Sitemap views
	 */
	Route::get( 'sitemap', [ 'uses' => 'SitemapController@index' ] );


	/*
	 * Contact Pages
	 */
	Route::group( [ 'prefix' => 'contact-us' ], function () {
		Route::get( '/', 'ContactController@index' );
		Route::post( '/send', 'ContactController@send' );
		Route::get( '/sent', 'ContactController@sent' );
	} );

	/*
	 * Contact Pages
	 */
	Route::group( [ 'prefix' => 'contact' ], function () {
		Route::get( '/', 'ContactController@index' );
		Route::post( '/send', 'ContactController@send' );
		Route::get( '/sent', 'ContactController@sent' );
	} );

	/*
	 * News functionality
	 */
	Route::group( [ 'prefix' => 'blog' ], function () {
		Route::get( '/', [ 'uses' => 'BlogController@index' ] );
		Route::get( '/article/{url}', [ 'uses' => 'BlogController@article' ] );
		Route::get( '/category/{slug}', [ 'uses' => 'BlogController@category' ] );
	} );


	/*
	 * Case Study functionality
	 */
	Route::group( [ 'prefix' => 'case-studies' ], function () {
		Route::get( '/', [ 'uses' => 'CaseStudyController@index' ] );
		Route::get( '/{url}', [ 'uses' => 'CaseStudyController@article' ] );
	} );

	/*
	 * Product Catalog Pages
	 */
	Route::get( '/product-qv/{product_id}/{slug}', [ 'uses' => 'ProductController@quickview' ] );
	Route::get( '/product/{product_id}/{slug}', [ 'uses' => 'ProductController@show' ] );
//	Route::get( '/categories', [ 'uses' => 'ProductCategoryController@index' ] );
	Route::get( '/category/{category_id}/{slug}', [ 'uses' => 'ProductCategoryController@show' ] );

	/*
	 * Basket Functionality
	 */
	Route::group( [ 'prefix' => 'basket' ], function () {
		//add functionality
		Route::get( '/widget', [ 'uses' => 'BasketController@widget' ] );
		Route::post( '/add', [ 'uses' => 'BasketController@add' ] );
		Route::get( '/add/{product_id}', [ 'uses' => 'BasketController@quickAdd' ] );
		Route::get( '/remove/{product_id', [ 'uses' => 'BasketController@remove' ] );

		Route::post( 'qty-change', [ 'uses' => 'BasketController@changeQty' ] );
	} );


	/**
	 * Checkout Functionality
	 */
	Route::group( [ 'prefix' => 'checkout' ], function () {
		Route::get( '/', [ 'uses' => 'CheckoutController@index' ] );

		Route::post( '/register', [ 'uses' => 'CheckoutController@registerAccount' ] );

		Route::post( '/order', [ 'uses' => 'CheckoutController@processPayment' ] );

		Route::any( '/complete/{order_id}', [ 'uses' => 'CheckoutController@paymentComplete' ] );

	} );


	Route::post( 'basket/changeQty', [ 'uses' => 'BasketController@changeQty' ] );
	Route::get( 'basket/remove/{id}', [ 'uses' => 'BasketController@remove' ] );

	/**
	 * Search page
	 */
	Route::get( 'search', [ 'uses' => 'SearchController@index' ] );

	/*
	 * Frontend authentication pages
	 */
	Auth::routes();
	Route::get( '/logout', [ 'uses' => 'Auth\LoginController@logout' ] );
	Route::get( '/register', [ 'uses' => 'RegisterController@index' ] );
	Route::post( '/register', [ 'uses' => 'RegisterController@register' ] );


	/**
	 * My Account Page
	 */
	Route::group( [ 'prefix' => 'my-account', 'middleware' => 'auth' ], function () {
		Route::get( '/', [ 'uses' => 'AccountController@index' ] );
		/**
		 * Addresses
		 */
		Route::group( [ 'prefix' => 'addresses' ], function () {
			Route::get( '/', [ 'uses' => 'AddressController@index' ] );
			Route::get( '/create', [ 'uses' => 'AddressController@create' ] );
			Route::post( '/create', [ 'uses' => 'AddressController@store' ] );

			Route::get( '/{address_id}/delete', [ 'uses' => 'AddressController@remove' ] );
			Route::get( '/{address_id}/edit', [ 'uses' => 'AddressController@edit' ] );

			//TODO:: update address functionality
		} );

		/**
		 * Personal Info
		 */

		Route::get( '/identity', [ 'uses' => 'PersonalInfoController@index' ] );
		Route::post( '/identity', [ 'uses' => 'PersonalInfoController@update' ] );

		/**
		 * Order History
		 */
		Route::get( '/orders', [ 'uses' => 'OrderHistoryController@index' ] );
	} );


	/* C02 Calculator */
	Route::group( [ 'prefix' => 'c02-calc' ], function () {

		Route::get( '/', 'C02CalcController@single' );

		Route::get( 'air', 'C02CalcController@air' );
		Route::get( 'land', 'C02CalcController@land' );
		Route::get( 'sea', 'C02CalcController@sea' );

		Route::group( [ 'prefix' => 'proxy' ], function () {

			Route::get( 'air-list', 'C02ProxyController@airList' );
			Route::get( 'air-route', 'C02ProxyController@airRoute' );
			Route::get( 'sea-list', 'C02ProxyController@seaList' );
			Route::get( 'sea-route', 'C02ProxyController@seaRoute' );

		} );
	} );


	/*
	 * Generic email form controller
	 */
	Route::post( 'email-form', 'EmailFormController@send' );


	/*
	 * CMS pages
	 */
	Route::group( [ 'middleware' => 'cms.components.bookcourse' ], function () {
		Route::any( '{slug}', 'PageController@view' );
		Route::any( '{slug}/{slug1}', 'PageController@view' );
		Route::any( '{slug}/{slug1}/{slug2}', 'PageController@view' );
		Route::any( '{slug}/{slug1}/{slug2}/{slug3}', 'PageController@view' );

	} );

} );



