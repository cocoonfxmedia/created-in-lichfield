<?php

use Illuminate\Support\Facades\Route;

// Authentication for artists, copied from Laravel core
Route::get('artist-login', 'Auth\ArtistLoginController@showLoginForm')->name('artists-auth.login');
Route::post('artist-login', 'Auth\ArtistLoginController@login');
Route::post('artist-logout', 'Auth\ArtistLoginController@logout')->name('artists-auth.logout');
Route::get('artist-register', 'Auth\ArtistRegisterController@showRegistrationForm')->name('artists-auth.register');
Route::post('artist-register', 'Auth\ArtistRegisterController@register');

// Social authentication
Route::get('auth/{provider}', 'Auth\SocialiteController@redirectToProvider')
	->where('provider', 'facebook|twitter|instagram')
	->name('socialite')
;
Route::get('auth/{provider}/callback', 'Auth\SocialiteController@handleProviderCallback')
	->where('provider', 'facebook|twitter|instagram')
	->name('socialite.callback')
;

// Artists
Route::get('artists', 'ArtistController@index')->name('artists.index');
Route::get('artist/{artist}', 'ArtistController@show')->name('artists.show');

Route::group(['middleware' => ['auth:artist', 'can:manage,artist']], function () {
	Route::get('artist/{artist}/edit', 'ArtistController@edit')->name('artists.edit');
	Route::put('artist/{artist}', 'ArtistController@update')->name('artists.update');
	
    Route::get('artist/{artist}/gallery/edit', 'ArtistGalleryController@edit')->name('artists.gallery.edit');
    Route::put('artist/{artist}/gallery', 'ArtistGalleryController@update')->name('artists.gallery.update');
});
Route::group(['middleware' => ['auth:artist', 'can:deleteGalleryImage,artist']], function () {
    Route::delete('artist/{artist}/galleryImage/{id}', 'ArtistGalleryController@destroyImage')->name('artists.gallery.removeImage');
});


// Events
Route::group(['middleware' => 'auth:artist'], function () {
	Route::group(['middleware' => 'can:create,App\\Models\\Event'], function () {
		Route::get('event/create', 'EventController@create')->name('events.create');
		Route::post('events', 'EventController@store')->name('events.store');
	});

	Route::group(['middleware' => 'can:manage,event'], function () {
		Route::get('event/{event}/edit', 'EventController@edit')->name('events.edit');
		Route::put('event/{event}', 'EventController@update')->name('events.update');
		Route::delete('event/{event}', 'EventController@destroy')->name('events.destroy');
	});
});

Route::get('events', 'EventController@index')->name('events.index');
Route::get('event/{event}', 'EventController@show')->name('events.show');

// Locations
Route::group(['middleware' => 'auth:artist'], function () {
	Route::group(['middleware' => 'can:create,App\\Models\\Location'], function () {
		Route::get('location/create', 'LocationController@create')->name('locations.create');
		Route::post('locations', 'LocationController@store')->name('locations.store');
	});

	Route::group(['middleware' => 'can:manage,location'], function () {
		Route::get('location/{location}/edit', 'LocationController@edit')->name('locations.edit');
		Route::put('location/{location}', 'LocationController@update')->name('locations.update');
		Route::delete('location/{location}', 'LocationController@destroy')->name('locations.destroy');
	});
});

Route::get('locations', 'LocationController@index')->name('locations.index');
Route::get('location/{location}', 'LocationController@show')->name('locations.show');

// Admin
Route::group(['prefix' => 'admin', 'middleware' => 'admin.auth'], function () {
	// Artists
	Route::group(['prefix' => 'artists'], function () {
		Route::get('/', 'Admin\\ArtistController@index');
		Route::get('delete/{artist}', 'Admin\\ArtistController@delete');
		Route::get('edit/{artist}', 'Admin\\ArtistController@edit');
		Route::post('update/{artist}', 'Admin\\ArtistController@update');
		Route::get('create', 'Admin\\ArtistController@create');
		Route::post('create', 'Admin\\ArtistController@store');

		Route::get('/trash', 'Admin\\ArtistController@trash');
		Route::get('/restore/{id}', 'Admin\\ArtistController@restore');
		Route::get('/destroy/{id}', 'Admin\\ArtistController@destroy');
		Route::get('/empty-trash', 'Admin\\ArtistController@emptyTrash');

		Route::post('toggle-event-permissions', 'Admin\\ArtistController@toggleEventPermissions');
		Route::post('toggle-location-permissions', 'Admin\\ArtistController@toggleLocationPermissions');
	});

	// Categories
	Route::group(['prefix' => 'categories'], function () {
		Route::get('/', 'Admin\\ArtCategoryController@index');
		Route::get('delete/{category}', 'Admin\\ArtCategoryController@destroy');
		Route::get('edit/{category}', 'Admin\\ArtCategoryController@edit');
		Route::post('update/{category}', 'Admin\\ArtCategoryController@update');
		Route::get('create', 'Admin\\ArtCategoryController@create');
		Route::post('create', 'Admin\\ArtCategoryController@store');
	});

	// Events
	Route::group(['prefix' => 'events'], function () {
		Route::get('/', 'Admin\\EventController@index');
		Route::get('delete/{event}', 'Admin\\EventController@destroy');
		Route::get('edit/{event}', 'Admin\\EventController@edit');
		Route::post('update/{event}', 'Admin\\EventController@update');
		Route::get('create', 'Admin\\EventController@create');
		Route::post('create', 'Admin\\EventController@store');
	});

	// Locations
	Route::group(['prefix' => 'locations'], function () {
		Route::get('/', 'Admin\\LocationController@index');
		Route::get('delete/{location}', 'Admin\\LocationController@destroy');
		Route::get('edit/{location}', 'Admin\\LocationController@edit');
		Route::post('update/{location}', 'Admin\\LocationController@update');
		Route::get('create', 'Admin\\LocationController@create');
		Route::post('create', 'Admin\\LocationController@store');
	});
});
