<?php
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Routes defined for the site admin functionality
|
*/


Route::group( [ 'prefix' => 'admin' ], function () {

	/**
	 * Authentication
	 */
	Route::post( '/login', [ 'uses' => 'Admin\LoginController@login' ] );
	Route::get( '/login', [ 'uses' => 'Admin\LoginController@showLoginForm' ] );

	Route::get( '/logout', [ 'uses' => 'Admin\LoginController@logout' ] );

	Route::group( [ 'middleware' => 'admin.auth' ], function () {


		Route::get( '/', [ 'uses' => 'Admin\DashboardController@index' ] );

		/*
		 * Site siwtcher
		 */
		Route::get( '/set-site/{newSiteId}', [ 'uses' => 'Admin\SwitchSiteController@index' ] );

		Route::post( '/wysiwyg/mp3', [ 'uses' => 'Admin\WysiwygController@mp3' ] );


		/*
		 * User Preferences
		 */
		Route::post( '/user-pref/main-nav', [ 'uses' => 'Admin\UserPrefferenceController@navMenuExpander' ] );

		/*
		 * Page Manager
		 */
		Route::group( [ 'prefix' => 'pages' ], function () {

			Route::get( '/', [ 'uses' => 'Admin\PageController@index' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\PageController@destroy' ] );
			Route::get( '/edit/{id}', [ 'uses' => 'Admin\PageController@edit' ] );
			Route::post( '/update/{id}', [ 'uses' => 'Admin\PageController@update' ] );

			Route::get( '/create', [ 'uses' => 'Admin\PageController@create' ] );
			Route::post( '/store', [ 'uses' => 'Admin\PageController@store' ] );

			Route::get( '/trash', [ 'uses' => 'Admin\PageController@trash' ] );
			Route::get( '/restore/{id}', [ 'uses' => 'Admin\PageController@restore' ] );
			Route::get( '/empty-trash', [ 'uses' => 'Admin\PageController@emptyTrash' ] );

			Route::get( '/repath', [ 'uses' => 'Admin\PageController@repath' ] );

		} );

		/*
		 * Blog Manager
		 */
		Route::group( [ 'prefix' => 'blog' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\BlogController@adminIndex' ] );
			Route::get( '/create', [ 'uses' => 'Admin\BlogController@create' ] );
			Route::post( '/store', [ 'uses' => 'Admin\BlogController@store' ] );
			Route::get( '/edit/{id}', [ 'uses' => 'Admin\BlogController@edit' ] );
			Route::post( '/update/{id}', [ 'uses' => 'Admin\BlogController@update' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\BlogController@destroy' ] );

			Route::get( '/trash', [ 'uses' => 'Admin\BlogController@trash' ] );
			Route::get( '/restore/{id}', [ 'uses' => 'Admin\BlogController@restore' ] );
			Route::get( '/empty-trash', [ 'uses' => 'Admin\BlogController@emptyTrash' ] );

		} );

		//blog categories
		Route::group( [ 'prefix' => 'blog-categories' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\BlogCategoryController@adminCategoryIndex' ] );
			Route::get( '/create', [ 'uses' => 'Admin\BlogCategoryController@categoryCreate' ] );
			Route::post( '/store', [ 'uses' => 'Admin\BlogCategoryController@categoryStore' ] );
			Route::get( '/edit/{id}', [ 'uses' => 'Admin\BlogCategoryController@categoryEdit' ] );
			Route::post( '/update/{id}', [ 'uses' => 'Admin\BlogCategoryController@categoryUpdate' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\BlogCategoryController@categoryDestroy' ] );
		} );


		/*
		 * Case Study Manager
		 */
		Route::group( [ 'prefix' => 'case-studies' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\CaseStudyController@adminIndex' ] );
			Route::get( '/create', [ 'uses' => 'Admin\CaseStudyController@create' ] );
			Route::post( '/store', [ 'uses' => 'Admin\CaseStudyController@store' ] );
			Route::get( '/edit/{id}', [ 'uses' => 'Admin\CaseStudyController@edit' ] );
			Route::post( '/update/{id}', [ 'uses' => 'Admin\CaseStudyController@update' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\CaseStudyController@destroy' ] );

			Route::get( '/trash', [ 'uses' => 'Admin\CaseStudyController@trash' ] );
			Route::get( '/restore/{id}', [ 'uses' => 'Admin\CaseStudyController@restore' ] );
			Route::get( '/empty-trash', [ 'uses' => 'Admin\CaseStudyController@emptyTrash' ] );

		} );

		//case study categories
		Route::group( [ 'prefix' => 'case-study-categories' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\CaseStudyCategoryController@adminCategoryIndex' ] );
			Route::get( '/create', [ 'uses' => 'Admin\CaseStudyCategoryController@categoryCreate' ] );
			Route::post( '/store', [ 'uses' => 'Admin\CaseStudyCategoryController@categoryStore' ] );
			Route::get( '/edit/{id}', [ 'uses' => 'Admin\CaseStudyCategoryController@categoryEdit' ] );
			Route::post( '/update/{id}', [ 'uses' => 'Admin\CaseStudyCategoryController@categoryUpdate' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\CaseStudyCategoryController@categoryDestroy' ] );
		} );


		Route::group( [ 'prefix' => 'sliders' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\SliderController@index' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\SliderController@destroy' ] );
			Route::get( '/edit/{id}', [ 'uses' => 'Admin\SliderController@edit' ] );
			Route::get( '/create', [ 'uses' => 'Admin\SliderController@create' ] );
			Route::post( '/create', [ 'uses' => 'Admin\SliderController@store' ] );
			Route::post( '/edit/{id}', [ 'uses' => 'Admin\SliderController@update' ] );

			/*
			 * Manage Slides
			*/
			Route::get( '/{slider_id}/slides/', [ 'uses' => 'Admin\SlideController@index' ] );
			Route::get( '/{slider_id}/slides/delete/{id}', [ 'uses' => 'Admin\SlideController@destroy' ] );
			Route::get( '/{slider_id}/slides/edit/{id}', [ 'uses' => 'Admin\SlideController@edit' ] );
			Route::post( '/{slider_id}/slides/edit/{id}', [ 'uses' => 'Admin\SlideController@update' ] );
			Route::get( '/{slider_id}/slides/create', [ 'uses' => 'Admin\SlideController@create' ] );
			Route::post( '/{slider_id}/slides/create', [ 'uses' => 'Admin\SlideController@store' ] );


		} );


		/*
		 * Team members
		 */
		Route::group( [ 'prefix' => 'team-members' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\TeamMemberController@index' ] );
			Route::get( '/create', [ 'uses' => 'Admin\TeamMemberController@create' ] );
			Route::post( '/create', [ 'uses' => 'Admin\TeamMemberController@store' ] );
			Route::post( '/order', [ 'uses' => 'Admin\TeamMemberController@updateOrder' ] );
			Route::get( '/{id}', [ 'uses' => 'Admin\TeamMemberController@edit' ] );
			Route::post( '/{id}', [ 'uses' => 'Admin\TeamMemberController@update' ] );
			Route::get( '/{id}/delete', [ 'uses' => 'Admin\TeamMemberController@destroy' ] );

		} );


		/*
		 * Menu Manger
		 */
		Route::group( [ 'prefix' => 'menus' ], function () {

			Route::get( '/', [ 'uses' => 'Admin\MenuController@index' ] );
			Route::post( '/', [ 'uses' => 'Admin\MenuController@menuSwitcher' ] );
			Route::get( '/delete/{id}', [ 'uses' => 'Admin\MenuController@destroy' ] );
			Route::post( '/create', [ 'uses' => 'Admin\MenuController@store' ] );

			Route::post( '/update', [ 'uses' => 'Admin\MenuController@update' ] );
			Route::post( '/order', [ 'uses' => 'Admin\MenuController@reorder' ] );

			Route::get( '/mass-create-categories', [ 'uses' => 'Admin\MenuController@massImportCategories' ] );

		} );

		/*
		 * News letter subscriptions
		 */
		Route::group( [ 'prefix' => 'newsletter-subscriptions' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\NewsletterSubscriptionsController@index' ] );
			Route::get( '/{subscription_id}/unsubscribe', [ 'uses' => 'Admin\NewsletterSubscriptionsController@unsubscribe' ] );
			Route::get( '/{subscription_id}/resubscribe', [ 'uses' => 'Admin\NewsletterSubscriptionsController@resubscribe' ] );

			Route::get( '/{subscriptionId}/edit', [ 'uses' => 'Admin\NewsletterSubscriptionsController@edit' ] );
			Route::get( '/create', [ 'uses' => 'Admin\NewsletterSubscriptionsController@create' ] );

			Route::post( '/{subscriptionId}/edit', [ 'uses' => 'Admin\NewsletterSubscriptionsController@update' ] );
			Route::post( '/create', [ 'uses' => 'Admin\NewsletterSubscriptionsController@store' ] );
		} );

		/**
		 * Config Screens
		 */
		Route::group( [ 'prefix' => 'config' ], function () {

			Route::get( '/', [ 'uses' => 'Admin\ConfigController@index' ] );

			Route::post( '/general', [ 'uses' => 'Admin\ConfigController@store' ] );

		} );

		Route::get( 'dupe-site', [ 'uses' => 'Admin\Tools\DupeSiteController@index' ] );
		Route::post( 'dupe-site', [ 'uses' => 'Admin\Tools\DupeSiteController@processor' ] );


		/**
		 * User Manager
		 */
		Route::resource( 'users', 'Admin\UserManagerController' );
		Route::get( '/users/{user_id}/delete', [ 'uses' => 'Admin\UserManagerController@delete' ] );


		/*
		 *
		 * ECOMMERCE FUNCTIONALITY
		 *
		 */

		/*
		 * Product Manager
		 */

		Route::group( [ 'prefix' => 'products' ], function () {

			Route::get( '/', [
				'uses' => 'Admin\ProductController@index',
				'as'   => 'products'
			] );

			Route::get( '/create', [ 'uses' => 'Admin\ProductController@create' ] );
			Route::post( '/create', [ 'uses' => 'Admin\ProductController@store' ] );

			Route::get( '/{id}/edit', [ 'uses' => 'Admin\ProductController@edit' ] );
			Route::post( '/update', [ 'uses' => 'Admin\ProductController@update' ] );

			Route::get( '/{id}/delete', [ 'uses' => 'Admin\ProductController@destroy' ] );

			Route::post( '/colour/create', [ 'uses' => 'Admin\ProductController@createColour' ] );
			Route::get( '/colour/delete/{id}', [ 'uses' => 'Admin\ProductController@deleteColour' ] );

			Route::post( '/size/create', [ 'uses' => 'Admin\ProductController@createSize' ] );
			Route::get( '/size/delete/{id}', [ 'uses' => 'Admin\ProductController@deleteSize' ] );

			Route::post( '/face/create', [ 'uses' => 'Admin\ProductController@createFace' ] );
			Route::get( '/face/delete/{id}', [ 'uses' => 'Admin\ProductController@deleteFace' ] );

			Route::get( '/import', [ 'uses' => 'Admin\ProductController@import' ] );
			Route::post( '/import-processor', [ 'uses' => 'Admin\ProductController@importProcessor' ] );

			Route::get( '/mass-update', [ 'uses' => 'Admin\ProductController@massUpdate' ] );
			Route::get( '/mass-update/download', [ 'uses' => 'Admin\ProductController@export' ] );
			Route::post( '/mass-update/processor', [ 'uses' => 'Admin\ProductController@massUpdateProcessor' ] );


			Route::get( '/face/edit/{face_id}', [ 'uses' => 'Admin\ProductController@productDesigner' ] );

			Route::post( '/{id}/related_products', [ 'uses' => 'Admin\ProductController@relatedProduct' ] );
			Route::get( '/{parent}/related/{child}/delete', [ 'uses' => 'Admin\ProductController@deleteRelaiton' ] );

			Route::get( '/order', [ 'uses' => 'Admin\ProductController@order' ] );
			Route::post( '/order/save', [ 'uses' => 'Admin\ProductController@orderSave' ] );
		} );

		/*
		 * Product Categories
		 */

		Route::group( [ 'prefix' => 'product-categories' ], function () {

			Route::get( '/', [
				'uses' => 'Admin\ProductCategoryController@index',
				'as'   => 'categories'
			] );

			Route::get( '/create', [ 'uses' => 'Admin\ProductCategoryController@create' ] );
			Route::post( '/create', [ 'uses' => 'Admin\ProductCategoryController@store' ] );

			Route::get( '/{id}/edit', [ 'uses' => 'Admin\ProductCategoryController@edit' ] );
			Route::post( '{id}/update', [ 'uses' => 'Admin\ProductCategoryController@update' ] );

			Route::get( '/{id}/delete', [ 'uses' => 'Admin\ProductCategoryController@destroy' ] );
		} );


		/*
		 * Voucher Manager
		 */
		Route::group( [ 'prefix' => 'vouchers' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\VoucherController@index' ] );
			Route::get( 'add', [ 'uses' => 'Admin\VoucherController@add' ] );
			Route::post( 'create', [ 'uses' => 'Admin\VoucherController@create' ] );
			Route::get( 'edit/{voucher_id}', [ 'uses' => 'Admin\VoucherController@edit' ] );
			Route::post( 'update/{voucher_id}', [ 'uses' => 'Admin\VoucherController@update' ] );
			Route::get( 'delete/{voucher_id}', [ 'uses' => 'Admin\VoucherController@delete' ] );
		} );

		/*
		 * Reviews
		 */

		Route::group( [ 'prefix' => 'reviews' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\ReviewController@index' ] );

			Route::get( '{review_id}/reject', [ 'uses' => 'Admin\ReviewController@reject' ] );
			Route::get( '{review_id}/approve', [ 'uses' => 'Admin\ReviewController@approve' ] );

			Route::get( '{review_id}', [ 'uses' => 'Admin\ReviewController@show' ] );

		} );

		/*
		 * Product Queries
		 */

		Route::group( [ 'prefix' => 'product-queries' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\ProductQueryController@index' ] );

			Route::get( '/{query_id}', [ 'uses' => 'Admin\ProductQueryController@view' ] );

			Route::get( '/replied/{query_id}', [ 'uses' => 'Admin\ProductQueryController@markReplied' ] );

		} );

		/*
		 * Customer Orders Page
		 */

		Route::group( [ 'prefix' => 'customers' ], function () {
			Route::get( '/', [ 'uses' => 'Admin\CustomerController@index' ] );
			Route::get( '{customer_id}/orders', [ 'uses' => 'Admin\CustomerController@orders' ] );

			Route::get( '{customer_id}/edit', [ 'uses' => 'Admin\CustomerController@edit' ] );
			Route::post( '{customer_id}/update', [ 'uses' => 'Admin\CustomerController@update' ] );
		} );


		/*
		 * Mass price adjustment functionality
		 */
		Route::get( 'price-adjust', [ 'uses' => 'admin\PriceAdjustController@index' ] );
		Route::post( 'price-adjust', [ 'uses' => 'admin\PriceAdjustController@process' ] );


		/*
		 * Page Blocks
		 */
		Route::group( [ 'prefix' => 'blocks' ], function () {
			Route::get( '/dupe', [ 'uses' => 'Admin\BlockController@dupe' ] );

			Route::get( '/', [ 'uses' => 'Admin\BlockController@index' ] );
			Route::get( '/{id}/publish', [ 'uses' => 'Admin\BlockController@publish' ] );
			Route::get( '/{id}/unpublish', [ 'uses' => 'Admin\BlockController@unpublish' ] );
			Route::get( '/{id}/delete', [ 'uses' => 'Admin\BlockController@delete' ] );

			Route::get( '/{id}/edit', [ 'uses' => 'Admin\BlockController@edit' ] );
			Route::post( '/{id}/update', [ 'uses' => 'Admin\BlockController@update' ] );
			Route::get( '/create', [ 'uses' => 'Admin\BlockController@create' ] );
			Route::post( '/store', [ 'uses' => 'Admin\BlockController@store' ] );


			Route::get( '/{block}', [ 'uses' => 'Admin\BlockController@view' ] );


			Route::get( '/{block}/create', [ 'uses' => 'Admin\BlockController@createPanel' ] );
			Route::post( '/{block}/create', [ 'uses' => 'Admin\BlockController@storePanel' ] );

			Route::get( '/{block}/edit/{panel}', [ 'uses' => 'Admin\BlockController@editPanel' ] );
			Route::post( '/{block}/update/{panel}', [ 'uses' => 'Admin\BlockController@updatePanel' ] );
			Route::get( '/{block}/delete/{panel}', [ 'uses' => 'Admin\BlockController@deletePanel' ] );

			Route::post( '/{block}/order', [ 'uses' => 'Admin\BlockController@updateOrder' ] );

			Route::post( '/fileupload', [ 'uses' => 'Admin\BlockController@uploadFile' ] );
		} );


		/*
		 * WYSIWYG Editor image uploads
		 */
		Route::get( '/images', [ 'uses' => 'Admin\ImageController@availableImages' ] );
		Route::post( '/images', [ 'uses' => 'Admin\ImageController@uploadImage' ] );


		/*
		 * Galleries
		 */
		Route::group( [ 'prefix' => '/gallery' ], function () {
			Route::post( '/image-properties/testing', [ 'uses' => 'Admin\GalleryController@updateImage' ] );

			Route::get( '/remove-image/{id}', [ 'uses' => 'Admin\GalleryController@removeImage' ] );

			Route::get( '/', [ 'uses' => 'Admin\GalleryController@index' ] );
			Route::get( '/{id}/unpublish', [ 'uses' => 'Admin\GalleryController@unpublish' ] );
			Route::get( '/{id}/publish', [ 'uses' => 'Admin\GalleryController@publish' ] );
			Route::get( '/{id}/delete', [ 'uses' => 'Admin\GalleryController@delete' ] );

			Route::get( '/{id}/edit', [ 'uses' => 'Admin\GalleryController@edit' ] );
			Route::get( '/create', [ 'uses' => 'Admin\GalleryController@create' ] );
			Route::post( '/store', [ 'uses' => 'Admin\GalleryController@store' ] );
			Route::post( '/{id}/update', [ 'uses' => 'Admin\GalleryController@update' ] );


			Route::get( '/{id}', [ 'uses' => 'Admin\GalleryController@view' ] );

			Route::post( '/{id}', [ 'uses' => 'Admin\GalleryController@imageUpload' ] );

			Route::post( '/{id}/order', [ 'uses' => 'Admin\GalleryController@updateOrder' ] );

		} );

		/*
		 * Currency exchange rate tracking
		 */
		Route::get( '/currency', [ 'uses' => 'Admin\CurrencyController@index' ] );

	} );

} );
