# CFX CMS 

A copy of the database file is in /database/database.sql


## Prerequisites

- For the geolocation, you must have an API key with Google Maps JavaScript API and Geolocation API enabled. Without this, the geolocation options are hidden. This API key should be set as `GOOGLE_MAPS_API_KEY` in the environment
- For Facebook login, you must have a Facebook app, with `FB_CLIENT_ID` and `FB_CLIENT_SECRET` in the environment
- For Twitter login, you must have an approved Twitter app, with `TWITTER_CLIENT_ID` and `TWITTER_CLIENT_SECRET`in the environment.
- For Instagram login, you must have an Instagram app, with the `INSTAGRAM_CLIENT_ID` and `INSTAGRAM_CLIENT_SECRET` in the environment.

## Artist + public routes

### Auth

Login: `/artist-login`
Register: `/artist-register`
Logout: `POST /artist-logout`
Facebook login/register: `/auth/facebook`
Twitter login/register: `/auth/twitter`
Instagram login/register: `/auth/instagram`

### Events

`/events`
`/event/create` (must be logged in, must be allowed to manage events)
`/event/:id`
`/event/:id/edit` (must be logged in, must be allowed to manage events, must be the owner)

### Locations

No index or view routes, as there were no designs for this, I assume this will done by the map interface

`/location/create` (must be logged in, must be allowed to manage locations)
`/location/:id/edit` (must be logged in, must be allowed to manage locations, must be the owner)

### Artists

`/artists`
`/artist/:id`
`/artist/:id/edit` (must be logged in as the corresponding artist)

### Galleries

Galleries were included in the codebase by default, so I assume they have a way of managing galleries on the front end. I didn't want to recreate something they likely already have built in house.

## Admin

There are sections in the CMS for all new resources, categories, artists, locations, artists and the existing gallery resource has been modified to allow for geolocation and categorisation of images.