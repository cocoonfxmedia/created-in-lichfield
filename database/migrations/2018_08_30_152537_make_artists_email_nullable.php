<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class MakeArtistsEmailNullable extends Migration
{
	public function up()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->string('email')->nullable()->default(null)->change();
		});
	}
	
	public function down()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->string('email')->change();
		});
	}
}
