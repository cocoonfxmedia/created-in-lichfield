<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLocationArtistRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropForeign(['artist_id']);
            $table->dropColumn('artist_id');
        });

        Schema::create('locations_artists', function (Blueprint $table) {
            $table->unsignedInteger('artist_id');
            $table->unsignedInteger('location_id');

            $table->foreign('artist_id')
                ->references('id')
                ->on('artists')
                ->onDelete('cascade');

            $table->foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations_artists');

        Schema::table('locations', function (Blueprint $table) {
            $table->unsignedInteger('artist_id')->nullable();
            $table->foreign('artist_id')
                ->references('id')
                ->on('artists');
        });
    }
}
