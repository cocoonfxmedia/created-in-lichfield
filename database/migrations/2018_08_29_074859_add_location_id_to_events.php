<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddLocationIdToEvents extends Migration
{
	public function up()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->unsignedInteger('location_id')->nullable();

			$table
				->foreign('location_id')
				->references('id')
				->on('locations')
			;
		});
	}

	public function down()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->dropForeign(['location_id']);
			$table->dropColumn('location_id');
		});
	}
}
