<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateLocationsTable extends Migration
{
	public function up()
	{
		Schema::create('locations', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('category_id')->nullable();
			$table->decimal('latitude', 10, 8);
			$table->decimal('longitude', 10, 8);
			$table->string('title');
			$table->string('address');
			$table->text('description')->nullable();
			$table->string('external_link')->nullable();
			$table->timestamps();

			$table
				->foreign('category_id')
				->references('id')
				->on('art_categories')
			;
		});
	}

	public function down()
	{
		Schema::dropIfExists('locations');
	}
}
