<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateArtCategoriesTable extends Migration
{
	public function up()
	{
		Schema::create('art_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('slug')->unique();
			$table->boolean('active')->default(true);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('art_categories');
	}
}
