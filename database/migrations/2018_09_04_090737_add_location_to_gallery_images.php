<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddLocationToGalleryImages extends Migration
{
	public function up()
	{
		Schema::table('gallery_images', function (Blueprint $table) {
			$table->decimal('latitude', 10, 8)->nullable();
			$table->decimal('longitude', 10, 8)->nullable();
			$table->unsignedInteger('category_id')->nullable();

			$table
				->foreign('category_id')
				->references('id')
				->on('art_categories')
			;
		});
	}

	public function down()
	{
		Schema::table('gallery_images', function (Blueprint $table) {
			$table->dropForeign(['category_id']);
			$table->dropColumn(['latitude', 'longitude', 'category_id']);
		});
	}
}
