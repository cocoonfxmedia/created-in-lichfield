<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateEventsTable extends Migration
{
	public function up()
	{
		Schema::create('events', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('category_id')->nullable();
			$table->decimal('latitude', 10, 8)->nullable();
			$table->decimal('longitude', 10, 8)->nullable();
			$table->string('title');
			$table->dateTime('at');
			$table->decimal('price');
			$table->text('description')->nullable();
			$table->string('external_link')->nullable();
			$table->string('header_image')->nullable();
			$table->string('event_code')->nullable();
			$table->timestamps();

			$table
				->foreign('category_id')
				->references('id')
				->on('art_categories')
			;
		});
	}

	public function down()
	{
		Schema::dropIfExists('events');
	}
}
