<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddAuthenticationToArtists extends Migration
{
	public function up()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->rememberToken();
			$table->string('email')->unique();
			$table->string('password');
		});
	}

	public function down()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->dropRememberToken();
			$table->dropColumn('email');
			$table->dropColumn('password');
		});
	}
}
