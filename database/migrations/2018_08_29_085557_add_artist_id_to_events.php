<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddArtistIdToEvents extends Migration
{
	public function up()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->unsignedInteger('artist_id');

			$table
				->foreign('artist_id')
				->references('id')
				->on('artists')
			;
		});
	}

	public function down()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->dropForeign(['artist_id']);
			$table->dropColumn('artist_id');
		});
	}
}
