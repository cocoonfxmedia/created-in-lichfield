<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeArtCategoriesRelationship extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->dropForeign(['category_id']);
			$table->dropColumn('category_id');
		});

		Schema::create('art_categories_artists', function (Blueprint $table) {
			$table->unsignedInteger('artist_id');
			$table->unsignedInteger('art_categories_id');

			$table->foreign('artist_id')
				->references('id')
				->on('artists')
				->onDelete('cascade');

			$table->foreign('art_categories_id')
				->references('id')
				->on('art_categories')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('art_categories_artists');

		Schema::table('artists', function (Blueprint $table) {
			$table->unsignedInteger('category_id')->nullable();
			$table->foreign('category_id')
				->references('id')
				->on('art_categories');
		});
	}
}
