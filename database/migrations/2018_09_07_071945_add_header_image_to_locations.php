<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddHeaderImageToLocations extends Migration
{
	public function up()
	{
		Schema::table('locations', function (Blueprint $table) {
			$table->string('header_image')->nullable();
		});
	}

	public function down()
	{
		Schema::table('locations', function (Blueprint $table) {
			$table->dropColumn('header_image');
		});
	}
}
