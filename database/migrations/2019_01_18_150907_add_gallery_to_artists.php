<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGalleryToArtists extends Migration
{
    public function up()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->unsignedInteger('gallery_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('artists', function (Blueprint $table) {
            $table->dropColumn('gallery_id');
        });
    }
}
