<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddDescriptionToGalleries extends Migration
{
	public function up()
	{
		Schema::table('galleries', function (Blueprint $table) {
			$table->text('description')->nullable();
		});
	}
	
	public function down()
	{
		Schema::table('galleries', function (Blueprint $table) {
			$table->dropColumn('description');
		});
	}
}
