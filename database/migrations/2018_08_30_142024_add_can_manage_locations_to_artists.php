<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddCanManageLocationsToArtists extends Migration
{
	public function up()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->boolean('can_manage_locations')->default(false);
		});
	}

	public function down()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->dropColumn('can_manage_locations');
		});
	}
}
