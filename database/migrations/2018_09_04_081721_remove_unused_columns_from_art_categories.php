<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class RemoveUnusedColumnsFromArtCategories extends Migration
{
	public function up()
	{
		Schema::table('art_categories', function (Blueprint $table) {
			$table->dropColumn(['slug', 'active']);
		});
	}
	
	public function down()
	{
		Schema::table('art_categories', function (Blueprint $table) {
			$table->string('slug')->unique();
			$table->boolean('active')->default(true);
		});
	}
}
