<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class MakeNewModelsSoftDeletable extends Migration
{
	public function up()
	{
		Schema::table('art_categories', function (Blueprint $table) {
			$table->softDeletes();
		});

		Schema::table('artists', function (Blueprint $table) {
			$table->softDeletes();
		});

		Schema::table('events', function (Blueprint $table) {
			$table->softDeletes();
		});

		Schema::table('locations', function (Blueprint $table) {
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::table('art_categories', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});

		Schema::table('artists', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});

		Schema::table('events', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});

		Schema::table('locations', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});
	}
}
