<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateArtistsTable extends Migration
{
	public function up()
	{
		Schema::create('artists', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('category_id')->nullable();
			$table->string('name');
			$table->string('website')->nullable();
			$table->text('bio')->nullable();
			$table->string('facebook_link')->nullable();
			$table->string('twitter_link')->nullable();
			$table->string('instagram_link')->nullable();
			$table->string('header_image')->nullable();
			$table->string('profile_image')->nullable();
			$table->timestamps();

			$table->foreign('category_id')
				->references('id')
				->on('art_categories')
			;
		});
	}

	public function down()
	{
		Schema::dropIfExists('artists');
	}
}
