<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class EnableArtistSocialLogin extends Migration
{
	public function up()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->string('password')->nullable()->change();
			$table->unsignedInteger('category_id')->nullable()->default(null)->change();

			$table->string('provider')->nullable();
			$table->string('provider_id')->nullable();
			$table->unique(['provider', 'provider_id']);
			$table->dropUnique(['email']);
		});
	}

	public function down()
	{
		Schema::table('artists', function (Blueprint $table) {
			$table->dropUnique(['provider', 'provider_id']);
			$table->dropColumn(['provider', 'provider_id']);

			$table->string('password')->change();
			$table->unsignedInteger('category_id')->change();
			$table->unique('email');
		});
	}
}
