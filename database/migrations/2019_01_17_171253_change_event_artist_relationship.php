<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEventArtistRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign(['artist_id']);
            $table->dropColumn('artist_id');
        });

        Schema::create('events_artists', function (Blueprint $table) {
            $table->unsignedInteger('artist_id');
            $table->unsignedInteger('event_id');

            $table->foreign('artist_id')
                ->references('id')
                ->on('artists')
                ->onDelete('cascade');

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events_artists');

        Schema::table('events', function (Blueprint $table) {
            $table->unsignedInteger('artist_id')->nullable();
            $table->foreign('artist_id')
                ->references('id')
                ->on('artists');
        });
    }
}
