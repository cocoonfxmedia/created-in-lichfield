<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddGalleryIdToEventsAndLocations extends Migration
{
	public function up()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->unsignedInteger('gallery_id')->nullable();
		});

		Schema::table('locations', function (Blueprint $table) {
			$table->unsignedInteger('gallery_id')->nullable();
		});
	}

	public function down()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->dropColumn('gallery_id');
		});

		Schema::table('locations', function (Blueprint $table) {
			$table->dropColumn('gallery_id');
		});
	}
}
