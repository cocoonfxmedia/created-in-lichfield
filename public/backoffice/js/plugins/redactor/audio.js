/**
 * Audio mp3 upload plugin for Redactor Editor
 * Author: Gareth @ openDYNAMIX
 */
if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.audio = function()
	{
		return {
			reUrlMp3: /https?:\/\/(www\.)*(.*?).(wav|mp3)/,
			langs: {
				en: {
					"audio": "Audio",
					"audio-html-code": "Audio link"
				}
			},
			getTemplate: function()
			{
				return String()
					+ '<div class="modal-section" id="redactor-modal-audio-insert">'
					+ '<section>'
					+ '<label>Select your MP3 to upload to the site</label>'
					+ '<input type="file" id="redactor-insert-audio-area"/>'
					+ '<p id="redactor-insert-audio-area-msg" class="text-danger"></p>'
					+ '</section>'
					+ '<section>'
					+ '<div id="redactor-insert-audio-area-response"></div>'
					+ '<button id="redactor-modal-button-action">Insert</button>'
					+ '<button id="redactor-modal-button-cancel">Cancel</button>'
					+ '</section>'
					+ '</div>';
			},
			init: function()
			{
				var button = this.button.add('clips', 'Audio');
				this.button.addCallback(button, this.audio.show);
			},
			show: function()
			{
				var self = this;
				this.modal.addTemplate('audio', this.audio.getTemplate());

				this.modal.load('audio', 'Insert Audio', 700);
				// this.modal.createCancelButton();

				// var button = this.modal.createActionButton('Insert');
				$('#redactor-modal-button-action').click(function() {
					self.audio.insert();
				});
				this.modal.show();

				$('#redactor-insert-audio-area').focus();
			},
			insert: function()
			{
				var data = $('#redactor-insert-audio-area').val();

				//validate that MP3 is being uploaded
				// if (! data.match(this.audio.reUrlMp3)) {
				// 	$('#redactor-insert-audio-area-msg p').html('You can only upload MP3 files here');
				// 	return false;
				// }

				//Upload the file to the server
				var formData = new FormData();
				formData.append('file', $('#redactor-insert-audio-area')[0].files[0]);

				var response = {};

				var obj = this;

				$.ajax({
					url : '/admin/wysiwyg/mp3',
					type : 'POST',
					data : formData,
					processData: false,  // tell jQuery not to process the data
					contentType: false,  // tell jQuery not to set contentType
					success : function(xhrResponse) {
						response = '<audio src="' + xhrResponse.url + '" controls controlsList="nodownload"></audio>';
						obj.modal.close();
						// obj.placeholder.remove();
						// obj.buffer.set();
						obj.insert.html( response );
					}
				});

			}
		};
	};
})(jQuery);