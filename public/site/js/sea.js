$(function () {


	google.maps.event.addDomListener(window, 'load', initializeMap);

	var map;
	var markerStart;
	var markerTarget;
	var items = 1000;

	var autocomplete_start;
	var autocomplete_target;
	var flightPath;
	var markersArray = [];

	var timeout;

	$('.calculate').on('click', calculateDistance);

	function initializeMap() {
		var mapOptions = {
			center: new google.maps.LatLng(0, 0), // {lat: -33.8688, lng: 151.2195},
			zoom: 2
		};
		map = new google.maps.Map(document.getElementById('source-map'), mapOptions);
		flightPath = new google.maps.Polyline({})

		var distanceData = document.getElementById("distance_data");
		if (distanceData) {
			fillInputsWithData();
		}

	}

	function manageMarkers() {

		flightPath.setMap(null);
		$('.after-calculate').hide();
		$('.after-calculate').attr('data-calculated', 'false');
		$('#slider').slider("destroy");

		autocomplete_start = new google.maps.LatLng($('#lat_start').val(), $('#long_start').val());
		autocomplete_target = new google.maps.LatLng($('#lat_target').val(), $('#long_target').val());

		clearOverlays();
		if ($('#lat_start').val()) {


			markerStart = new google.maps.Marker({
				map: map,
				draggable: true,
				position: autocomplete_start,
				title: 'A',
				label: 'A'
			});

			markersArray.push(markerStart);

		}
		if ($('#lat_target').val()) {

			markerTarget = new google.maps.Marker({
				map: map,
				position: autocomplete_target,
				draggable: true,
				title: 'B',
				label: 'B'
			});
			markersArray.push(markerTarget);

		}


		// if(!$('#lat_target').val() && $('#lat_start').val() ) {
		//     console.log('1', $('#lat_start').val() + 1.01, $('#long_start').val() + 1.01)
		//     autocomplete_target = new google.maps.LatLng(parseFloat($('#lat_start').val()) + 50.01, parseFloat($('#long_start').val()) - 30.01);
		// }
		// if($('#lat_target').val() && !$('#lat_start').val() ) {
		//     console.log('2')
		//     autocomplete_start = new google.maps.LatLng(parseFloat($('#lat_target').val()) + 50.01, parseFloat($('#long_target').val()) - 30.01);
		// }

		var bounds = new google.maps.LatLngBounds();
		bounds.extend(autocomplete_start);
		bounds.extend(autocomplete_target);
		map.fitBounds(bounds);

		if (!$('#lat_target').val() && !$('#lat_start').val()) {
			map.setZoom(2);
		}

	}

	function clearOverlays() {
		for (var i = 0; i < markersArray.length; i++) {
			markersArray[i].setMap(null);
		}
		// markersArray.length = 0;
		markersArray = [];
	}

	function calculateDistance() {

		var from_id = $('#port_id_start').val();
		var to_id = $('#port_id_target').val();

		var bounds = new google.maps.LatLngBounds();

		if (from_id.length !== 0 && to_id.length !== 0) {
			$('#overlay').show();
			var from = $('#port_name_start').val().toLowerCase();
			var to = $('#port_name_target').val().toLowerCase();
			history.pushState(null, null, '/distances/' + from + '/' + to);
			$.ajax({
				url: "/c02-calc/proxy/sea-route?long1=" + $('#long_start').val() + "&lat1=" + $('#lat_start').val() + "&long2=" + $('#long_target').val() + "&lat2=" + $('#lat_target').val(),
				success: function (response) {
					var tmp = {};
					var data = [];
					for (var i = 0; i < response.length; i++) {
						for (var j = 0; j < response[i].x_y.length; j++) {
							tmp = {};
							tmp.lat = parseFloat(response[i].x_y[j][1]);
							tmp.lng = parseFloat(response[i].x_y[j][0]);
							data.push(tmp);

							var coords = new google.maps.LatLng(tmp.lat, tmp.lng);
							bounds.extend(coords);

						}
					}


					if (data.length != 0) {
						data.push({lat: parseFloat($('#lat_target').val()), lng: parseFloat($('#long_target').val())});
						data.unshift({lat: parseFloat($('#lat_start').val()), lng: parseFloat($('#long_start').val())});
					}

					var flightPlanCoordinates = data;
					flightPath = new google.maps.Polyline({
						path: flightPlanCoordinates,
						geodesic: true,
						strokeColor: '#FF0000',
						strokeOpacity: 1.0,
						strokeWeight: 2
					});

					var lengthInMeters = google.maps.geometry.spherical.computeLength(flightPath.getPath());


					var distance = lengthInMeters / 1000;
					var c02Standard = 4.3;
					var weight = parseInt($('#weight').val()) / 1000;

					var emissions = (weight * distance * c02Standard) / 1000;

					$('#distance').html(distanceTrans + ':' + parseInt(distance) + ' Km');
					$('#emissions').html(emissionsTrans + ': ' + parseFloat(emissions).toFixed(4) + ' Kg C02');

				}
			});
		}
	}


	function fillInputsWithData() {

		var start = {
			lat: parseFloat($('#data_port_from_lat').val()),
			lng: parseFloat($('#data_port_from_lon').val())
		};
		var target = {
			lat: parseFloat($('#data_port_to_lat').val()),
			lng: parseFloat($('#data_port_to_lon').val())
		};


		$('#start-place').val($('#data_port_from_name').val() + ', ' + $('#data_port_from_country').val());
		$('#target-place').val($('#data_port_to_name').val() + ', ' + $('#data_port_to_country').val());

		$('#port_name_start').val($('#data_port_from_name').val());
		$('#country_start').val($('#data_port_from_country').val());
		$('#port_id_start').val(parseInt($('#data_port_from_id').val()));
		$('#lat_start').val(start.lat);
		$('#long_start').val(start.lng);

		$('#port_name_target').val($('#data_port_to_name').val());
		$('#country_target').val($('#data_port_to_country').val());
		$('#port_id_target').val(parseInt($('#data_port_to_id').val()));
		$('#lat_target').val(target.lat);
		$('#long_target').val(target.lng);

		manageMarkers();
	}


	$("#start-place").typeahead({
		source: function (request, response) {
			if (timeout) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(function () {
				$.ajax({
					url: "/c02-calc/proxy/sea-list",
					dataType: "json",
					displayField: 'port_name',
					data: {searchText: request, maxResults: items},
					success: function (data) {
						response($.map(data, function (item) {
							return {
								label: item.port_name + ', ' + item.country,
								value: item.port_name + ', ' + item.country,
								id: item.id,
								port_name: item.port_name,
								country: item.country,
								lat: item.lat,
								long: item.long
							}
						}));
					}
				});
			}, 500);
		},
		items: 'all',
		autoSelect: true,
		hint: true,
		highlight: true,
		displayText: function (item) {
			return item.label;
		},
		afterSelect: function (item) {
			$('#port_name_start').val(item.port_name);
			$('#country_start').val(item.country);
			$('#port_id_start').val(item.id);
			$('#lat_start').val(item.lat);
			$('#long_start').val(item.long);

			manageMarkers();

			return false;
		}
	});

	$("#target-place").typeahead({
		source: function (request, response) {
			if (timeout) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(function () {
				$.ajax({
					url: "/c02-calc/proxy/sea-list",
					dataType: "json",
					displayField: 'port_name',
					data: {searchText: request, maxResults: items},
					success: function (data) {
						response($.map(data, function (item) {
							return {
								label: item.port_name + ', ' + item.country,
								value: item.port_name + ', ' + item.country,
								id: item.id,
								port_name: item.port_name,
								country: item.country,
								lat: item.lat,
								long: item.long
							}
						}));
					}
				});
			}, 500);
		},
		items: 'all',
		autoSelect: true,
		hint: true,
		highlight: true,
		displayText: function (item) {
			return item.label;
		},
		afterSelect: function (item) {
			$('#port_name_target').val(item.port_name);
			$('#country_target').val(item.country);
			$('#port_id_target').val(item.id);
			$('#lat_target').val(item.lat).change;
			$('#long_target').val(item.long).change;

			// $('#lat_target').change();
			manageMarkers();

			return false;
		}
	});


});